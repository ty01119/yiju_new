<?php 

/**
 * bbpress
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

	get_header();
	global $codevz;
	$codevz->content_before();

	echo '<section class="def-block clr">';
	if ( have_posts() ) { 
		while ( have_posts() ) {
			the_post();
			the_content();
		}
	}
	echo '</section>';

	$codevz->content_after();
	get_sidebar();
	get_footer(); 
?>