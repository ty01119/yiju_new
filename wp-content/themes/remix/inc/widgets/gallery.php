<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( !class_exists( 'CodevzGallery' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzGallery');
	});

	class CodevzGallery extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Gallery', 'remix' ), 
				array( 'classname' => 'cd_gallery' )
			);
		}

		public function widget($args, $instance) {
			extract( $args );
			$title = apply_filters( 'widget_title', $instance['title'] );
			$out = $before_widget . "\n";
			$out .= $title ? $before_title . $title . $after_title : '';
			ob_start();
			$gallery_order = isset($instance['gallery_order']) ? $instance['gallery_order'] : 'DESC';
			global $post;
			$popular = new WP_Query( array(
				'post_type'		=> 'gallery',
				'order'			=> $gallery_order,
				'showposts'		=> $instance['posts_num']
			) );
		?>
			
		<div class="cd_gallery_in clr">
			<?php while ( $popular->have_posts() ): $popular->the_post(); ?>
					<?php if ( has_post_thumbnail() ): ?>
						<a class="cdEffect noborder" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail('tumb'); ?>
							<i class="fa fa-image"></i>
						</a>
					<?php endif; ?>
			<?php endwhile; wp_reset_query(); ?>
		</div>

		<?php
			$out .= ob_get_clean();
			$out .= $after_widget."\n";
			echo $out;
		}
		
		public function update($new,$old) {
			$instance = $old;
			$instance['title'] = strip_tags($new['title']);
			$instance['gallery_order'] = strip_tags($new['gallery_order']);
			$instance['posts_num'] = strip_tags($new['posts_num']);
			$instance['posts_height'] = strip_tags($new['posts_height']);
			return $instance;
		}

		public function form($instance) {
			$defaults = array(
				'title' 			=> 'Photo Gallery',
				'gallery_order' 	=> 'DESC',
				'posts_num' 		=> '9'
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			
			$title_value = esc_attr( $instance['title'] );
			$title_field = array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			);
			echo csf_add_field( $title_field, $title_value );

			$posts_num_value = esc_attr( $instance['posts_num'] );
			$posts_num_field = array(
				'id'    => $this->get_field_name('posts_num'),
				'name'  => $this->get_field_name('posts_num'),
				'type'  => 'number',
				'title'	=> esc_html__('Count', 'remix'),
			);
			echo csf_add_field( $posts_num_field, $posts_num_value );

			$gallery_order_value = esc_attr( $instance['gallery_order'] );
			$gallery_order_field = array(
				'id'    => $this->get_field_name('gallery_order'),
				'name'  => $this->get_field_name('gallery_order'),
				'type'  => 'radio',
				'options' => array(
					'DESC' => 'DESC',
					'ASC' => 'ASC'
				),
				'title' => esc_html__('Order', 'remix')
			);
			echo csf_add_field( $gallery_order_field, $gallery_order_value );
		}
	}

}
