<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( !class_exists( 'CodevzInstagram' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzInstagram');
	});

	class CodevzInstagram extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Instagram', 'remix' ), 
				array( 'classname' => 'cd_instagram' )
			);
		}

		public function widget( $args, $instance ) {
			extract( $args );
			$title = apply_filters( 'widget_title', $instance['title'] );
			$out = $before_widget."\n";
			$out .= $title ? $before_title . $title . $after_title : '';
			$count = $instance['count'] ? $instance['count'] : 9;

			$query = $this->scrape_instagram( $instance['username'] );

			if ( ! is_array( $query ) ) {
				echo $before_widget . $title;
				delete_transient( 'instagram-codevz-' . sanitize_title_with_dashes( $instance['username'] ) );
				echo $after_widget;
				return;
			}
			
			ob_start();
			$out .= '<ul class="in_insta clr">';
			$i = 0;
			foreach ( $query as $q ) {
				$out .= '<li><a class="cdEffect noborder" href="' . $q['link'] . '" target="_blank" title="' . $q['description'] . '"><img src="' . $q['thumbnail'] . '" alt="' . $q['description'] . '" width="auto" height="auto" /><i class="fa fa-instagram"></i></a></li>';
				$i++;
				if ( $i == $count ) {
					break;
				}
			}
			$out .= '</ul>';
			$out .= ob_get_clean();
			$out .= $after_widget."\n";
			echo $out;
		}
		
		public function update( $new, $old ) {
			$instance = $old;
			$instance['title'] = strip_tags( $new['title'] );
			$instance['username'] = strip_tags( $new['username'] );
			$instance['count'] = strip_tags( $new['count'] );

			return $instance;
		}

		public function form( $instance ) {
			$defaults = array(
				'title' 	=> 'Instagram',
				'count' 	=> '9',
				'username' 	=> 'Codevz'
			);
			$instance = wp_parse_args( (array) $instance, $defaults );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			), esc_attr( $instance['title'] ) );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('username'),
				'name'  => $this->get_field_name('username'),
				'type'  => 'text',
				'title' => esc_html__('Username', 'remix')
			), esc_attr( $instance['username'] ) );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('count'),
				'name'  => $this->get_field_name('count'),
				'type'  => 'number',
				'title' => esc_html__('Count', 'remix')
			), esc_attr( $instance['count'] ) );

		}

		function scrape_instagram( $username ) {

			$username = strtolower( $username );
			$username = str_replace( '@', '', $username );

			if ( false === ( $instagram = get_transient( 'instagram-codevz-'.sanitize_title_with_dashes( $username ) ) ) ) {

				$remote = wp_remote_get( 'http://instagram.com/'.trim( $username ) );

				if ( is_wp_error( $remote ) ) {
					return new WP_Error( 'site_down', esc_html__( 'Unable to communicate with Instagram.', 'remix' ) );
				}

				if ( 200 != wp_remote_retrieve_response_code( $remote ) ) {
					return new WP_Error( 'invalid_response', esc_html__( 'Instagram did not return a 200.', 'remix' ) );
				}

				$shards = explode( 'window._sharedData = ', $remote['body'] );
				$insta_json = explode( ';</script>', $shards[1] );
				$insta_array = json_decode( $insta_json[0], TRUE );

				if ( ! $insta_array ) {
					return new WP_Error( 'bad_json', esc_html__( 'Instagram has returned invalid data.', 'remix' ) );
				}

				if ( isset( $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'] ) ) {
					$images = $insta_array['entry_data']['ProfilePage'][0]['user']['media']['nodes'];
				} else {
					return new WP_Error( 'bad_json_2', esc_html__( 'Instagram has returned invalid data.', 'remix' ) );
				}

				if ( ! is_array( $images ) ) {
					return new WP_Error( 'bad_array', esc_html__( 'Instagram has returned invalid data.', 'remix' ) );
				}

				$instagram = array();

				foreach ( $images as $image ) {

					$image['thumbnail_src'] = preg_replace( '/^https?\:/i', '', $image['thumbnail_src'] );
					$image['display_src'] = preg_replace( '/^https?\:/i', '', $image['display_src'] );

					// handle both types of CDN url
					if ( ( strpos( $image['thumbnail_src'], 's640x640' ) !== false ) ) {
						$image['thumbnail'] = str_replace( 's640x640', 's160x160', $image['thumbnail_src'] );
						$image['small'] = str_replace( 's640x640', 's320x320', $image['thumbnail_src'] );
					} else {
						$urlparts = wp_parse_url( $image['thumbnail_src'] );
						$pathparts = explode( '/', $urlparts['path'] );
						array_splice( $pathparts, 3, 0, array( 's160x160' ) );
						$image['thumbnail'] = '//' . $urlparts['host'] . implode( '/', $pathparts );
						$pathparts[3] = 's320x320';
						$image['small'] = '//' . $urlparts['host'] . implode( '/', $pathparts );
					}

					$image['large'] = $image['thumbnail_src'];

					if ( $image['is_video'] == true ) {
						$type = 'video';
					} else {
						$type = 'image';
					}

					$caption = esc_html__( 'Instagram Image', 'remix' );
					if ( ! empty( $image['caption'] ) ) {
						$caption = $image['caption'];
					}

					$instagram[] = array(
						'description'   => $caption,
						'link'		  	=> trailingslashit( '//instagram.com/p/' . $image['code'] ),
						'time'		  	=> $image['date'],
						'comments'	  	=> $image['comments']['count'],
						'likes'		 	=> $image['likes']['count'],
						'thumbnail'	 	=> strtok( $image['thumbnail'], '?' ),
						'small'			=> strtok( $image['small'], '?' ),
						'large'			=> strtok( $image['large'], '?' ),
						'original'		=> strtok( $image['display_src'], '?' ),
						'type'		  	=> $type
					);
				}

				// do not set an empty transient - should help catch private or empty accounts
				if ( ! empty( $instagram ) ) {
					set_transient( 'instagram-codevz-'.sanitize_title_with_dashes( $username ), $instagram, 3600*72 );
				}
			}

			if ( ! empty( $instagram ) ) {
				return $instagram;
			} else {
				return new WP_Error( 'no_images', esc_html__( 'Instagram did not return any images.', 'remix' ) );
			}

		}

	}

}
