var ajax_auth_object = {
  "ajaxurl" : "http:\/\/test.multisupport.co.nz\/wp-admin\/admin-ajax.php",
  "redirecturl" : window.location.href,
}
var revapi = null;
jQuery(document).ready(function($){ 
  getLinks(); 
  jQuery("#page-content").bind('DOMSubtreeModified',function($){
    getLinks();
  });
  var selector 	= '#page-content',
  $selector 	= $( selector );
  var selectorCopy = '#page-content-copy';
  $selector.after("<div id=\"page-content-copy\" style=\"position: absolute; z-index: -88; width: 100%;\">"
      + "<div class=\"page-content\">"
      + "<div class=\"row clr\">"
      + "<div class=\"grid_12 clr\">"
      + "</div>"
      + "</div>"
      + "</div>"
      );

  var positionLeft = $selector.position().left;
  var positionTop = $selector.position().top;
  var selectorHeight = $selector.height();
  var selectorWidth = $selector.width();
  var $selectorCopy = $('#page-content-copy');
  $selectorCopy.css({top:0});

  $("body").on("click","#arrow-prev", function(){
    setTimeout(function(){
    if(revapi != null){
      revapi.revkill();
    }
    $selectorCopy.html($selector.html());
    $selectorCopy.css({top:0, "z-index": '88', opacity: 1 });
    $selector.find(".grid_12 article").html("");
    $selector.css({ left: '-2000px' });
    $selectorCopy.animate( { left: '2000px' }, 900);
    $selector.animate({ left: positionLeft }, 900);
    $selectorCopy.animate( { opacity: 0 }, 0.1 );
    $selectorCopy.animate( { left:positionLeft, 'z-index': '-88' }, 100 );
    }, 200);
  });
  $("body").on("click","#arrow-next", function(){
    setTimeout(function(){
    if(revapi != null){
      revapi.revkill();
    }
    $selectorCopy.html($selector.html());
    $selectorCopy.css({top:0, "z-index": '88', opacity: 1 });
    $selector.find(".grid_12 article").html("");
    $selector.css({ left: '2000px' });
    $selectorCopy.animate( { left: '-2000px' }, 900);
    $selector.animate({ left: positionLeft }, 900);
    $selectorCopy.animate( { opacity: 0 }, 0.1 );
    $selectorCopy.animate( { left:positionLeft, 'z-index': '-88' }, 100 );
    }, 200);
  });
  $("#arrows a#arrow-prev").on("mouseenter", function(){
    $(this).find("div").animate({ left: '0px' }, 200 );
  }).on("mouseleave", function(){
    $(this).find("div").animate( {left: '-150px' }, 200 )
  });
  $("#arrows a#arrow-next").on("mouseenter", function(){
    $(this).find("div").animate({ right: '0px' }, 200 );
  }).on("mouseleave", function(){
    $(this).find("div").animate( {right: '-150px' }, 200 )
  });
});

jQuery(document).ready(function($){
  $("#left-interest-rand-post-button").click(function(event){
      event.preventDefault();
      $.ajax({
        url:ajaxurl,
        data: {
          'action' : 'ms_load_random_post_ajax',
          'limit' : 6,
          'is_first_time' : 0,
          'position' : 'left_interest_img',
          'post_type' : 'post,investments,cptnews,nzlife,services',
          'begin_col' : 4,
          'num_per_row' : 1
        },
        success: function(data){
          $("#left-interest-rand-posts").html(data);
        },
        error: function(errorThrown){
          console.log(errorThrown);
        }
      });
  });
});

function getLinks(){
    var nav = $("nav#mobile");
    var items = nav.find("li");
    var prev, next;
    for(index = 0; index<items.length; index++){
      if($(items[index]).hasClass("current-menu-item")){
        if(index == 0){
          prev = items[items.length-1];
          next = items[index+1];
        }
        else if(index == items.length-1){
          prev = items[index-1];
          next = items[0];
        }
        else{
          prev = items[index-1];
          next = items[index+1];
        }
      }
    }
    var prevUrl = $(prev).find("a").attr('href');
    var prevText = $(prev).find("a").text();
    $("#arrow-prev").attr('href', prevUrl);
    $("#arrow-prev-text").html(prevText);
    var nextUrl = $(next).find("a").attr('href');
    var nextText = $(next).find("a").text();
    $("#arrow-next").attr('href', nextUrl);
    $("#arrow-next-text").html(nextText);
}

