<?php
/**
 * Plugin Name: dFlip - NULL24.NET
 * Description: dFlip - 3D & 2D FlipBook
 * Version: 1.1.2
 *
 * Author: Deepak Ghimire
 * Author URI: http://codecanyon.net/user/deip?ref=deip
 *
 */


// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Customized translation for DFLIP.
 * Alternative to __() function of WordPress
 *
 * @param string $text Text to translate.
 *
 * @return string Translated text.
 */
function __DFLIP( $text ) {
	return translate( $text, 'DFLIP' );
}


/**
 * Main dFlip plugin class.
 *
 * @since   1.0.0
 *
 * @package DFlip
 * @author  Deepak Ghimire
 */
class DFlip {

	/**
	 * Holds the singleton class object.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	public static $instance;

	/**
	 * Plugin version
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $version = '1.2.1';

	/**
	 * The name of the plugin.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $plugin_name = 'dFLip';

	/**
	 * Unique plugin slug identifier.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $plugin_slug = 'dflip';

	/**
	 * Plugin file.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $file = __FILE__;

	/**
	 * Primary class constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// Load the plugin.
		add_action( 'init', array( $this, 'init' ), 0 );

	}

	/**
	 * Loads the plugin into WordPress.
	 *
	 * @since 1.0.0
	 */
	public function init() {

		// Load admin only components.
		if ( is_admin() ) {
			$this->init_admin();
		} else { // Load frontend only components.
			$this->init_front();
		}

		// Load global components.
		$this->init_global();

	}

	/**
	 * Loads all admin related files into scope.
	 *
	 * @since 1.0.0
	 */
	public function init_admin() {

		//include the metaboxes file
		require_once dirname( __FILE__ ) . "/inc/metaboxes.php";

		/*
		//register admin style
		wp_register_style(
			$this->plugin_slug . 'admin-style', plugins_url() . '/dflip/assets/css/df-admin.css', array(),
			$this->version
		);

		//enqueue the register style
		wp_enqueue_style($this->plugin_slug . 'admin-style');
		*/

		require_once( dirname( __FILE__ ) . '/inc/settings.php' );

	}

	/**
	 * Loads all frontend user related files
	 *
	 * @since 1.0.0
	 */
	public function init_front() {

		//include the shortcode parser
		require_once dirname( __FILE__ ) . "/inc/shortcode.php";

		//include the scripts and styles for front end
		add_action( 'wp_enqueue_scripts', array( $this, 'init_front_scripts' ) );

		//some custom js that need to be passed
		add_action( 'wp_head', array( $this, 'hook_script' ) );

	}

	/**
	 * Loads all global files into scope.
	 *
	 * @since 1.0.0
	 */
	public function init_global() {

		//include the post-type that manages the custom post
		require_once dirname( __FILE__ ) . '/inc/post-type.php';

	}

	/**
	 * Loads all script and style sheets for frontend into scope.
	 *
	 * @since 1.0.0
	 */
	public function init_front_scripts() {

		//cache for plugin_slug
		$_slug = $this->plugin_slug;

		//required for cache busting
		$_version = $this->version;

		//register scripts
		wp_register_script(
			$_slug . '-script', plugins_url() . '/dflip/assets/js/dflip.min.js', array( "jquery" ), $_version, true
		);
		/*wp_register_script(
			$_slug . '-script', plugins_url() . '/dflip/assets/js/dflip.js', array("jquery"), $_version, true
		);
		*/
		wp_register_script(
			$_slug . '-parse-script', plugins_url() . '/dflip/assets/js/parse.js', array( 'jquery' ), $_version, true
		);

		//register scripts
		wp_register_style(
			$_slug . '-icons-style', plugins_url() . '/dflip/assets/css/themify-icons.css', array(), $_version
		);
		wp_register_style(
			$_slug . '-style', plugins_url() . '/dflip/assets/css/dflip.css', array(), $_version
		);
		/*		wp_register_style(
					$_slug . '-book-style', plugins_url() . '/dflip/assets/css/book.css', array(), $_version
				);*/

		//enqueue scripts
		wp_enqueue_script( $_slug . '-script' );
		wp_enqueue_script( $_slug . '-parse-script' );

		//enqueue styles
		wp_enqueue_style( $_slug . '-icons-style' );
		wp_enqueue_style( $_slug . '-style' );
		//		wp_enqueue_style($_slug . '-book-style');


	}

	/**
	 * Registers a javascript variable into HTML DOM for url access
	 *
	 * @since 1.0.0
	 */
	public function hook_script() {

		//registers a variable that stores the location of plugin
		$output = '<script> var dFlipLocation = "' . plugins_url() . '/dflip"; </script>';
		echo $output;

	}

	/**
	 * Returns the singleton instance of the class.
	 *
	 * @since 1.0.0
	 *
	 * @return object DFlip object.
	 */
	public static function get_instance() {

		if ( ! isset( self::$instance ) && ! ( self::$instance instanceof DFlip ) ) {
			self::$instance = new DFlip();
		}

		return self::$instance;

	}

}

//Load the dFlip Plugin Class
$dflip = DFlip::get_instance();