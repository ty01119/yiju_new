<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( ! class_exists( 'CodevzEvents' ) ) {

	class CodevzEvents extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Events', 'remix' ), 
				array( 'classname' => 'cd_events' )
			);
		}

		public function widget( $args, $instance ) {

			extract( $args );
			$title = apply_filters( 'widget_title', $instance['title'] );
			$out = $before_widget."\n";
			$out .= $title ? $before_title . $title . $after_title : '';
			ob_start();
			$args = array(
				'post_type'		=> 'events',
				'order'			=> 'ASC',
				'orderby'		=> 'meta_value',
				'meta_key'		=> 'cd_end_event_date',
				'meta_query' 	=> array(
					array(
						'key'		=> 'cd_end_event_date',
						'value' 	=> current_time('Y/m/d H:i'),
						'type' 		=> 'DATETIME',
						'compare' 	=> $instance['events_order']
					)
				),
				'showposts'		=> $instance['posts_num']
			);

			if ( ! empty( $instance['artists'] ) ) {
				$meta_query = array( 'relation' => 'OR' );
				foreach ( (array) $instance['artists'] as $key ) {
					$meta_query[] = array( 'key' => 'cd_meta', 'value' => '"' . $key . '"', 'compare' => 'LIKE' );
				}
				$args['meta_query'] = $meta_query;
			}

			$query = new WP_Query( $args );

			global $codevz;
			if ( $query->have_posts() ) :
				while ( $query->have_posts() ): $query->the_post();
				$meta = $codevz->meta(); ?>
					<div class="item_small">
						<?php if ( has_post_thumbnail() ): ?>
							<a class="cdEffect noborder" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail( 'tumb' ); ?>
								<i class="fa fa-ticket"></i>
							</a>
						<?php endif; ?>
						<h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
						<span><i class="fa fa-clock-o mi"></i><?php echo date_i18n( get_option('date_format') . ' ' . get_option('time_format'), strtotime( $meta['end_event_date'] ) ); ?></span>
						<?php if ( ! empty( $meta['venue'] ) ) : ?><span itemprop="location" itemscope itemtype="http://data-vocabulary.org/Organization"><i class="fa fa-map-marker mi"></i> <b itemprop="name"><?php echo $meta['venue']; ?></b></span><?php endif; ?>
					</div>
				<?php endwhile;
			endif;
			wp_reset_query();

			$out .= ob_get_clean();
			$out .= $after_widget."\n";

			echo $out;
		}
		
		public function update($new,$old) {

			$instance = $old;
			$instance['title'] = strip_tags( $new['title'] );
			$instance['events_order'] = $new['events_order'];
			$instance['artists'] = $new['artists'];
			$instance['posts_num'] = strip_tags( $new['posts_num'] );

			return $instance;
		}

		public function form($instance) {

			$defaults = array(
				'title' 		=> 'Latest Events',
				'events_order' 	=> '>=',
				'posts_num' 	=> '4',
				'artists' 		=> null,
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			
			echo csf_add_field( array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			), esc_attr( $instance['title'] ) );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('posts_num'),
				'name'  => $this->get_field_name('posts_num'),
				'type'  => 'number',
				'title'	=> esc_html__('Count', 'remix'),
			), esc_attr( $instance['posts_num'] ) );

			$events_order_value = $instance['events_order'];
			$events_order_field = array(
				'id'    => $this->get_field_name('events_order'),
				'name'  => $this->get_field_name('events_order'),
				'type'  => 'radio',
				'options' => array(
					'>=' => esc_html__('Upcoming', 'remix'),
					'<=' => esc_html__('Past', 'remix')
				),
				'title' => esc_html__('Sort', 'remix')
			);
			echo csf_add_field( $events_order_field, $events_order_value );

			$artists_field = array(
				'id'    => $this->get_field_name('artists'),
				'name'  => $this->get_field_name('artists'),
				'type'  => 'autocomplete',
                'class' => 'multiple',
                'query_args'  => array(
                  'post_type' => 'artists',
                  'orderby'   => 'title',
                  'order'     => 'ASC',
                  'posts_per_page' => 20
                ),
				'title' => esc_html__('Artist(s)', 'remix')
			);
			echo csf_add_field( $artists_field, $instance['artists'] );

		}

	}

	add_action( 'widgets_init', function() {
		register_widget('CodevzEvents');
	});

}
