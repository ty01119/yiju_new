<?php 

/**
 * Woocommerce
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

	get_header();
	global $codevz;
	$meta = $codevz->meta();
	$codevz->content_before();

	echo '<section class="def-block clr">';
		$codevz->page_title();
		woocommerce_content();
	echo '</section>';

	$codevz->content_after();
	get_sidebar();
	get_footer(); 
