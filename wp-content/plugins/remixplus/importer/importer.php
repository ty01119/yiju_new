<?php 

/**
 * Demo importer
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzDemoImporter {
	
	public function __construct() {
		$this->spinner = get_admin_url() . '/images/spinner.gif';
		$this->yes = get_admin_url() . '/images/yes.png';
		$this->no = get_admin_url() . '/images/no.png';

		add_action( 'wp_ajax_cd_importer', array( $this, 'cd_importer' ) );
		add_filter( 'init', array( $this, 'options' ), 99 );
	}

	public function options() {

		$options = array();
		$plg_url = plugins_url();

		$options[]   = array(
		  'name'     => 'demos',
		  'title'    => __('Demo Importer', 'remix'),
		  'icon'     => 'fa fa-magic',
		  'fields'   => array(
			array(
				'type'    => 'notice',
				'class'   => 'warning',
				'content' => '<div style="font-size: 14px;line-height: 24px;">Please before importing demo, make sure your server PHP memory limit and max_execution_time is ready for this work.</div>'
			),
		    array(
		      'type'    => 'heading',
		      'content' => 'Step 1: Select demo'
		    ),
			array(
				'id'		=> 'demos',
				'type'		=> 'image_select',
				'title'		=> __('Demos', 'remix'),
				'setting_args'	=> array(
					'transport'	=> 'postMessage'
				),
				'options'	=> array(
					'1' => $plg_url . '/remixplus/importer/demo-1/screenshot.jpg',
					'2' => $plg_url . '/remixplus/importer/demo-2/screenshot.jpg',
					'3' => $plg_url . '/remixplus/importer/demo-3/screenshot.jpg',
					'4' => $plg_url . '/remixplus/importer/demo-4/screenshot.jpg',
					'5' => $plg_url . '/remixplus/importer/demo-5/screenshot.jpg',
				),
				'default'			=> '1',
				'default_option'	=> '1',
				'radio'				=> true
			),
		    array(
		      'type'    => 'heading',
		      'content' => 'Step 2: Import features'
		    ),
			array(
				'type'    => 'content',
				'content' => '
<script type="text/javascript">
	jQuery(document).ready(function ($) {
		$(".cd_importer").on("click","input[type=\'button\']", function(event) {
			event.preventDefault();
			var $this 	= $(this).parent(),
				demo 	= $(".csf-field-demos input:checked").val(),
				act 	= $(this).attr("name"),
				attach 	= $( \'input[type="checkbox"]\', $this ).is(":checked"),
				img 	= $this.find("img"),
				msg 	= $this.find("b"),
				nonce 	= $(this).data("nonce"),
				action 	= $(this).data("action");

			var r = confirm("Are you sure ?");
			if (!r) return;

			img.fadeIn();
			msg.html( "Please wait! it may take a minutes." );
			$(this).attr( "disabled", "disabled" );

			$.ajax({
				type: "GET",
				url: ajaxurl,
				data: {
					action: action,
					nonce: nonce,
					attach: attach,
					demo: demo,
					act: act
				},
				success: function( response ) {
					var obj = $.parseJSON( response );

					if ( obj.imported === 0 || response === "" || response === null || response === "0" || response === "-1" ) {
						$this.find("input").removeAttr( "disabled" );
					}
					img.fadeOut();
					msg.html( obj.msg );
					console.log( response );
				}
			});
		});
	});
</script>

<div class="cd_importer">
	<div>
		<input 
			type="button" 
			name="theme_options" 
			data-action="cd_importer" 
			data-nonce="' . wp_create_nonce('cd_importer_nonce') . '" 
			class="button button-primary" 
			value="Import theme options"
		>
		<img src="'. $this->spinner.'" style="display:none">
		<b></b>
	</div><br />

	<div>
		<input 
			type="button" 
			name="content" 
			data-action="cd_importer" 
			data-nonce="' . wp_create_nonce('cd_importer_nonce') . '" 
			class="button button-primary" 
			value="Import content data + pages"
		>
		<img src="'. $this->spinner.'" style="display:none">
		<br />
		<input type="checkbox" name="attach" style="vertical-align: bottom"><label> Attachments </label>
		<b></b>
	</div><br />

	<div>
		<input 
			type="button" 
			name="pages" 
			data-action="cd_importer" 
			data-nonce="' . wp_create_nonce('cd_importer_nonce') . '" 
			class="button button-primary" 
			value="Import only pages"
		>
		<img src="'. $this->spinner.'" style="display:none">
		<b></b>
	</div><br />

	<div>
		<input 
			type="button" 
			name="widgets" 
			data-action="cd_importer" 
			data-nonce="' . wp_create_nonce('cd_importer_nonce') . '" 
			class="button button-primary" 
			value="Import widgets"
		>
		<img src="'. $this->spinner.'" style="display:none">
		<b></b>
	</div><br />

	<div>
		<input 
			type="button" 
			name="revslider" 
			data-action="cd_importer" 
			data-nonce="' . wp_create_nonce('cd_importer_nonce') . '" 
			class="button button-primary" 
			value="Import revolution slider data"
		>
		<img src="'. $this->spinner.'" style="display:none">
		<b></b>
	</div><br />

	<div>
		<input 
			type="button" 
			name="masterslider" 
			data-action="cd_importer" 
			data-nonce="' . wp_create_nonce('cd_importer_nonce') . '" 
			class="button button-primary" 
			value="Import master slider data"
		>
		<img src="'. $this->spinner.'" style="display:none">
		<b></b>
	</div><br />

	<div>
		<input 
			type="button" 
			name="essential-grid" 
			data-action="cd_importer" 
			data-nonce="' . wp_create_nonce('cd_importer_nonce') . '" 
			class="button button-primary" 
			value="Import essential grid data"
		>
		<img src="'. $this->spinner.'" style="display:none">
		<b></b>
	</div><br />
</div><br />',
			),
		    array(
		      'type'    => 'notice',
		      'class'   => 'warning',
		      'content' => '<div style="font-size: 14px;line-height: 24px;">Please note: If after importing demo data, you got this error "Record not found" on homepage, Just edit home page from Dashboard > Pages > Home and from page setting change revSlider option.</div>'
		    ),
		  )
		);

		if ( class_exists('CSF_Customize') ) {
			CSF_Customize::instance( $options, 'codevz_demo_importer' );
		}
	}

	public function get_string_between( $string, $start, $end ) {
		$string = " " . $string;
		$ini = strpos( $string, $start );
		if ( $ini === 0 ) {
			return '';
		}
		$ini += strlen( $start );
		$len = strpos( $string, $end, $ini ) - $ini;
		return substr( $string, $ini, $len );
	}

	public function cd_importer() {
		check_ajax_referer( 'cd_importer_nonce', 'nonce' );

		$act = @$_GET['act'];
		$demo = @$_GET['demo'];
		$attach = @$_GET['attach'];

		$msg = array( 'imported' => 0, 'attach' => $attach );
		$path = CD_DIR_PATH . 'importer/demo-' . $demo . '/';

		if ( empty( $demo ) || empty( $act ) || ! file_exists( $path ) ) {
			$msg['msg'] = '<p><img src="'. $this->no .'"> ' . __( 'Error: Please select a demo and try again ...', 'remix' ) . ' </p>';
			
			echo json_encode( $msg );
			die();
		}

		if ( $act === 'theme_options' && function_exists( 'csf_decode_string' ) ) {

			// Options
			$options = $path . $act . '.txt';
			$options = file_get_contents( $options );
			$options = csf_decode_string( $options );
			update_option( '_cs_options', $options );
			update_option( 'css_cached', false );

			$msg['imported'] = 1;

		} else if ( $act === 'widgets' ) {

			$widgets = $path . $act . '.wie';
			$widgets = file_get_contents( $widgets );
			$widgets = @json_decode( $widgets );
			delete_option( 'sidebars_widgets' );
			$this->import_widgets( $widgets );

			$msg['imported'] = 1;

		} else if ( $act === 'content' ) {

			// Delete old menus if exists
			wp_delete_nav_menu( 'Primary' );
			wp_delete_nav_menu( 'One page' );
			wp_delete_nav_menu( 'Footer' );

			// Start
			ob_start();
			$xml = $path . 'content.xml';
			$this->import_content( $xml, $attach );

			// Menus to import and assign
			$primary_menu = get_term_by('name', 'primary', 'nav_menu');
			$onepage_menu = get_term_by('name', 'onepage', 'nav_menu');
			$footer_menu = get_term_by('name', 'footer',  'nav_menu');
			
			$locations = get_theme_mod('nav_menu_locations');
			$locations['primary'] = $primary_menu->term_id;
			$locations['onepage'] = $onepage_menu->term_id;
			$locations['footer'] = $footer_menu->term_id;

			set_theme_mod('nav_menu_locations', $locations);

			// Set homepage
			$homepage = get_page_by_title( 'Home' );
			$posts_page = get_page_by_title( 'News' );

			if ( $homepage->ID > 0 ) {
				update_option( 'page_on_front', $homepage->ID );
				update_option( 'show_on_front', 'page' );	
			}	
			if ( $posts_page->ID > 0 ) {
				update_option('page_for_posts', $posts_page->ID);
			}

			$content_msg = ob_get_clean();

			$msg['imported'] = 1;

		} else if ( $act === 'pages' ) {

			ob_start();
			$xml = $path . 'pages.xml';
			$this->import_content( $xml, false );
			$pages_msg = ob_get_clean();

			$msg['imported'] = 1;

		} else if ( $act === 'masterslider--error' ) {

			global $ms_importer;
			if ( ! empty( $ms_importer ) ) {

				$masterslider = $path . 'masterslider.json';
				ob_start();
				$ms_importer->import_data( $masterslider );
				$masterslider_msg = ob_get_clean();

				$msg['imported'] = 1;
			} else {
				$msg['plugin_error'] = 1;
			}

		} else if ( $act === 'revslider' ) {

			if ( class_exists( 'RevSlider' ) ) {

				$revsliders = array();
				foreach ( glob( $path . '*.zip' ) as $i ) {
					$revsliders[] = $i;
				}

				ob_start();
				foreach ( $revsliders as $slider ) {
					$revslider = new RevSlider();
					$revslider->importSliderFromPost( true, true, $slider );
				}
				$rev_msg = ob_get_clean();

				$msg['imported'] = 1;
			} else {
				$msg['plugin_error'] = 1;
			}

		} else if ( $act === 'essential-grid' ) {

			$ess_data = $path . 'ess_grid.json';
			if ( is_file( $ess_data ) ) {
				$ess_data = file_get_contents( $ess_data );
				$ess_data = json_decode( $ess_data, true );

				if ( class_exists( 'Essential_Grid_Import' ) && is_array( $ess_data ) ) {
					$ess = new Essential_Grid_Import();

					$ess->import_grids( $ess_data['grids'], true );
					$ess->import_skins( $ess_data['skins'], true );
					$ess->import_navigation_skins( $ess_data['navigation-skins'], true );
					$ess->import_elements( $ess_data['elements'], true );
					$ess->import_global_styles( $ess_data['global-css'], true );
					$ess->import_punch_fonts( $ess_data['punch-fonts'], true );
					$ess->import_custom_meta( (array) $ess_data['custom-meta'], true );

					$msg['imported'] = 1;
				} else {
					$msg['plugin_error'] = 1;
				}
			} else {
				$msg['not_needed'] = 1;
			}

		} else {}

		if ( $msg['imported'] ) {
			$echo = sprintf( __( '%s imported successfully.', 'remix' ), ucwords( $act ) );
			$img = $this->yes;
		} else if ( isset( $msg['plugin_error'] ) ) {
			$echo = sprintf( __( '%s plugin not installed, please install it then import date.', 'remix' ), ucwords( $act ) );
			$img = $this->no;
		} else if ( isset( $msg['not_needed'] ) ) {
			$echo = sprintf( __( 'This demo does not need %s data.', 'remix' ), ucwords( $act ) );
			$img = $this->yes;
		} else {
			$echo = sprintf( __( 'Could not import %s, Please try again ... If again get same message, so you need to import data manually.', 'remix' ), ucwords( $act ) );
			$img = $this->no;
		}

		$msg['msg'] = '<p><img src="' . $img . '"> ' . $echo . '</p>';

		echo json_encode( $msg );
		die();
	}

	public function import_content( $file, $attachments ) {
		
		if ( ! defined('WP_LOAD_IMPORTERS') ) {
			define( 'WP_LOAD_IMPORTERS', true );
		}

	    require_once ABSPATH . 'wp-admin/includes/import.php';
	    $importer_error = false;

	    if ( ! class_exists( 'WP_Importer' ) ) {

	        $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';

	        if ( file_exists( $class_wp_importer ) ){
	            require_once( $class_wp_importer );
	        } else {
	            $importer_error = true;
	        }
	    }

	    if ( ! class_exists( 'WP_Import' ) ) {
	        $class_wp_import = CD_DIR_PATH .'importer/wordpress-importer.php';
	        if ( file_exists( $class_wp_import ) ) {
	            require_once( $class_wp_import );
	        } else {
	            $importer_error = true;
	        }
	    }

	    if ( $importer_error ) {
	        die( "Error on import" );
	    } else {
	        if( ! is_file( $file ) ) {
	            echo "The XML file containing the dummy content is not available or could not be read .. You might want to try to set the file permission to chmod 755.<br/>If this doesn't work please use the Wordpress importer and import the XML file (should be located in your download .zip: Sample Content folder) manually ";
	        } else {
				$wp_import = new WP_Import();
				$wp_import->fetch_attachments = $attachments;
				$wp_import->import( $file );
	     	}
		}

	}

	public function import_widgets( $data ) {

		global $wp_registered_sidebars;

		if ( empty( $data ) || ! is_object( $data ) ) {
			return;
		}

		$available_widgets = $this->available_widgets();

		// Get all existing widget instances
		$widget_instances = array();
		foreach ( $available_widgets as $widget_data ) {
			$widget_instances[$widget_data['id_base']] = get_option( 'widget_' . $widget_data['id_base'] );
		}

		// Begin results
		$results = array();

		// Loop import data's sidebars
		foreach ( $data as $sidebar_id => $widgets ) {

			// Skip inactive widgets
			if ( 'wp_inactive_widgets' == $sidebar_id ) {
				continue;
			}

			// Check if sidebar is available on this site
			// Otherwise add widgets to inactive, and say so
			if ( isset( $wp_registered_sidebars[$sidebar_id] ) ) {
				$sidebar_available = true;
				$use_sidebar_id = $sidebar_id;
			} else {
				$sidebar_available = false;
				$use_sidebar_id = 'wp_inactive_widgets';
			}

			// Result for sidebar
			$results[$sidebar_id]['name'] = ! empty( $wp_registered_sidebars[$sidebar_id]['name'] ) ? $wp_registered_sidebars[$sidebar_id]['name'] : $sidebar_id; // sidebar name if theme supports it; otherwise ID
			$results[$sidebar_id]['widgets'] = array();

			// Loop widgets
			foreach ( $widgets as $widget_instance_id => $widget ) {

				$fail = false;

				// Get id_base (remove -# from end) and instance ID number
				$id_base = preg_replace( '/-[0-9]+$/', '', $widget_instance_id );
				$instance_id_number = str_replace( $id_base . '-', '', $widget_instance_id );

				// Does site support this widget?
				if ( ! $fail && ! isset( $available_widgets[$id_base] ) ) {
					$fail = true;
				}

				// Does widget with identical settings already exist in same sidebar?
				if ( ! $fail && isset( $widget_instances[$id_base] ) ) {

					// Get existing widgets in this sidebar
					$sidebars_widgets = get_option( 'sidebars_widgets' );
					$sidebar_widgets = isset( $sidebars_widgets[$use_sidebar_id] ) ? $sidebars_widgets[$use_sidebar_id] : array(); // check Inactive if that's where will go

					// Loop widgets with ID base
					$single_widget_instances = ! empty( $widget_instances[$id_base] ) ? $widget_instances[$id_base] : array();
					foreach ( $single_widget_instances as $check_id => $check_widget ) {

						// Is widget in same sidebar and has identical settings?
						if ( in_array( "$id_base-$check_id", $sidebar_widgets ) && (array) $widget == $check_widget ) {

							$fail = true;

							break;

						}

					}

				}

				// No failure
				if ( ! $fail ) {

					// Add widget instance
					$single_widget_instances = get_option( 'widget_' . $id_base ); // all instances for that widget ID base, get fresh every time
					$single_widget_instances = ! empty( $single_widget_instances ) ? $single_widget_instances : array( '_multiwidget' => 1 ); // start fresh if have to
					$single_widget_instances[] = (array) $widget; // add it

					// Get the key it was given
					end( $single_widget_instances );
					$new_instance_id_number = key( $single_widget_instances );

					// If key is 0, make it 1
					// When 0, an issue can occur where adding a widget causes data from other widget to load, and the widget doesn't stick (reload wipes it)
					if ( '0' === strval( $new_instance_id_number ) ) {
						$new_instance_id_number = 1;
						$single_widget_instances[$new_instance_id_number] = $single_widget_instances[0];
						unset( $single_widget_instances[0] );
					}

					// Move _multiwidget to end of array for uniformity
					if ( isset( $single_widget_instances['_multiwidget'] ) ) {
						$multiwidget = $single_widget_instances['_multiwidget'];
						unset( $single_widget_instances['_multiwidget'] );
						$single_widget_instances['_multiwidget'] = $multiwidget;
					}

					// Update option with new widget
					update_option( 'widget_' . $id_base, $single_widget_instances );

					// Assign widget instance to sidebar
					$sidebars_widgets = get_option( 'sidebars_widgets' ); // which sidebars have which widgets, get fresh every time
					$new_instance_id = $id_base . '-' . $new_instance_id_number; // use ID number from new widget instance
					$sidebars_widgets[$use_sidebar_id][] = $new_instance_id; // add new instance to sidebar
					update_option( 'sidebars_widgets', $sidebars_widgets ); // save the amended data

				}

				// Result for widget instance
				$results[$sidebar_id]['widgets'][$widget_instance_id]['name'] = isset( $available_widgets[$id_base]['name'] ) ? $available_widgets[$id_base]['name'] : $id_base; // widget name or ID if name not available (not supported by site)
				$results[$sidebar_id]['widgets'][$widget_instance_id]['title'] = isset( $widget->title ) ? $widget->title : __( 'No Title', 'remix' );

			}
		}
	}

	public function available_widgets() {

		global $wp_registered_widget_controls;
		$widget_controls = $wp_registered_widget_controls;
		$available_widgets = array();

		foreach ( $widget_controls as $widget ) {
			if ( ! empty( $widget['id_base'] ) && ! isset( $available_widgets[$widget['id_base']] ) ) { // no dupes

				$available_widgets[$widget['id_base']]['id_base'] = $widget['id_base'];
				$available_widgets[$widget['id_base']]['name'] = $widget['name'];

			}
		}

		return $available_widgets;
	}

}

new CodevzDemoImporter();