/**
 * Codevz Ajax
 * 
 * @package: Remix
 * @author: Codevz
 * @copyright: http://codevz.com/
 */

;(function ( $, window, document, undefined ) {
	'use strict';

    $( document ).ready(function() {

		var History = window.History;

		if ( !History.enabled ) {
			return false;
		}

		/* Ajaxify */
		$.fn.ajaxify = function() {

			if ( window.location.search.indexOf( 'submission' ) > -1 ) {
				return;
			}

			var $t = $( this );
			$( '.playable,a[href$=".jpg"],a[href$=".jpeg"],a[href$=".png"],a[href$=".gif"],a[href$=".mp3"],a[href$=".zip"],a[href$=".rar"],a[href$=".mov"],a[href$=".mp4"],a[href$=".pdf"],a[href$=".mpeg"],.comment-reply-link' ).addClass('no-ajax');

			$t.on('click', 'a:not(.no-ajax,.likes_dislikes a,.player a,.load_more a,[href^="#"],[href*="wp-login"],[href*="wp-admin"],[data-rel^="prettyPhoto"])', function( e ) {
				var $t = $(this),
					url = $t.attr('href'),
					title = $t.attr('title') || null;

				/* Continue as normal for cmd clicks etc */
				if ( e.which == 2 || e.metaKey ) { return true; }

				History.pushState( null, title, url );
				e.preventDefault();

				/* Fix Nicescroll */
				//$( '.scroll' ).getNiceScroll().remove();

				return false;
			});

			/* Demo purpose */
			if ( window.location.search.indexOf( 'ajax' ) > -1 ) {
				$( '#layout a[href*="' + window.location.hostname + '"]:not(.popup_player,.no-ajax)' ).each(function() {
					var href = $(this).attr('href');
					if ( href ) {
						href = href.replace(/\?.+/, '');
						href += ( href.match(/\?/) ? '&' : '?' ) + 'ajax';
						$(this).attr('href', href);
					}
				});
			}

			/* Ajax add comment */
			if ( $('.single').length ) {
				var commentform = $('#commentform');
				commentform.prepend('<div id="comment-status"></div>');
				var statusdiv = $('#comment-status');
				commentform.submit(function(){
				    var formdata = commentform.serialize();
				    statusdiv.html('<pre>Please wait ...</pre>');
				    var formurl = commentform.attr('action');
				    $.ajax({
				        type: 'post',
				        url: formurl,
				        data: formdata,
				        error: function(XMLHttpRequest, textStatus, errorThrown){
				        	statusdiv.html('<pre>You might have left one of the fields blank, or be posting too quickly</pre>');
				        },
				        success: function(data, textStatus) {
				            if ( data ) {
				                statusdiv.html('<pre>Thanks for your comment. We will review and approve it soon.</pre>');
				                commentform.find('textarea[name=comment]').val('');
				            }
				        }
				    });

				    return false;
				});
			}

			return $t;
		};

		$( 'body' ).ajaxify();

		/* Ajax Prepare */
		var selector 	= '#page-content',
    	$selector 	= $( selector ),
    	rootUrl 	= History.getRootUrl();
		/* Hook into StateChange */
		$(window).bind('statechange',function(){

			var State 	= History.getState(),
				  url 	= State.url,
				  relativeUrl = url.replace( rootUrl, '' );
			    if($( document ).scrollTop() > 0){
			      $( 'body, html' ).animate( { scrollTop: 0 }, 100 );
			    }
			$.ajax({
				url: url,
				cache: false,
				success: function( data, textStatus, jqXHR ) {
      			setTimeout(function(){
					var $data 		= $( data ),
						Scripts 	= $data.filter( 'script' ),
						Styles 		= $data.filter( 'link[rel="stylesheet"]' ),
						inStyles 	= $data.filter( 'style' ),
						found 		= false;

					/* Fetch content */
					if ( ! $data ) {
						console.log( 'Success: no data' );
						document.location.href = url;
						return false;
					}

					/* Scripts */
					if ( ! $( '#inline_js' ).length ) {
						$( 'body' ).append( '<div id="inline_js"></div>' );
					} else {
						$( '#inline_js' ).html('');
					}
					Scripts.each(function(i){
						var script 	= $( Scripts[i].outerHTML ),
							src 	= script.attr('src');

						if ( src ) {
							$( 'script[src]' ).each(function(){
								if ( $(this).attr('src') === src ) {
									found = true;
									return found;
								}
							});
							if ( found === false ) {
								$.getScript( src );
							} else if ( src.match( /(js\/custom.js|js_composer|contact-form-7|woocommerce|addthis|ratings)/ ) ) {
								if ( window.addthis ) {
									window.addthis = null;
								}
								$.getScript( src );
							}
							found = false;
						} else { 
							$( '#inline_js' ).append( script );
						}
					});


					/* Styles */
					Styles.each(function(i){
						var style 	= $( Styles[i].outerHTML ),
							href 	= style.attr('href');

						if ( href ) {
							$( 'link[rel="stylesheet"]' ).each(function(){
								if ( $(this).attr('href') === href ) {
									found = true;
									return found;
								}
							});
							if ( found === false ) {
								$( 'head' ).append( style );
							}
							found = false;
						}
					});

					/* inline Styles */
					inStyles.each(function(i){
						$( 'style' ).each(function(){
							if ( $( this ).html() === $( inStyles[i] ).html() ) {
								found = true;
								return found;
							}
						});
						if ( found === false ) {
							$( 'head' ).append( $( inStyles[i].outerHTML ) );
						}
						found = false;
					});

					/* Classes */
					var matches = data.match(/<body.*class=["']([^"']*)["'].*>/);
					$( 'body' ).removeClass().addClass( matches && matches[1] );

					/* Menus */
					$( 'header nav' ).parent().html( $data.find( 'header nav' ) );


					/* Content */
					$selector.html( $data.find( selector ) ).animate( { opacity: 1 }, 1000 );

					/* Title */
					try {
						document.getElementsByTagName('title')[0].innerHTML = $data.filter( 'title' ).html();
					}
					catch ( Exception ) { }

					/* Complete changes */
					$(window).trigger( 'statechangecomplete' );

					/* Inform Google Analytics of the change */
					if ( typeof window._gaq !== 'undefined' ) {
						window._gaq.push(['_trackPageview', relativeUrl]);
					}

					/* Inform ReInvigorate of a state change */
					if ( typeof window.reinvigorate !== 'undefined' && typeof window.reinvigorate.ajax_track !== 'undefined' ) {
						reinvigorate.ajax_track(url);
					}

					/* Ajaxify new links */
					$( 'body' ).ajaxify().addClass( 'ajax_loaded' );
          		}, 1000);
				},
				error: function( jqXHR, textStatus, errorThrown ) {
					console.log( 'errorThrown_CD_AJAX' );
					console.log( errorThrown );
					document.location.href = url;
					return false;
				}

			}); /* ajax */

		}); /* onStateChange */

	}); /* Ready */

})( jQuery, window, document );
