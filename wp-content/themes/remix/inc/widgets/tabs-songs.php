<?php

/**
 * Songs tabs widget
 *
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( ! class_exists('CodevzTabbedSongs') ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzTabbedSongs');
	});

	class CodevzTabbedSongs extends WP_Widget {

		function __construct() {
			parent::__construct( 
				false, 
				esc_html__('CD - Tabbed songs', 'remix'), 
				array('classname' => 'cd_tabbed_songs') 
			);

			$this->all = array( 'a', 'b', 'c', 'd' );
		}

		static public function slugify( $text ) {
		  // replace non letter or digits by -
		  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

		  // transliterate
		  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		  // remove unwanted characters
		  $text = preg_replace('~[^-\w]+~', '', $text);

		  // trim
		  $text = trim($text, '-');

		  // remove duplicate -
		  $text = preg_replace('~-+~', '-', $text);

		  // lowercase
		  $text = strtolower($text);

		  if (empty($text)) {
		    return 'n-a';
		  }

		  return $text;
		}
		
		public function widget( $args, $instance ) {
			extract( $args );

			$count = 0;
			$tabs = array();

			foreach ( $this->all as $a ) {
				$tabs[] = array_filter( array(
					'post_type' => $instance[ $a . '_post_type' ],
					'title' => $instance[ $a . '_title' ],
					'order' => $instance[ $a . '_orderby' ],
				) );

				if ( !empty( $instance[ $a . '_title' ] ) ) {
					$count++;
				}
			}

			ob_start();
			$out = '<section class="widget def-block tabs clr">';
			$out .= '<div class="tabs-nav wtext clr count-'.$count.'">';

			foreach ( $tabs as $tab ) {
				$out .= empty( $tab['title'] ) ? '' : '<a href="#tab-'. $this->slugify( $tab['title'] ) .'">'.$tab['title'].'</a>';
			}

			$out .= '</div><div class="tabs-content clr">';

			global $post, $codevz;

			foreach ( $tabs as $tab ) {
				if ( ! empty( $tab['title'] ) ) {

					$is_dr = ( $tab['order'] === 'date' || $tab['order'] === 'rand' ) ? 1 : 0;

					$query = new WP_Query(array(
						'post_type'			=> array( 'songs' ),
						'order' 			=> 'DESC',
						'posts_per_page'	=> $instance['count'],
						'meta_key'			=> $is_dr ? null : $tab['order'],
						'orderby'			=> $is_dr ? $tab['order'] : 'meta_value_num'
					));

					$tab['order'] = $is_dr ? 'cd_plays' : $tab['order'];

					$out .= '<div id="tab-' . $this->slugify( $tab['title'] ) . '" class="tab">';
					while ( $query->have_posts()): $query->the_post(); 
						$meta = $codevz->meta( $post->ID ); 

						$out .= '<div class="item_small">';
						if ( has_post_thumbnail() ): 
							$out .= '<a class="cdEffect noborder" href="' . get_the_permalink() . '" title="' . get_the_title() . '">';
							$out .= get_the_post_thumbnail( $post->ID, 'tumb' );
							$out .= '<i class="fa fa-music"></i>';
							$out .= '</a>';
						endif;

						$out .= '<div class="item-details">';
						$out .= '<h3><a href="' . get_the_permalink() . '" rel="bookmark" title="' . get_the_title() . '">' . get_the_title() . '</a></h3>';
						$out .= '<div class="post_meta">';
						$out .= '<span class="block">' . $codevz->get_artists( isset($meta['artist']) ? $meta['artist'] : '' ) . '</span>';
						if ( $tab['order'] === 'cd_likes' || $tab['order'] === 'cd_dislikes' ) :
							$out .= '<span><i class="fa fa-heart"></i> ' . get_post_meta($post->ID,'cd_likes', true);
							$out .= '&nbsp; <i class="fa fa-heart-o"></i> ' . get_post_meta($post->ID,'cd_dislikes', true) . '</span>';
						elseif ( $tab['order'] === 'cd_downloads' ) :
							$out .= '<span><i class="fa fa-download mi"></i>' . get_post_meta($post->ID,'cd_downloads', true) . '</span>';
						else :
							$out .= '<span><i class="fa fa-play mi"></i>' . $codevz->get_plays( $post->ID, $tab['order'] ) . '</span>';
						endif;
						$out .= '</div>';
						$out .= '</div>';
						$out .= '</div>';
					endwhile;
					$out .= '</div>';

					wp_reset_postdata();
				}
			}

			$out .= '</div></section>';

			$out .= ob_get_clean();
			$out .= "\n";
			echo $out;
		}
		
		public function update( $new, $old ) {

			foreach ( $this->all as $a ) {
				$old[ $a . '_title'] 		= strip_tags( $new[ $a . '_title'] );
				$old[ $a . '_post_type'] 	= strip_tags( $new[ $a . '_post_type'] );
				$old[ $a . '_orderby'] 		= strip_tags( $new[ $a . '_orderby'] );
			}

			$old['count'] = strip_tags( $new['count'] );

			return $old;
		}

		public function form( $instance ) {

			$defaults = array( 'count' => '' );

			foreach ( $this->all as $a ) {
				$defaults[ $a . '_title'] 		= '';
				$defaults[ $a . '_post_type'] 	= '';
				$defaults[ $a . '_orderby'] 	= '';
			}

			$instance = wp_parse_args( (array) $instance, $defaults );
			
			foreach ( $this->all as $a ) {

				echo csf_add_field( array(
					'id'    => $this->get_field_name( $a . '_title' ),
					'name'  => $this->get_field_name( $a . '_title' ),
					'type'  => 'text',
					'title' => esc_html__('Tab', 'remix') . ' ' . ucwords( $a )
				), esc_attr( $instance[ $a . '_title' ] ) );

				echo csf_add_field( array(
					'id'    => $this->get_field_name( $a . '_post_type' ),
					'name'  => $this->get_field_name( $a . '_post_type' ),
					'type'  => 'select',
					'options' => array(
						'songs' 	=> esc_html__( 'Songs', 'remix' ),
						'podcasts' 	=> esc_html__( 'Podcasts', 'remix' ),
						'playlists' => esc_html__( 'Playlists', 'remix' ),
					),
					'title' => esc_html__('Post type', 'remix')
				), $instance[ $a . '_post_type' ] );

				echo csf_add_field( array(
					'id'    => $this->get_field_name( $a . '_orderby' ),
					'name'  => $this->get_field_name( $a . '_orderby' ),
					'type'  => 'select',
					'options' => array(
						'cd_plays' 		=> esc_html__( 'Total plays', 'remix' ),
						'cd_plays_nd' 	=> esc_html__( 'Today plays', 'remix' ),
						'cd_plays_nw' 	=> esc_html__( 'This week plays', 'remix' ),
						'cd_plays_nm' 	=> esc_html__( 'This month plays', 'remix' ),
						'cd_likes' 		=> esc_html__( 'Likes', 'remix' ),
						'cd_dislikes' 	=> esc_html__( 'Dislikes', 'remix' ),
						'cd_downloads' 	=> esc_html__( 'Total downloads', 'remix' ),
						'cd_downloads_nd' 	=> esc_html__( 'Today downloads', 'remix' ),
						'cd_downloads_nw' 	=> esc_html__( 'This week downloads', 'remix' ),
						'cd_downloads_nm' 	=> esc_html__( 'This month downloads', 'remix' ),
						'date' 			=> esc_html__( 'Recent', 'remix' ),
						'rand' 			=> esc_html__( 'Random', 'remix' ),
					),
					'title' => esc_html__('Sort by', 'remix')
				), $instance[ $a . '_orderby' ] );

			}

			echo csf_add_field( array(
				'id'    => $this->get_field_name('count'),
				'name'  => $this->get_field_name('count'),
				'type'  => 'number',
				'title'	=> esc_html__('Count', 'remix'),
			), esc_attr( $instance['count'] ) );

		}

	}

}
