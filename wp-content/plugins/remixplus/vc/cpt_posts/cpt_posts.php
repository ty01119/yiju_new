<?php

/**
 * @author Codevz
 * @link http://Codevz.com
 *
 * Post type posts add-one for visual composer
 * Shortcode name: codevz_cpt_posts
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzPostTypePosts extends CodevzVc {

	function __construct() {
		add_action( 'init', array( $this, 'in' ) );
		add_shortcode( 'codevz_cpt_posts', array( $this, 'out' ) );
	}

	function in() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'codevz_cpt_posts',
			'name'			=> __( 'Post type posts', 'cd' ), 
			'icon'			=> 'dashicons-before dashicons-screenoptions cd_ico',
			'params'		=> array(
				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Title', 'cd'),
					'param_name' 	=> 'block_title',
					'edit_field_class'=> $this->col('6'),
					'admin_label' 	=> true
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Post type', 'cd'),
					'param_name' 	=> 'post_type',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'Posts', 'cd' ) 	=> 'post',
						__( 'Artists', 'cd' ) 	=> 'artists',
						__( 'Songs', 'cd' ) 	=> 'songs',
						__( 'Playlists', 'cd' ) => 'playlists',
						__( 'Podcasts', 'cd' ) 	=> 'podcasts',
						__( 'Videos', 'cd' ) 	=> 'videos',
						__( 'Lyrics', 'cd' ) 	=> 'lyrics',
						__( 'Events', 'cd' ) 	=> 'events',
						__( 'Gallery', 'cd' ) 	=> 'gallery',
					),
					'admin_label' 	=> true
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Gallery type', 'cd'),
					'param_name' 	=> 'gallery_type',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'Classic', 'cd' ) 	=> 0,
						__( 'Modern', 'cd' ) 	=> 1
					),
					"std" 			=> 0,
					"dependency" 	=> array(
						"element" 		=> "post_type",
						"value" 		=> "gallery"
					)
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Events type', 'cd'),
					'param_name' 	=> 'events_type',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'Upcoming', 'cd' ) 	=> 0,
						__( 'Past', 'cd' ) 		=> 1
					),
					"std" 			=> 0,
					"dependency" 	=> array(
						"element" 		=> "post_type",
						"value" 		=> "events"
					)
				),
				array(
					"type"			=> "dropdown",
					"heading"		=> __("Columns", "cd"),
					"description"	=> __("Except posts & events", "cd"),
					'edit_field_class'=> $this->col('6'),
					"param_name"	=> "col",
					"value"			=> array( "4", "3", "2", "1" )
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Pagination', 'cd'),
					'param_name' 	=> 'pagination',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'Yes', 'cd' ) 	=> 'yes',
						__( 'No', 'cd' ) 	=> 'no'
					)
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Class', 'cd'),
					'param_name'	=> 'extra_class',
					'edit_field_class'=> $this->col('6'),
				),

				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Posts count', 'cd'),
					'param_name' 	=> 'posts_per_page',
					'group'			=> $this->WP_Query()
				), 
				array(
					"type"			=> "dropdown",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading"		=> __("Order", "cd"),
					"param_name"	=> "order",
					"value"			=> array(
						__("Descending", "cd") => 'DESC',
						__("Ascending", "cd") => 'ASC',
					)
				), 
				array(
					"type"			=> "dropdown",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading"		=> __("Orderby", "cd"),
					"param_name"	=> "orderby",
					"value"			=> array(
						__("Date", "cd")	=> 'date',
						__("ID", "cd")		=> 'ID',
						__("Random", "cd") => 'rand',
						__("Author", "cd") => 'author',
						__("Title", "cd")	=> 'title',
						__("Name", "cd")	=> 'name',
						__("Type", "cd")	=> 'type',
						__("Modified", "cd") => 'modified',
						__("Parent ID", "cd") => 'parent',
						__("Comment Count", "cd") => 'comment_count',
					)
				),
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Post parent", "cd"),
					"param_name" 	=> "post_parent"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Offset", "cd"),
					"param_name" 	=> "offset"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Year", "cd"),
					"param_name" 	=> "year"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Month num", "cd"),
					"param_name" 	=> "monthnum"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Day", "cd"),
					"param_name" 	=> "day"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Meta key", "cd"),
					"param_name" 	=> "meta_key"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Meta value", "cd"),
					"param_name" 	=> "meta_value"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Meta compare", "cd"),
					"param_name" 	=> "meta_compare"
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Posts category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> '_cats',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Tags', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> '_tags',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Artists alphabet', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'alphabet',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Artists category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'artists_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Songs category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'songs_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Playlists category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'playlists_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Podcasts category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'podcasts_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Lyrics category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'lyrics_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Videos category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'videos_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Events category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'events_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Gallery category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'gallery_cat',
					'group'			=> $this->WP_Query()
				),
			)

		));
	}

	function out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'block_title' 	=> '',
			'col'			=> '4',
			'gallery_type'	=> 0,
			'events_type'	=> 0,
			'post_type'		=> 'post',
			'pagination'	=> 'yes',
			'extra_class'	=> '',
			'posts_per_page'=> '',
			'post_parent' 	=> '',
			'offset' 		=> '',
			'order' 		=> '',
			'orderby' 		=> '',
			'year' 			=> '',
			'monthnum' 		=> '',
			'day' 			=> '',
			'meta_key' 		=> '',
			'meta_value' 	=> '',
			'meta_compare' 	=> '',
			'_cats'			=> '',
			'_tags' 		=> '',
			'alphabet' 		=> '',
			'artists_cat' 	=> '',
			'songs_cat' 	=> '',
			'playlists_cat' => '',
			'podcasts_cat'  => '',
			'lyrics_cat' 	=> '',
			'videos_cat' 	=> '',
			'gallery_cat' 	=> '',
			'events_cat' 	=> ''
		), $atts );

		global $codevz;

		if ( $codevz ) {
			ob_start(); 

			/* Start section */
			$cpt = $atts['post_type'];
			$modern_gallery = ( $cpt === 'gallery' && $atts['gallery_type'] );
			$fix_grid = ( $cpt === 'artists' || $cpt === 'gallery' || $cpt === 'lyrics' || $cpt === 'videos' || $cpt === 'podcasts' );
			echo '<section class="def-block clr ' . 'cpt_' . $fix_grid . ' ' . 'gt_'. $modern_gallery . ' ' . $atts['extra_class'] . '">';
			echo $atts['block_title'] ? '<h4 class="tt">' . $atts['block_title'] . '</h4><span class="liner"></span>' : '';

			/* Tax query */
			$taxes = array( '_cats', '_tags', 'alphabet', 'artists_cat', 'songs_cat', 'playlists_cat', 'podcasts_cat', 'lyrics_cat', 'videos_cat', 'gallery_cat', 'events_cat' );
			$tax_query = array();
			foreach ( $taxes as $tax ) {
				if ( isset( $atts[ $tax ] ) ) {
					$tax_array = explode( ',', $atts[ $tax ] );
					switch ( $tax ) {
						case '_cats':
							$tax = 'category';
							break;
						case '_tags':
							$tax = 'post_tags';
							break;
						case 'alphabet':
							$tax = 'artist';
							break;
					}
					foreach ( $tax_array as $cat ) {
						if ( ! empty( $cat ) ) {
							$tax_query[] = array( 'taxonomy' => $tax, 'field' => 'slug', 'terms' => $cat );
						}
					}
				}
			}
			$atts['tax_query'] = !empty( $tax_query ) ? wp_parse_args( $tax_query, array( 'relation' => 'OR' ) ) : null;

			global $paged;
			$atts['paged'] = is_front_page() ? get_query_var('page') : ( get_query_var('paged') ? get_query_var('paged') : 1 );   

			/* Events */
			if ( $atts['post_type'] === 'events' ) {
				$atts['order'] = $atts['events_type'] ? 'DESC' : 'ASC';
				$atts['orderby'] = 'meta_value';
				$atts['meta_key'] = 'cd_end_event_date';
				$atts['meta_query'] = array(
					array(
						'key' => 'cd_end_event_date', 
						'value' => current_time( 'Y/m/j H:i' ), 
						'type' 	=> 'DATETIME',
						'compare' => $atts['events_type'] ? '<=' : '>='
					)
				);
				$atts['post_type'] = 'events';
			}

			$posts = new WP_Query( array_filter( $atts ) );

			$col = preg_match( "/post|events/", $atts['post_type'] ) ? '1' : $atts['col'];
			switch ( $col ) {
				case '2':
					$class = 'grid_6';
					break;
				case '3':
					$class = 'grid_4';
					break;
				case '4':
					$class = 'grid_3';
					break;
				default:
					$class = 'grid_12';
					break;
			}
			$i = 1;
			if ( $posts->have_posts() ) :
				echo '<div class="clr">'; 

				if ( $modern_gallery ) {
					echo '<div id="close" class="button small"><span><i class="fa fa-caret-left"></i></span></div>';
					echo '<ul id="tp-grid" class="tp-grid">';
				}

				while ( $posts->have_posts() ): $posts->the_post(); 

					$classes = array(
						( $i % $col == 1 ) ? 'alpha' : '',
						( $i % $col == 0 ) ? 'omega' : '',
						$atts['post_type'],
						$class
					);

					$codevz->loop( $atts['post_type'], implode( ' ', $classes ), $atts['gallery_type'] );

					if ( $i % $col == 0 && $atts['post_type'] !== 'gallery' ) {
						echo '</div><div class="clr">';
					}

					$i++;
				endwhile;

				echo $modern_gallery ? '</ul>' : '';
				echo '</div>';
				$atts['pagination'] == 'yes' ? $codevz->pagination( $posts ) : '';
			else:
				echo '<h3>'.$codevz->option( 'not_found', 'Not found any items' ).'</h3>';
			endif;

			echo '</section>';

			wp_reset_postdata();
			return ob_get_clean();
		} else {
			return 'This theme do not support this shortcode.';
		}
	}
}

new CodevzPostTypePosts();