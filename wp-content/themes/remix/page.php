<?php

/**
 * Page
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

	get_header();
	global $codevz;
	$codevz->content_before();
	$meta = $codevz->meta(); ?>

	<article>
	<?php 
		$codevz->page_title();
		
		if ( have_posts() ) { 
			the_post();
			the_content();
		} else {
			echo '<h3>' . $codevz->option( 'not_found' ) . '</h3>';
		}
	?>
	</article>

<?php
	$codevz->comments();
	$codevz->content_after();

	get_sidebar();
	get_footer();
