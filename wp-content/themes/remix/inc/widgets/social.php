<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( !class_exists( 'CodevzSocial' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzSocial');
	});

	class CodevzSocial extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Social icons', 'remix' ), 
				array( 'classname' => 'cd_social_icons' )
			);
			$this->icons = array(
				'twitter',
				'facebook',
				'dribbble',
				'rss',
				'github',
				'vimeo',
				'instagram',
				'linkedin',
				'pinterest',
				'google-plus',
				'foursquare',
				'skype',
				'soundcloud',
				'spotify',
				'youtube',
				'tumblr',
				'star',
				'flickr',
				'behance',
				'deviantart',
				'digg',
				'reddit',
				'envelopeo',
			);
		}
		
		function widget( $args, $instance ) {
			extract( $args );
			$title = apply_filters('widget_title', $instance['title'] );
			echo $before_widget;
			echo $title ? $before_title.$title.$after_title : ''; 
			echo '<div class="social colored clr">';

			foreach ( $this->icons as $i ) {
				if ( $instance[ $i ] ) {
					echo '<a rel="nofollow" class="tip" href="' . $instance[ $i ] . '" target="_blank" title="' . ucwords( $i ) . '"><i class="fa fa-' . $i . '"></i></a>';
				}
			}

			echo '</div>';
			echo $after_widget;
		}

		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title'] = strip_tags( $new_instance['title'] );
			foreach ( $this->icons as $i ) {
				$instance[ $i ] = $new_instance[ $i ];
			}

			return $instance;
		}
		
		function form( $instance ) {

			$defaults = array(
				'title' => 'Follow Us',
				'twitter' => '',
				'facebook' => '',
				'dribbble' => '',
				'rss' => '',
				'github' => '',
				'vimeo' => '',
				'instagram' => '',
				'linkedin' => '',
				'pinterest' => '',
				'google-plus' => '',
				'foursquare' => '',
				'skype' => '',
				'soundcloud' => '',
				'spotify' => '',
				'youtube' => '',
				'tumblr' => '',
				'star' => '',
				'flickr' => '',
				'behance' => '',
				'yahoo' => '',
				'deviantart' => '',
				'digg' => '',
				'reddit' => '',
				'envelopeo' => '',
			);
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>

			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'remix') ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
			</p>

			<?php foreach ( $this->icons as $i ) { ?>
				<p>
					<label for="<?php echo $this->get_field_id( $i ); ?>"><?php echo ucwords( $i ); ?></label>
					<input class="widefat" id="<?php echo $this->get_field_id( $i ); ?>" name="<?php echo $this->get_field_name( $i ); ?>" value="<?php echo $instance[ $i ]; ?>" />
				</p>
			<?php }

		}

	}

}
