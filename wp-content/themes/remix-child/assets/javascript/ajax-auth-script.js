jQuery(document).ready(function ($) {
    // Display form from link inside a popup
	$(document).on('click','#popup_login', function (e) {
        e.preventDefault();
        $(".login-modal").fadeIn();
        $(".login-modal .modal-tabs .login-tab").addClass("active");
        $(".login-modal .modal-tabs .register-tab").removeClass("active");
        $(".login-modal .login-container").show();
        $(".login-modal .register-container").hide();
        return false;
    });
    $(document).on('click','#popup_signup', function (e) {
        e.preventDefault();
        $(".login-modal").fadeIn();
        $(".login-modal .modal-tabs .register-tab").addClass("active");
        $(".login-modal .modal-tabs .login-tab").removeClass("active");
        $(".login-modal .register-container").show();
        $(".login-modal .login-container").hide();
        return false;
    });
    $(".login-modal .modal-tabs .register-tab a").click(function(e){
        e.preventDefault();
        $(".login-modal .modal-tabs .login-tab").removeClass("active");
        $(".login-modal .modal-tabs .register-tab").addClass("active");
        $(".login-modal .login-container").fadeOut(500);
        $(".login-modal .register-container").delay(500).fadeIn();
    });
    $(".login-modal .modal-tabs .login-tab a").click(function(e){
        e.preventDefault();
        $(".login-modal .modal-tabs .register-tab").removeClass("active");
        $(".login-modal .modal-tabs .login-tab").addClass("active");
        $(".login-modal .register-container").fadeOut(500);
        $(".login-modal .login-container").delay(500).fadeIn();
    });

	// Close popup
    $(".close").on("click", function () {
		$('.login-modal').fadeOut(500)
        return false;
    });

    // Show the login/signup popup on click
   //  $('#show_login, #show_signup').on('click', function (e) {
   //      $('body').prepend('<div class="login_overlay"></div>');
   //      if ($(this).attr('id') == 'show_login') 
			// $('form#login').fadeIn(500);
   //      else 
			// $('form#register').fadeIn(500);
   //      e.preventDefault();
   //  });

	// Perform AJAX login/register on form submit
	$(document).on('submit','form#login, form#register', function (e) {
        if (!$(this).valid()) return false;
        $('p.status', this).show().text(ajax_auth_object.loadingmessage);
		action = 'ajaxlogin';
		username = 	$('form#login #username').val();
		password = $('form#login #password').val();
		email = '';
		security = $('form#login #security').val();
		if ($(this).attr('id') == 'register') {
			action = 'ajaxregister';
			username = $('#signonname').val();
			password = $('#signonpassword').val();
        	email = $('#email').val();
        	security = $('#signonsecurity').val();	
		}  
		ctrl = $(this);
		$.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_auth_object.ajaxurl,
            data: {
                'action': action,
                'username': username,
                'password': password,
				'email': email,
                'security': security
            },
            success: function (data) {
				$('p.status', ctrl).text(data.message);
				if (data.loggedin == true) {
                    document.location.href = window.location.href;
                }
            }
        });
        e.preventDefault();
    });
});