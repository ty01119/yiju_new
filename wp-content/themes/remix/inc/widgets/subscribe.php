<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( !class_exists( 'CodevzSubscribe' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzSubscribe');
	});

	class CodevzSubscribe extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Feedburner', 'remix' ), 
				array( 'classname' => 'cd_subscribe' )
			);
		}

		function form($instance) {	
			$instance = wp_parse_args( (array) $instance, array('title' => 'Subscribe to RSS Feeds', 'subscribe_text' => 'Get all latest content delivered to your email a few times a month.', 'feedid' => '', 'placeholder' => 'Your Email', 'icon' => 'fa fa-check') );
			
			$title_value = esc_attr( $instance['title'] );
			$title_field = array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			);
			echo csf_add_field( $title_field, $title_value );

			$subscribe_text_value = esc_attr( $instance['subscribe_text'] );
			$subscribe_text_field = array(
				'id'    => $this->get_field_name('subscribe_text'),
				'name'  => $this->get_field_name('subscribe_text'),
				'type'  => 'textarea',
				'title' => esc_html__('Description', 'remix')
			);
			echo csf_add_field( $subscribe_text_field, $subscribe_text_value );

			$icon_value = esc_attr( $instance['icon'] );
			$icon_field = array(
				'id'    => $this->get_field_name('icon'),
				'name'  => $this->get_field_name('icon'),
				'type'  => 'icon',
				'title'	=> esc_html__('Icon', 'remix'),
			);
			echo csf_add_field( $icon_field, $icon_value );

			$placeholder_value = esc_attr( $instance['placeholder'] );
			$placeholder_field = array(
				'id'    => $this->get_field_name('placeholder'),
				'name'  => $this->get_field_name('placeholder'),
				'type'  => 'text',
				'title' => esc_html__('Placeholder', 'remix')
			);
			echo csf_add_field( $placeholder_field, $placeholder_value );

			$feedid_value = esc_attr( $instance['feedid'] );
			$feedid_field = array(
				'id'    => $this->get_field_name('feedid'),
				'name'  => $this->get_field_name('feedid'),
				'type'  => 'text',
				'title' => esc_html__('Feedburner ID or Name', 'remix')
			);
			echo csf_add_field( $feedid_field, $feedid_value );
	    }

		function update($new_instance, $old_instance) {
			$instance=$old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['feedid'] = $new_instance['feedid'];
			$instance['icon'] = $new_instance['icon'];
			$instance['placeholder'] = $new_instance['placeholder'];
			$instance['subscribe_text'] = $new_instance['subscribe_text'];
			$instance['checkbox'] = $new_instance['checkbox'];
			
			return $instance;
		}

		function widget($args, $instance) {
			extract($args);
			$title = apply_filters('widget_title', $instance['title']);
			if ( empty($title) ) $title = false;
			$feedid = $instance['feedid'];	
			$feedbtn = $instance['icon'];	
			$placeholder = $instance['placeholder'];	
			$subscribe_text = $instance['subscribe_text'];	
			echo $before_widget;

			if($title) {
				echo $before_title.$title.$after_title;
			}
		?>
			<p><?php echo $subscribe_text; ?></p>
			<form class="widget_rss_subscription" action="http://feedburner.google.com/fb/a/mailverify" method="post" target="popupwindow" onsubmit="window.open('http://feedburner.google.com/fb/a/mailverify?uri=<?php echo $feedid ?>', 'popupwindow', 'scrollbars=yes,width=550,height=520');return true">
				<input type="text" placeholder="<?php echo $placeholder ?>" name="email" required />
				<input type="hidden" value="<?php echo $feedid ?>" name="uri"/>
				<input type="hidden" name="loc" value="en_US"/>
				<button type="submit" id="submit" value="Subscribe"><i class="<?php echo $feedbtn ?>"></i></button>
			</form>
		<?php
			echo $after_widget;
		}

	}

}
