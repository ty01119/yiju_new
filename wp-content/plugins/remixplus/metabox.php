<?php

/**
 * Metabox
 * 
 * @author Codevz
 * @link http://codevz.com
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzMetabox {

  public function __construct() {
    $this->id = 'cd_meta';
    $this->title = __('Settings', 'cd');
    add_filter( 'wp_head', array( $this, 'page_custom_css' ), 11 );
    add_filter( 'admin_init', array( $this, 'metabox' ), 99 );
  }

  public function page_custom_css() {
    if ( is_single() || is_page() ) {
      global $post;
      $meta = get_post_meta( $post->ID, 'cd_meta', true );
      $out_css = isset( $meta['outer_bg'] ) ? $this->bg_parser( 'body', $meta['outer_bg'] ) : '';
      $out_css .= isset( $meta['inner_bg'] ) ? $this->bg_parser( '.boxed, .boxed-margin', $meta['inner_bg'] ) : '';
   
      echo $out_css ? '<style type="text/css" media="screen">' . $out_css . '</style>' : '';
    }
  }

  public function bg_parser( $c, $i, $o = '' ) {
    foreach ( (array) $i as $k => $v ) {
      $o .= $v ? 'background-' . $k . ': ' . ( $k === 'image' ? 'url(' : '' ) . $v . ( $k === 'image' ? ')' : '' ) . ';' : '';
    }
    return $o ? $c . ' {' . $o . '}' : '';
  }

  public function metabox( $options ) {

    $options = array();

    /* Post */
    $options[] = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'post',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array( 
        array(
          'name'  => 'post_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-pencil',
          'fields' => array(
            array(
              'id'      => 'artist',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Artist(s)', 'cd'),
              'query_args'  => array(
                'post_type' => 'artists',
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
          ),
        ),
        $this->page_meta()
      )
    );

    $options[] = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'page',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array( $this->page_meta() )
    );

    $options[] = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'product',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array( 
        array(
          'name'  => 'product_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-pencil',
          'fields' => array(
            array(
              'id'      => 'artist',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Artist(s)', 'cd'),
              'query_args'  => array(
                'post_type' => 'artists',
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
          ),
        ),
        $this->page_meta()
      )
    );

    $options[] = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'lyrics',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array( 
        array(
          'name'  => 'lyric_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-pencil',
          'fields' => array(
            array(
              'id'      => 'artist',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Artist(s)', 'cd'),
              'query_args'  => array(
                'post_type' => 'artists',
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
            array(
              'id'      => 'tracks',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Related to', 'cd'),
              'query_args'  => array(
                'post_type' => array( 'songs', 'videos' ),
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
          ),
        ),
        $this->page_meta()
      )
    );

    $options[] = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'artists',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array( 
        array(
          'name'  => 'artist_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-pencil',
          'fields' => array(
            array(
              'id'          => 'fb',
              'type'        => 'text',
              'title'       => __('Facebook page URL', 'cd'),
            ),
            array(
              'id'              => 'social',
              'type'            => 'group',
              'title'           => __('Social icon(s)', 'cd'),
              'button_title'    => __('Add new', 'cd'),
              'accordion_title' => 'title',
              'fields'          => array(
                array(
                  'id'          => 'title',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                ),
                array(
                  'id'          => 'social-icon',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd'),
                ),
                array(
                  'id'          => 'social-link',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'default'     => 'http://',
                ),
              ),
            ),
          ),
        ),
        $this->page_meta()
      )
    );

    $options[]    = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'songs',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array(
        array(
          'name'  => 'song_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-music',
          'fields' => array(
            array(
              'id'      => 'artist',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Artist(s)', 'cd'),
              'query_args'  => array(
                'post_type' => 'artists',
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
            array(
              'id'      => 'player',
              'type'    => 'select',
              'title'   => __('Player', 'cd'),
              'options' => array(
                'rx_player'     => __('Remix player', 'cd'),
                'rx_manager'    => __('360° Circle', 'cd'),
                'custom_player' => __('Custom embed code', 'cd')
              ),
              'radio'   => true,
              'default'  => 'rx_player'
            ),
            array(
              'id'              => 'tracks',
              'type'            => 'group',
              'title'           => __('Track(s)', 'cd'),
              'button_title'    => __('Add track', 'cd'),
              'accordion_title' => 'title',
              'fields'          => array(

                array(
                  'id'          => 'title',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                ),
                array(
                  'id'      => 'type',
                  'type'    => 'select',
                  'title'   => __('Type', 'cd'),
                  'options' => array(
                    'mp3'       => __('MP3', 'cd'),
                    'shoutcast' => __('Shoutcast', 'cd'),
                    //'radionomy' => __('Radionomy', 'cd'),
                    'icecast'   => __('Icecast (Beta)', 'cd'),
                  ),
                  'radio'   => true,
                  'default' => 'mp3'
                ),
                array(
                  'id'          => 'mp3',
                  'type'        => 'upload',
                  'title'       => __('Audio', 'cd'),
                  'attributes'    => array(
                    'placeholder' => __('MP3 or Radio stream URL', 'cd')
                  ),
                  'settings'   => array(
                    'upload_type'  => 'audio/mpeg',
                    'frame_title'  => 'Upload / Select',
                    'insert_title' => 'Insert',
                  ),
                ),
                array(
                  'id'          => 'history',
                  'type'        => 'text',
                  'title'       => __('Radio history URL', 'cd'),
                  'dependency'  => array( 'type', '!=', 'mp3' )
                ),
                array(
                  'id'          => 'poster',
                  'type'        => 'upload',
                  'title'       => __('Cover', 'cd'),
                  'attributes'    => array(
                    'placeholder' => __('Empty = will use featured image', 'cd')
                  ),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_icon_a',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd') . ' 1',
                ),
                array(
                  'id'          => 'buy_title_a',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'buy_icon_a', '!=', '' )
                ),
                array(
                  'id'          => 'buy_link_a',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'buy_icon_a', '!=', '' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_icon_b',
                  'type'        => 'icon',
                  'title'        => __('Icon', 'cd') . ' 2',
                ),
                array(
                  'id'          => 'buy_title_b',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'buy_icon_b', '!=', '' )
                ),
                array(
                  'id'          => 'buy_link_b',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'buy_icon_b', '!=', '' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_icon_c',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd') . ' 3',
                ),
                array(
                  'id'          => 'buy_title_c',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'buy_icon_c', '!=', '' )
                ),
                array(
                  'id'          => 'buy_link_c',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'buy_icon_c', '!=', '' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_icon_d',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd') . ' 4',
                ),
                array(
                  'id'          => 'buy_title_d',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'buy_icon_d', '!=', '' )
                ),
                array(
                  'id'          => 'buy_link_d',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'buy_icon_d', '!=', '' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),

                array(
                  'id'      => 'lyric',
                  'type'    => 'autocomplete',
                  'title'   => __('Lyric', 'cd'),
                  'query_args'  => array(
                    'post_type' => 'lyrics',
                    'orderby'   => 'title',
                    'order'     => 'ASC',
                    'posts_per_page' => 20,
                  )
                ),
                array(
                  'id'          => 'buy_custom',
                  'type'        => 'textarea',
                  'title'       => __('Custom HTML', 'cd'),
                  'sanitize'    => false
                ),
              ),
              'dependency'  => array( 'player', '!=', 'custom_player' )
            ),
            array(
              'id'      => 'embed',
              'type'    => 'textarea',
              'title'   => __('Custom embed code', 'cd'),
              'dependency'  => array( 'player', '==', 'custom_player' ),
              'sanitize'    => false
            ),
            array(
              'id'    => 'player_cover',
              'type'  => 'switcher',
              'title' => __('Cover', 'cd'),
              'default' => true,
              'dependency'  => array( 'player', '!=', 'custom_player' )
            ),
            array(
              'id'    => 'autoplay',
              'type'  => 'switcher',
              'title' => __('Auto play', 'cd'),
              'default' => true,
              'dependency'  => array( 'player', '!=', 'custom_player' )
            ),
            array(
              'id'    => 'repeat',
              'type'  => 'switcher',
              'title' => __('Repeat', 'cd'),
              'default' => true,
              'dependency'  => array( 'player', '!=', 'custom_player' )
            ),
            array(
              'id'    => 'loadringcolor',
              'type'  => 'color_picker',
              'title' => __('loadRingColor', 'cd'),
              'default' => '#ddd',
              'dependency'  => array( 'player', '==', 'rx_manager' )
            ),
            array(
              'id'    => 'playringcolor',
              'type'  => 'color_picker',
              'title' => __('playRingColor', 'cd'),
              'default' => '#ff0078',
              'dependency'  => array( 'player', '==', 'rx_manager' )
            ),
            array(
              'id'    => 'waveformdatacolor',
              'type'  => 'color_picker',
              'title' => __('waveformDataColor', 'cd'),
              'default' => '#ccc',
              'dependency'  => array( 'player', '==', 'rx_manager' )
            ),
            array(
              'id'    => 'eqdatacolor',
              'type'  => 'color_picker',
              'title' => __('eqDataColor', 'cd'),
              'default' => '#7F1949',
              'dependency'  => array( 'player', '==', 'rx_manager' )
            ),
            array(
              'id'    => 'peakdatacolor',
              'type'  => 'color_picker',
              'title' => __('peakDataColor', 'cd'),
              'default' => '#ff33ff',
              'dependency'  => array( 'player', '==', 'rx_manager' )
            ),
          ),
        ),
        $this->page_meta()
      )
    );

    $options[]    = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'podcasts',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array(
        array(
          'name'  => 'podcasts_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-music',
          'fields' => array(
            array(
              'id'      => 'artist',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Artist(s)', 'cd'),
              'query_args'  => array(
                'post_type' => 'artists',
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
            array(
              'id'      => 'player',
              'type'    => 'select',
              'title'   => __('Player', 'cd'),
              'options' => array(
                'rx_player'     => __('Default', 'cd'),
                'rx_manager'    => __('360° Circle', 'cd'),
                'custom_player' => __('Custom embed code', 'cd')
              ),
              'radio'   => true,
              'default'  => 'rx_player'
            ),
            array(
              'id'              => 'tracks',
              'type'            => 'group',
              'title'           => __('Track(s)', 'cd'),
              'button_title'    => __('Add track', 'cd'),
              'accordion_title' => 'title',
              'fields'          => array(

                array(
                  'id'          => 'title',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                ),
                array(
                  'id'          => 'mp3',
                  'type'        => 'upload',
                  'title'       => __('MP3', 'cd'),
                  'attributes'    => array(
                    'placeholder' => __('MP3', 'cd')
                  ),
                  'settings'   => array(
                    'upload_type'  => 'audio/mpeg',
                    'frame_title'  => 'Upload / Select',
                    'insert_title' => 'Insert',
                  ),
                ),
                array(
                  'id'          => 'buy_icon_a',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd') . ' 1',
                ),
                array(
                  'id'          => 'buy_title_a',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'buy_icon_a', '!=', '' )
                ),
                array(
                  'id'          => 'buy_link_a',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'buy_icon_a', '!=', '' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_icon_b',
                  'type'        => 'icon',
                  'title'        => __('Icon', 'cd') . ' 2',
                ),
                array(
                  'id'          => 'buy_title_b',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'buy_icon_b', '!=', '' )
                ),
                array(
                  'id'          => 'buy_link_b',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'buy_icon_b', '!=', '' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_icon_c',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd') . ' 3',
                ),
                array(
                  'id'          => 'buy_title_c',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'buy_icon_c', '!=', '' )
                ),
                array(
                  'id'          => 'buy_link_c',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'buy_icon_c', '!=', '' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_icon_d',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd') . ' 4',
                ),
                array(
                  'id'          => 'buy_title_d',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'buy_icon_d', '!=', '' )
                ),
                array(
                  'id'          => 'buy_link_d',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'buy_icon_d', '!=', '' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_custom',
                  'type'        => 'textarea',
                  'title'       => __('Custom HTML', 'cd'),
                  'sanitize'    => false
                ),
              ),
              'dependency'  => array( 'player', '!=', 'custom_player' )
            ),
            array(
              'id'      => 'embed',
              'type'    => 'textarea',
              'title'   => __('Custom embed code', 'cd'),
              'dependency'  => array( 'player', '==', 'custom_player' ),
              'sanitize'    => false
            ),
            array(
              'id'    => 'player_cover',
              'type'  => 'switcher',
              'title' => __('Cover', 'cd'),
              'default' => true,
              'dependency'  => array( 'player', '!=', 'custom_player' )
            ),
            array(
              'id'    => 'autoplay',
              'type'  => 'switcher',
              'title' => __('Auto play', 'cd'),
              'default' => true,
              'dependency'  => array( 'player', '!=', 'custom_player' )
            ),
            array(
              'id'    => 'repeat',
              'type'  => 'switcher',
              'title' => __('Repeat', 'cd'),
              'default' => true,
              'dependency'  => array( 'player', '!=', 'custom_player' )
            ),
          ),
        ),
        $this->page_meta()
      )
    );

    $options[] = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'playlists',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array(
        array(
          'name'  => 'playlist_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-music',
          'fields' => array(
            array(
              'id'      => 'artist',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Artist(s)', 'cd'),
              'query_args'  => array(
                'post_type' => 'artists',
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
            array(
              'id'              => 'tracks',
              'type'            => 'group',
              'title'           => __('Track(s)', 'cd'),
              'button_title'    => __('Add track', 'cd'),
              'accordion_title' => 'title',
              'fields'          => array(
                array(
                  'id'      => 'track_type',
                  'type'    => 'select',
                  'title'   => __('Type', 'cd'),
                  'options' => array(
                    'old' => __( 'Existed tracks', 'cd' ),
                    'new' => __( 'New', 'cd' )
                  ),
                  'radio'   => true,
                  'default' => 'old'
                ),
                array(
                  'id'      => 'track',
                  'type'    => 'autocomplete',
                  'title'   => __('Track', 'cd'),
                  'query_args'  => array(
                    'post_type' => array( 'songs', 'playlists', 'podcasts' ),
                    'orderby'   => 'title',
                    'order'     => 'ASC',
                    'posts_per_page' => 20,
                  ),
                  'dependency'  => array( 'track_type', '==', 'old' ),
                ),
                array(
                  'id'          => 'title',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'      => 'type',
                  'type'    => 'select',
                  'title'   => __('Type', 'cd'),
                  'options' => array(
                    'mp3'       => __('MP3', 'cd'),
                    'shoutcast' => __('Shoutcast', 'cd'),
                    //'radionomy' => __('Radionomy', 'cd'),
                    'icecast'   => __('Icecast (Beta)', 'cd'),
                  ),
                  'radio'   => true,
                  'default' => 'mp3',
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'mp3',
                  'type'        => 'upload',
                  'title'       => __('Audio', 'cd'),
                  'attributes'    => array(
                    'placeholder' => __('MP3 or Radio stream URL', 'cd')
                  ),
                  'settings'   => array(
                    'upload_type'  => 'audio/mpeg',
                    'frame_title'  => 'Upload / Select',
                    'insert_title' => 'Insert',
                  ),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'history',
                  'type'        => 'text',
                  'title'       => __('Radio history URL', 'cd'),
                  'dependency'  => array( 'type', '!=', 'mp3' )
                ),
                array(
                  'id'          => 'poster',
                  'type'        => 'image',
                  'title'       => __('Cover', 'cd'),
                  'attributes'    => array(
                    'placeholder' => __('Empty = will use featured image', 'cd')
                  ),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_icon_a',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd') . ' 1',
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_title_a',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_link_a',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_icon_b',
                  'type'        => 'icon',
                  'title'        => __('Icon', 'cd') . ' 2',
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_title_b',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_link_b',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_icon_c',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd') . ' 3',
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_title_c',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_link_c',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'          => 'buy_icon_d',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd') . ' 4',
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_title_d',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd'),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_link_d',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
                array(
                  'id'      => 'lyric',
                  'type'    => 'autocomplete',
                  'title'   => __('Lyric', 'cd'),
                  'query_args'  => array(
                    'post_type' => 'lyrics',
                    'orderby'   => 'title',
                    'order'     => 'ASC',
                    'posts_per_page' => 20,
                  ),
                  'dependency'  => array( 'track_type', '==', 'new' ),
                ),
                array(
                  'id'          => 'buy_custom',
                  'type'        => 'textarea',
                  'dependency'  => array( 'track_type', '==', 'new' ),
                  'title'       => __('Custom HTML', 'cd'),
                  'sanitize'    => false
                ),
              ),
            ),
            array(
              'id'    => 'player_cover',
              'type'  => 'switcher',
              'title' => __('Cover', 'cd'),
              'default' => true
            ),
            array(
              'id'    => 'autoplay',
              'type'  => 'switcher',
              'title' => __('Auto play', 'cd'),
              'default' => true
            ),
            array(
              'id'    => 'repeat',
              'type'  => 'switcher',
              'title' => __('Repeat', 'cd'),
              'default' => true
            ),
            array(
              'id'    => 'album_title',
              'type'  => 'text',
              'title' => __('Buy album', 'cd')
            ),
            array(
              'id'              => 'album_buy',
              'type'            => 'group',
              'title'           => __('Buy album links', 'cd'),
              'button_title'    => __('Add new', 'cd'),
              'accordion_title' => 'title',
              'fields'          => array(
                array(
                  'id'          => 'title',
                  'type'        => 'text',
                  'title'       => __('Title', 'cd')
                ),
                array(
                  'id'          => 'icon',
                  'type'        => 'icon',
                  'title'       => __('Icon', 'cd')
                ),
                array(
                  'id'          => 'link',
                  'type'        => 'text',
                  'title'       => __('Link', 'cd'),
                  'attributes'    => array(
                    'placeholder' => 'http://'
                  ),
                ),
              ),
            ),

          ),
        ),
        $this->page_meta()
      )
    );

    $options[] = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'gallery',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array(
        array(
          'name'  => 'gallery_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-image',
          'fields' => array(
            array(
              'id'      => 'artist',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Artist(s)', 'cd'),
              'query_args'  => array(
                'post_type' => 'artists',
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
            array(
              'id'    => 'gallery',
              'type'  => 'gallery',
              'title' => __('Gallery', 'cd'),
            ),
          ),
        ),
        $this->page_meta()
      )
    );

    $options[]    = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'events',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array(
        array(
          'name'  => 'event_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-calendar',
          'fields' => array(
            array(
              'id'      => 'artist',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Artist(s)', 'cd'),
              'query_args'  => array(
                'post_type' => 'artists',
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
            array(
              'id'    => 'event_date',
              'type'  => 'DateTimePicker',
              'title' => __('Start Date/Time', 'cd'),
            ),
            array(
              'id'    => 'end_event_date',
              'type'  => 'DateTimePicker',
              'title' => __('End Date/Time', 'cd')
            ),
            array(
              'id'    => 'venue',
              'type'  => 'text',
              'title' => __('Venue', 'cd')
            ),
            array(
              'id'    => 'buy_title',
              'type'  => 'text',
              'title' => __('Buy title', 'cd'),
              'attributes' => array(
                'placeholder' => 'Buy Ticket $10'
              )
            ),
            array(
              'id'    => 'buy_link',
              'type'  => 'text',
              'title' => __('Buy link', 'cd'),
              'attributes' => array(
                'placeholder' => 'http://example.com/'
              )
            ),
            array(
              'id'      => 'class',
              'type'    => 'select',
              'title'   => __('Type', 'cd'),
              'options' => array(
                '1' => __('Is Open', 'cd'),
                'free_event'      => __('Free Entry', 'cd'),
                'cancelled_event' => __('Cancelled', 'cd'),
                'sold_out_event'  => __('Sold Out', 'cd')
              ),
              'radio'   => true,
              'default' => '1'
            ),
          ),
        ),
        $this->page_meta()
      )
    );


    $options[]    = array(
      'id'        => $this->id,
      'title'     => $this->title,
      'post_type' => 'videos',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array(
        array(
          'name'  => 'video_settings',
          'title' => $this->title,
          'icon'  => 'fa fa-play',
          'fields' => array(
            array(
              'id'      => 'artist',
              'class'   => 'multiple',
              'type'    => 'autocomplete',
              'title'   => __('Artist(s)', 'cd'),
              'query_args'  => array(
                'post_type' => 'artists',
                'orderby'   => 'title',
                'order'     => 'ASC',
                'posts_per_page' => 20,
              )
            ),
            array(
              'id'      => 'video_type',
              'type'    => 'select',
              'title'   => __('Type', 'cd'),
              'options' => array(
                'youtube' => __('Youtube', 'cd'),
                'vimeo'   => __('Vimeo', 'cd'),
                'mp4'     => __('Self hosted MP4', 'cd'),
                'custom'  => __('Custom embed code', 'cd')
              ),
              'radio'   => true,
              'default' => 'youtube'
            ),
            array(
              'id'    => 'youtube',
              'type'  => 'text',
              'title' => __('Youtube ID', 'cd'),
              'dependency'  => array( 'video_type', '==', 'youtube' ),
              'attributes'    => array(
                'placeholder' => '5T_WZ1-67fU'
              ),
            ),
            array(
              'id'    => 'vimeo',
              'type'  => 'text',
              'title' => __('Vimeo ID', 'cd'),
              'dependency'  => array( 'video_type', '==', 'vimeo' ),
              'attributes'    => array(
                'placeholder' => '60892469'
              ),
            ),
            array(
              'id'    => 'mp4',
              'type'  => 'upload',
              'title' => __('Self hosted MP4', 'cd'),
              'dependency'  => array( 'video_type', '==', 'mp4' ),
              'settings'   => array(
                'upload_type'  => 'video/mp4',
                'frame_title'  => 'Upload / Select',
                'insert_title' => 'Insert',
              ),
            ),
            array(
              'id'    => 'embed',
              'type'  => 'textarea',
              'title' => __('Custom embed code', 'cd'),
              'dependency'  => array( 'video_type', '==', 'custom' ),
              'sanitize'    => false
            ),
            array(
              'id'    => 'lightbox',
              'type'  => 'switcher',
              'title' => __('Lightbox', 'cd'),
              'default' => true,
              'dependency'  => array( 'video_type', '!=', 'custom' )
            ),
          ),
        ),
        $this->page_meta()
      )
    );

    if ( class_exists('CSF_Metabox') ) {
      CSF_Metabox::instance( $options );
    }
  }

  public function page_meta() {
    $o = array(
      'name'    => 'page_settings',
      'title'   => __( 'Page settings', 'cd' ),
      'icon'    => 'fa fa-cog',
      'fields'  => array(
        array(
          'id'    => 'cover',
          'type'  => 'select',
          'title' => __( 'Page cover', 'cd' ),
          'options' => array(
            '1' => __( 'Default', 'cd' ),
            '2' => __( 'None', 'cd' ),
            '3' => __( 'Image', 'cd' ),
            '4' => __( 'RevSlider', 'cd' ),
            '5' => __( 'MasterSlider', 'cd' ),
          ),
          'radio'   => true,
          'default' => '1',
        ),
        array(
          'id'    => 'img',
          'type'  => 'upload',
          'title' => __('Image', 'cd'),
          'dependency'  => array( 'cover', '==', '3' )
        ),
        array(
          'id'    => 'rev',
          'type'  => 'select',
          'title' => __('RevSlider', 'cd'),
          'options' => $this->rev(),
          'dependency'  => array( 'cover', '==', '4' )
        ),
        array(
          'id'    => 'master',
          'type'  => 'select',
          'title' => __('MasterSlider', 'cd'),
          'options' => $this->master(),
          'dependency'  => array( 'cover', '==', '5' )
        ),
        array(
          'id'    => 'cover_position',
          'type'    => 'switcher',
          'title'   => __('Cover after header', 'cd'),
          'default' => false,
          'dependency'=> array( 'cover', 'any', '3,4,5,6' )
        ),
        array(
          'id'          => 'layout',
          'type'        => 'image_select',
          'title'       => __('Layout', 'cd'),
          'options'     => array(
            'inherit'         => CD_URI .'/img/admin/default.png',
            'without-sidebar' => CD_URI .'/img/admin/full.png',
            'sidebar-right'   => CD_URI .'/img/admin/right.png',
            'sidebar-left'    => CD_URI .'/img/admin/left.png',
            'both-sidebar'    => CD_URI .'/img/admin/both-sides.png',
            'both-sidebar-right'=> CD_URI .'/img/admin/both-right.png',
            'both-sidebar-left' => CD_URI .'/img/admin/both-left.png',
          ),
          'attributes' => array(
            'data-depend-id' => 'layout'
          ),
          'radio'        => true,
          'default'      => 'inherit'
        ),
        array(
          'id'    => 'primary',
          'type'  => 'select',
          'title' => __('Primary Sidebar', 'cd'),
          'options' => $this->sidebars(),
          'default'      => 'primary',
          'dependency' => array( 'layout', 'any', 'sidebar-right,sidebar-left,both-sidebar,both-sidebar-left,both-sidebar-right' )
        ),
        array(
          'id'    => 'secondary',
          'type'  => 'select',
          'title' => __('Secondary Sidebar', 'cd'),
          'options' => $this->sidebars(),
          'default'      => 'secondary',
          'dependency' => array( 'layout', 'any', 'both-sidebar,both-sidebar-left,both-sidebar-right' )
        ),
        array(
          'id'      => 'hiding_elms',
          'type'    => 'checkbox',
          'title'   => __( 'Hiding', 'cd' ),
          'options' => array(
            'title'       => __('Title', 'cd'),
            'header'      => __('Header', 'cd'),
            'footer'      => __('Footer', 'cd'),
            'breadcrumbs' => __('Breadcrumbs', 'cd'),
          )
        ),
        array(
          'id'      => 'show_alphabet',
          'type'    => 'switcher',
          'title'   => __('Alphabet list', 'cd'),
          'default' => false
        ),
        array(
          'id'      => 'outer_bg',
          'type'    => 'background',
          'title'   => __('Body background', 'cd')
        ),
        'inner' => array(
          'id'      => 'inner_bg',
          'type'    => 'background',
          'title'   => __('Layout background', 'cd')
        ),
      )
    );

    return $o;
  }

  public function sidebars() {
    $options = array();
    $sidebars = $GLOBALS['wp_registered_sidebars'];
    if( $sidebars ) {
      foreach ( $sidebars as $sidebar ) {
        $options[$sidebar['id']] = $sidebar['name'];
      }
    } else {
      $options = array( '-' );
    }

    return $options;
  }

  public function master() {
    if ( class_exists( 'Master_Slider' ) ) {
      global $mspdb;
      $ms = $mspdb->get_sliders_list( $limit = 0, $offset  = 0, $orderby = 'ID', $sort = 'ASC' );
      if( $ms ) {
        foreach ( $ms as $slider ) {
          $options[$slider['ID']] = esc_attr( $slider['title'] );
        }
      }
      if( empty( $options ) ) {
        $options = array( __('Not found, Please create new from MasterSlider menu', 'cd') );
      }
    } else {
      $options = array( __('Sorry! MasterSlider is not Installed or Activated', 'cd') );
    }

    return $options;
  }

  public function rev() {
    if ( class_exists( 'RevSliderAdmin' ) ) {
      $slider  = new RevSlider();
      $options = $slider->getArrSlidersShort();
      if( empty( $options ) ) {
        $options = array( __('Not found, Please create new from Revolution Slider menu', 'cd') );
      }
    } else {
      $options = array( __('Sorry! Revolution Slider is not Installed or Activated', 'cd') );
    }

    return $options;
  }

}

new CodevzMetabox;