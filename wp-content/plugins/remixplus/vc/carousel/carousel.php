<?php

/**
 * @author Codevz
 * @link http://Codevz.com
 *
 * Carousel add-one for visual composer
 * Shortcode name: codevz_carousel
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzCarousel extends CodevzVc {

	function __construct() {
		add_action( 'init', array( $this, 'in' ) );
		add_shortcode( 'codevz_carousel', array( $this, 'out' ) );
	}

	function in() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'codevz_carousel',
			'name'			=> __('Carousel', 'cd'), 
			'icon'			=> 'dashicons-before dashicons-leftright cd_ico',
			'params'		=> array(
				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Title', 'cd'),
					'param_name' 	=> 'section_title',
					'edit_field_class'=> $this->col('6'),
					'admin_label' 	=> true
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Class', 'cd'),
					'param_name'	=> 'extra_class',
					'edit_field_class'=> $this->col('6'),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Slides to show', 'cd'),
					'param_name'	=> 'slidestoshow',
					'edit_field_class'=> $this->col('6'),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Slides to scroll', 'cd'),
					'param_name'	=> 'slidestoscroll',
					'edit_field_class'=> $this->col('6'),
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Infinite', 'cd'),
					'param_name' 	=> 'infinite',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'False', 'cd' ) => 'false',
						__( 'True', 'cd' ) => 'true',
					)
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Speed (ms)', 'cd'),
					'param_name'	=> 'speed',
					'edit_field_class'=> $this->col('6'),
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Center mode', 'cd'),
					'param_name' 	=> 'centermode',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'False', 'cd' ) => 'false',
						__( 'True', 'cd' ) => 'true',
					)
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Center padding (eg. 40px)', 'cd'),
					'param_name'	=> 'centerpadding',
					'edit_field_class'=> $this->col('6'),
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Variable width', 'cd'),
					'param_name' 	=> 'variablewidth',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'False', 'cd' ) => 'false',
						__( 'True', 'cd' ) => 'true',
					)
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Auto play', 'cd'),
					'param_name' 	=> 'autoplay',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'False', 'cd' ) => 'false',
						__( 'True', 'cd' ) => 'true',
					)
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Autoplay speed (ms)', 'cd'),
					'param_name'	=> 'autoplayspeed',
					'edit_field_class'=> $this->col('6'),
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Arrows', 'cd'),
					'param_name' 	=> 'arrows',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'Top', 'cd' ) => 'top',
						__( 'Bottom center', 'cd' ) => 'bottom',
						__( 'Both side inner', 'cd' ) => 'both_inner',
						__( 'Both side outer', 'cd' ) => 'both_outer',
						__( 'Hide', 'cd' ) => 'hide',
					)
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Available images size', 'cd'),
					'param_name' 	=> 'image_size',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						'280, 255' 	=> 'medium_post',
						'1000, 1000' => 'custom_carousel',
						'500, 500' 	=> 'cover',
						'180, 180' 	=> 'player_cover',
						'540, 9999' => 'masonry',
						'9999, 500' => 'free_width',
						'85, 85' 	=> 'tumb',
					)
				),
				array(
					'type' => 'param_group',
					'heading' => __( 'Custom images', 'cd' ),
					'description' => __( 'If adding images, post types will not works', 'cd' ),
					'edit_field_class'=> $this->col('12'),
					'param_name' => 'custom_images',
					'params' => array(
						array(
							'type' => 'attach_image',
							'heading' => __( 'Image', 'cd' ),
							'param_name' => 'image'
						),
						array(
							'type' => 'textfield',
							'heading' => __( 'URL', 'cd' ),
							'param_name' => 'url'
						),
					),
					'callbacks' => array(
						'after_add' => 'vcChartParamAfterAddCallback',
					),
				),
				array(
					'type'			=> 'checkbox',
					'heading'		=> __('Outside of block', 'cd'),
					'param_name'	=> 'outside_block',
					'edit_field_class'=> $this->col('12'),
				),

				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Posts count', 'cd'),
					'param_name' 	=> 'posts_per_page',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'posttypes',
					'heading'		=> __('Post type(s)', 'cd'),
					'param_name'	=> 'post_type',
					'edit_field_class'=> $this->col('12'),
					'group'			=> $this->WP_Query()
				), 
				array(
					"type"			=> "dropdown",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading"		=> __("Order", "cd"),
					"param_name"	=> "order",
					"value"			=> array(
						__("Descending", "cd") => 'DESC',
						__("Ascending", "cd") => 'ASC',
					)
				), 
				array(
					"type"			=> "dropdown",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading"		=> __("Orderby", "cd"),
					"param_name"	=> "orderby",
					"value"			=> array(
						__("Date", "cd")	=> 'date',
						__("ID", "cd")		=> 'ID',
						__("Random", "cd") => 'rand',
						__("Author", "cd") => 'author',
						__("Title", "cd")	=> 'title',
						__("Name", "cd")	=> 'name',
						__("Type", "cd")	=> 'type',
						__("Modified", "cd") => 'modified',
						__("Parent ID", "cd") => 'parent',
						__("Comment Count", "cd") => 'comment_count',
					)
				),
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Post parent", "cd"),
					"param_name" 	=> "post_parent"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Offset", "cd"),
					"param_name" 	=> "offset"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Year", "cd"),
					"param_name" 	=> "year"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Month num", "cd"),
					"param_name" 	=> "monthnum"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Day", "cd"),
					"param_name" 	=> "day"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Meta key", "cd"),
					"param_name" 	=> "meta_key"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Meta value", "cd"),
					"param_name" 	=> "meta_value"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Meta compare", "cd"),
					"param_name" 	=> "meta_compare"
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Posts category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> '_cats',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Tags', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> '_tags',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Artists alphabet', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'alphabet',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Artists category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'artists_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Songs category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'songs_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Playlists category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'playlists_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Podcasts category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'podcasts_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Lyrics category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'lyrics_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Videos category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'videos_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Events category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'events_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Gallery category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'gallery_cat',
					'group'			=> $this->WP_Query()
				),
			)

		));
	}

	function out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'section_title' => '',
			'by'			=> 'custom',
			'custom_images'	=> '',
			'outside_block'	=> '',
			'post_type'		=> '',
			'posts_per_page'=> '',
			'extra_class' 	=> '',
			'post_parent' 	=> '',
			'offset' 		=> '',
			'order' 		=> '',
			'orderby' 		=> '',
			'year' 			=> '',
			'monthnum' 		=> '',
			'day' 			=> '',
			'meta_key' 		=> '',
			'meta_value' 	=> '',
			'meta_compare' 	=> '',
			'_cats'			=> '',
			'_tags' 		=> '',
			'alphabet' 		=> '',
			'artists_cat' 	=> '',
			'songs_cat' 	=> '',
			'playlists_cat' => '',
			'podcasts_cat'  => '',
			'lyrics_cat' 	=> '',
			'videos_cat' 	=> '',
			'gallery_cat' 	=> '',
			'events_cat' 	=> '',
			'slidestoshow' 	=> '',
			'slidestoscroll' => '',
			'infinite' 		=> '',
			'speed' 		=> '',
			'centermode' 	=> '',
			'centerpadding' => '',
			'variablewidth' => '',
			'autoplay' 		=> '',
			'autoplayspeed' => '',
			'arrows' 		=> 'top',
			'image_size' 	=> 'medium_post',
		), $atts );

		global $codevz;

		if ( $codevz ) {
			ob_start(); 

			if ( isset( $_GET['vc_editable'] ) ) { ?>
				<script type='text/javascript'>
					/* <![CDATA[ */
						codevzCarousel();
					/* ]]> */
				</script>
			<?php }

			$imgs = $atts['custom_images'];
			if ( $imgs && $imgs !== '%5B%5D' && $imgs !== '%5B%7B%7D%5D' ) {

				$settings = array(
					'slidestoshow'		=> $atts['slidestoshow'], 
					'slidestoscroll'	=> $atts['slidestoscroll'], 
					'infinite'			=> $atts['infinite'] ? true : false, 
					'speed'				=> $atts['speed'], 
					'centermode'		=> $atts['centermode'] ? true : false, 
					'centerpadding'		=> $atts['centerpadding'], 
					'variablewidth'		=> $atts['variablewidth'] ? true : false, 
					'autoplay'			=> $atts['autoplay'] ? true : false, 
					'autoplayspeed'		=> $atts['autoplayspeed'], 
				);

				$class = $atts['outside_block'] ? ' def-block ' : '';
				$class .= $atts['centermode'] ? ' is_center ' : '';
				$class .= $args['section_title'] ? '' : ' without_title ';
				$class .= $atts['extra_class'] ? $atts['extra_class'] : '';
				echo '<section class="mbf clr' . $class . '">';
				echo $atts['section_title'] ? '<h4 class="tt">' . $atts['section_title'] . '</h4><span class="liner"></span>' : '';
				echo '<div class="carousel" data-carousel=\'' . json_encode( array_filter( $settings ) ) . '\'>';

				$images = (array) vc_param_group_parse_atts( $imgs );
				foreach ( $images as $img ) {
					$size = ( $atts['image_size'] === 'medium_post' && $atts['variablewidth'] ) ? 'free_width' : $atts['image_size'];
					$url = empty( $img['url'] ) ? wp_get_attachment_image_src( $img['image'], 'large' ) : $img['url'];
					$url = empty( $url[0] ) ? $img['url'] : $url[0];
					echo '<div class="item">';
					echo '<a class="cdEffect" href="' . $url . '">';
					echo wp_get_attachment_image( $img['image'], $size );
					echo '<i class="fa fa-plus"></i>';
					echo '<h3>' . $codevz->get_attachment( 'title', $img['image'] ) . '<small>' . $codevz->get_attachment( 'caption', $img['image'] ) . '</small></h3>';
					echo '</a>';
					echo '</div>';
				}
				echo '</div>';
				echo '</section>';
			} else {
				echo '<div class="' . $atts['extra_class'] . '">';
				$atts['post_type'] = $atts['post_type'] ? explode( ',', $atts['post_type'] ) : '';
				$codevz->related( array_filter( $atts ) );
				echo '</div>';	
			}

			
			return ob_get_clean();
		} else {
			return 'This theme do not support this shortcode.';
		}
	}
}

new CodevzCarousel();
