<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( ! class_exists( 'CodevzAds' ) ) {
	
	class CodevzAds extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__('CD - Ads', 'remix'), 
				array('classname' => 'cd_ads')
			);	
		}
		
		function widget( $args, $instance ) {

			extract( $args );
			$title = apply_filters('widget_title', $instance['title'] );
			$out = $before_widget."\n";
			$out .= $title ? $before_title.$title.$after_title : '';
			$out .= '<a href="'.$instance['link'].'" target="_blank" title="'.$title.'"><img src="'.$instance['img'].'" alt="'.$title.'" width="auto" height="auto" /></a>';
			$out .= $instance['custom'];
			$out .= $after_widget."\n";

			echo $out;
		}

		public function update($new,$old) {

			$instance = $old;
			$instance['title'] = strip_tags( $new['title'] );
			$instance['img'] = strip_tags( $new['img'] );
			$instance['link'] = strip_tags( $new['link'] );
			$instance['custom'] = $new['custom'];

			return $instance;
		}
		 
		public function form($instance) {

			$defaults = array('title' => '','link' => '','img' => '', 'custom' => '');
			$instance = wp_parse_args( (array) $instance, $defaults );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			), esc_attr( $instance['title'] ) ); 

			echo csf_add_field( array(
				'id'    => $this->get_field_name('img'),
				'name'  => $this->get_field_name('img'),
				'type'  => 'upload',
				'title' => esc_html__('Image', 'remix')
			), esc_attr( $instance['img'] ) );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('link'),
				'name'  => $this->get_field_name('link'),
				'type'  => 'text',
				'title' => esc_html__('Link', 'remix')
			), esc_attr( $instance['link'] ) );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('custom'),
				'name'  => $this->get_field_name('custom'),
				'type'  => 'textarea',
				'sanitize' => false,
				'title' => esc_html__('Custom Ads', 'remix')
			), $instance['custom'] );

		}

	}
	
	add_action( 'widgets_init', function() {
		register_widget('CodevzAds');
	});

}
