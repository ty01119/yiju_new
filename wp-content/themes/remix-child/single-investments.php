<?php 

/**
 * Single page
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

	get_header();
	global $codevz;
	$codevz->content_before();
	$meta = $codevz->meta();
	$cpt = get_post_type();
	$show = array_flip( $codevz->option( $cpt . '_meta', array() ) );
	$related_grid = $codevz->option( $cpt . '_related_grid' ) ? 'related_grid' : '';
?>

<article id="<?php echo get_the_ID(); ?>" <?php post_class( 'mbf clr def-block' ); ?>>
	<?php 
		/* Content */
		while ( have_posts() ): the_post();		
	?>
	<div class="vc_row">
		<div class="single-investment-content-row">
			<div class="vc_row">
				<div class="image-slider">
					<div class="ms-slider seq" id="image-slider">
						<div class="seq-screen">
							<ul class="seq-canvas">
								<?php 
									$singleInvestmentImages = get_field("single-investment-images");
									foreach ($singleInvestmentImages as $singleInvestmentImage) {
										echo "<li>";
											echo "<div class=\"slider-thumbnail\">";
												echo  "<img src=\"" . $singleInvestmentImage['single-investment-image'] . "\">";
											echo "</div>";
										echo "</li>";
									}
								?>
							</ul>	
						</div>
					</div>
				</div>
			</div>
			<div class="vc_row">
				<div class="grid_8 single-investment-content">
					<p class="title"><strong><?php the_title() ?></strong></p>
					<?php 
						if(get_field("single-investment-description")){
							echo "<p class=\"color-orange single-investment-label single-investment-intro-label\"><strong>项目介绍：</strong></p>";
							echo get_field("single-investment-description");
						}

						if(get_field("single-investment-description-image")){
							echo "<p><img src=\"" . get_field("single-investment-description-image") . "\" ></p>";
						}

						if(get_field("single-investment-advantage")){
							echo "<p class=\"color-orange single-investment-label single-investment-advantage-label\"><strong>项目优势：</strong></p>";
							echo get_field("single-investment-advantage");
						}

						if(get_field("project_address")){
							echo "<p><span class=\"color-orange single-investment-label single-investment-intro-label\"><strong>地址：</strong></span>" . get_field("project_address") . "</p>";
							echo "<p><iframe width=\"100%\" height=\"400\" frameborder=\"0\" style=\"border:0\" src=\"https://www.google.com/maps/embed/v1/place?key=AIzaSyBMwzNak7PXmHgJOGPSVwSlr50N6WK1Cvg&q=".str_replace(",+" , "," , str_replace(" ", "+",  get_field("project_address")))."\" allowfullscreen></iframe></p>";
						}

						if(get_field("single-investment-comment")){
							echo "<p class=\"color-orange single-investment-label single-investment-comment-label\"><strong>项目介绍：</strong></p>";
							echo get_field("single-investment-comment");
						}
					?>
				</div>
				<div class="grid_4 single-investment-sidebar">
					<div class="vc_row">
						<?php
						if(get_field("project_doc")){
						?>
						<div class="single-investment-contact">
							<div class="label-wrap">
								<p class="color-white contact-label"><strong>资料下载</strong></p>
							</div>
							<div class="contact-content">
							<?php foreach (get_field("project_doc") as $doc) {
								echo "<div class=\"single-content\">";
									if(!empty($doc["image"])){
										echo "<div class=\"agent-image\">";
											echo "<img src=\"" . $doc["image"] . "\">";
										echo "</div>";
									}
									if(!empty($doc["title"])){
										echo "<div class=\"agent-title\">";
											echo "<p><a href=\"" . $doc["file"] . "\" target=_blank >" . $doc["title"] . "</a></p>";
										echo "</div>";
									}
								echo "</div>";

							}
							?>
							</div>
						<?php } ?>
						</div>
					</div>
					<div class="clearfix-sm"></div>
					<div class="vc_row">
						<?php
						if(get_field("project_agent")){
						?>
						<div class="single-investment-contact">
							<div class="label-wrap">
								<p class="color-white contact-label"><strong>联系方式</strong></p>
							</div>
							<div class="contact-content">
							<?php foreach (get_field("project_agent") as $agent) {
								echo "<div class=\"single-content\">";
									if(!empty($agent["photo"])){
										echo "<div class=\"agent-image\">";
											echo "<img src=\"" . $agent["photo"] . "\">";
										echo "</div>";
									}
									if(!empty($agent["name"])){
										echo "<div class=\"agent-name\">";
											echo "<p>" . $agent["name"] . "</p>";
										echo "</div>";
									}
									if(!empty($agent["mobile"])){
										echo "<div class=\"agent-mobile\">";
											echo "<p>手机：" . $agent["mobile"] . "</p>";
										echo "</div>";
									}
									if(!empty($agent["email"])){
										echo "<div class=\"agent-email\">";
											echo "<p>邮箱：" . $agent["email"] . "</p>";
										echo "</div>";
									}
								echo "</div>";

							}
							?>
							</div>
						<?php } ?>
						</div>
					</div>
					
				</div>
			</div>
			<div class="vc_row">
				<div class="clearfix">
					<hr class="hr-home hr-home-interest">
				</div>
				<div class="vc_row">
					<?php
						echo do_shortcode('[loadRandomPost limit=3 is_first_time=0 position="project_image" post_type="investments" begin_col="12" num_per_row="3" show_position="null"]');
					?>
				</div>
			</div>
		</div>
	</div>
	<?php 
		endwhile;
	?>
	
</article>

<script type="text/javascript">
	jQuery(document).ready(function(){
		var sequenceElement = document.getElementById("image-slider");
		var options = {
		  animateCanvas: false,
		  phaseThreshold: false,
		  reverseWhenNavigatingBackwards: true,
		  autoPlay: true
		}
		var mySequence = sequence(sequenceElement, options);
	});
</script>
<?php 


	/* After all */
	$codevz->comments();
	$codevz->content_after();

	get_sidebar();
	get_footer(); 
