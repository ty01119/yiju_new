<?php 

/**
 * Footer
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

	global $codevz, $sfoot;
?>
				</div><!-- /row -->
			</div><!-- /page_content -->
		</div><!-- /#page_content -->
	</div><!-- /row -->
<?php 
	$codevz->hook( 'before_footer' );

	if ( $sfoot ) { ?>
		<footer id="footer">
			<?php 
				$total = (int) $codevz->option( 'footer' );
				if ( $total === 1 ) {
					$class = 'grid_12';
				} else if ( $total === 2 ) {
					$class = 'grid_6';
				} else if ( $total === 3 ) {
					$class = 'grid_4';
				} else {
					$class = 'grid_3';
				}
	
				if ( is_active_sidebar( 'footer-1' ) && $total > 0 ) {
			?>
			<div class="row mbf clr">
				<?php $i = 0; while ( $i < $total ) { $i++; ?>
					<?php if ( is_active_sidebar( 'footer-' . $i ) ) { ?>
						<div class="<?php if ( $i === $total ) { echo 'omega '; } ?><?php if ( $i === 1 ) { echo 'alpha '; } ?><?php echo $class; ?> footer_w">
							<?php dynamic_sidebar( 'footer-' . $i ); ?>
						</div>
					<?php } ?>
				<?php } ?>
			</div><!-- /row -->
			<?php }

			$codevz->hook( 'between_footer' );

			if ( $codevz->option( 'subfooter' ) ) { ?>
			<div class="footer-last">
				<div class="row clr">
					<span class="copyright"><?php echo $codevz->option( 'copyright' ); ?></span>

					<?php echo $codevz->option( 'totop' ) ? '<div id="toTop"><i class="fa fa-angle-up"></i></div>' : ''; ?>

					<div class="foot-menu">
						<nav><?php $codevz->menu( 'footer', 0 ); ?></nav>
					</div>
				</div><!-- /row -->
			</div><!-- /last-footer -->
			<?php } ?>
		</footer><!-- /footer -->
	<?php } $codevz->hook( 'after_footer' ); ?>
	</div><!-- /layout -->
<?php wp_footer();?>
</body>
</html>