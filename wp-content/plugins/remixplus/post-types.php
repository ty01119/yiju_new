<?php

/**
 * Post Types
 * 
 * @author Codevz
 * @link http://codevz.com
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class Codevz_CPT {

	public function __construct() {

		add_action( 'admin_head', array( $this, 'admin_head' ) );

		$this->types = array(
			'artists'	=>	'dashicons-admin-users', 
			'songs'		=>	'dashicons-format-audio', 
			'playlists'	=>	'dashicons-playlist-audio',
			'podcasts'	=>	'dashicons-microphone',
			'lyrics'	=>	'dashicons-welcome-write-blog', 
			'videos'	=>	'dashicons-video-alt3', 
			'events'	=>	'dashicons-calendar', 
			'gallery'	=>	'dashicons-format-gallery'
		);

		/* Register post types */
		add_action( 'init', array( $this, 'register' ), 0 );

		/* Manage CPT columns action */
		add_action( 'manage_posts_custom_column', array( $this, 'manage_columns' ), 10, 2 );

		/* Manage CPT columns filter */
		foreach ( $this->types as $name => $i ) {
			$columns = array();
			add_filter( "manage_edit-".$name."_columns", function () use ( $columns ) {
				global $typenow;
				$i = array();
				$i['cb']	= 'cb-select-all-1';
				$i['cover']	= __( 'Cover', 'cd' );
				$i['title']	= __( 'Title', 'cd' );

				if ( $typenow !== 'artists' ) {
					$i['cd_artist'] = __( 'Artist', 'cd' );
				}

				if ( $typenow === 'artists' ) {
					$i['cd_items'] = __( 'Items', 'cd' );
					$i['taxonomy-artist'] = __( 'Under', 'cd' );
				}
				if ( $typenow === 'events' ) {
					$i['cd_event_date'] = __( 'Start', 'cd' );
					$i['cd_end_event_date'] = __( 'End', 'cd' );
					$i['cd_venue'] = __( 'Venue', 'cd' );
				}
				if ( $typenow === 'songs' ) {
					$i['cd_plays'] 	= __( 'Plays', 'cd' );
					$i['cd_downloads'] = __( 'Downloads', 'cd' );
				}
				if ( $typenow === 'videos' ) {
					$i['cd_views'] = __( 'Views', 'cd' );
				}

				$i['cd_likes'] = __( 'Likes', 'cd' );
				$i['cd_dislikes'] = __( 'Dislikes', 'cd' );
				$i['comments'] = __( 'CM', 'cd' );
				$i['author'] = __( 'By', 'cd' );
				if ( $typenow === 'post' ) {
					$i['categories'] = __( 'Categories', 'cd' );
				}
				$i['date'] = __( 'Date', 'cd' );

				$columns = $i;
				return $columns;
			});
		}

		/* Sortable cols */
		$this->sortable = array( 'cd_event_date', 'cd_end_event_date', 'cd_plays', 'cd_likes', 'cd_dislikes', 'cd_downloads', 'cd_views' );

		foreach ( $this->types as $name => $i ) {
			$columns = array();
			add_filter( "manage_edit-".$name."_sortable_columns", function () use ( $columns ) {
				foreach ( $this->sortable as $i ) {
					$columns[$i] = $i;
				}

				return $columns;
			});
		}

		add_action( 'load-edit.php', array( $this, 'sorting_request' ) );

	}

	public function admin_head() {
		echo '<style>
			.vc_license-activation-notice, 
			.eg-update-notice-wrap, 
			.rs-update-notice-wrap {
			    position: absolute;
			    height: 1px !important;
			    left: -10000px !important
			}
			.dashicons-format-audio:before, 
			.dashicons-playlist-audio:before, 
			.dashicons-microphone:before, 
			.dashicons-welcome-write-blog:before, 
			.menu-icon-artists .dashicons-admin-users:before, 
			.dashicons-video-alt3:before, 
			.dashicons-format-gallery:before, 
			.dashicons-calendar:before {
			    color: #2ea2cc !important
			}
			.wp-has-current-submenu .dashicons-format-audio:before, 
			.wp-has-current-submenu .dashicons-playlist-audio:before, 
			.wp-has-current-submenu .dashicons-microphone:before, 
			.wp-has-current-submenu .dashicons-welcome-write-blog:before, 
			.wp-has-current-submenu .dashicons-admin-users:before, 
			.wp-has-current-submenu .dashicons-video-alt3:before, 
			.wp-has-current-submenu .dashicons-format-gallery:before, 
			.wp-has-current-submenu .dashicons-calendar:before {
			    color: #fff !important 
			}
			th#artist, 
			th#cd_plays, 
			th#cd_likes, 
			th#cd_dislikes, 
			th#alphabet, 
			th#cd_downloads, 
			th#cd_views, 
			th#items, 
			th#taxonomy-artist, 
			th#categories, 
			th#venue, 
			th#status {
			    width: 8%
			}  
			th#cd_event_start, 
			th#cd_event_end {
			    width: 10%
			} 
			th#cover { 
			    width: 42px
			} 
			.cover img, 
			.riv_post_thumbs img { 
			    border-radius: 4px;
			    width: 40px;
			    height: 40px
			}
			.cd_ico { 
			    background-image: none !important;
			    background-color: #2A81CC;
			    text-align: center;
			    border-radius: 2px;
			    padding: 5px;
			    width: inherit !important;
			    height: inherit !important;
			}
			.cd_ico:before { color: #fff !important }
			.wpb_element_title .cdz_ico {
			    width: 22px !important;
			    height: 20px !important
			}
			[data-param_type|=posttypes] .edit_form_line, 
			[data-param_type|=checkbox] .edit_form_line { 
			    height: 126px;
			    overflow: auto;
			    border: 1px solid #F3F3F3;
			    padding: 2px 0 0 10px
			}
			[data-param_type|=posttypes] .vc_checkbox-label,
			[data-param_type|=checkbox] .vc_checkbox-label {
			    display: block;
			    margin: 6px 0
			}
		</style>';
	}

	public function register() {
		$this->menu_separator( 26 );
		$options = get_option( '_cs_options' );

		foreach ( $this->types as $slug => $icon ) {

			$opt = isset( $options[ $slug . '_settings' ] ) ? $options[ $slug . '_settings' ] : array();
			$opt = array(
				'slug' 			=> !empty( $opt['slug'] ) ? $opt['slug'] : $slug, 
				'title' 		=> !empty( $opt['title'] ) ? $opt['title'] : ucfirst( $slug ), 
				'alphabet_slug' => !empty( $opt['alphabet_slug'] ) ? $opt['alphabet_slug'] : $slug.'/browse', 
				'alphabet_title'=> !empty( $opt['alphabet_title'] ) ? $opt['alphabet_title'] : 'Alphabet', 
				'cat_slug' 		=> !empty( $opt['cat_slug'] ) ? $opt['cat_slug'] : $slug.'/cat', 
				'cat_title' 	=> !empty( $opt['cat_title'] ) ? $opt['cat_title'] : 'Categories'
			);

			register_taxonomy( $slug.'_cat', $slug, 
				array(
					'hierarchical'		=> true,
					'labels'		=> array(
						'name'			=> $opt['cat_title'],
						'menu_name'		=> $opt['cat_title']
					),
					'show_ui'			=> true,
					'show_admin_column'	=> true,
					'query_var'			=> true,
					'rewrite'			=> array( 'slug' => $opt['cat_slug'], 'with_front' => false ),
				)
			);

			if ( $slug === 'artists' ) {
				register_taxonomy( 'artist', 'artists', 
					array(
						'hierarchical'		=> true,
						'labels'		=> array(
							'name'			=> $opt['alphabet_title'],
							'menu_name'		=> $opt['alphabet_title']
						),
						'show_ui'			=> true,
						'show_admin_column'	=> true,
						'query_var'			=> true,
						'rewrite'			=> array( 'slug' => $opt['alphabet_slug'], 'with_front' => false ),
					)
				);
			}

			register_post_type( $slug, 
				array(
					'labels'		=> array(
						'name'			=> $opt['title'],
						'menu_name'		=> $opt['title']
					),
					'public'			=> true,
					'menu_icon'			=> $icon,
					'supports'			=> array( 'title', 'editor', 'thumbnail', 'custom-fields', 'comments', 'author' ),
					'has_archive'		=> true,
					'show_in_rest'		=> true,
					'rewrite'			=> array( 'slug' => $opt['slug'], 'with_front' => false )
			 	)
			);

			register_taxonomy_for_object_type( 'post_tag', $slug );
		}

		$this->menu_separator( 36 );
	}

	public function sorting_request() {
		add_filter( 'request', array( $this, 'sorting' ) );
	}

	public function sorting( $vars ) {
		foreach ( $this->sortable as $i ) {
			if ( isset( $vars['orderby'] ) && $i === $vars['orderby'] ) {
				if ( $i == 'cd_event_date' || $i === 'cd_end_event_date' ) {
					$vars = array_merge(
						$vars,
						array(
							'meta_query' => array(
								array(
									'key' => $i,
									'value' => date('2030/12/28 10:30'),
									'compare' => '<'
								)
							)
						)
					);
				} else {
					$vars = array_merge(
						$vars,
						array(
							'meta_key' => $i,
							'orderby' => 'meta_value_num'
						)
					);
				}
			}
		}

		return $vars;
	}

	public function manage_columns( $column, $post_id ) {

		$meta = get_post_meta( $post_id, 'cd_meta', true );
		$meta = isset( $meta ) ? $meta : array();
		$out = '';

		switch( $column ) {
			case 'cover' :
				ob_start();
				the_post_thumbnail( 'thumbnail' );
				$out = ob_get_clean();
				break;
			case 'cd_items' :
				$out .= $this->get_artist_items( 'post', $post_id );
				$out .= $this->get_artist_items( 'songs', $post_id );
				$out .= $this->get_artist_items( 'videos', $post_id );
				$out .= $this->get_artist_items( 'podcasts', $post_id );
				$out .= $this->get_artist_items( 'playlists', $post_id );
				$out .= $this->get_artist_items( 'events', $post_id );
				$out .= $this->get_artist_items( 'gallery', $post_id );
				$out .= $this->get_artist_items( 'lyrics', $post_id );
				$out .= $this->get_artist_items( 'posts', $post_id );
				$out .= $this->get_artist_items( 'product', $post_id );
				break;
			case 'cd_event_date' :
				$out .= date_i18n( "j F, Y", strtotime( isset( $meta['event_date'] ) ? $meta['event_date'] : '' ) ) . '<br />';
				$out .= date_i18n( "H:i", strtotime( isset( $meta['event_date'] ) ? $meta['event_date'] : '' ) );
				break;
			case 'cd_end_event_date' :
				$out .= date_i18n( "j F, Y", strtotime( $meta['end_event_date'] ) ) . '<br />';
				$out .= date_i18n( "H:i", strtotime( $meta['end_event_date'] ) );
				break;
			case 'cd_venue' :
				$out = isset( $meta['venue'] ) ? $meta['venue'] : '';
				break;
			case 'cd_artist' :
				$out = $this->get_artists( $post_id );
				break;
			case 'cd_plays' :
				$out = get_post_meta( $post_id, 'cd_plays', true );
				$out = !is_array( $out ) ? $out : '';
				break;
			case 'cd_likes' :
				$out = get_post_meta( $post_id, 'cd_likes', true );
				break;
			case 'cd_dislikes' :
				$out = get_post_meta( $post_id, 'cd_dislikes', true );
				break;
			case 'cd_downloads' :
				$out = get_post_meta( $post_id, 'cd_downloads', true );
				break;
			case 'cd_views' :
				$out = get_post_meta( $post_id, 'cd_views', true );
				break;
		}

		echo $out ? $out : '_';
	}

	public function menu_separator( $p ) {
		global $menu;
		static $id = 0;

		if ( !is_int( $p ) ) {
			$mp = 0;
			foreach( $menu as $mp => $i ) {
				if ( $i[2] === $p ) { break; }
			}
			$p = $mp + 1;
		}
		$menuFile = 'separator-custom-' . $id++;
		$menu[$p] = array( '', 'read', $menuFile, '', 'wp-menu-separator cd_separator' );
		ksort( $menu );
	}

	public function get_artist_items( $cpt, $id ) {
		$query = new WP_Query( array(
			'post_type'     => $cpt,
			'meta_query'    => array(
				array( 'relation' => 'OR' ),
				array(
					'key' 		=> 'cd_meta', 
					'value' 	=> '"' . $id . '"', 
					'compare' 	=> 'LIKE'
				)
			),
			'posts_per_page' => -1
		) );

		return $query->post_count ? '[' . $query->post_count . '] ' . ucwords( $cpt ) . '<br />' : '';
		wp_reset_postdata();
	}

	public function get_artists( $id, $out = '' ) {

		$meta = get_post_meta( $id, 'cd_meta', true );
		$meta = isset( $meta['artist'] ) ? $meta['artist'] : array();
		$artists = array();

		if ( $meta ) {
			foreach( $meta as $i ) {
				if ( get_the_title( $id ) !== get_the_title( $i ) ) {
					$artists[] = '<a href="' . get_post_permalink( $i ) . '">' . get_the_title( $i ) . '</a>';
				}
			}
			$out .= implode( '<br />', $artists );
		}

		return $out;
	}

}

new Codevz_CPT();
