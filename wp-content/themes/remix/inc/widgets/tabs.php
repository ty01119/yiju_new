<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( !class_exists( 'CodevzTabbedNews' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzTabbedNews');
	});

	class CodevzTabbedNews extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Tabbed news', 'remix' ), 
				array( 'classname' => 'cd_tabbed_news' )
			);
		}

		private function _create_tabs( $tabs, $count ) {
			$titles = array(
				'recent'	=> esc_html__('Recent Posts', 'remix'),
				'popular'	=> esc_html__('Popular Posts', 'remix'),
				'comments'	=> esc_html__('Recent Comments', 'remix'),
				'tags'		=> esc_html__('Tags', 'remix')
			);
			$icons = array(
				'recent'	=> 'fa fa-clock-o',
				'popular'	=> 'fa fa-heart',
				'comments'	=> 'fa fa-comments-o',
				'tags'		=> 'fa fa-tag'
			);
			$out = sprintf('<div class="tabs-nav clr count-%s">', $count);
			foreach ( $tabs as $tab ) {
				$out .= sprintf('<a href="#tab-%2$s" title="%4$s"><i class="%3$s"></i></a>',$tab, $tab, $icons[$tab], $titles[$tab]);
			}
			$out .= '</div>';

			return $out;
		}
		
		public function widget($args, $instance) {
			extract( $args );
			$instance['title']?NULL:$instance['title']='';
			$title = apply_filters('widget_title',$instance['title']);
			$out = "\n";
			ob_start();
			
			$tabs = array();
			$count = 0;
			$order = array(
				'recent'	=> $instance['order_recent'],
				'popular'	=> $instance['order_popular'],
				'comments'	=> $instance['order_comments'],
				'tags'		=> $instance['order_tags']
			);
			asort($order);

			global $post;
			foreach ( $order as $key => $value ) {
				if ( $instance[$key.'_enable'] ) {
					$tabs[] = $key;
					$count++;
				}
			}
			$out .= '<section class="widget tabs clr">';
			if ( $tabs && ($count >= 1) ) { $out .= $this->_create_tabs($tabs,$count); }
			$out .= '<div class="tabs-content def-block clr">';

				if($instance['recent_enable']) { // Recent posts enabled? ?>
					
					<?php $recent=new WP_Query(); ?>
					<?php wp_reset_query(); $recent->query('post_type=post&showposts='.$instance["recent_num"].'&cat='.$instance["recent_cat_id"].'&ignore_sticky_posts=1'); ?>
					
					<div id="tab-recent" class="tab">
						<?php while ($recent->have_posts()): $recent->the_post(); ?>
							<div class="item_small">
								<?php if ( has_post_thumbnail() ): ?>
									<a class="cdEffect noborder" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php the_post_thumbnail( 'tumb' ); ?>
										<i class="fa fa-plus"></i>
									</a>
								<?php endif; ?>
								<div class="item-details">
									<h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
									<div class="post_meta">
										<?php if($instance['tabs_date']) { ?><a class="mid" href="<?php echo get_the_time( get_option('date_format') ); ?>" rel="date" title="<?php the_title(); ?>"><i class="fa fa-clock-o mi"></i><?php the_time( get_option('date_format') ); ?></a> 
										<a href="<?php comments_link(); ?>"><i class="fa fa-comments-o mi"></i><?php comments_number( '0', '1', '%' ); ?></a><?php } ?>
									</div>
								</div>
							</div>
						<?php endwhile; wp_reset_query(); ?>
					</div><!--/.CD-tab-->

				<?php } ?>


				<?php if($instance['popular_enable']) { // Popular posts enabled? ?>
						
					<?php
						wp_reset_query(); 
						$popular = new WP_Query( array(
							'post_type'		=> array( 'post' ),
							'showposts'		=> $instance['popular_num'],
							'cat'			=> $instance['popular_cat_id'],
							'ignore_sticky_posts'	=> true,
							'order'			=> 'dsc',
							'meta_key'		=> 'like',
							'orderby'		=> 'meta_value_num',
							'date_query' 		=> array(
								array(
									'after' => $instance['popular_time'],
								),
							),
						) );
					?>
					<div id="tab-popular" class="tab">
						
						<?php while ( $popular->have_posts() ): $popular->the_post(); ?>
							<div class="item_small">
								<?php if ( has_post_thumbnail() ): ?>
									<a class="cdEffect noborder" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php the_post_thumbnail( 'tumb' ); ?>
										<i class="fa fa-plus"></i>
									</a>
								<?php endif; ?>
								<div class="item-details">
									<h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
									<div class="post_meta">
										<?php if($instance['tabs_date']) { ?><a class="mid" href="<?php echo get_the_time( get_option('date_format') ); ?>" rel="date" title="<?php the_title(); ?>"><i class="fa fa-clock-o mi"></i><?php the_time( get_option('date_format') ); ?></a> 
										<a href="<?php comments_link(); ?>"><i class="fa fa-comments-o mi"></i><?php comments_number( '0', '1', '%' ); ?></a><?php } ?>
									</div>
								</div>
							</div>
						<?php endwhile; wp_reset_query(); ?>
					</div><!--/.CD-tab-->
					
				<?php } ?>
			
				<?php if($instance['comments_enable']) { // Recent comments enabled? ?>

					<?php $comments = get_comments(array('number'=>$instance["comments_num"],'status'=>'approve','post_status'=>'publish')); ?>
					
					<div id="tab-comments" class="tab">
						<?php foreach ($comments as $comment): ?>
							<div class="item_small">
								<div class="one_post">
									<?php if($instance['comments_avatars']) { // Avatars enabled? ?>
									<a class="cdEffect noborder" href="<?php echo esc_url(get_comment_link($comment->comment_ID)); ?>">
										<?php echo get_avatar($comment->comment_author_email,$size='96'); ?>
										<i class="fa fa-comments"></i>
									</a>
									<?php } ?>
									<div class="item-details">
										<?php $str=explode(' ',get_comment_excerpt($comment->comment_ID)); $comment_excerpt=implode(' ',array_slice($str,0,11)); if(count($str) > 11 && substr($comment_excerpt,-1)!='.') $comment_excerpt.='...' ?>
										<a href="<?php echo esc_url(get_comment_link($comment->comment_ID)); ?>"><?php echo $comment->comment_author; ?> <small><?php _e('says:','remix'); ?></small></a>
										<div class="post_meta">
											<?php echo $comment_excerpt; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div><!--/.CD-tab-->

				<?php } ?>

				<?php if($instance['tags_enable']) { // Tags enabled? ?>
					<div id="tab-tags" class="CD-tab tagcloud group">
						<?php wp_tag_cloud(); ?>
					</div><!--/tab-->
				<?php } ?>
			</section>
		<?php
			$out .= ob_get_clean();
			$out .= "\n";
			echo $out;
		}
		
		public function update($new,$old) {
			$instance = $old;
			$instance['title'] = strip_tags($new['title']);
			$instance['tabs_category'] = $new['tabs_category']?1:0;
			$instance['tabs_date'] = $new['tabs_date']?1:0;
		// Recent posts
			$instance['recent_enable'] = $new['recent_enable']?1:0;
			$instance['recent_thumbs'] = $new['recent_thumbs']?1:0;
			$instance['recent_cat_id'] = strip_tags($new['recent_cat_id']);
			$instance['recent_num'] = strip_tags($new['recent_num']);
		// Popular posts
			$instance['popular_enable'] = $new['popular_enable']?1:0;
			$instance['popular_thumbs'] = $new['popular_thumbs']?1:0;
			$instance['popular_cat_id'] = strip_tags($new['popular_cat_id']);
			$instance['popular_time'] = strip_tags($new['popular_time']);
			$instance['popular_num'] = strip_tags($new['popular_num']);
		// Recent comments
			$instance['comments_enable'] = $new['comments_enable']?1:0;
			$instance['comments_avatars'] = $new['comments_avatars']?1:0;
			$instance['comments_num'] = strip_tags($new['comments_num']);
		// Tags
			$instance['tags_enable'] = $new['tags_enable']?1:0;
		// Order
			$instance['order_recent'] = strip_tags($new['order_recent']);
			$instance['order_popular'] = strip_tags($new['order_popular']);
			$instance['order_comments'] = strip_tags($new['order_comments']);
			$instance['order_tags'] = strip_tags($new['order_tags']);
			return $instance;
		}

		public function form($instance) {
			$defaults = array(
				'title' 			=> '',
				'tabs_category' 	=> 1,
				'tabs_date' 		=> 1,
			// Recent posts
				'recent_enable' 	=> 1,
				'recent_thumbs' 	=> 1,
				'recent_cat_id' 	=> '0',
				'recent_num' 		=> '5',
			// Popular posts
				'popular_enable' 	=> 1,
				'popular_thumbs' 	=> 1,
				'popular_cat_id' 	=> '0',
				'popular_time' 		=> '0',
				'popular_num' 		=> '5',
			// Recent comments
				'comments_enable' 	=> 1,
				'comments_avatars' 	=> 1,
				'comments_num' 		=> '5',
			// Tags
				'tags_enable' 		=> 1,
			// Order
				'order_recent' 		=> '1',
				'order_popular' 	=> '2',
				'order_comments' 	=> '3',
				'order_tags' 		=> '4',
			);
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>
			
			<div class="CD-options-tabs">
				<h4>Recent Posts</h4>
				
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('recent_enable'); ?>" name="<?php echo $this->get_field_name('recent_enable'); ?>" <?php checked( (bool) $instance["recent_enable"], true ); ?>>
					<label for="<?php echo $this->get_field_id('recent_enable'); ?>">Enable recent posts</label>
				</p>
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('recent_thumbs'); ?>" name="<?php echo $this->get_field_name('recent_thumbs'); ?>" <?php checked( (bool) $instance["recent_thumbs"], true ); ?>>
					<label for="<?php echo $this->get_field_id('recent_thumbs'); ?>">Show thumbnails</label>
				</p>	
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id("recent_num"); ?>">Items to show</label>
					<input style="width:20%;" id="<?php echo $this->get_field_id("recent_num"); ?>" name="<?php echo $this->get_field_name("recent_num"); ?>" type="text" value="<?php echo absint($instance["recent_num"]); ?>" size='3' />
				</p>
				<p>
					<label style="width: 100%; display: inline-block;" for="<?php echo $this->get_field_id("recent_cat_id"); ?>">Category:</label>
					<?php wp_dropdown_categories( array( 'name' => $this->get_field_name("recent_cat_id"), 'selected' => $instance["recent_cat_id"], 'show_option_all' => 'All', 'show_count' => true ) ); ?>		
				</p>
				
				<hr>
				<h4>Most Popular</h4>
				
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('popular_enable'); ?>" name="<?php echo $this->get_field_name('popular_enable'); ?>" <?php checked( (bool) $instance["popular_enable"], true ); ?>>
					<label for="<?php echo $this->get_field_id('popular_enable'); ?>">Enable most popular posts</label>
				</p>
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('popular_thumbs'); ?>" name="<?php echo $this->get_field_name('popular_thumbs'); ?>" <?php checked( (bool) $instance["popular_thumbs"], true ); ?>>
					<label for="<?php echo $this->get_field_id('popular_thumbs'); ?>">Show thumbnails</label>
				</p>	
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id("popular_num"); ?>">Items to show</label>
					<input style="width:20%;" id="<?php echo $this->get_field_id("popular_num"); ?>" name="<?php echo $this->get_field_name("popular_num"); ?>" type="text" value="<?php echo absint($instance["popular_num"]); ?>" size='3' />
				</p>
				<p>
					<label style="width: 100%; display: inline-block;" for="<?php echo $this->get_field_id("popular_cat_id"); ?>">Category:</label>
					<?php wp_dropdown_categories( array( 'name' => $this->get_field_name("popular_cat_id"), 'selected' => $instance["popular_cat_id"], 'show_option_all' => 'All', 'show_count' => true ) ); ?>		
				</p>
				<p style="padding-top: 0.3em;">
					<label style="width: 100%; display: inline-block;" for="<?php echo $this->get_field_id("popular_time"); ?>">Post with most comments from:</label>
					<select style="width: 100%;" id="<?php echo $this->get_field_id("popular_time"); ?>" name="<?php echo $this->get_field_name("popular_time"); ?>">
					  <option value="0"<?php selected( $instance["popular_time"], "0" ); ?>>All time</option>
					  <option value="1 year ago"<?php selected( $instance["popular_time"], "1 year ago" ); ?>>This year</option>
					  <option value="1 month ago"<?php selected( $instance["popular_time"], "1 month ago" ); ?>>This month</option>
					  <option value="1 week ago"<?php selected( $instance["popular_time"], "1 week ago" ); ?>>This week</option>
					  <option value="1 day ago"<?php selected( $instance["popular_time"], "1 day ago" ); ?>>Past 24 hours</option>
					</select>	
				</p>
				
				<hr>
				<h4>Recent Comments</h4>
				
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('comments_enable'); ?>" name="<?php echo $this->get_field_name('comments_enable'); ?>" <?php checked( (bool) $instance["comments_enable"], true ); ?>>
					<label for="<?php echo $this->get_field_id('comments_enable'); ?>">Enable recent comments</label>
				</p>
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('comments_avatars'); ?>" name="<?php echo $this->get_field_name('comments_avatars'); ?>" <?php checked( (bool) $instance["comments_avatars"], true ); ?>>
					<label for="<?php echo $this->get_field_id('comments_avatars'); ?>">Show avatars</label>
				</p>
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id("comments_num"); ?>">Items to show</label>
					<input style="width:20%;" id="<?php echo $this->get_field_id("comments_num"); ?>" name="<?php echo $this->get_field_name("comments_num"); ?>" type="text" value="<?php echo absint($instance["comments_num"]); ?>" size='3' />
				</p>

				<hr>
				<h4>Tags</h4>
				
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('tags_enable'); ?>" name="<?php echo $this->get_field_name('tags_enable'); ?>" <?php checked( (bool) $instance["tags_enable"], true ); ?>>
					<label for="<?php echo $this->get_field_id('tags_enable'); ?>">Enable tags</label>
				</p>
			
				<hr>
				<h4>Tab Order</h4>
				
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id("order_recent"); ?>"><?php _e('Recent posts', 'remix'); ?></label>
					<input class="widefat" style="width:20%;" type="text" id="<?php echo $this->get_field_id("order_recent"); ?>" name="<?php echo $this->get_field_name("order_recent"); ?>" value="<?php echo $instance["order_recent"]; ?>" />
				</p>
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id("order_popular"); ?>"><?php _e('Most popular', 'remix'); ?></label>
					<input class="widefat" style="width:20%;" type="text" id="<?php echo $this->get_field_id("order_popular"); ?>" name="<?php echo $this->get_field_name("order_popular"); ?>" value="<?php echo $instance["order_popular"]; ?>" />
				</p>
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id("order_comments"); ?>"><?php _e('Recent comments', 'remix'); ?></label>
					<input class="widefat" style="width:20%;" type="text" id="<?php echo $this->get_field_id("order_comments"); ?>" name="<?php echo $this->get_field_name("order_comments"); ?>" value="<?php echo $instance["order_comments"]; ?>" />
				</p>
				<p>
					<label style="width: 55%; display: inline-block;" for="<?php echo $this->get_field_id("order_tags"); ?>"><?php _e('Tags', 'remix'); ?></label>
					<input class="widefat" style="width:20%;" type="text" id="<?php echo $this->get_field_id("order_tags"); ?>" name="<?php echo $this->get_field_name("order_tags"); ?>" value="<?php echo $instance["order_tags"]; ?>" />
				</p>
				
				<hr>
				<h4><?php _e('Tab Info', 'remix'); ?></h4>
				<p>
					<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('tabs_date'); ?>" name="<?php echo $this->get_field_name('tabs_date'); ?>" <?php checked( (bool) $instance["tabs_date"], true ); ?>>
					<label for="<?php echo $this->get_field_id('tabs_date'); ?>"><?php _e('Show posts meta', 'remix'); ?></label>
				</p>
				
			</div>
		<?php
		}

	}

}
