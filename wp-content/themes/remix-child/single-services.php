<?php 

/**
 * Single page
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

	get_header();
	global $codevz;
	$codevz->content_before();
	$cpt = get_post_type();
?>

<article id="<?php echo get_the_ID(); ?>" <?php post_class( 'mbf clr def-block' ); ?>>
	<?php 
		/* Content */
		while ( have_posts() ): the_post();		
	?>
	<div class="service-content">
		<div class="ms_row header">
			<div class="company-logo grid_2">
				<img src="<?php the_field("service_logo"); ?>">
			</div>
			<div class="company-details grid_10">
				<div class="company-name grid_12"><?php the_field("company_name"); ?></div>
				<div class="company-details-inner">
					<div class="company-info grid_6">地址：<?php the_field("service_address"); ?></div>
					<div class="company-info grid_6">时间：<?php the_field("service_time"); ?></div>
					<div class="company-info grid_6">电话：<?php the_field("service_phone"); ?></div>
					<div class="company-info grid_6">邮箱：<?php the_field("service_email"); ?></div>
				</div>
			</div>
		</div>
		<div class="ms_row">
			<div class="company-intro"><?php the_field("service_intro"); ?></div>
		</div>
	</div>
	<?php 
		endwhile;
	?>
	
</article>
<?php 


	/* After all */
	$codevz->content_after();

	get_sidebar();
	get_footer(); 
