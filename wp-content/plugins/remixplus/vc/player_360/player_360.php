<?php

/**
 * @author Codevz
 * @link http://Codevz.com
 *
 * Player 360° add-one for visual composer
 * Shortcode name: codevz_player_sm
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzPlayerSm extends CodevzVc {

	function __construct() {
		add_action( 'init', array( $this, 'player' ) );
		add_shortcode( 'codevz_player_sm', array( $this, 'out' ) );
	}

	function player() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'codevz_player_sm',
			'name'			=> __('Player 360°', 'cd'), 
			'icon'			=> 'dashicons-before dashicons-format-audio cd_ico',
			'params'		=> array(
				array(
					"type"        	=> "cs_upload",
					"heading"     	=> __("MP3", 'cd'),
					'edit_field_class'=> $this->col('12'),
					"param_name"  	=> "mp3",
					"admin_label" => true
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Auto play', 'cd'),
					'param_name' 	=> 'autoplay',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'False', 'cd' ) => 'false',
						__( 'True', 'cd' ) => 'true'
					)
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Class', 'cd'),
					'param_name'	=> 'extra_class',
					'edit_field_class'=> $this->col('6'),
				),
				array(
					'type' 			=> 'colorpicker',
					'heading' 		=> __('loadRingColor', 'cd'),
					'param_name' 	=> 'loadringcolor',
					'edit_field_class'=> $this->col('4')
				),
				array(
					'type' 			=> 'colorpicker',
					'heading' 		=> __('playRingColor', 'cd'),
					'param_name' 	=> 'playringcolor',
					'edit_field_class'=> $this->col('4')
				),
				array(
					'type' 			=> 'colorpicker',
					'heading' 		=> __('waveformDataColor', 'cd'),
					'param_name' 	=> 'waveformdatacolor',
					'edit_field_class'=> $this->col('4')
				),
				array(
					'type' 			=> 'colorpicker',
					'heading' 		=> __('eqDataColor', 'cd'),
					'param_name' 	=> 'eqdatacolor',
					'edit_field_class'=> $this->col('4')
				),
				array(
					'type' 			=> 'colorpicker',
					'heading' 		=> __('peakDataColor', 'cd'),
					'param_name' 	=> 'peakdatacolor',
					'edit_field_class'=> $this->col('4')
				),
			)

		));
	}

	function out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'mp3'			=> '',
			'autoplay'		=> false,
			'extra_class' 	=> '',
			'loadringcolor' => '',
			'playringcolor' => '',
			'waveformdatacolor' => '',
			'eqdatacolor' 	=> '',
			'peakdatacolor' => '',
		), $atts );

		global $codevz;

		if ( $codevz ) {
			ob_start();

			echo "<div class='sm_player mbf clr " . $atts['extra_class'] . "' data-id='" . $codevz->post->ID . "' data-sm-player='" . json_encode( $atts, JSON_HEX_APOS ) . "'>";
			echo '<div class="ui360 ui360-vis"><a href="';
			echo $atts['mp3'];
			echo '"></a></div>';
			echo "</div>";
			?>
				<link rel='stylesheet' id='360player-css' href='<?php echo get_template_directory_uri() .'/js/sm/360player.css'; ?>' type='text/css' media='all' />
				<link rel='stylesheet' id='visualization-css' href='<?php echo get_template_directory_uri() .'/js/sm/360player-visualization.css'; ?>' type='text/css' media='all' />
				<script type="text/javascript" src="<?php echo get_template_directory_uri() .'/js/sm/berniecode-animator.js'; ?>"></script>
				<script type="text/javascript" src="<?php echo get_template_directory_uri() .'/js/sm/soundmanager2.js'; ?>"></script>
				<script type="text/javascript" src="<?php echo get_template_directory_uri() .'/js/sm/360player.js'; ?>"></script>
			<?php

			return ob_get_clean();
		} else {
			return 'This theme do not support this shortcode.';
		}
	}

}

new CodevzPlayerSm();
