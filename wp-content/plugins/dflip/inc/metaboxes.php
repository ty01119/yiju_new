<?php

/**
 * dFlip Metaboxes
 *
 * creates, displays and saves metaboxes and their values
 *
 * @since   1.0.0
 *
 * @package dFlip
 * @author  Deepak Ghimire
 */
class DFlip_Meta_boxes
{

	/**
	 * Holds the singleton class object.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	public static $instance;

	/**
	 * Holds the base DFlip class object.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	public $base;

	/**
	 * Holds the base DFlip class fields.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	public $fields;

	/**
	 * Primary class constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// Load the base class object.
		$this->base = DFlip::get_instance();

		//Fields structure and defaults
		$this->fields = array(
			'auto_outline'    => array(
				'std'     => 'false', //isset mis-interprets 0 and false differently than expected
				'choices' => array(
					'true'  => __DFLIP('True'),
					'false' => __DFLIP('False')
				)
			),
			'overwrite_outline'    => array(
				'std'     => 'false', //isset mis-interprets 0 and false differently than expected
				'choices' => array(
					'true'  => __DFLIP('True'),
					'false' => __DFLIP('False')
				)
			),
			'auto_sound'      => array(
				'std'     => 'true',
				'choices' => array(
					'true'  => __DFLIP('True'),
					'false' => __DFLIP('False')
				)
			),
			'enable_download' => array(
				'std'     => 'true',
				'choices' => array(
					'true'  => __DFLIP('True'),
					'false' => __DFLIP('False')
				)
			),
			'direction'       => array(
				'std'     => 'true',
				'choices' => array(
					1 => __DFLIP('Left to Right'),
					2 => __DFLIP('Right to Left')
				)
			),
			'webgl'           => array(
				'std'     => 'true',
				'choices' => array(
					'true'  => __DFLIP('WebGL 3D'),
					'false' => __DFLIP('CSS 3D/2D')
				)
			),
			'source_type'     => array(
				'std'     => 'pdf',
				'choices' => array(
					'pdf'   => __DFLIP('PDF File'),
					'image' => __DFLIP('Images')
				)
			),
			'hard'            => array(
				'std'     => 'cover',
				'choices' => array(
					'cover' => __DFLIP('Cover Pages'),
					'all'   => __DFLIP('All Pages'),
					'none'  => __DFLIP('None')
				)
			),
			'pages'           => array(),
			'duration'        => 800,
			'bg_color'        => "#fff",
			'height'          => 500,
			'texture_size'    => array(
				'std'     => '1600',
				'choices' => array(
					'1024' => 1024,
					'1600' => 1600,
					'2048' => 2048
				)
			),
			'page_mode'    => array(
				'std'     => '0',
				'choices' => array(
					'0' => __DFLIP('Auto'),
					'1' => __DFLIP('Single Page'),
					'2' => __DFLIP('Double Page')
				)
			)
		);

		// Load metabox assets.
		add_action(
			'admin_enqueue_scripts', array($this, 'meta_box_styles_scripts')
		);

		// Load the metabox hooks and filters.
		add_action('add_meta_boxes', array($this, 'add_meta_boxes'), 100);

		// Add action to save metabox config options.
		add_action('save_post', array($this, 'save_meta_boxes'), 10, 2);
	}

	/**
	 * Loads styles and scripts for our metaboxes.
	 *
	 * @since 1.0.0
	 *
	 * @return null Bail out if not on the proper screen.
	 */
	public function meta_box_styles_scripts() {

		global $id, $post;

		if (isset(get_current_screen()->base) && 'post' !== get_current_screen()->base) {
			return;
		}
		if (isset(get_current_screen()->post_type) && $this->base->plugin_slug !== get_current_screen()->post_type) {
			return;
		}

		// Set the post_id for localization.
		$post_id = isset($post->ID) ? $post->ID : (int)$id;

		// Load necessary metabox styles.
		wp_register_style(
			$this->base->plugin_slug . '-metabox-style',
			plugins_url('assets/css/metaboxes.css', $this->base->file),
			array(),
			$this->base->version
		);
		wp_enqueue_style($this->base->plugin_slug . '-metabox-style');

		// Load necessary metabox scripts.
		wp_register_script(
			$this->base->plugin_slug . '-metabox-script',
			plugins_url('assets/js/metaboxes.js', $this->base->file),
			array('jquery-ui-sortable', 'jquery-ui-draggable', 'jquery-ui-resizable'),
			$this->base->version
		);
		wp_enqueue_script($this->base->plugin_slug . '-metabox-script');

		wp_enqueue_media(array('post' => $post_id,));

	}

	/**
	 * Adds metaboxes for handling settings
	 *
	 * @since 1.0.0
	 */
	public function add_meta_boxes() {

		add_meta_box(
			'dflip_post_meta_box', __DFLIP('dFlip Settings'),
			array($this, 'create_meta_boxes'), 'dflip', 'normal', 'high'
		);

	}

	/**
	 * Creates metaboxes for handling settings
	 *
	 * @since 1.0.0
	 *
	 * @param object $post The current post object.
	 */
	public function create_meta_boxes($post) {

		// Keep security first.
		wp_nonce_field($this->base->plugin_slug, $this->base->plugin_slug);

		$tabs = array(
			'source'  => __DFLIP('Source'),
			'layout'  => __DFLIP('Layout'),
			'outline' => __DFLIP('Outline')
		);

		//create tabs and content
		?>
		<div id="dflip-tabs" class="">
			<ul id="dflip-tabs-list" class="">
				<?php
				//create tabs
				$active_set = false;
				foreach ((array)$tabs as $id => $title) {
					?>
					<li class="<?php echo($active_set == false ? 'dflip-active' : '') ?>">
						<a href="#dflip-tab-<?php echo $id ?>"><?php echo $title ?></a></li>
					<?php $active_set = true;
				}
				?>
			</ul>
			<?php

			$active_set = false;
			foreach ((array)$tabs as $id => $title) {
				?>
				<div id="dflip-tab-<?php echo $id ?>"
				     class="dflip-tabs <?php echo($active_set == false ? "dflip-active" : "") ?>">

					<?php
					$active_set = true;

					//create content for tab
					$function = $id . "_tab";
					call_user_func(array($this, $function), $post);

					?>
				</div>
			<?php } ?>
		</div>
	<?php

	}

	/**
	 * Creates the UI for Source tab
	 *
	 * @since 1.0.0
	 *
	 * @param object $post The current post object.
	 */
	public function source_tab($post) {

		?>

		<!--Type of source-->
		<div id="dflip_source_type_box" class="dflip-box ">

			<label for="dflip_source_type" class="dflip-label">
				<?php echo __DFLIP('Book Source Type'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Choose the source of this book. "PDF" for pdf files. "Images" for image files.'); ?>
			</div>

			<div class="dflip-option dflip-select">
				<select name="_dflip[source_type]" id="dflip_source_type" class="">
					<?php foreach ((array)$this->fields['source_type']['choices'] as $val => $label) { ?>
						<option value="<?php echo $val; ?>"
							<?php selected($this->get_config('source_type', $post), $val); ?>>
							<?php echo $label; ?>
						</option>
					<?php } ?>
				</select>
			</div>

		</div>

		<!--PDF Source for the book-->
		<div id="dflip_pdf_source_box" class="dflip-box" data-condition="dflip_source_type:is(pdf)">

			<label for="dflip_pdf_source" class="dflip-label">
				<?php echo __DFLIP('PDF File'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Choose a PDF File to use as source for the book.'); ?>
			</div>

			<div class="dflip-option dflip-upload">
				<input type="text" name="_dflip[pdf_source]" id="dflip_pdf_source"
				       value="<?php echo $this->get_config('pdf_source', $post); ?>"
				       class="widefat dflip-upload-input "/>
				<a href="javascript:void(0);" class="dflip_upload_media dflip-button button button-primary light"
				   title="Select PDF File">
					<?php echo __DFLIP('Select PDF'); ?>
				</a>
			</div>

		</div>

		<!--PDF thumb for the book-->
		<div id="dflip_pdf_thumb_box" class="dflip-box" data-condition="dflip_source_type:is(pdf)">

			<label for="dflip_pdf_thumb" class="dflip-label">
				<?php echo __DFLIP('PDF Thumbnail Image'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Choose an image file for pdf thumb.'); ?>
			</div>

			<div class="dflip-option dflip-upload">
				<input type="text" name="_dflip[pdf_thumb]" id="dflip_pdf_thumb"
				       value="<?php echo $this->get_config('pdf_thumb', $post); ?>"
				       class="widefat dflip-upload-input "/>
				<a href="javascript:void(0);" class="dflip_upload_media dflip-button button button-primary light"
				   title="Select PDF Thumb Image">
					<?php echo __DFLIP('Select Thumb'); ?>
				</a>
			</div>

		</div>

		<!--Pages for the book-->
		<div id="dflip_pages_box" class="dflip-box " data-condition="dflip_source_type:is(image)" data-operator="and">

			<label for="dflip_pages" class="dflip-label">
				<?php echo __DFLIP('Custom Pages'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP(
					'Add or remove pages as per your requirement. Plus reorder them in the order needed.'
				); ?>
			</div>
			<div class="dflip-option dflip-page-list">
				<a href="javascript:void(0);" class="dflip-page-list-add button button-primary"
				   title="Add New Page">
					<?php echo __DFLIP('Add New Page'); ?>
				</a>
				<ul id="dflip_page_list">
					<?php
					$page_list = $this->get_config('pages', $post);
					$index = 0;
					foreach ((array)$page_list as $page) {

						/* build the arguments*/
						$title = isset($page['title']) ? $page['title'] : '';
						$url = isset($page['url']) ? $page['url'] : '';
						$content = isset($page['content']) ? $page['content'] : '';

						if ($url != '') {
							?>
							<li class="dflip-page-item">
								<img class="dflip-page-thumb" src="<?php echo $url; ?>" alt=""/>

								<div class="dflip-page-options">

									<label for="dflip-page-<?php echo $index; ?>-title">
										<?php echo __DFLIP('Title'); ?>
									</label>
									<input type="text"
									       name="_dflip[pages][<?php echo $index; ?>][url]"
									       id="dflip-page-<?php echo $index; ?>-url"
									       value="<?php echo $url; ?>"
									       class="widefat">

									<label for="dflip-page-<?php echo $index; ?>-content">
										<?php echo __DFLIP('Content'); ?>
									</label>
									<textarea rows="10" cols="40"
									          name="_dflip[pages][<?php echo $index; ?>][content]"
									          id="dflip-page-<?php echo $index; ?>-content">
										<?php echo esc_textarea($content); ?>
									</textarea>
									<?php
									if (isset($page['hotspots'])) {
										$spotindex = 0;
										foreach (
											(array)$page['hotspots'] as $spot
										) {
											?>
											<input class="dflip-hotspot-input"
											       name="_dflip[pages][<?php echo $index; ?>][hotspots][<?php echo $spotindex; ?>]"
											       value="<?php echo $spot; ?>">
											<?php
											$spotindex++;
										}
									}
									?>
								</div>
							</li>
						<?php
						}
						$index++;
					} ?>
				</ul>
			</div>
		</div>

		<!--Clear-fix-->
		<div class="dflip-box"></div>

	<?php

	}

	/**
	 * Sanitizes an array value even if not existent
	 *
	 * @since 1.0.0
	 *
	 * @param object $arr     The array to lookup
	 * @param mixed  $key     The key to look into array
	 * @param mixed  $default Default value in-case value is not found in array
	 *
	 * @return mixed appropriate value if exists else default value
	 */
	private function val($arr, $key, $default = '') {
		return isset($arr[ $key ])
			? $arr[ $key ] : $default;
	}

	/**
	 * Creates the UI for layout tab
	 *
	 * @since 1.0.0
	 *
	 * @param object $post The current post object.
	 */
	public function layout_tab($post) {
		?>

		<!--3D or 2D-->
		<div id="dflip_webgl_box" class="dflip-box">
			<label for="dflip_webgl" class="dflip-label">
				<?php echo __DFLIP('3D or 2D'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Choose the mode of display. WebGL for realistic 3d'); ?>
			</div>
			<div class="dflip-option dflip-select">
				<select name="_dflip[webgl]" id="dflip_webgl" class="">
					<?php foreach ((array)$this->fields['webgl']['choices'] as $val => $label) { ?>

						<option value="<?php echo $val; ?>"
							<?php selected($this->get_config('webgl', $post), $val); ?>>
							<?php echo $label; ?>
						</option>

					<?php } ?>
				</select>
			</div>
		</div>

		<!--Hard Pages-->
		<div id="dflip_hard_box" class="dflip-box" data-condition="dflip_webgl:is(false)">

			<label for="dflip_hard" class="dflip-label">
				<?php echo __DFLIP('Hard Pages(CSS 3D FLIP)'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Choose which pages to act as hard(3D Flip)'); ?>
			</div>

			<div class="dflip-option dflip-select">
				<select name="_dflip[hard]" id="dflip_hard" class="">
					<?php foreach ((array)$this->fields['hard']['choices'] as $val => $label) { ?>

						<option
							value="<?php echo $val; ?>"
							<?php selected($this->get_config('hard', $post), $val); ?>>
							<?php echo $label; ?>
						</option>

					<?php } ?>
				</select>
			</div>

		</div>

		<!--Background Color-->
		<div id="dflip_bg_color_box" class="dflip-box">

			<label for="dflip_bg_color" class="dflip-label">
				<?php echo __DFLIP('Background Color'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Background color in hexadecimal format eg:'); ?>
				<code>#FFF</code> or <code>#666666</code>
			</div>

			<div class="dflip-option">
				<input placeholder="Example: #ffffff" value="<?php echo $this->get_config('bg_color', $post); ?>"
				       type="text" name="_dflip[bg_color]" id="dflip_bg_color" class=""/>
			</div>

		</div>

		<!--Duration-->
		<div id="dflip_duration_box" class="dflip-box">

			<label for="dflip_duration" class="dflip-label">
				<?php echo __DFLIP('Flip Duration'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Time in milliseconds eg:'); ?>
				<code>1000</code>for 1second
			</div>

			<div class="dflip-option">
				<input placeholder="Example: 1000" value="<?php echo $this->get_config('duration', $post); ?>"
				       type="number" name="_dflip[duration]" id="dflip_duration" class=""/>
			</div>

		</div>

		<!--Height-->
		<div id="dflip_height_box" class="dflip-box">

			<label for="dflip_height" class="dflip-label">
				<?php echo __DFLIP('Container Height'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Height of the flipbook container when in normal mode'); ?>
			</div>
			<div class="dflip-option">
				<input placeholder="Example: 500" value="<?php echo $this->get_config('height', $post); ?>"
				       type="number" name="_dflip[height]" id="dflip_height" class=""/>
			</div>
		</div>

		<!--Texture Size-->
		<div id="dflip_texture_size_box" class="dflip-box" data-condition="dflip_source_type:is(pdf)">

			<label for="dflip_texture_size" class="dflip-label">
				<?php echo __DFLIP('Page Render Size'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Choose the size of image to be generated.'); ?>
			</div>

			<div class="dflip-option dflip-select">
				<select name="_dflip[texture_size]" id="dflip_texture_size" class="">
					<?php foreach ((array)$this->fields['texture_size']['choices'] as $val => $label) { ?>

						<option value="<?php echo $val; ?>"
							<?php selected($this->get_config('texture_size', $post), $val); ?>>
							<?php echo $label; ?>
						</option>

					<?php } ?>
				</select>
			</div>

		</div>
		
		<!--Page Mode-->
		<div id="dflip_page_mode_box" class="dflip-box">

			<label for="dflip_page_mode" class="dflip-label">
				<?php echo __DFLIP('Page Mode'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Choose whether you want single mode or double page mode. Recommended Auto'); ?>
			</div>

			<div class="dflip-option dflip-select">
				<select name="_dflip[page_mode]" id="dflip_page_mode" class="">
					<?php foreach ((array)$this->fields['page_mode']['choices'] as $val => $label) { ?>

						<option value="<?php echo $val; ?>"
							<?php selected($this->get_config('page_mode', $post), $val); ?>>
							<?php echo $label; ?>
						</option>

					<?php } ?>
				</select>
			</div>

		</div>

		<!--Sound-->
		<div id="dflip_auto_sound_box" class="dflip-box">

			<label for="dflip_auto_sound" class="dflip-label">
				<?php echo __DFLIP('Auto Enable Sound'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Sound will play from the start.'); ?>
			</div>

			<div class="dflip-option dflip-select">
				<select name="_dflip[auto_sound]" id="dflip_auto_sound" class="">
					<?php foreach ((array)$this->fields['auto_sound']['choices'] as $val => $label) { ?>

						<option value="<?php echo $val; ?>"
							<?php selected($this->get_config('auto_sound', $post), $val); ?>>
							<?php echo $label; ?>
						</option>

					<?php } ?>
				</select>
			</div>

		</div>

		<!--Download-->
		<div id="dflip_enable_download_box" class="dflip-box">

			<label for="dflip_enable_download" class="dflip-label">
				<?php echo __DFLIP('Enable Download'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Enable PDF download'); ?>
			</div>

			<div class="dflip-option dflip-select">
				<select name="_dflip[enable_download]" id="dflip_enable_download" class="">
					<?php foreach ((array)$this->fields['enable_download']['choices'] as $val => $label) { ?>

						<option value="<?php echo $val; ?>"
							<?php selected($this->get_config('enable_download', $post), $val); ?>>
							<?php echo $label; ?>
						</option>

					<?php } ?>
				</select>
			</div>

		</div>

		<!--Direction-->
		<div id="dflip_direction_box" class="dflip-box">

			<label for="dflip_direction" class="dflip-label">
				<?php echo __DFLIP('Direction'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Left to Right or Right to Left.'); ?>
			</div>

			<div class="dflip-option dflip-select">
				<select name="_dflip[direction]" id="dflip_direction" class="">
					<?php foreach ((array)$this->fields['direction']['choices'] as $val => $label) { ?>

						<option value="<?php echo $val; ?>"
							<?php selected($this->get_config('direction', $post), $val); ?>>
							<?php echo $label; ?>
						</option>

					<?php } ?>
				</select>
			</div>

		</div>

		<!--Clear-fix-->
		<div class="dflip-box"></div>
	<?php

	}

	/**
	 * Creates the UI for outline tab
	 *
	 * @since 1.0.0
	 *
	 * @param object $post The current post object.
	 */
	public function outline_tab($post) {

		?>

		<!--Auto Outline-->
		<div id="dflip_auto_outline_box" class="dflip-box">

			<label for="dflip_auto_outline" class="dflip-label">
				<?php echo __DFLIP('Auto Enable Outline'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Choose if outline will be auto enabled on start.'); ?>
			</div>

			<div class="dflip-option dflip-select">
				<select name="_dflip[auto_outline]" id="dflip_auto_outline" class="">
					<?php foreach ((array)$this->fields['auto_outline']['choices'] as $val => $label) { ?>

						<option value="<?php echo $val; ?>"
							<?php selected($this->get_config('auto_outline', $post), $val); ?>>
							<?php echo $label; ?>
						</option>

					<?php } ?>
				</select>
			</div>

		</div>

		<!--OverWrite Outline-->
		<div id="dflip_overwrite_outline_box" class="dflip-box"  data-condition="dflip_source_type:is(pdf)">

			<label for="dflip_overwrite_outline" class="dflip-label">
				<?php echo __DFLIP('Overwrite PDF Outline'); ?>
			</label>

			<div class="dflip-desc">
				<?php echo __DFLIP('Choose if PDF Outline will overwritten.'); ?>
			</div>

			<div class="dflip-option dflip-select">
				<select name="_dflip[overwrite_outline]" id="dflip_overwrite_outline" class="">
					<?php foreach ((array)$this->fields['overwrite_outline']['choices'] as $val => $label) { ?>

						<option value="<?php echo $val; ?>"
							<?php selected($this->get_config('overwrite_outline', $post), $val); ?>>
							<?php echo $label; ?>
						</option>

					<?php } ?>
				</select>
			</div>

		</div>

		<!--Outline/Bookmark-->
		<div id="dflip_outline_box" class="dflip-box dflip-js-code">

			<div class="dflip-desc">
				<p>
					<?php echo sprintf(
						__DFLIP('Create a tree structure bookmark/outline of your book for easy access:<br>%s'),
						'<code>	Outline Name : (destination as blank or link to url or page number)</code>'
					); ?>
				</p>
			</div>

			<div class="dflip-option dflip-textarea-simple">
				<textarea rows="8" cols="40" id="dflip_outline">
					<?php
					$outline = $this->get_config('outline', $post);
					echo json_encode($this->get_config('outline', $post));
					?>
				</textarea>
			</div>
		</div>

		<!--Clear-fix-->
		<div class="dflip-box"></div>
	<?php
	}

	/**
	 * Helper method for retrieving config values.
	 *
	 * @since 1.0.0
	 *
	 * @param string $key  The config key to retrieve.
	 * @param object $post The current post object.
	 *
	 * @return string Key value on success, empty string on failure.
	 */
	public function get_config($key, $post) {

		$values = get_post_meta($post->ID, '_dflip_data', true);
		$value = isset($values[ $key ]) ? $values[ $key ] : '';

		$default = isset($this->fields[ $key ])
			? is_array($this->fields[ $key ])
				? isset($this->fields[ $key ]['std'])
					? $this->fields[ $key ]['std']
					: ''
				: $this->fields[ $key ]
			: '';

		/* set standard value */
		if ($default !== null) {
			$value = $this->filter_std_value($value, $default);
		}

		return $value;

	}

	/**
	 * Helper function to filter standard option values.
	 *
	 * @param     mixed $value Saved string or array value
	 * @param     mixed $std   Standard string or array value
	 *
	 * @return    mixed     String or array
	 *
	 * @access    public
	 * @since     1.0.0
	 */
	public function filter_std_value($value = '', $std = '') {

		$std = maybe_unserialize($std);

		if (is_array($value) && is_array($std)) {

			foreach ($value as $k => $v) {

				if ('' == $value[ $k ] && isset($std[ $k ])) {

					$value[ $k ] = $std[ $k ];

				}

			}

		} else {
			if ('' == $value && $std !== null) {

				$value = $std;

			}
		}

		return $value;

	}

	/**
	 * Saves values from dFlip metaboxes.
	 *
	 * @since 1.0.0
	 *
	 * @param int    $post_id The current post ID.
	 * @param object $post    The current post object.
	 */
	public function save_meta_boxes($post_id, $post) {

		// Bail out if we fail a security check.
		if (!isset($_POST['dflip'])
			|| !wp_verify_nonce(
				$_POST['dflip'], 'dflip'
			)
			|| !isset($_POST['_dflip'])
		) {
			return;
		}

		// Bail out if running an autosave, ajax, cron or revision.
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}
		if (defined('DOING_AJAX') && DOING_AJAX) {
			return;
		}
		if (defined('DOING_CRON') && DOING_CRON) {
			return;
		}
		if (wp_is_post_revision($post_id)) {
			return;
		}

		// Bail if this is not the correct post type.
		if (isset($post->post_type)
			&& $this->base->plugin_slug !== $post->post_type
		) {
			return;
		}

		// Bail out if user is not authorized
		if (!current_user_can('edit_post', $post_id)) {
			return;
		}

		// Sanitize all user inputs.
		$settings = get_post_meta($post_id, '_dflip_data', true);
		if (empty($settings)) {
			$settings = array();
		}

		$data = $_POST['_dflip'];

		$settings = array_merge($settings, $data);


		$settings['outline'] = $this->array_val($settings['outline'], 'items');

		if (isset($post->post_type) && 'dflip' == $post->post_type) {
			if (empty($settings['title'])) {
				$settings['title'] = trim(strip_tags($post->post_title));
			}

			if (empty($settings['slug'])) {
				$settings['slug'] = sanitize_text_field($post->post_name);
			}
		}

		// Get publish/draft status from Post
		$settings['status'] = $post->post_status;

		// Update the post meta.
		update_post_meta($post_id, '_dflip_data', $settings);

	}

	/**
	 * Removes index of array and returns only values array
	 *
	 * @since 1.0.0
	 *
	 * @param        array Array to be sanitized
	 * @param string $scan key index that needs to be re-sanitized
	 *
	 * @return array sanitized array
	 */
	private function array_val($arr = array(), $scan = '') {

		if ($arr == null) {
			return array();
		}

		$_arr = array_values($arr);
		if ($_arr != null && $scan !== '') {
			foreach ($_arr as &$val) {
				if (is_array($val)) {
					if (isset($val[ $scan ])) {
						$val[ $scan ] = $this->array_val($val[ $scan ], $scan);
					}
				}
			}
		}

		return $_arr;

	}

	/**
	 * Returns the singleton instance of the class.
	 *
	 * @since 1.0.0
	 *
	 * @return object dFlip_PostType object.
	 */
	public static function get_instance() {

		if (!isset(self::$instance)
			&& !(self::$instance instanceof DFlip_Meta_Boxes)
		) {
			self::$instance = new DFlip_Meta_Boxes();
		}

		return self::$instance;

	}
}

// Load the DFlip_Metaboxes class.
$dflip_meta_boxes = DFlip_Meta_Boxes::get_instance();

