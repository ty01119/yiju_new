<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( !class_exists( 'CodevzFlickr' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzFlickr');
	});

	class CodevzFlickr extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Flickr', 'remix' ), 
				array( 'classname' => 'cd_flickr' )
			);
		}
		
		function form($instance) {
			$defaults = array(
				'title' => 'Flickr Photostream',
				'id' => '7388060@N08',
				'type' => 'user',
				'number' => '9',
				'shorting' => 'latest',
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			
			$title_value = esc_attr( $instance['title'] );
			$title_field = array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			);
			echo csf_add_field( $title_field, $title_value );

			$id_value = esc_attr( $instance['id'] );
			$id_field = array(
				'id'    => $this->get_field_name('id'),
				'name'  => $this->get_field_name('id'),
				'type'  => 'text',
				'title' => esc_html__('Flikr ID ( idgettr.com )', 'remix')
			);
			echo csf_add_field( $id_field, $id_value );

			$number_value = esc_attr( $instance['number'] );
			$number_field = array(
				'id'    => $this->get_field_name('number'),
				'name'  => $this->get_field_name('number'),
				'type'  => 'text',
				'title' => esc_html__('Count', 'remix')
			);
			echo csf_add_field( $number_field, $number_value );

			$type_value = esc_attr( $instance['type'] );
			$type_field = array(
				'id'    => $this->get_field_name('type'),
				'name'  => $this->get_field_name('type'),
				'type'  => 'select',
				'options' => array(
					'user' => esc_html__('User', 'remix'),
					'group' => esc_html__('Group', 'remix')
				),
				'title' => esc_html__('Type', 'remix')
			);
			echo csf_add_field( $type_field, $type_value );

			$shorting_value = esc_attr( $instance['shorting'] );
			$shorting_field = array(
				'id'    => $this->get_field_name('shorting'),
				'name'  => $this->get_field_name('shorting'),
				'type'  => 'select',
				'options' => array(
					'latest' => esc_html__('Latest Photos', 'remix'),
					'random' => esc_html__('Random', 'remix')
				),
				'title' => esc_html__('Sorting', 'remix')
			);
			echo csf_add_field( $shorting_field, $shorting_value );
		}

		function update($new_instance, $old_instance) {
			$instance = $old_instance;
			$instance['title'] = strip_tags( $new_instance['title'] );
			$instance['number'] = strip_tags( $new_instance['number'] );
			$instance['id'] = strip_tags( $new_instance['id'] );
			$instance['type'] = strip_tags( $new_instance['type'] );
			$instance['shorting'] = strip_tags( $new_instance['shorting'] );

			return $instance;
		}

		function widget( $args, $instance ) {
			extract( $args );
			$title = apply_filters( 'widget_title', $instance['title'] );
			$number = $instance['number'];
			$shorting = $instance['shorting'];
			$type = $instance['type'];
			$id = $instance['id'];
			echo $before_widget;
			echo $title ? $before_title . $title . $after_title : '';

			if ( $id ) : ?>
				<div class="flickr-widget">
					<script type="text/javascript" src="<?php echo '//www.flickr.com/badge_code_v2.gne?count=' . $number . '&amp;display=' . $shorting . '&amp;&amp;layout=x&amp;source=' . $type . '&amp;' . $type . '=' . $id . '&amp;size=s'; ?>"></script> 
				</div>
				<div class="clr"></div>
			<?php endif;

			echo $after_widget;
		}
	 
	}

}
