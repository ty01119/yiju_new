<?php

/**
 * @author Codevz
 * @link http://Codevz.com
 *
 * Events calendar add-one for visual composer
 * Shortcode name: codevz_events_table
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzEventsTable extends CodevzVc {

	function __construct() {
		add_action( 'init', array( $this, 'player' ) );
		add_shortcode( 'codevz_events_table', array( $this, 'out' ) );
	}

	function player() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'codevz_events_table',
			'name'			=> __('Event table', 'cd'), 
			'icon'			=> 'dashicons-before dashicons-calendar cd_ico',
			'params'		=> array(
				array(
					"type"        	=> "textfield",
					"heading"     	=> __("Title", 'cd'),
					'edit_field_class'=> $this->col('6'),
					"param_name"  	=> "title",
					'admin_label' 	=> true
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Count', 'cd'),
					'param_name'	=> 'count',
					'edit_field_class'=> $this->col('6'),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Class', 'cd'),
					'param_name'	=> 'extra_class',
					'edit_field_class'=> $this->col('6'),
				),
			)

		));
	}

	function out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'title'			=> '',
			'count'			=> '',
			'extra_class' 	=> '',
		), $atts );

		global $codevz;

		if ( $codevz ) {
			ob_start();

			echo '<div class="def-block '.$atts['extra_class'].'">';

			if ( !empty( $atts['title'] ) ) {
				echo '<h4 class="tt">'.$atts['title'].'</h4><span class="liner"></span>';
			}

			$query = new WP_Query(array(
				'post_type' 	=> 'events',	
				'posts_per_page' => $atts['count'],
				'order' 		=> 'ASC',
				'orderby' 		=> 'meta_value',
				'meta_key' 		=> 'cd_end_event_date',
				'meta_query' 	=> array(
					array(
						'key' 	=> 'cd_end_event_date', 
						'value' => current_time( 'Y/m/j H:i' ), 
						'type' 	=> 'DATETIME',
						'compare' => '>='
					)
				)
			));

		if ( $query->have_posts() ) : ?>

			<table class="events_table <?php echo $atts['extra_class']; ?>">
				<tbody>
					<?php 
						while ( $query->have_posts() ): $query->the_post(); 
						$meta = $codevz->meta();
						$start = isset( $meta['event_date'] ) ? strtotime( $meta['event_date'] ) : strtotime( '0' );
						$end = isset( $meta['end_event_date'] ) ? strtotime( $meta['end_event_date'] ) : strtotime( '0' );
					?>
					<tr>
						<td><h4><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h4></td>
						<td><?php echo isset( $meta['venue'] ) ? $meta['venue'] : ''; ?></td>
						<td><?php echo date_i18n( get_option('date_format') . ' ' . get_option('time_format'), $start ); ?></td>
						<td>
							<?php $artists = $codevz->get_artists( isset( $meta['artist'] ) ? $meta['artist'] : '' );
								if ( $artists ) { ?>
									<?php echo $artists; ?>
							<?php } ?>
						</td>
						<td>
							<?php $buy_link_event = isset( $meta['buy_link'] ) ? $meta['buy_link'] : ''; if ( isset( $meta['buy_title'] ) && $meta['buy_title'] ) { ?>
								<a target="_blank" href="<?php echo $buy_link_event; ?>" class="button mi mt small <?php echo $meta['class']; ?>"><?php if ( $meta['class'] === '1') { ?><i class="fa fa-ticket mi"></i><?php } ?><?php echo $meta['buy_title']; ?></a>
							<?php } ?>
						</td>
					</tr>
					<?php endwhile; ?>

				</tbody>
			</table>
		<?php 
			endif;

			echo '</div>';

			wp_reset_postdata();
			return ob_get_clean();
		} else {
			return 'This theme do not support this shortcode.';
		}
	}

}

new CodevzEventsTable();
