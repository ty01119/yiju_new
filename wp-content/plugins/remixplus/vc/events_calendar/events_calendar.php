<?php

/**
 * @author Codevz
 * @link http://Codevz.com
 *
 * Events calendar add-one for visual composer
 * Shortcode name: codevz_events_calendar
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzEventsCalendar extends CodevzVc {

	function __construct() {
		add_action( 'init', array( $this, 'player' ) );
		add_shortcode( 'codevz_events_calendar', array( $this, 'out' ) );
	}

	function player() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'codevz_events_calendar',
			'name'			=> __('Event Calendar', 'cd'), 
			'icon'			=> 'dashicons-before dashicons-calendar cd_ico',
			'params'		=> array(
				array(
					"type"        	=> "textfield",
					"heading"     	=> __("Title", 'cd'),
					'edit_field_class'=> $this->col('6'),
					"param_name"  	=> "title",
					'admin_label' 	=> true
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Class', 'cd'),
					'param_name'	=> 'extra_class',
					'edit_field_class'=> $this->col('6'),
				),
			)

		));
	}

	function out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'title'			=> '',
			'extra_class' 	=> '',
		), $atts );

		global $codevz;

		if ( $codevz ) {
			ob_start();

			echo '<div class="def-block '.$atts['extra_class'].'">';

			if ( !empty( $atts['title'] ) ) {
				echo '<h4 class="tt">'.$atts['title'].'</h4><span class="liner"></span>';
			}

			echo '<div id="codevz_calendar"></div>';
			$events = new WP_Query(array(
				'post_type'			=> 'events',
				'posts_per_page'	=> -1
			));

		?>
			<script>
				var $ = window.jQuery;
				$(document).ready(function() {
					$('#codevz_calendar').fullCalendar({
						defaultDate: '<?php echo current_time( 'Y-m-j' ); ?>',
						editable: false,
						eventLimit: true, // allow "more" link when too many events
						events: [
						<?php 
							if ( $events->have_posts() ) :
								while ( $events->have_posts() ): $events->the_post();
									$meta = $codevz->meta();

									$start = isset( $meta['event_date'] ) ? $meta['event_date'] : '1990';
									$start = date( 'Y-m-d H:i', strtotime( $start ) );
									$end = isset( $meta['end_event_date'] ) ? $meta['end_event_date'] : '';
									$end = date( 'Y-m-d H:i', strtotime( $end ) );

									if ( date( 'd', strtotime( $start ) ) === date( 'd', strtotime( $end ) ) ) {
										$end = $start;
									}
									echo '{';
									echo 'title: "' . get_the_title() . '",';
									echo 'url: "' . get_the_permalink() . '",';
									echo 'start: "' . $start . '",';
									echo 'end: "' . $end . '"';
									echo '},';
								endwhile;
							endif;
						?>
						]
					});
				});
			</script>

			<link href='<?php echo get_template_directory_uri() .'/css/fullcalendar.min.css'; ?>' rel='stylesheet' />
			<script src='<?php echo get_template_directory_uri() .'/js/fullcalendar.min.js'; ?>'></script>
		<?php 

			echo '</div>';

			wp_reset_postdata();
			return ob_get_clean();
		} else {
			return 'This theme do not support this shortcode.';
		}
	}

}

new CodevzEventsCalendar();
