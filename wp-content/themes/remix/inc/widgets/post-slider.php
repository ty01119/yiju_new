<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( !class_exists( 'CodevzPostsSlider' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzPostsSlider');
	});

	class CodevzPostsSlider extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Slideshow', 'remix' ), 
				array( 'classname' => 'cd_posts_slider' )
			);
		}

		public function widget( $args, $instance ) {
			extract( $args );
			global $post, $codevz;
			$codevz->related(array( 
				'post_type'		=> $instance['post_type'],
				'order'			=> $instance['order'],
				'orderby'		=> $instance['orderby'],
				'posts_per_page'=> $instance['count'], 
				'by' 			=> null, 
				'section_title' => apply_filters( 'widget_title', $instance['title'] )
			));
		}
		
		public function update( $new, $old ) {
			$instance = $old;
			$instance['title'] = strip_tags($new['title']);
			$instance['count'] = strip_tags($new['count']);
			$instance['order'] = strip_tags($new['order']);
			$instance['orderby'] = strip_tags($new['orderby']);
			$instance['post_type'] = $new['post_type'];

			return $instance;
		}

		public function form( $instance ) {
			$defaults = array(
				'title' 		=> 'Slideshow',
				'count' 		=> '4',
				'post_type' 	=> array( 'post' ),
				'order' 		=> 'DESC',
				'orderby' 		=> 'date',
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			
			echo csf_add_field( array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			), esc_attr( $instance['title'] ) );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('count'),
				'name'  => $this->get_field_name('count'),
				'type'  => 'number',
				'title'	=> esc_html__('Count', 'remix'),
			), esc_attr( $instance['count'] ) );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('post_type'),
				'name'  => $this->get_field_name('post_type'),
				'type'  => 'select',
                'options' => get_post_types(array('public' => true)),
                'class'   => 'chosen',
                'attributes' => array(
                  'data-placeholder' => esc_html__('Post type(s)', 'remix'),
                  'multiple' => 'multiple',
                  'style'    => 'width: 200px'
                ),
				'title' => esc_html__('Post type(s)', 'remix')
			), $instance['post_type'] );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('order'),
				'name'  => $this->get_field_name('order'),
				'type'  => 'select',
				'options' => array(
					'DESC' => 'DESC',
					'ASC' => 'ASC'
				),
				'title' => esc_html__('Order', 'remix')
			), esc_attr( $instance['order'] ) );

			echo csf_add_field( array(
				'id'    => $this->get_field_name('orderby'),
				'name'  => $this->get_field_name('orderby'),
				'type'  => 'select',
				'options' => array(
					'date' 	=> 'date',
					'title' => 'title',
					'rand' 	=> 'rand',
					'id' 	=> 'id',
					'type' 	=> 'type',
					'comment_count' => 'comment_count',
				),
				'title' => esc_html__('Orderby', 'remix')
			), esc_attr( $instance['orderby'] ) );

		}

	}

}
