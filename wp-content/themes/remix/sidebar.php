<?php 

/**
 * Sidebar
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

global $codevz;

$cpt = $codevz->get_post_type();
$p = $codevz->sidebar_p();
$s = $codevz->sidebar_s();
$l = $codevz->option( $cpt . 'layout', $codevz->option( 'layout', 'sidebar-right' ) );

if ( is_page() || is_single() && $cpt !== 'bbpress_' ) {
	$m = $codevz->meta();
	if ( isset( $m['layout'] ) && $m['layout'] !== 'inherit' ) {
		$l = $m['layout'];
	} 
}

if ( $l === 'sidebar-right' ) {
	echo '<aside class="grid_4 omega">';
	$codevz->dynamic_sidebar( $p );
	echo '</aside>';
} else if ( $l === 'sidebar-left' ) {
	echo '<aside class="grid_4 alpha">';
	$codevz->dynamic_sidebar( $p );
	echo '</aside>';
} else if ( $l === 'both-sidebar' ) {
	echo '<aside class="grid_3 righter omega">';
	$codevz->dynamic_sidebar( $s );
	echo '</aside>';
} else if ( $l === 'both-sidebar-right' ) {
	echo '<aside class="grid_3">';
	$codevz->dynamic_sidebar( $s );
	echo '</aside><aside class="grid_3 omega">';
	$codevz->dynamic_sidebar( $p );
	echo '</aside>';
} else if ( $l === 'both-sidebar-left' ) {
	echo '<aside class="grid_3 alpha">';
	$codevz->dynamic_sidebar( $p );
	echo '</aside><aside class="grid_3">';
	$codevz->dynamic_sidebar( $s );
	echo '</aside>';
}
