<?php

/**
 * @author Codevz
 * @link http://Codevz.com
 *
 * Post type posts add-one for visual composer
 * Shortcode name: codevz_cpt_posts
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzChartsPopular extends CodevzVc {

	function __construct() {
		add_action( 'init', array( $this, 'in' ) );
		add_shortcode( 'codevz_charts_popular', array( $this, 'out' ) );
	}

	function in() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'codevz_charts_popular',
			'name'			=> __( 'Charts popular', 'cd' ), 
			'icon'			=> 'dashicons-before dashicons-sort cd_ico',
			'params'		=> array(
				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Title', 'cd'),
					'param_name' 	=> 'block_title',
					'edit_field_class'=> $this->col('6'),
					'admin_label' 	=> true
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Post type', 'cd'),
					'param_name' 	=> 'post_type',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'Songs', 'cd' ) 	=> 'songs',
						__( 'Playlists', 'cd' ) => 'playlists',
						__( 'Podcasts', 'cd' ) 	=> 'podcasts',
						__( 'Videos', 'cd' ) 	=> 'videos',
					),
					'admin_label' 	=> true
				),
				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Count', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name' 	=> 'posts_per_page'
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Chart according to', 'cd'),
					'description' 	=> __('Videos just works with views count', 'cd'),
					'param_name' 	=> 'meta_key',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'Most plays', 'cd' ) 			=> 'cd_plays',
						__( 'Yesterday plays', 'cd' ) 		=> 'cd_plays_pd',
						__( 'Past week plays', 'cd' ) 		=> 'cd_plays_pw',
						__( 'Past month plays', 'cd' ) 		=> 'cd_plays_pm',
						__( 'Most downloads', 'cd' ) 		=> 'cd_downloads',
						__( 'Yesterday downloads', 'cd' ) 	=> 'cd_downloads_pd',
						__( 'Past week downloads', 'cd' ) 	=> 'cd_downloads_pw',
						__( 'Past month downloads', 'cd' ) 	=> 'cd_downloads_pm',
						__( 'Most views', 'cd' ) 			=> 'cd_views',
						__( 'Yesterday views', 'cd' ) 		=> 'cd_views_pd',
						__( 'Past week views', 'cd' ) 		=> 'cd_views_pw',
						__( 'Past month views', 'cd' ) 		=> 'cd_views_pm'
					)
				),
				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Prev rank', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name' 	=> 'prev_rank'
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Class', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'extra_class',
				), 
			)

		));
	}

	function out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'block_title' 	=> '',
			'post_type'		=> 'songs',
			'extra_class'	=> '',
			'posts_per_page'=> '',
			'prev_rank' 	=> '',
			'meta_key' 		=> 'cd_plays',
			'orderby' 		=> 'meta_value_num'
		), $atts );

		global $codevz;

		if ( $codevz ) {
			ob_start(); 

			echo '<section class="def-block popular_chart clr ' . $atts['extra_class'] . '">';
			echo $atts['block_title'] ? '<h4 class="tt">' . $atts['block_title'] . '</h4><span class="liner"></span>' : '';

			// Get old values
			if ( $atts['meta_key'] === 'cd_plays' || $atts['meta_key'] === 'cd_downloads' || $atts['meta_key'] === 'cd_views' ) {
				$oldest = (array) get_option( $atts['meta_key'] . '_until_yesterday_' . $atts['post_type'] );
			} else {
				$oldest = (array) get_option( $atts['meta_key'] . '_' . $atts['post_type'] );
			}
			arsort( $oldest );

			// Fix meta_key for new counts
			$atts['meta_key'] = str_replace( 's_p', 's_n', $atts['meta_key'] );
			$posts = new WP_Query( array_filter( $atts ) );

			if ( $posts->have_posts() ) :
				echo '<div class="clr">';
				$i = 1;
				while ( $posts->have_posts() ): $posts->the_post();
					echo '<div class="charts_item clr">';
					$ii = $is_done = 1;
					foreach ( $oldest as $prev_id => $prev_val ) {
						if ( $prev_id === $codevz->post->ID ) {
							echo '<div class="charts_pos"><b>' . $i . '</b>';
							if ( $i === $ii ) {
								echo '<i class="fa fa-chevron-right"></i>';
							} else if ( $i < $ii ) {
								echo '<i class="fa fa-chevron-up"></i>';
							} else if ( $i > $ii ) {
								echo '<i class="fa fa-chevron-down"></i>';
							}
							echo '<span class="charts_prev">' . $atts['prev_rank'] . ' ' . $ii . '</span>';
							echo '</div>';
							$codevz->loop( $atts['post_type'], implode( ' ', array( $atts['post_type'], 'grid_12' ) ), '', $atts['meta_key'] );
							$is_done = 0;
							break;
						}
						$ii++;
					}

					if ( $is_done ) {
						echo '<div class="charts_pos"><b>' . $i . '</b>';
						echo '<i class="charts_pos_new"> NEW </i>';
						echo '</div>';
						$codevz->loop( $atts['post_type'], implode( ' ', array( $atts['post_type'], 'grid_12' ) ), '', $atts['meta_key'] );
					}

					echo '<div id="fb-root"></div>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=114004138666009";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, \'script\', \'facebook-jssdk\'));</script>
						<fb:like href="' . get_the_permalink( $codevz->post->ID ) . '" send="true" layout="button_count" width="100" show_faces="true" action="like"></fb:like>';

					if ( $i === $atts['posts_per_page'] ) {
						break;
					}
				$i++;
				echo '</div>';
				endwhile;
				echo '</div>';
			else:
				echo '<h3>'.$codevz->option( 'not_found' ).'</h3>';
			endif;
			echo '</section>';
			wp_reset_postdata();
			return ob_get_clean();
		} else {
			return 'This theme do not support this shortcode.';
		}
	}
}

new CodevzChartsPopular();