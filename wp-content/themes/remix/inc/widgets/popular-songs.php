<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( !class_exists( 'CodevzPopularSongs' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzPopularSongs');
	});

	class CodevzPopularSongs extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Popular Songs', 'remix' ), 
				array( 'classname' => 'cd_popular_songs' )
			);
		}

		public function widget($args, $instance) {
			extract( $args );
			$title = apply_filters( 'widget_title', $instance['title'] );
			$out = $before_widget."\n";
			$out .= $title ? $before_title.$title.$after_title : '';
			
			ob_start();
			global $post, $codevz; 

			$orderby = $instance['posts_orderby'];

			$args = array(
				'post_type'			=> array( $instance['post_type'] ),
				'order' 			=> 'DESC',
				'posts_per_page'	=> $instance['posts_num'],
				'meta_key'			=> $orderby,
				'orderby'			=> 'meta_value_num',
				'post__in' 			=> isset( $instance['items'] ) ? $instance['items'] : null
			);

			if ( $orderby === 'date' || $orderby === 'rand' ) {
				$args['meta_key'] = null;
				$args['orderby'] = $orderby;
				$orderby = 'cd_plays';
			}

			if ( !empty( $instance['artists'] ) ) {
				$meta_query = array( 'relation' => 'OR' );
				foreach ( (array) $instance['artists'] as $key ) {
					$meta_query[] = array( 'key' => 'cd_meta', 'value' => '"' . $key . '"', 'compare' => 'LIKE' );
				}
				$args['meta_query'] = $meta_query;
			}

			$popular = new WP_Query( $args ); ?>
			
				<div class="more_posts scroll" style="height: <?php echo $instance['posts_height'] ?>px">
					<?php 
						while ( $popular->have_posts() ): $popular->the_post(); 
						$meta = $codevz->meta();
					?>
						<div class="item_small">
							<?php if ( has_post_thumbnail() ): ?>
								<a class="cdEffect noborder" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<?php the_post_thumbnail( 'tumb' ); ?>
									<i class="fa fa-<?php echo $codevz->get_icon( $instance['post_type'] ); ?>"></i>
								</a>
							<?php endif; ?>
							<div class="item-details">
								<h3><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
								<div class="post_meta">
									<span class="block"><?php echo $codevz->get_artists( isset($meta['artist']) ? $meta['artist'] : '' ); ?></span>

									<?php 
										if ( $orderby === 'cd_likes' || $orderby === 'cd_dislikes' ) :
											echo '<span><i class="fa fa-heart"></i> ' . get_post_meta($post->ID,'cd_likes', true);
											echo '&nbsp; <i class="fa fa-heart-o"></i> ' . get_post_meta($post->ID,'cd_dislikes', true) . '</span>';
										elseif ( $orderby === 'cd_downloads' ) :
											echo '<span><i class="fa fa-download mi"></i>' . get_post_meta($post->ID,'cd_downloads', true) . '</span>';
										else :
											echo '<span><i class="fa fa-play mi"></i>' . $codevz->get_plays( $post->ID, $orderby ) . '</span>';
										endif;
									?> 
								</div>
							</div>
						</div>
					<?php endwhile; wp_reset_postdata(); ?>
				</div>

			<?php
			$out .= ob_get_clean();
			$out .= $after_widget."\n";
			echo $out;
		}
		
		public function update($new,$old) {
			$instance = $old;
			$instance['title'] 			= strip_tags( $new['title'] );
			$instance['posts_num'] 		= strip_tags( $new['posts_num'] );
			$instance['posts_orderby'] 	= strip_tags( $new['posts_orderby'] );
			$instance['posts_height'] 	= strip_tags( $new['posts_height'] );
			$instance['post_type'] 		= $new['post_type'];
			$instance['items'] 			= $new['items'];
			$instance['artists'] 		= $new['artists'];
			return $instance;
		}

		public function form($instance) {
			$defaults = array(
				'title' 		=> 'Popular Songs',
				'posts_num' 	=> '20',
				'posts_orderby'	=> 'cd_plays',
				'posts_height'	=> '400',
				'post_type'		=> 'songs',
				'items'			=> null,
				'artists'		=> null
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			
			$title_value = esc_attr( $instance['title'] );
			$title_field = array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			);
			echo csf_add_field( $title_field, $title_value );

			$post_type = array(
				'id'    => $this->get_field_name('post_type'),
				'name'  => $this->get_field_name('post_type'),
				'type'  => 'select',
                'options' => array(
                	'songs'		=> esc_html__( 'Songs', 'remix' ),
                	'playlists'	=> esc_html__( 'Playlists', 'remix' ),
                	'podcasts'	=> esc_html__( 'Podcasts', 'remix' ),
                ),
				'title' => esc_html__('Post type', 'remix')
			);
			echo csf_add_field( $post_type, $instance['post_type'] );

			$posts_num_value = esc_attr( $instance['posts_num'] );
			$posts_num_field = array(
				'id'    => $this->get_field_name('posts_num'),
				'name'  => $this->get_field_name('posts_num'),
				'type'  => 'number',
				'title'	=> esc_html__('Count', 'remix'),
			);
			echo csf_add_field( $posts_num_field, $posts_num_value );

			$posts_height_value = esc_attr( $instance['posts_height'] );
			$posts_height_field = array(
				'id'    => $this->get_field_name('posts_height'),
				'name'  => $this->get_field_name('posts_height'),
				'type'  => 'number',
				'title'	=> esc_html__('Height', 'remix'),
			);
			echo csf_add_field( $posts_height_field, $posts_height_value );

			$posts_orderby_value = $instance['posts_orderby'];
			$posts_orderby_field = array(
				'id'    => $this->get_field_name('posts_orderby'),
				'name'  => $this->get_field_name('posts_orderby'),
				'type'  => 'select',
				'options' => array(
					'cd_plays' 		=> esc_html__( 'Total plays', 'remix' ),
					'cd_plays_nd' 	=> esc_html__( 'Today plays', 'remix' ),
					'cd_plays_nw' 	=> esc_html__( 'This week plays', 'remix' ),
					'cd_plays_nm' 	=> esc_html__( 'This month plays', 'remix' ),
					'cd_likes' 		=> esc_html__( 'Likes', 'remix' ),
					'cd_dislikes' 	=> esc_html__( 'Dislikes', 'remix' ),
					'cd_downloads' 	=> esc_html__( 'Total downloads', 'remix' ),
					'cd_downloads_nd' 	=> esc_html__( 'Today downloads', 'remix' ),
					'cd_downloads_nw' 	=> esc_html__( 'This week downloads', 'remix' ),
					'cd_downloads_nm' 	=> esc_html__( 'This month downloads', 'remix' ),
					'date' 			=> esc_html__( 'Recent', 'remix' ),
					'rand' 			=> esc_html__( 'Random', 'remix' ),
				),
				'title' => esc_html__('Sort by', 'remix')
			);
			echo csf_add_field( $posts_orderby_field, $posts_orderby_value );

			$artists_field = array(
				'id'    => $this->get_field_name('artists'),
				'name'  => $this->get_field_name('artists'),
				'type'  => 'autocomplete',
                'class' => 'multiple',
                'query_args'  => array(
                  'post_type' => 'artists',
                  'orderby'   => 'title',
                  'order'     => 'ASC',
                  'posts_per_page' => 20
                ),
				'title' => esc_html__('Artist(s)', 'remix')
			);
			echo csf_add_field( $artists_field, $instance['artists'] );

			$artists_field = array(
				'id'    => $this->get_field_name('items'),
				'name'  => $this->get_field_name('items'),
				'type'  => 'autocomplete',
                'class'   => 'multiple',
                'query_args'  => array(
                  'post_type' => array( 'songs', 'podcasts', 'playlists' ),
                  'orderby'   => 'title',
                  'order'     => 'ASC',
                  'posts_per_page' => 20,
                ),
				'title' => esc_html__('Show only', 'remix')
			);
			echo csf_add_field( $artists_field, $instance['items'] );
		}
	}

}
