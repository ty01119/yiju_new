<?php 

/**
 * Archive
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

get_header();

	global $codevz;
	$codevz->content_before();

	$cpt = str_replace( '_', '', $codevz->get_post_type() );
	$col = $codevz->option( $cpt . '_col', '1' );
	if ( $col === '1' ) {
		$class = 'grid_12';
	} else if ( $col === '2' ) {
		$class = 'grid_6';
	} else if ( $col === '3' ) {
		$class = 'grid_4';
	} else {
		$class = 'grid_3';
	}
	$fix_grid = ( $cpt === 'artists' || $cpt === 'gallery' || $cpt === 'lyrics' || $cpt === 'videos' || $cpt === 'podcasts' );
	$modern_gallery = ( $cpt === 'gallery' && $codevz->option( 'gallery_modern' ) );

	echo '<section class="def-block clr cpt_' . $fix_grid . ' gt_' . $modern_gallery . '">';
		$codevz->page_title();

		/* Author box */
		echo is_author() ? $codevz->author_box() : '';

		$i = 1;
		if ( have_posts() ) :
			echo '<div class="clr">'; 

			if ( $modern_gallery ) {
				echo '<div id="close" class="button small"><span><i class="fa fa-caret-left"></i></span></div>';
				echo '<ul id="tp-grid" class="tp-grid">';
			}

			while ( have_posts() ): the_post(); 

				$classes = array(
					( $i % $col === 1 ) ? 'alpha' : '',
					( $i % $col === 0 ) ? 'omega' : '',
					$cpt,
					$class
				);
				
				$cpt = is_tag() ? 'post' : $cpt;
				$codevz->loop( $cpt, implode( ' ', $classes ), $codevz->option( 'gallery_modern' ) );

				if ( $i % $col === 0 && $cpt !== 'gallery' ) {
					echo '</div><div class="clr">';
				}
				$i++;

			endwhile;
			wp_reset_postdata();

			echo $modern_gallery ? '</ul>' : '';
			echo '</div>';
			$codevz->pagination(); 
		else:
			echo '<h3>'.$codevz->option( 'not_found', 'Not found any items' ).'</h3>';
		endif;

	echo '</section>';

	$codevz->content_after();
	get_sidebar();

get_footer();