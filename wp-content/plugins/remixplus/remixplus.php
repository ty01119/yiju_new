<?php

/*
	Plugin Name: RemixPlus
	Plugin URI: http://codevz.com/
	Description: Required plugin for Remix music theme by Codevz. 
	Version: 3.3
	Author: Codevz
	License URI: http://codevz.com/license
*/

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

define( 'CD_DIR_PATH', plugin_dir_path( __FILE__ ) );

include_once CD_DIR_PATH . 'post-types.php';
include_once CD_DIR_PATH . 'metabox.php';
include_once CD_DIR_PATH . 'vc/vc.php';
include_once CD_DIR_PATH . 'importer/importer.php';
include_once CD_DIR_PATH . 'shortcodes/shortcodes.php';
