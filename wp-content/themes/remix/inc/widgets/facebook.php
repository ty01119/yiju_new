<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( !class_exists( 'CodevzFacebook' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzFacebook');
	});

	class CodevzFacebook extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Facebook', 'remix' ), 
				array( 'classname' => 'cd_facebook' )
			);
		}

		function widget($args, $instance){
			extract($args);

			$title = apply_filters('widget_title', $instance['title']);
			$head = $instance['head'] ? 'true' : 'false';
			$cover = $instance['cover'] ? 'true' : 'false';
			$faces = $instance['faces'] ? 'true' : 'false';
			$posts = $instance['posts'] ? 'true' : 'false';

			echo $before_widget;
			echo $title ? $before_title.$title.$after_title : ''; 
			echo '<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=376512092550885";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, \'script\', \'facebook-jssdk\'));</script>';
			?>
				<div class="fb-page" 
					data-href="<?php echo esc_url($instance['url']); ?>" 
					data-small-header="<?php echo $head; ?>" 
					data-adapt-container-width="true" 
					data-hide-cover="<?php echo $cover; ?>" 
					data-hide-cta="false" 
					data-show-facepile="<?php echo $faces; ?>" 
					data-show-posts="<?php echo $posts; ?>">
				</div>
			<?php 
			if (empty($instance['url'])){ echo 'Please insert correct facebook url page.'; }
			echo $after_widget;
		}
		
		function update($new_instance, $old_instance){
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['url'] = $new_instance['url'];
			$instance['head'] = $new_instance['head'];
			$instance['cover'] = $new_instance['cover'];
			$instance['posts'] = $new_instance['posts'];
			$instance['faces'] = $new_instance['faces'];

			return $instance;
		}

		function form($instance){
			$defaults = array('title' => 'Like us on Facebook', 'url' => '', 'head' => false, 'cover' => false, 'posts' => true, 'faces' => true);
			$instance = wp_parse_args((array) $instance, $defaults); 
			
			$title_value = esc_attr( $instance['title'] );
			$title_field = array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			);
			echo csf_add_field( $title_field, $title_value );

			$url_value = esc_attr( $instance['url'] );
			$url_field = array(
				'id'    => $this->get_field_name('url'),
				'name'  => $this->get_field_name('url'),
				'type'  => 'text',
				'title' => esc_html__('Page URL', 'remix')
			);
			echo csf_add_field( $url_field, $url_value );

			$head_value = esc_attr( $instance['head'] );
			$head_field = array(
				'id'    => $this->get_field_name('head'),
				'name'  => $this->get_field_name('head'),
				'type'  => 'switcher',
				'default' => false,
				'title' => esc_html__('Use Small Header', 'remix')
			);
			echo csf_add_field( $head_field, $head_value );

			$cover_value = esc_attr( $instance['cover'] );
			$cover_field = array(
				'id'    => $this->get_field_name('cover'),
				'name'  => $this->get_field_name('cover'),
				'type'  => 'switcher',
				'default' => false,
				'title' => esc_html__('Hide Cover Photo', 'remix')
			);
			echo csf_add_field( $cover_field, $cover_value );

			$posts_value = esc_attr( $instance['posts'] );
			$posts_field = array(
				'id'    => $this->get_field_name('posts'),
				'name'  => $this->get_field_name('posts'),
				'type'  => 'switcher',
				'default' => true,
				'title' => esc_html__('Show Page Posts', 'remix')
			);
			echo csf_add_field( $posts_field, $posts_value );

			$faces_value = esc_attr( $instance['faces'] );
			$faces_field = array(
				'id'    => $this->get_field_name('faces'),
				'name'  => $this->get_field_name('faces'),
				'type'  => 'switcher',
				'default' => true,
				'title' => esc_html__('Show Friends Faces', 'remix')
			);
			echo csf_add_field( $faces_field, $faces_value );
		}
	}

}
