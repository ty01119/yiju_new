<?php
/* ------------------------------------------------------------------------- *
 *  Custom functions
/* ------------------------------------------------------------------------- */
	
	// Add your custom functions here, or overwrite existing ones. Read more how to use:
	// http://codex.wordpress.org/Child_Themes
include_once('custom-ajax-auth.php');
function addScripts(){
  wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . 
    '/assets/javascript/addition.js', array( 'jquery' ), '1.0', true );
  wp_enqueue_script('sequence-script', get_stylesheet_directory_uri() . 
    '/assets/javascript/sequence.min.js', array( 'jquery' ), '1.0', true );
  wp_enqueue_script('move-script', '//stephband.info/jquery.event.move/js/jquery.event.move.js', 
    array( 'jquery' ), '1.0', true );
  wp_enqueue_script('swipe-script', '//stephband.info/jquery.event.swipe/js/jquery.event.swipe.js', 
    array( 'jquery' ), '1.0', true );
}

add_action( 'wp_enqueue_scripts', 'addScripts');
add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
add_action( 'wp_ajax_nopriv_ajaxregister', 'ajax_register' );

function loadRandomPost($categorySlug = '', $limit = 1, 
  $isFirstTime = 1, $position, 
  $postType = array('post,investments,cptnews,nzlife,services'), 
  $beginCol = 8, $numPerRow = 1, $isshow = 'isshow'){
  $html = '';
  if($isshow == "featured_image"){
    $args = array(
      'numberposts' => $limit,
      'category_name' => $categorySlug,
      'post_type' => $postType, 
      'post_status' => 'publish'
    );
  }
  elseif ($isshow == "isshow") {
    $args = array(
      'numberposts' => $limit,
      'category_name' => $categorySlug,
      'post_type' => $postType, 
      'post_status' => 'publish',
      'meta_query' => array(
        'relation' => 'AND',
        array(
          'key' => $isshow,
          'value' => '1',
          'compare' => '='
        ),
        array(
          'key' => $position,
          'value' => '',
          'compare' => '!='
        ),
        array(
          'key' => 'imgposition',
          'value' => $position,
          'compare' => '='
        ),
      )
    );
  }
  elseif ($isshow == "recommend_top"){
    $args = array(
      'numberposts' => $limit,
      'category_name' => $categorySlug,
      'post_type' => $postType, 
      'post_status' => 'publish',
      'meta_query' => array(
        'relation' => 'AND',
        array(
          'key' => $isshow,
          'value' => '1',
          'compare' => '='
        ),
        array(
          'key' => $position,
          'value' => '',
          'compare' => '!='
        ),
        array(
          'key' => 'recommend_position',
          'value' => $position,
          'compare' => '='
        ),
      )
    );
  }
  else{
    $args = array(
      'numberposts' => $limit,
      'category_name' => $categorySlug,
      'post_type' => $postType, 
      'post_status' => 'publish',
      'meta_query' => array(
        array(
          'key' => $position,
          'value' => '',
          'compare' => '!='
        )
      )
    );
  }

  
  // var_dump($args);
  if($categorySlug != ''){
    $cat = get_term_by( 'slug', $categorySlug, 'category');
  }

  $posts = null;
  if($isFirstTime){
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    $posts = get_posts($args);
  }
  else{
    $args['orderby'] = 'rand';
    $posts = get_posts($args);
  }
  $leftCol = 12-intval($beginCol);
  $divCol = 12/intval($numPerRow);
  // var_dump($posts);
  foreach ($posts as $post) {
    $html .= "<div class=\"rand-single-post"; 
    $html .= ($categorySlug != '' ? " rand-single-post-$categorySlug " : ' '); 
    $html .= "vc_row wpb_row vc_inner vc_row-fluid grid_$divCol\">";
    $html .= "<div class=\"container\">";
      $html .= "<div class=\"rand-post-thumbnail wpb_column grid_$beginCol\">";
      $html .= "<a href=\"" . get_permalink($post) . "\">";
        if($isshow == "featured_image"){
          $html .= "<img src=\"" . 
            get_the_post_thumbnail_url($post, 'full') . "\">";
        }else{
          $html .= "<img src=\"";
            $html .= get_field($position,$post->ID) ? get_field($position,$post->ID) : "http://test.multisupport.co.nz/wp-content/uploads/2017/01/Group-25.png";
          $html .= "\">";
        }
      $html .="</a>";
      $html .= "</div>";

      $html .= "<div class=\"rand-post-content wpb_column grid_$leftCol\">";
      if(isset($cat)){
        $html .= "<div class=\"rand-category-name\">";
          $html .= "<h3>" . $cat->name . "</h3>";
        $html .= "</div>";
      }
        $html .= "<div class=\"rand-post-title\">";
          $html .= "<p><a class=\"no-ajax\" href=\"" . 
            get_permalink($post) . "\">" . $post->post_title . "</a></p>";
        $html .= "</div>";
        $html .= "<div class=\"rand-post-excerpt\">";
          $html .= "<p>" . $post->post_excerpt . "</p>";
        $html .= "</div>";
        $html .= "<div class=\"rand-post-date-and-author\">";
          $html .= "<p>" . get_the_date('Y.m.d',$post) . "&nbsp;";
          $html .= get_the_author_meta('display_name', $post->post_author) . "</p>";
        $html .= "</div>";
      $html .= "</div>";
    $html .= "</div>";
    $html .= "</div>";
  }
  return $html;
}

function msLoadRandomPostShortCode($atts = [], $content = null, $tag = ''){
  $limit = $atts['limit'];
  $isFirstTime = $atts['is_first_time'];
  $position = $atts['position'];
  $postType = explode("," , $atts['post_type']);
  $beginCol = isset($atts['begin_col']) ? $atts['begin_col'] : 8;
  $numPerRow = isset($atts['num_per_row']) ? $atts['num_per_row'] : 1;
  $isshow = isset($atts['show_position']) ? $atts['show_position'] : 'isshow';
  $html = '';
  if(isset($atts['category_slugs']))
  {
      $categorySlugs = explode("," , $atts['category_slugs']);
      foreach($categorySlugs as $categorySlug){
        $html .= loadRandomPost($categorySlug, $limit, $isFirstTime, 
          $position, $postType, $beginCol, $numPerRow, $isshow);
      }
  }else{
    $html .= loadRandomPost( '', $limit, $isFirstTime, $position, 
      $postType, $beginCol, $numPerRow, $isshow);
  }

  return $html;
}
function msLoadRandomPostAjax(){
  if(isset($_REQUEST)){
    $limit = $_REQUEST['limit'];
    $isFirstTime = $_REQUEST['is_first_time'];
    $position = $_REQUEST['position'];
    $postType = explode("," , $_REQUEST['post_type']);
    $beginCol = isset($_REQUEST['begin_col']) ? $_REQUEST['begin_col'] : 4;
    $numPerRow = isset($_REQUEST['num_per_row']) ? $_REQUEST['num_per_row'] : 1;
    $isshow = isset($_REQUEST['show_position']) ? $_REQUEST['show_position'] : 'isshow';
    $html = '';
    if(isset($_REQUEST['category_slugs'])){
      $categorySlugs = explode(",", $_REQUEST['category_slugs']);
      foreach($categorySlugs as $categorySlug){
        $html .= loadRandomPost($categorySlug, $limit, $isFirstTime, 
          $position, $postType, $beginCol, $numPerRow, $isshow);
      }
    }else{
      $html .= loadRandomPost( '', $limit, $isFirstTime, $position, 
      $postType, $beginCol, $numPerRow, $isshow);
    }
    
    echo $html;
    die();
  }
}

function loadPostSlider($categorySlug = '', 
  $limit = 1, $position, 
  $postType = array('post,investments,cptnews,nzlife,services'), 
  $isshow = 'isshow'){
  $html = '';
  $args = array(
    'numberposts' => $limit,
    'category_name' => $categorySlug,
    'post_type' => $postType, 
    'post_status' => 'publish',
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'key' => $isshow,
        'value' => '1',
        'compare' => '='
      ),
      array(
        'key' => $position,
        'value' => '',
        'compare' => '!='
      )
    )
  );
  if($categorySlug != ''){
    $cat = get_term_by( 'slug', $categorySlug, 'category');
  }

  $posts = null;
  $args['orderby'] = 'date';
  $args['order'] = 'DESC';
  $posts = get_posts($args);
  $html .= "<div class=\"ms-slider seq " . $position . "\" id=\"". $position . "\">";
    $html .= "<div class=\"seq-screen\">";
      $html .= "<ul class=\"seq-canvas\">";
      foreach($posts as $post){
        $html .= "<li>";
          $html .= "<div class=\"slider-thumbnail\">";
            $html .= "<a class=\"no-ajax\" href=\"" . get_permalink($post) . "\">";
              $html .= "<img src=\"" . get_field($position, $post->ID) . "\">";
            $html .= "</a>";
          $html .= "</div>"; 
          $html .= "<div class=\"slider-title\">";
            $html .= "<p>" . $post->post_title . "</p>";
          $html .= "</div>";
          if(get_field("project_addr", $post->ID)){
          $html .= "<div class=\"slider-address\">";
            $html .= "<p>地址： " . get_field("project_addr", $post->ID) . "</p>";
          $html .= "</div>";
          }
        $html .= "</li>";
      }
      $html .= "</ul>";
    $html .= "</div>";
  $html .= "</div>";
  return $html;
}

/*
loadPostSlider
$categorySlug = ''
$limit = 1
$position
$postType = array(
  'post,
  investments,
  cptnews,
  nzlife,
  services')
*/
function msLoadPostSliderShortcode($atts = [], $content = null, $tag = ''){
  $limit = $atts['limit'];
  $position = $atts['position'];
  $postType = explode("," , $atts['post_type']);
  $isshow = isset($atts['show_position']) ? $atts['show_position'] : 'isshow';
  $html = '';
  if(isset($atts['category_slugs']))
  {
      $categorySlugs = explode("," , $atts['category_slugs']);
      foreach($categorySlugs as $categorySlug){
        $html .= loadPostSlider($categorySlug, $limit, $position, $postType, $isshow);
      }
  }else{
    $html .= loadPostSlider( '', $limit, $position, $postType, $isshow);
  }

  return $html;
}
function my_facetwp_pager_html( $output, $params ) {
    // $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    var_dump($output);
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page no-ajax" data-page="' . ($page - 1) . '">Previous</a>';
    }
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page no-ajax" data-page="' . ($page + 1) . '">Next</a>';
    }
    return $output;
}

//* REMOVE DASHBOARD WIDGETS
function remove_dashboard_meta() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}

function loadFacetwpSearch($position){
  $html = '';
  if($position == "nzlife"){
    $html = facetwp_display( 'facet', 'nzlifesearch' );
    $html .= '<a id="'. $position . '-search-button" class="no-ajax" href="#" ><i class="fa fa-search fa-3x" aria-hidden="true"></i></a>';
  }
  elseif($position == "home"){
    $html = facetwp_display( 'facet', 'homesearch' );
    $html .= '<a id="'. $position . '-search-button" class="no-ajax" href="#" ><i class="fa fa-search fa-3x" aria-hidden="true"></i></a>';
  }
  elseif($position == "propertynews"){
    $html = facetwp_display( 'facet', 'propertynewssearch' );
    $html .= '<a id="'. $position . '-search-button" class="no-ajax" href="#" ><i class="fa fa-search fa-3x" aria-hidden="true"></i></a>';
  }
  return $html;
}

function msLoadFacetwpSearchShortcode($atts = [], $content = null, $tag = ''){
  $position = $atts['position'];
  $html = '';
  $html = loadFacetwpSearch($position);
  return $html;
}

add_shortcode('loadRandomPost', 'msLoadRandomPostShortCode');
add_action('wp_ajax_ms_load_random_post_ajax', 'msLoadRandomPostAjax');
add_action('wp_ajax_nopriv_ms_load_random_post_ajax', 'msLoadRandomPostAjax');
add_shortcode('loadPostSlider', 'msLoadPostSliderShortcode');
add_shortcode('loadFacetwpSearch', 'msLoadFacetwpSearchShortcode');
add_filter( 'facetwp_facet_dropdown_show_counts', '__return_false' );
add_filter( 'facetwp_facet_fselect_show_counts', '__return_false' );
add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );
//sadd_action( 'admin_init', 'remove_dashboard_meta' );

//增强编辑器开始

function add_editor_buttons($buttons) {

$buttons[] = 'fontselect';

$buttons[] = 'fontsizeselect';

$buttons[] = 'cleanup';

$buttons[] = 'styleselect';

$buttons[] = 'hr';

$buttons[] = 'del';

$buttons[] = 'sub';

$buttons[] = 'sup';

$buttons[] = 'copy';

$buttons[] = 'paste';

$buttons[] = 'cut';

$buttons[] = 'undo';

$buttons[] = 'image';

$buttons[] = 'anchor';

$buttons[] = 'backcolor';

$buttons[] = 'wp_page';

$buttons[] = 'charmap';

return $buttons;

}

add_filter("mce_buttons_3", "add_editor_buttons");

//增强编辑器结束