<?php 

/**
 * Single page
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

	get_header();
	global $codevz;
	$codevz->content_before();
	$meta = $codevz->meta();
	$cpt = get_post_type();
	$show = array_flip( $codevz->option( $cpt . '_meta', array() ) );
	$related_grid = $codevz->option( $cpt . '_related_grid' ) ? 'related_grid' : '';
?>

<article id="<?php echo get_the_ID(); ?>" <?php post_class( 'mbf clr def-block' ); ?>>
	<?php 
		/* Content */
		while ( have_posts() ): the_post();		
	?>
	<div class="vc_row">
		<div class="content">
			<div class="vc_row ad-zone">
				<?php echo do_shortcode("[pro_ad_display_adzone id=1595]"); ?>
			</div>
			<div class="vc_row cptnews-content">
				<div class="grid_8 content">
					<div class="meta">
						<p class="title"><strong><?php the_title() ?></strong></p>
						<hr>
						<p>
							<span class="time"><?php the_time('Y.m.d'); ?></span>&nbsp;&nbsp;
							<span class="author"><?php the_author() ;?></span>
							<span class="tags-wrap"><span class="tag-label">标签：</span><span class="tags"><?php the_tags('','、'); ?></span></span>
						</p>
					</div>
					<div class="details">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="grid_4 sidebar">
					<div class="vc_row ad-zone">
						<p><?php echo do_shortcode("[pro_ad_display_adzone id=1595]"); ?></p>
					</div>
					<div class="vc_row related-article">
						<div class="label-wrap">
							<p class="color-white related-article-label"><strong>相关文章</strong></p>
						</div>
						<div class="related-article-content">
							<?php
								$tags = get_the_tags();
								// var_dump($tags);
								if ($tags) {
									$tag_ids = array();
									foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
									// var_dump($tag_ids);
									$args=array(
                    'numberposts' => 10,
                    'post_type' => "cptnews",
                    'post_status' => 'publish',
                    'exclude' => get_the_ID(),
                    'tag__in' => $tag_ids

									);
                  // var_dump($args);
                  // echo "<br><br>";
                  $relatedPosts = get_posts($args);
                  // var_dump($relatedPosts);
                  foreach ($relatedPosts as $post) {
                    echo "<p><a href=\"" . get_permalink($post) . "\">" . get_the_title($post) . "</a></p>";
                  }
								}
								?>
						</div>
            <div class="vc_row ad-zone">
              <p><?php echo do_shortcode("[pro_ad_display_adzone id=1595]"); ?></p>
              <div class="clearfix-sm"></div>
              <p><?php echo do_shortcode("[pro_ad_display_adzone id=1595]"); ?></p>
            </div>
					</div>
					<div class="vc_row">
						
					</div>
					
				</div>
			</div>
			<div class="vc_row">
				<div class="clearfix">
					<hr class="hr-home hr-home-interest">
				</div>
				<div class="vc_row">
					<?php
						echo do_shortcode('[loadRandomPost limit=3 is_first_time=0 position="null" post_type="nzlife" begin_col="12" num_per_row="3" show_position="featured_image"]');
					?>
				</div>
			</div>
      <div class="vc_row">
        <p><?php echo do_shortcode("[pro_ad_display_adzone id=1595]"); ?></p>
      </div>
		</div>
	</div>
	<?php 
		endwhile;
	?>
	
</article>

<?php 


	/* After all */
	$codevz->comments();
	$codevz->content_after();

	get_sidebar();
	get_footer(); 
