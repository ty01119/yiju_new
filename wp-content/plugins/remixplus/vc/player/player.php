<?php

/**
 * @author Codevz
 * @link http://Codevz.com
 *
 * Music Player add-one for visual composer
 * Shortcode name: T20_player
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzPlayer extends CodevzVc {

	function __construct() {
		add_action( 'init', array( $this, 'player' ) );
		add_action( 'init', array( $this, 'tracks' ) );
		add_action( 'init', array( $this, 'tracks_existed' ) );
		add_shortcode( 'T20_player', array( $this, 'player_out' ) );
		add_shortcode( 'T20_player_tracks', array( $this, 'tracks_out' ) );
		add_shortcode( 'T20_tracks_existed', array( $this, 'tracks_existed_out' ) );
	}

	function player() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'T20_player',
			'name'			=> __('Music Player', 'cd'), 
			'icon'			=> 'dashicons-before dashicons-playlist-audio cd_ico',
			'as_parent'		=> array( 'only' => 'T20_player_tracks, T20_tracks_existed' ), 
			'js_view'		=> 'VcColumnView',
			'params'		=> array(
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Auto play', 'cd'),
					'param_name' 	=> 'autoplay',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'True', 'cd' ) => 'true',
						__( 'False', 'cd' ) => 'false'
					)
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Repeat', 'cd'),
					'param_name' 	=> 'repeat',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'True', 'cd' ) => 'true',
						__( 'False', 'cd' ) => 'false'
					)
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Show cover', 'cd'),
					'param_name' 	=> 'showcover',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'True', 'cd' ) => 'true',
						__( 'False', 'cd' ) => 'false'
					)
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Player inside block', 'cd'),
					'param_name' 	=> 'def-block',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'True', 'cd' ) => 'no-def',
						__( 'False', 'cd' ) => 'def-block'
					)
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Class', 'cd'),
					'param_name'	=> 'extra_class',
					'edit_field_class'=> $this->col('6'),
				),
			)

		));
	}

	function tracks() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'T20_player_tracks',
			'name'			=> __('New track', 'cd'), 
			'icon'			=> 'dashicons-before dashicons-format-audio cd_ico',
			"content_element" => true,
			"as_child"		=> array( 'only' => 'T20_player' ),
			'params'		=> array(
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Type', 'cd'),
					'param_name' 	=> 'type',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'MP3', 'cd' ) 		=> 'mp3',
						__( 'Shoutcast', 'cd' ) => 'shoutcast',
						__( 'Icecast', 'cd' ) 	=> 'icecast',
						__( 'Other Radio\'s', 'cd' ) => 'other',
					)
				),
				array(
					"type"        => "textfield",
					"heading"     => __( "Title", 'cd' ),
					'edit_field_class'=> $this->col('6'),
					"param_name"  => "title",
					"admin_label" => true
				),
				array(
					"type"        => "attach_image",
					"heading"     => __("Cover", 'cd'),
					'edit_field_class'=> $this->col('6'),
					"param_name"  => "cover"
				),
				array(
					"type"        => "textfield",
					"heading"     => __("Artist(s)", 'cd'),
					"description" => 'ID(s) eg. 31,22,45',
					"param_name"  => "artists",
					'edit_field_class'=> $this->col('6')
				),
				array(
					"type"        	=> "textfield",
					"heading"    	=> __("Custom artists", 'cd'),
					'edit_field_class'=> $this->col('12'),
					"param_name"  	=> "artist",
					"admin_label" 	=> true
				),
				array(
					"type"        	=> "cs_upload",
					"heading"     	=> __("MP3 or Radio stream URL", 'cd'),
					'edit_field_class'=> $this->col('12'),
					"param_name"  	=> "mp3"
				),
				array(
					"type"        	=> "textfield",
					"heading"     	=> __("Radio history URL", 'cd'),
					'edit_field_class'=> $this->col('12'),
					"param_name"  	=> "history"
				),
				array(
					"type"		=> "dropdown",
					"heading"     => __("Icon 1", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"	=> "buy_icon_a",
					"value"		=> array(
						__("Download", 'cd') => 'fa fa-download',
						__("Shopping Cart", 'cd') => 'fa fa-shopping-cart',
						__("Apple itunes", 'cd') => 'fa fa-apple',
						__("Credit Card", 'cd') => 'fa fa-credit-card',
						__("Soundcloud", 'cd') => 'fa fa-soundcloud',
						__("Spotify", 'cd') => 'fa fa-spotify',
						__("Reverbnation", 'cd') => 'fa fa-star',
						__("Youtube", 'cd') => 'fa fa-youtube-play',
						__("Chain", 'cd') => 'fa fa-chain'
					)
				),
				array(
					"type"        => "textfield",
					"heading"     => __("Title", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "buy_title_a"
				), 
				array(
					"type"        => "textfield",
					"heading"     => __("Link", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "buy_link_a"
				),
				array(
					"type"		=> "dropdown",
					"heading"     => __("Icon 2", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"	=> "buy_icon_b",
					"value"		=> array(
						__("Download", 'cd') => 'fa fa-download',
						__("Shopping Cart", 'cd') => 'fa fa-shopping-cart',
						__("Apple itunes", 'cd') => 'fa fa-apple',
						__("Credit Card", 'cd') => 'fa fa-credit-card',
						__("Soundcloud", 'cd') => 'fa fa-soundcloud',
						__("Spotify", 'cd') => 'fa fa-spotify',
						__("Reverbnation", 'cd') => 'fa fa-star',
						__("Youtube", 'cd') => 'fa fa-youtube-play',
						__("Chain", 'cd') => 'fa fa-chain'
					)
				), 
				array(
					"type"        => "textfield",
					"heading"     => __("Title", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "buy_title_b"
				),
				array(
					"type"        => "textfield",
					"heading"     => __("Link", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "buy_link_b"
				),
				array(
					"type"			=> "dropdown",
					"heading"   	=> __("Icon 3", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"	=> "buy_icon_c",
					"value"		=> array(
						__("Download", 'cd') => 'fa fa-download',
						__("Shopping Cart", 'cd') => 'fa fa-shopping-cart',
						__("Apple itunes", 'cd') => 'fa fa-apple',
						__("Credit Card", 'cd') => 'fa fa-credit-card',
						__("Soundcloud", 'cd') => 'fa fa-soundcloud',
						__("Spotify", 'cd') => 'fa fa-spotify',
						__("Reverbnation", 'cd') => 'fa fa-star',
						__("Youtube", 'cd') => 'fa fa-youtube-play',
						__("Chain", 'cd') => 'fa fa-chain'
					)
				),
				array(
					"type"        => "textfield",
					"heading"     => __("Title", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "buy_title_c"
				), 
				array(
					"type"        => "textfield",
					"heading"     => __("Link", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "buy_link_c"
				),
				array(
					"type"		=> "dropdown",
					"heading"     => __("Icon 4", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"	=> "buy_icon_d",
					"value"		=> array(
						__("Download", 'cd') => 'fa fa-download',
						__("Shopping Cart", 'cd') => 'fa fa-shopping-cart',
						__("Apple itunes", 'cd') => 'fa fa-apple',
						__("Credit Card", 'cd') => 'fa fa-credit-card',
						__("Soundcloud", 'cd') => 'fa fa-soundcloud',
						__("Spotify", 'cd') => 'fa fa-spotify',
						__("Reverbnation", 'cd') => 'fa fa-star',
						__("Youtube", 'cd') => 'fa fa-youtube-play',
						__("Chain", 'cd') => 'fa fa-chain'
					)
				), 
				array(
					"type"        => "textfield",
					"heading"     => __("Title", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "buy_title_d"
				),
				array(
					"type"        => "textfield",
					"heading"     => __("Link", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "buy_link_d"
				), 
				array(
					"type"        => "textfield",
					"heading"     => __("Lyric ID", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "lyric"
				),
				array(
					"type"        => "textfield",
					"heading"     => __("Info title", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "info_title"
				), 
				array(
					"type"        => "textfield",
					"heading"     => __("Info popup", 'cd'),
					'edit_field_class'=> $this->col('4'),
					"param_name"  => "info_content"
				),
				array(
					"type"        => "textarea_raw_html",
					"heading"     => __("Custom code (Will display in icons area)", 'cd'),
					'edit_field_class'=> $this->col('12'),
					"param_name"  => "buy_custom"
				),
			)

		));
	}

	function tracks_existed() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'T20_tracks_existed',
			'name'			=> __( 'Existed tracks', 'cd' ), 
			'icon'			=> 'dashicons-before dashicons-format-audio cd_ico',
			"content_element" => true,
			"as_child"		=> array( 'only' => 'T20_player' ),
			'params'		=> array(
				array(
					"type"        => "textfield",
					"heading"     => __( "Track ID", 'cd' ),
					"param_name"  => "track",
					"admin_label" => true
				),
			)

		));
	}

	function player_out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'autoplay'		=> 'true',
			'repeat'		=> 'true',
			'showcover' 	=> 'true',
			'def-block' 	=> 'no-def',
			'extra_class' 	=> '',
		), $atts );

		global $codevz, $post;

		if ( $codevz ) {
			ob_start(); 

			$atts['tracks'] = array();

			$existed = array_filter( explode( 'CODEVZ ', do_shortcode( $content ) ) );
			foreach ( (array) $existed as $id ) {
				if ( ctype_digit( $id ) ) {
					$meta = $codevz->meta( $id );
					$atts['tracks'][ $id ]['track'] = $id;
					$atts['tracks'][ $id ]['track_type'] = 'old';
				} else {
					$atts['tracks'] = wp_parse_args( array( (array) json_decode( $id ) ), $atts['tracks'] );
				}
			}

			$atts['player_cover'] = filter_var( $atts['showcover'], FILTER_VALIDATE_BOOLEAN );
			$atts['class'] = 'vc';
			$codevz->player( $post->ID, $atts );
			
			return ob_get_clean();
		} else {
			return 'This theme do not support this shortcode.';
		}
	}

	function tracks_out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'type' 			=> 'mp3',
			'title' 		=> '',
			'cover' 		=> '',
			'lyric'			=> '',
			'mp3' 			=> '',
			'history' 		=> '',
			'artists' 		=> '',
			'artist' 		=> '',
			'extra_class' 	=> '',
			'info_title'	=> '',
			'info_content'	=> '',
			'buy_title_a'	=> '',
			'buy_icon_a'	=> 'fa fa-download',
			'buy_link_a'	=> '',
			'buy_title_b'	=> '',
			'buy_icon_b'	=> 'fa fa-download',
			'buy_link_b'	=> '',
			'buy_title_c'	=> '',
			'buy_icon_c'	=> 'fa fa-download',
			'buy_link_c'	=> '',
			'buy_title_d'	=> '',
			'buy_icon_d'	=> 'fa fa-download',
			'buy_link_d'	=> '',
			'buy_custom' 	=> ''
		), $atts );

		global $codevz;

		if ( $codevz ) {

			$atts['poster_big'] = $codevz->get_attachment( '', $atts['cover'], 'full' );
			$atts['poster'] = $codevz->get_attachment( '', $atts['cover'], $codevz->option( 'player_cover', 'player_cover' ) );
			$atts['track_type'] = 'new';

			// Fix from v2.x to v3
			$atts['custom_artist'] = $atts['artist'];
			$atts['artist'] = empty( $atts['artists'] ) ? '' : explode( ',', $atts['artists'] );

			$atts['buy_custom'] = urldecode( base64_decode( $atts['buy_custom'] ) );

			return json_encode( $atts ) . 'CODEVZ ';
		} else {
			return 'This theme do not support this shortcode.';
		}
	}

	function tracks_existed_out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'track' => ''
		), $atts );

		global $codevz;

		if ( $codevz ) {
			return $atts['track'] . 'CODEVZ ';
		} else {
			return 'This theme do not support this shortcode.';
		}
	}

}

class WPBakeryShortCode_T20_player extends WPBakeryShortCodesContainer {}
class WPBakeryShortCode_T20_player_tracks extends WPBakeryShortCode {}
class WPBakeryShortCode_T20_tracks_existed extends WPBakeryShortCode {}

new CodevzPlayer();
