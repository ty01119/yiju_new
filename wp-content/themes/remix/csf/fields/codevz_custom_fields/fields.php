<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
/**
 *
 * Field: Slider
 *
 * @since 1.0.0
 * @version 1.0.0
 *
 */
if( ! class_exists( 'CSF_Field_slider' ) ) {
  class CSF_Field_slider extends CSF_Fields {

    public function __construct( $field, $value = '', $unique = '', $where = '' ) {
      parent::__construct( $field, $value, $unique, $where );
    }

    public function output(){

      $options = array(
          'step'  => ( empty( $this->field['options']['step'] ) ) ? 1 : $this->field['options']['step'],
          'unit'  => ( empty( $this->field['options']['unit'] ) ) ? '' : $this->field['options']['unit'],
          'min'   => ( empty( $this->field['options']['min'] ) ) ? 0 : $this->field['options']['min'],
          'max'   => ( empty( $this->field['options']['max'] ) ) ? 200 : $this->field['options']['max']
      );

      echo '<style>
.csf-field-slider .csf-fieldset {
  display: table
}
.csf-slider {
  display: table-cell;
  vertical-align: middle;
  width: 76%;
  padding: 0 5% 0 0
}
.csf-field-slider input {
  display: table-cell;
  vertical-align: middle;
  width: 100%
}
.csf-field-slider .ui-slider {
  position: relative;
  width: 100%;
  height: 2px;
  background: #d2d2d2
}
.csf-field-slider .ui-slider-range {
  background: #111;
  height: 2px
}
.csf-field-slider .ui-slider-handle {
  height: 10px;
  width: 10px;
  background: #fff;
  border: 2px solid #111;
  position: absolute;
  top: -6px;
  border-radius: 100%
}
      </style>';

      echo $this->element_before();
      echo '<div class="csf-slider"><div></div></div>';
      echo '<input data-slider=\'' . json_encode( $options ) . '\' type="'. $this->element_type() .'" name="'. $this->element_name() .'" value="'. $this->element_value() .'"'. $this->element_class() . $this->element_attributes() .'/>';
      echo $this->element_after();

    }

  }
}




class CSF_Field_autocomplete extends CSF_Fields {

  public function __construct( $field, $value = '', $unique = '' ) {
    parent::__construct( $field, $value, $unique );
  }

  public function output() {

    echo '<style>
.cs-autocomplete {position: relative}
.cs-autocomplete > input {width: 340px;max-width: 100%}
.cs-autocomplete > i {position: absolute;top: 8px;margin-left: -24px}
.widget .cs-autocomplete > i {right: 10px}
.cs-autocomplete .ajax_items {background: #f9f9f9;border: 1px solid #e6e6e6;position: absolute;z-index: 9}
.cs-autocomplete .ajax_items div {cursor: pointer;line-height: 22px;padding: 4px 10px;border-bottom: 1px dotted #d8d8d8}
.cs-autocomplete .ajax_items b {line-height: 22px;padding: 4px 10px;border-bottom: 1px dotted #d8d8d8}
.cs-autocomplete .ajax_items div:hover {background: #fff}
.cs-autocomplete .selected_items {margin: 10px 0 0}
.cs-autocomplete .selected_items div {position: relative;display: inline-block;margin: 0 4px 4px 0}
.cs-autocomplete .selected_items input {display: none;}
.cs-autocomplete .selected_items span {display: block;padding: 6px 10px;background: #f3f3f3;border-radius: 4px}
.cs-autocomplete .selected_items i {cursor: pointer;margin: 0 0 0 10px}
</style>';

  $value = $this->element_value();
  $query = $values = '';

    if ( isset( $this->field['query_args'] ) ) {
      $query = json_encode(array(
        'action'    => 'codevz_autocomplete', 
        'query_args'  => $this->field['query_args'],
        'elm_name'    => $this->element_name()
      ));
    }

    if ( ! empty( $value ) ) {
      if ( is_array( $value ) ) {
        foreach ( $value as $id ) {
          $values .= empty( $id ) ? '' : '<div id="' . $id . '"><input name="' . $this->element_name() . '[]" value="' . $id . '" /><span> ' . get_the_title( $id ) . '<i class="fa fa-remove"></i></span></div>';
        }
      } else {
        $values .= '<div id="' . $value . '"><input name="' . $this->element_name() . '" value="' . $value . '" /><span> ' . get_the_title( $value ) . '<i class="fa fa-remove"></i></span></div>';
      }
    }

    echo $this->element_before();
    echo '<div class="cs-autocomplete" data-query=\'' . $query . '\'>';
    echo '<input type="text"'. $this->element_class() . $this->element_attributes() .' />';
    echo '<i class="fa fa-codevz"></i>';
    echo '<div class="ajax_items"></div>';
    echo '<div class="selected_items">' . $values . '</div>';
    echo '</div>';
    echo $this->element_after();

  }

}

add_action( 'wp_ajax_codevz_autocomplete', 'codevz_autocomplete' );
function codevz_autocomplete() {

  if ( empty( $_GET['query_args'] ) || empty( $_GET['s'] ) ) {
    echo '<b>Query is empty ...</b>';
    die();
  }

  $out = array();
  ob_start();

  $query = new WP_Query( wp_parse_args( $_GET['query_args'], array( 's' => $_GET['s'] ) ) );
  if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
      $query->the_post();
      echo '<div data-id="' . get_the_ID() . '">' . get_the_title() . '</div>';
    }
  } else {
    echo '<b>Not found</b>';
  }

  echo ob_get_clean();
  wp_reset_postdata();
  die();
}

class CSF_Field_DateTimePicker extends CSF_Fields {

  public function __construct( $field, $value = '', $unique = '' ) {
    parent::__construct( $field, $value, $unique );
  }

  public function output(){

    echo $this->element_before();
    echo '<script type="text/javascript" src="'.CD_URI.'/csf/fields/codevz_custom_fields/jquery.simple-dtpicker.js"></script>';
    echo '<link type="text/css" href="'.CD_URI.'/csf/fields/codevz_custom_fields/jquery.simple-dtpicker.css" rel="stylesheet" />';
    echo '<input class="datetimepicker" type="'. $this->element_type() .'" name="'. $this->element_name() .'" value="'. $this->element_value() .'"'. $this->element_class() . $this->element_attributes() .'/>';
    echo '
    <script type="text/javascript">
      var $ = window.jQuery;
      $(function(){
        $(".datetimepicker").each(function(){
          $( this ).appendDtpicker({
            "dateFormat": "YYYY/MM/DD h:m",
            "minuteInterval": 15,
            "autodateOnStart": false
          });
        });
      });
    </script>
    ';
    echo $this->element_after();

  }

}