<?php

/**
 * Visual Composer add-ones
 * 
 * @package RemixPlus
 * @author Codevz
 * @link http://Codevz.com
 * 
 * Required: visual composer, cs-framework and Remix music theme
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

if ( function_exists( 'vc_map' ) ) {

	class CodevzVc {

		public function __construct() {

			/* New fields */
			vc_add_shortcode_param( 
				'cs_upload', 
				array( $this, 'cs_upload' ), 
				get_template_directory_uri() . '/csf/assets/js/csf.js'
			);

			/* unused */
			$vcr = array(
				'accordion', 'tour', 'wp_search', 'wp_meta',
				'wp_recentcomments', 'wp_calendar', 'wp_pages', 'wp_tagcloud',
				'wp_text', 'wp_posts', 'wp_links', 'wp_categories',
				'wp_archives', 'wp_rss', 'cta_button2', 'posts_grid'
			);
			foreach ($vcr as $i) {
				vc_remove_element('vc_'.$i);
			}

		}
		
		public function cat() {
			return __('RemixPlus', 'cd');
		}

		public function WP_Query() {
			return __('WP_Query', 'cd');
		}

		public function col( $i ) {
			return 'vc_col-xs-'.$i.' vc_column-with-padding';
		}

		function cs_upload( $settings, $value ) {
			$field = array(
				'id'    => esc_attr( $settings['param_name'] ),
				'name'  => esc_attr( $settings['param_name'] ),
				'type'  => 'upload',
				'title' => '',
				'attributes' => array(
					'class' => 'csf-onload wpb_vc_param_value '.esc_attr( $settings['param_name'] ) .' '. esc_attr( $settings['type'] ).''
				),
				'settings'   => array(
					'upload_type'  => 'audio/mpeg',
					'frame_title'  => 'Upload / Select',
					'insert_title' => 'Insert',
				),
			);

			if ( function_exists('csf_add_field') ) {
				return '<div class="csf-onload">' . csf_add_field( $field, $value ) . '</div><style>.vc_ui-panel-content-container .csf-field{padding: 0}</style>';
			} else {
				return '<div class="my_param_block">'
					.'<input name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value wpb-textinput ' .
					esc_attr( $settings['param_name'] ) . ' ' .
					esc_attr( $settings['type'] ) . '_field" type="text" value="' . esc_attr( $value ) . '" />' .
					'</div>';
			}
		}

	}

	new CodevzVc();

	/* Include add-ones */
	foreach ( glob( dirname( __FILE__ ) . '/*/*.php' ) as $file ) {
		include_once $file;
	}

}
