<?php

/**
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( ! class_exists( 'CodevzEventTimer' ) ) {

	add_action( 'widgets_init', function() {
		register_widget('CodevzEventTimer');
	});

	class CodevzEventTimer extends WP_Widget {

		function __construct() {
			parent::__construct(
				false, 
				esc_html__( 'CD - Event timer', 'remix' ), 
				array( 'classname' => 'cd_event_timer' )
			);
		}

		function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'title' => '','pid_timer' => '' ) );

			$title_field = array(
				'id'    => $this->get_field_name('title'),
				'name'  => $this->get_field_name('title'),
				'type'  => 'text',
				'title' => esc_html__('Title', 'remix')
			);
			echo csf_add_field( $title_field, esc_attr( $instance['title'] ) );

			$pid_timer_field = array(
				'id'    => $this->get_field_name('pid_timer'),
				'name'  => $this->get_field_name('pid_timer'),
				'type'  => 'autocomplete',
                'class' => 'multiple',
                'query_args'  => array(
                  'post_type' => 'events',
                  'orderby'   => 'title',
                  'order'     => 'ASC',
                  'posts_per_page' => 20
                ),
				'title' => esc_html__('Events', 'remix'),
				'desc' => esc_html__('Leave empty for auto detection', 'remix')
			);
			echo csf_add_field( $pid_timer_field, $instance['pid_timer'] );
		}
	 
		function update( $new, $old ) {
			$instance = $old;
			$instance['title'] = strip_tags( $new['title'] );
			$instance['pid_timer'] = $new['pid_timer'];
			
			return $instance;
		}
	 
		function widget( $args, $instance ) {
			extract( $args );
			$title = apply_filters( 'widget_title', $instance['title'] );
			$out = $before_widget."\n";
			$out .= $title ? $before_title.$title.$after_title : '';
			ob_start();

			$args = array(
				'post_type' 		=> 'events',
				'posts_per_page'	=> -1,
				'post__in' 			=> $instance['pid_timer'],
				'order'			=> 'ASC',
				'orderby'		=> 'meta_value',
				'meta_key'		=> 'cd_end_event_date',
				'meta_query' 	=> array(
					array(
						'key'		=> 'cd_end_event_date',
						'value' 	=> current_time('Y/m/d H:i'),
						'type' 		=> 'DATETIME',
						'compare' 	=> '>='
					)
				),
			);

			$query = new WP_Query( $args );

			global $codevz;
			if ( $query->have_posts() ) :
				echo '<div class="clr">';
				while ( $query->have_posts() ) : $query->the_post();
					$meta = $codevz->meta();
					$data = isset( $meta['end_event_date'] ) ? $meta['end_event_date'] : '';
					if ( ! $data ) {
						continue;
					}

					echo '<div class="mb clr">';
						$codevz->countdown( $data, 'fll' );
					echo '</div>';
					echo '<h5><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h5>';
					$meta['buy_title'] = empty( $meta['buy_title'] ) ? '' : $meta['buy_title'];
					$meta['buy_link'] = empty( $meta['buy_link'] ) ? '' : $meta['buy_link'];
					$meta['class'] = empty( $meta['class'] ) ? '' : $meta['class'];
					if ( $meta['buy_title'] !== '' ) { ?><a target="_blank" href="<?php echo $meta['buy_link']; ?>" class="button small <?php echo $meta['class']; ?>"><?php if ( $meta['class'] === '1') { ?><i class="fa fa-ticket mi"></i><?php } ?><?php echo $meta['buy_title']; ?></a><?php }

					break;
				endwhile;
				echo '</div>';
			endif;
			wp_reset_query();

			$out .= ob_get_clean();
			$out .= $after_widget."\n";
			echo $out;

		}

	}

}
