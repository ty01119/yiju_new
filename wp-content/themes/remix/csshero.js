/**
 * CSS Hero compatiblity
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

function csshero_theme_declarations(){
    'use strict';

    csshero_declare_item( 'body', 'Body' );
    csshero_declare_item( 'p', 'Paragraphs' );
    csshero_declare_item( '.full', 'Fullwide layout' );
    csshero_declare_item( '.boxed', 'Boxed layout' );
    csshero_declare_item( '.boxed-margin', 'Boxed margin layout' );
    csshero_declare_item( '.row', 'Row\'s' );
    csshero_declare_item( '#header', 'Header' );
    csshero_declare_item( '.little-head', 'Extra header bar' );
    csshero_declare_item( 'header .popup_link', 'Logged in user avatar' );
    csshero_declare_item( '.popup', 'Popup box' );
    csshero_declare_item( '.popup .def-block', 'Popup inner' );
    csshero_declare_item( '.cart_head', 'Shop cart' );
    csshero_declare_item( 'header .social', 'Scoial area' );
    csshero_declare_item( 'header .social a', 'Scoial links' );
    csshero_declare_item( 'header .social a i', 'Scoial icons' );
    csshero_declare_item( 'header .search', 'Search area' );
    csshero_declare_item( 'header .search input', 'Search input' );
    csshero_declare_item( 'header .search i', 'Search icons' );
    csshero_declare_item( '.ajax_search_results, .post_types', 'Search ajax area' );
    csshero_declare_item( '.ONsticky', 'Sticky' );
    csshero_declare_item( 'header .sf-menu', 'Menu area' );
    csshero_declare_item( 'header .sf-menu li', 'Menu li' );
    csshero_declare_item( 'header .sf-menu li a', 'Menu li a' );
    csshero_declare_item( '.sf-menu > li:hover > a, .sf-menu li > a:hover, .sf-menu > .selectedLava > a, .page-template-page-onepage .sf-menu > .selectedLava > a', 'Menu li a hover and active' );
    csshero_declare_item( '.sf-menu .back .left', 'Menu active border' );
    csshero_declare_item( 'header .sf-menu li li', 'Menu li li' );
    csshero_declare_item( 'header .sf-menu li li a', 'Menu li li a' );
    csshero_declare_item( '.sf-menu li li a:hover, .sf-menu .current-menu-parent .current_page_item > a, .sf-menu li .current_page_parent > a', 'Menu li li a hove and active ' );
    csshero_declare_item( '.logo', 'Logo' );
    csshero_declare_item( '.jp-playlist ', 'Playlist' );
    csshero_declare_item( '.jp-play i, .jp-pause i', 'Play Pause' );
    csshero_declare_item( '.rotating', 'Pause border' );
    csshero_declare_item( '.jp-previous, .jp-next', 'Next prev' );
    csshero_declare_item( '.jp-progress', 'Progress bar' );
    csshero_declare_item( '.jp-play-bar', 'Play bar' );
    csshero_declare_item( '.jp-mute, .jp-unmute, .jp-repeat, .jp-repeat-off, .popup_share .fa, .player .fa-bars', 'Mute, Repeat, etc.' );
    csshero_declare_item( '.jp-playlist .jp-playlist-item', 'Playlist item' );
    csshero_declare_item( '.jp-playlist .jp-playlist-item:hover, .jp-playlist li.played a', 'Playlist item active' );
    csshero_declare_item( 'article, .def-block, .wpb_flickr_widget, .vc_carousel, .wpb_video_widget', 'Box' );
    csshero_declare_item( 'h4.tt, h3.tt, .widget_gardengeneralposts h4, .list-custom-taxonomy-widget h4', 'Box title' );
    csshero_declare_item( '.liner', 'Title line' );
    csshero_declare_item( 'h1', 'H1' );
    csshero_declare_item( 'h2', 'H2' );
    csshero_declare_item( 'h3', 'H3' );
    csshero_declare_item( 'h4', 'H4' );
    csshero_declare_item( 'h5', 'H5' );
    csshero_declare_item( 'h6', 'H6' );
    csshero_declare_item( 'hr', 'HR' );
    csshero_declare_item( '.countdown', 'Countdown' );
    csshero_declare_item( '.widget .countdown li', 'Countdown li' );
    csshero_declare_item( '.countdown li span', 'Countdown span' );
    csshero_declare_item( '.button, .page-numbers a, .page-numbers span, .pagination a, .pagination > b, .widget_product_search #searchsubmit, .post-password-form input[type="submit"], .wpcf7-submit, .submit_user, #commentform #submit', 'Buttons' );
    csshero_declare_item( '.footer-last', 'Sub footer' );
    csshero_declare_item( '.copyright', 'Copyright' );
    csshero_declare_item( '#toTop', 'Totop button' );
    csshero_declare_item( '.foot-menu', 'Footer menu' );
    csshero_declare_item( 'input[type="text"], textarea', 'Input and textarea' );
    csshero_declare_item( '.cdEffect', 'Effect' );
    csshero_declare_item( '.cdEffect img', 'Effect img' );
    csshero_declare_item( '.cdEffect i', 'Effect i' );
    csshero_declare_item( '.cdEffect:before, .cdEffect:after', 'Effect overlay' );
    csshero_declare_item( '.tagcloud a', 'Tags' );
    csshero_declare_item( '.Alphabet li a', 'Alphabet item' );
    csshero_declare_item( '.Alphabet li a:hover, .Alphabet li a:hover, .Alphabet li.current a', 'Alphabet item active hover' );
    csshero_declare_item( '.wtext a', 'Tabs nav items' );
    csshero_declare_item( '.wtext a.active, .wtext a:hover', 'Tabs nav items hover active' );
    csshero_declare_item( 'table', 'Table' );
    csshero_declare_item( 'table td', 'td' );
    csshero_declare_item( 'table tr', 'tr' );
    csshero_declare_item( 'table th', 'th' );
    csshero_declare_item( 'table tfoot td', 'footer td' );
    csshero_declare_item( 'table th, table caption', 'caption & th' );
}

