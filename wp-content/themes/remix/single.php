<?php 

/**
 * Single page
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

	get_header();
	global $codevz;
	$codevz->content_before();

	$meta = $codevz->meta();
	$cpt = get_post_type();
	$show = array_flip( $codevz->option( $cpt . '_meta', array() ) );
	$related_grid = $codevz->option( $cpt . '_related_grid' ) ? 'related_grid' : '';
?>

<article id="<?php echo get_the_ID(); ?>" <?php post_class( 'mbf clr def-block' ); ?>>
	<?php 
		/* Likes & title */
		echo '<div class="clr">';
			$codevz->get_likes_dislikes(array( 
				$codevz->option( $cpt . '_like' ), 
				$codevz->option( $cpt . '_dislike' )
			));

			$codevz->page_title();
		echo '</div>';

		/* Content */
		while ( have_posts() ): the_post();		
			
			/* featured image */
			if ( has_post_thumbnail() && isset( $show['image'] ) ) { ?>
				<div class="post_img">
					<?php 
						the_post_thumbnail( 'full' );
						$cap = $codevz->get_attachment( 'caption' );
						echo $cap ? '<p>' . $cap . '</p>' : '';
					?>
				</div>
				<?php echo '<p>' . $codevz->get_attachment( 'description' ) . '</p>'; ?>
			<?php }
			
				/* Events */
				if ( $cpt === 'events' ) {
					echo '<div class="mb vc_timer_m clr">';
						if ( isset( $meta['buy_title'] ) && $meta['buy_title'] ) {
							echo '<a itemprop="url" target="_blank" href="'.$meta['buy_link'].'" class="button large flr mt ' . $meta['class'] . '" data-class="'.$meta['class'].'"><i class="fa fa-ticket mi"></i>'.$meta['buy_title'].'</a>';
						}
						$codevz->countdown( $meta['end_event_date'], 'fll' );
					echo '</div>';
				}
			
				/* Music player */
				if ( $cpt === 'songs' || $cpt === 'playlists' || $cpt === 'podcasts' ) {
					$codevz->player( $post->ID );
				}
			
				/* Videos */
				if ( $cpt === 'videos' && isset( $meta['video_type'] ) ) {
					$vid_type = $meta['video_type'];
					if ( $vid_type === 'youtube' && isset( $meta['youtube'] ) ) {
						echo '<' . 'iframe' . ' class="cd_iframe" width="640" height="400" src="//youtube.com/embed/' . $meta['youtube'] . '" allowfullscreen></' . 'iframe' . '>';
					} else if ( $vid_type === 'vimeo' && isset( $meta['vimeo'] ) ) {
						echo '<' . 'iframe' . ' class="cd_iframe" width="640" height="400" src="//player.vimeo.com/video/' . $meta['vimeo'] . '" allowfullscreen></' . 'iframe' . '>';
					} else if ( $vid_type === 'mp4' && isset( $meta['mp4'] ) ) {
						echo do_shortcode( '[video width="640" height="400" mp4="' . $meta['mp4'] . '"][/video]' );
					} else {
						echo $meta['embed'];
					}
					echo !empty( $meta['artist'] ) ? '<i class="fa fa-user mi"></i>' . $codevz->get_artists( $meta['artist'] ) . '&nbsp;&nbsp;&nbsp;&nbsp;' : '';
					echo '<i class="fa fa-eye mi"></i>' . number_format_i18n( (int) get_post_meta( $post->ID, 'cd_views', true ) ) . '<br /><br />';
				}
			?>

			<div class="mb" itemprop="description"><?php the_content(); ?></div>

			<?php 
				/* Artists */
				if ( $cpt === 'artists' ) {
					if ( ! empty( $meta['fb'] ) ) {
						echo '<div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=376512092550885";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, \'script\', \'facebook-jssdk\'));</script>';
						echo '<div class="mbt fb-page" data-href="' . $meta['fb'] . '" data-tabs="timeline" data-width="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>';
					}
					if ( isset( $meta['social'] ) ) {
						echo '<div class="clr mb">';
						$codevz->social( $meta['social'] );
						echo '</div>';
					}
				} 

				/* Gallery */
				if ( $cpt === 'gallery' ) {
					echo '<ul class="clr mb">';
					$images = isset( $meta['gallery'] ) ? explode( ',', $meta['gallery'] ) : array();
					foreach( $images as $id ) {
						if ( $id ) { ?>
							<li class="grid_3 mb">
								<a class="cdEffect" data-sub-html="<?php echo $codevz->get_attachment( 'caption', $id ); ?>" href="<?php echo wp_get_attachment_image_src( $id, 'large' )[0]; ?>">
									<img src="<?php echo wp_get_attachment_image_src( $id, 'cover' )[0]; ?>" width="auto" height="auto" />
									<i class="fa fa-expand"></i>
									<h3><small>&nbsp;<?php echo $codevz->get_attachment( 'caption', $id ); ?></small></h3>
								</a>
							</li>
						<?php
						}
					}
					echo '</ul>';
				} 

				/* Related to: lyrics */
				if ( $cpt === 'lyrics' && !empty( $meta['tracks'] ) ) {
					echo '<h5>Related to:</h5><ul class="clr mb list">';
					foreach ( $meta['tracks'] as $key ) {
						echo '<li><a href="' . get_the_permalink( $key ) . '">' . get_the_title( $key ) . '</a></li>';
					}
					echo '</ul>';
				}

				wp_link_pages( array(
					'before'=>'<div class="pagination mt mb clr">', 
					'after'=>'</div>', 
					'link_after'=>'</b>', 
					'link_before'=>'<b>'
				)); 
			?>
				<div class="clr">
					<?php if ( isset( $show['author'] ) ) { ?>
						<span class="post_author mt mid"> <a rel="author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 25 ); ?> <?php echo get_the_author(); ?></a> </span>
					<?php } ?>
					<?php if ( isset( $show['date'] ) ) { ?>
						<span class="post_date mt mid"> <i class="fa fa-clock-o mi"></i><a href="<?php echo get_month_link(get_the_time('Y'), get_the_time('m')); ?>" rel="date"><?php the_time('j M, Y'); ?></a> </span>
					<?php } 
						if ( isset( $show['cats'] ) ) {
							ob_start();
							echo ( $cpt === 'post' ) ? the_category( ', ' ) : get_the_term_list( $post->ID, $cpt . '_cat', '', ', ' );
							$cats = ob_get_clean();
							echo $cats ? '<span class="post_cats mt mid"><i class="fa fa-folder-open mi"></i>'. $cats .'</span>' : '';
						}
					?>
				</div>
			<?php 
				/* Tags */
				if ( isset( $show['tags'] ) ) {
					the_tags('<p class="tagcloud mt">','','</p>');
				}
		endwhile;
	?>
</article>

<?php 
	echo isset( $show['author_box'] ) ? $codevz->author_box() : '';
	echo isset( $show['next_prev'] ) ? $codevz->next_prev_item() : '';

	/* Related items artist page */
	foreach ( get_post_types( '', 'names' ) as $ptype ) {
		$artists_related = array_flip( $codevz->option( 'artists_related_cpt', array() ) );
		if ( $cpt === 'artists' && isset( $artists_related[ $ptype ] ) ) {
			$codevz->related( array( 
				'addClass'		=> $related_grid,
				'post_type' 	=> $ptype, 
				'by' 			=> ( $ptype === 'artists' ) ? $codevz->option( 'artists_related_by' ) : 'artists', 
				'posts_per_page'=> $codevz->option( 'artists_related' ), 
				'section_title' => ( $ptype === 'artists' ) ? $codevz->option( 'related_title' ) : ucwords( $ptype )
			) );
		}
	}

	/* Related items */
	if ( $cpt !== 'artists' && $codevz->option( $cpt . '_related' ) ) {
		$codevz->related( array( 
			'addClass'		=> $related_grid,
			'posts_per_page'=> $codevz->option( $cpt . '_related' ), 
			'by' 			=> $codevz->option( $cpt . '_related_by' ), 
			'section_title' => $codevz->option( 'related_title' )
		) );	
	}

	/* After all */
	$codevz->comments();
	$codevz->content_after();

	get_sidebar();
	get_footer(); 
