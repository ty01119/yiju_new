<?php 

/**
 * index
 * 
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

	get_header();
	global $codevz;
	$codevz->content_before();

	echo '<section class="def-block clr">';

	if ( is_404() ) {
		echo $codevz->option('404');
	} else if ( have_posts() ) {
		$codevz->page_title();
	
		while ( have_posts() ) {
			the_post();
			$codevz->loop( 'post' );
		}
		$codevz->pagination();
	} else {
		$codevz->page_title();
		echo '<h3>' . $codevz->option( 'not_found' ) . '</h3>';
	}
	echo '</section>';

	$codevz->content_after();
	get_sidebar();
	get_footer(); 
?>