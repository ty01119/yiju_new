<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/> -->
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Oswald" type="text/css" media="all" />
	<?php wp_head(); ?>
</head>
<?php 
	global $codevz;

	/* Body attrs */
	$attrs = ' data-ajax="'.admin_url( 'admin-ajax.php' ).'" data-theme="'.CD_URI.'"';
	$body_class = array(
		( (int) $codevz->option( 'scrollbar' ) === 2 ) ? 'scroll' : '',
		$codevz->option( 'isGlass' ) ? 'isGlass' : ''
	);
?>

<body <?php body_class( implode( ' ', $body_class ) ); echo $attrs; ?>>
	<div class="login-modal">
	    <div class="modal-tabs">
	        <div class="register-tab"><a href="#">注册</a></div>
	        <div class="login-tab"><a href="#">登录</a></div>
	    </div>
	    <div class="register-container">
	        <form id="register" class="ajax-auth"  action="register" method="post">
	            <p class="status"></p>
	            <?php wp_nonce_field('ajax-register-nonce', 'signonsecurity'); ?>         
	            <input id="signonname" type="text" name="signonname" class="required" placeholder="姓名/name">
	            <input id="email" type="text" class="required email" name="email" placeholder="邮箱/Email">
	            <input id="signonpassword" type="password" class="required" name="signonpassword" placeholder="密码/Password">
	            <input type="password" id="password2" class="required" name="password2" placeholder="确认密码/Confirm Password">
	            <input class="submit_button" type="submit" value="提交">   
	            <p class="status"></p>
	        </form>
	    </div>
	    <div class="login-container">
	        <form id="login" class="ajax-auth" action="login" method="post">
	        	<?php wp_nonce_field('ajax-login-nonce', 'security'); ?>  
	            <input id="username" type="text" class="required" name="username" placeholder="邮箱/Email">
	            <input id="password" type="password" class="required" name="password" placeholder="密码/Password">
	            <a class="text-link" href="<?php echo wp_lostpassword_url(); ?>">忘记密码</a>
	        	<p class="status"></p>  
	            <input class="submit_button" type="submit" value="提交">
	        </form>
	    </div>
    	<a class="close" href=""><i class="fa fa-times fa-2x" aria-hidden="true"></i></a>
	</div>
	<?php if ( $codevz->option( 'pageloader' ) ) {
		$pbg = $codevz->option( 'pageloader_bg', array() );
		echo '<style>.pageloader {
				background-image: url(' . isset( $pbg['image'] ) ? $pbg['image'] : '' . ') !important;
				background-color: ' . isset( $pbg['color'] ) ? $pbg['color'] : 'inherit' . ' !important;
				background-repeat: ' . isset( $pbg['repeat'] ) ? $pbg['repeat'] : 'repeat' . ' !important;
				background-position: ' . isset( $pbg['position'] ) ? $pbg['position'] : 'left top' . ' !important;
				background-attachment: ' . isset( $pbg['attachment'] ) ? $pbg['attachment'] : 'scroll' . ' !important;
		}</style>';
		echo '
			<div class="pageloader" data-time="' . $codevz->option( 'pageloader_time' ) . '">
				<img src="' . $codevz->option( 'pageloader_img' ) . '" width="auto" height="auto" />
			</div>
		';
	} ?>

	<div id="layout" class="<?php echo $codevz->option( 'full_boxed', 'full' ); ?>">

		<?php 
			/* Options */
			$cpt 	= $codevz->get_post_type();
			$shead 	= $GLOBALS['sfoot'] = 1;
			$header = $codevz->option( 'header', '1' );
			$type 	= $codevz->option( $cpt . 'cover', '1' );

			$cpt 	= ( $type === '1' ) ? '' : $cpt;
			$type 	= empty( $cpt ) ? $codevz->option( 'cover', '1' ) : $type;
			$img 	= $codevz->option( $cpt . 'img' );
			$master = $codevz->option( $cpt . 'master' );
			$rev 	= $codevz->option( $cpt . 'rev' );
			$pos 	= $codevz->option( $cpt . 'cover_position', 0 );
			$show_alphabet = 0;
			
			/* Reset cpt */
			$cpt = $codevz->get_post_type();
			$br = $codevz->option( $cpt . 'breadcrumbs' );

			/* Singular */
			if ( is_page() || is_single() && $cpt !== 'bbpress_' ) {
				$meta = $codevz->meta();
				if ( isset( $meta['cover'] ) && $meta['cover'] !== '1' ) {
					$type 	= $meta['cover'];
					$img 	= $meta['img'];
					$master = $meta['master'];
					$rev 	= $meta['rev'];
					$pos 	= $meta['cover_position'];
				}

				$hmeta = empty( $meta['hiding_elms'] ) ? array() : array_flip( $meta['hiding_elms'] );
				$shead = !isset( $hmeta['header'] );
				$br = !isset( $hmeta['breadcrumbs'] );
				$GLOBALS['sfoot'] = !isset( $hmeta['footer'] );
				$show_alphabet = isset( $meta['show_alphabet'] ) ? $meta['show_alphabet'] : 0;
			}

			/* Header */
			$codevz->hook( 'before_header' );
			ob_start(); ?>
				<header id="header">
					<div class="row clr">
						<div class="headdown clr<?php echo $codevz->option( 'sticky' ) ? ' is_sticky' : ''; echo $codevz->option( 'sticky_smart' ) ? '' : ' smart_off'; ?>">

							<div class="row clr">
								<div class="logo is_logo top">
									Y<span style="color: #FF8900;">I</span>J<span style="color: #FF8900;">U</span>&nbsp;NZ
								</div>
								<?php 
									echo $codevz->option( 'sticky' ) ? '<div class="row clr">' : '';
								?>
								
								<nav id="mobile">
									<?php $codevz->menu( 'primary', ( $codevz->option( 'fullscreen_menu' ) ? 'sf-menu fullscreen_menu' : 'sf-menu' ) ); ?>
									<?php if(is_user_logged_in()){ 
										$current_user = wp_get_current_user();
									?>
										<span class="popup-link"><?php echo $current_user->display_name ?>，你好！/ <a href="<?php echo wp_logout_url( get_permalink() ); ?>">登出</a></span>
									<?php }else{?>
										<span class="popup-link"><a href="#" id="popup_signup">注册</a> 
										/ <a href="#" id="popup_login">登录</a></span>
									<?php }?>
								</nav>
								<?php echo $codevz->option( 'sticky' ) ? '</div>' : ''; ?>
							</div>
						</div><!--/headdown -->

					</div><!--/row -->
				</header>
      <div id="arrows" class="arrows">
        <a id="arrow-prev" class="arrow"><i class="fa fa-5x fa-chevron-left"></i><div id="arrow-prev-text"></div></a>
        <a id="arrow-next" class="arrow"><i class="fa fa-5x fa-chevron-right"></i><div id="arrow-next-text"></div></a>
      </div>
			<?php 
			$head_out = ob_get_clean();
			echo $shead ? $head_out : '';
			$codevz->hook( 'after_header' );

			/* Cover */
			$type = (int) $type;
			if ( $type === 3 ) {
				echo '<div class="page_cover under_header'. ( $pos ? ' after_header' : '' ) . '"><img src="'.$img.'" alt="Header" width="auto" height="auto" /></div>';
			} elseif ( $type === 4 && class_exists( 'RevSliderFront' ) ) {
				echo '<div class="page_cover '. ( $pos ? '' : ' overlay_header' ) . '">';
				echo putRevSlider( $rev );
				echo '</div>';
			} elseif ( $type === 5 && class_exists( 'Master_Slider' ) ) {
				echo '<div class="page_cover '. ( $pos ? '' : ' overlay_header' ) . '">';
				echo masterslider( $master );
				echo '</div>';
			}

			/* Start content */
			echo "<div class=\"row\">";
			echo '<div id="page-content">';

				echo '<div class="page-content">';

				/* Breadcrumbs */
				if ( $br && ! is_front_page() ) { 
					echo '<div class="row clr mb">';
						echo '<div class="breadcrumbIn" itemprop="breadcrumb">';
							$codevz->breadcrumbs();
						echo '</div>';
					echo '</div>';
				} 

				/* Alphabet list */
				$alphabet = array_flip( (array) $codevz->option( $cpt . 'alphabet' ) );
				if ( isset( $alphabet[ $codevz->get_page_type() ] ) || $show_alphabet ) {
					$codevz->alphabet_list();
				}

				echo '<div class="row clr">';
