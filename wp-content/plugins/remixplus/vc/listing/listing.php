<?php

/**
 * @author Codevz
 * @link http://Codevz.com
 *
 * Listing posts add-one for visual composer
 * Shortcode name: news
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzListing extends CodevzVc {

	function __construct() {
		add_action( 'init', array( $this, 'in' ) );
		add_shortcode( 'news', array( $this, 'out' ) );
	}

	function in() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'news',
			'name'			=> __('Listing', 'cd'), 
			'icon'			=> 'dashicons-before dashicons-list-view cd_ico',
			'params'		=> array(
				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Title', 'cd'),
					'param_name' 	=> 'news_title',
					'edit_field_class'=> $this->col('6'),
					'admin_label' 	=> true
				), 
				array(
					'type'			=> 'dropdown',
					'heading'		=> __('Ajax', 'cd'),
					'param_name'	=> 'ajax_style',
					'edit_field_class'=> $this->col('6'),
					'value'			=> array( 'button', 'scroll', 'none' )
				), 
				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Button title', 'cd'),
					'value'			=> 'Load More',
					'edit_field_class'=> $this->col('6'),
					'param_name' 	=> 'button_label'
				),
				array(
					'type' 			=> 'textfield',
					'heading' 		=> __( 'Not more posts text', 'cd' ),
					'param_name' 	=> 'no_more',
					'edit_field_class'=> $this->col('6'),
				), 
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Class', 'cd'),
					'param_name'	=> 'extra_class',
					'edit_field_class'=> $this->col('6'),
				),

				array(
					'type' 			=> 'textfield',
					'heading' 		=> __('Posts count', 'cd'),
					'param_name' 	=> 'posts_per_page',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'posttypes',
					'heading'		=> __('Post type(s)', 'cd'),
					'param_name'	=> 'post_type',
					'edit_field_class'=> $this->col('12'),
					'group'			=> $this->WP_Query()
				), 
				array(
					"type"			=> "dropdown",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading"		=> __("Order", "cd"),
					"param_name"	=> "order",
					"value"			=> array(
						__("Descending", "cd") => 'DESC',
						__("Ascending", "cd") => 'ASC',
					)
				), 
				array(
					"type"			=> "dropdown",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading"		=> __("Orderby", "cd"),
					"param_name"	=> "orderby",
					"value"			=> array(
						__("Date", "cd")	=> 'date',
						__("ID", "cd")		=> 'ID',
						__("Random", "cd") => 'rand',
						__("Author", "cd") => 'author',
						__("Title", "cd")	=> 'title',
						__("Name", "cd")	=> 'name',
						__("Type", "cd")	=> 'type',
						__("Modified", "cd") => 'modified',
						__("Parent ID", "cd") => 'parent',
						__("Comment Count", "cd") => 'comment_count',
					)
				),
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Post parent", "cd"),
					"param_name" 	=> "post_parent"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Offset", "cd"),
					"param_name" 	=> "offset"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Year", "cd"),
					"param_name" 	=> "year"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Month num", "cd"),
					"param_name" 	=> "monthnum"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Day", "cd"),
					"param_name" 	=> "day"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Meta key", "cd"),
					"param_name" 	=> "meta_key"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Meta value", "cd"),
					"param_name" 	=> "meta_value"
				), 
				array(
					"type" 			=> "textfield",
					"group" 		=> $this->WP_Query(),
					'edit_field_class'=> $this->col('6'),
					"heading" 		=> __("Meta compare", "cd"),
					"param_name" 	=> "meta_compare"
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Posts category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> '_cats',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Tags', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> '_tags',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Artists alphabet', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'alphabet',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Artists category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'artists_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Songs category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'songs_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Playlists category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'playlists_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Podcasts category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'podcasts_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Lyrics category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'lyrics_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Videos category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'videos_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Events category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'events_cat',
					'group'			=> $this->WP_Query()
				),
				array(
					'type'			=> 'textfield',
					'description'	=> 'ID(s) eg. 12,35,66',
					'heading'		=> __('Gallery category', 'cd'),
					'edit_field_class'=> $this->col('6'),
					'param_name'	=> 'gallery_cat',
					'group'			=> $this->WP_Query()
				),
			)

		));
	}

	function out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'news_title' 	=> '',
			'no_more' 		=> '',
			'post_type'		=> '',
			'posts_per_page'=> '',
			'ajax_style' 	=> '',
			'button_label' 	=> '',
			'extra_class' 	=> '',
			'post_parent' 	=> '',
			'offset' 		=> '',
			'order' 		=> '',
			'orderby' 		=> '',
			'year' 			=> '',
			'monthnum' 		=> '',
			'day' 			=> '',
			'meta_key' 		=> '',
			'meta_value' 	=> '',
			'meta_compare' 	=> '',
			'_cats'			=> '',
			'_tags' 		=> '',
			'alphabet' 		=> '',
			'artists_cat' 	=> '',
			'songs_cat' 	=> '',
			'playlists_cat' => '',
			'podcasts_cat'  => '',
			'lyrics_cat' 	=> '',
			'videos_cat' 	=> '',
			'gallery_cat' 	=> '',
			'events_cat' 	=> ''
		), $atts );

		global $codevz;

		if ( $codevz ) {
			ob_start(); ?>
				<div class="def-block mb clr <?php echo $atts['extra_class']; ?>">
					<?php echo $atts['news_title'] ? '<h4 class="tt">' . $atts['news_title'] . '</h4><span class="liner"></span>' : ''; ?>
					<div class="listing">
						<?php echo $codevz->posts( $atts ); ?>
					</div>
				</div>
			<?php 

			return ob_get_clean();
		} else {
			return 'This theme do not support this shortcode.';
		}
	}
}

new CodevzListing();
