<?php

/**
 * Author : DeipGroup
 * Date: 8/11/2016
 * Time: 4:15 PM
 *
 * @package dflip
 *
 * @since   dflip 1.2
 */
class DFlip_Settings {

	/**
	 * Holds the singleton class object.
	 *
	 * @since 1.2.0
	 *
	 * @var object
	 */
	public static $instance;

	public $hook;

	/**
	 * Holds the base DFlip class object.
	 *
	 * @since 1.2.0
	 *
	 * @var object
	 */
	public $base;

	/**
	 * Holds the base DFlip class fields.
	 *
	 * @since 1.2.0
	 *
	 * @var object
	 */
	public $fields;

	/**
	 * Primary class constructor.
	 *
	 * @since 1.2.0
	 */
	public function __construct() {

		// Load the base class object.
		$this->base = DFlip::get_instance();

		add_action( 'admin_menu', array( $this, 'settings_menu' ) );

		//Fields structure and defaults
		$this->fields = array(

			'text_toggle_sound'     => "Turn on/off Sound",
			'text_toggle_thumbnails' => "Toggle Thumbnails",
			'text_toggle_outline'    => "Toggle Outline/Bookmark",
			'text_previous_page'     => "Previous Page",
			'text_next_page'         => "Next Page",
			'text_toggle_fullscreen' => "Toggle Fullscreen",
			'text_zoom_in'           => "Zoom In",
			'text_zoom_out'          => "Zoom Out",
			'text_toggle_help'       => "Toggle Help",
			'text_single_page_mode'   => "Single Page Mode",
			'text_double_page_mode'   => "Double Page Mode",
			'text_download_PDF_file'  => "Download PDF File",
			'text_goto_first_page'    => "Goto First Page",
			'text_goto_last_page'     => "Goto Last Page"

		);

		// Load the metabox hooks and filters.
		//		add_action('add_meta_boxes', array($this, 'add_meta_boxes'), 100);

		// Add action to save metabox config options.
		//		add_action('save_post', array($this, 'save_meta_boxes'), 10, 2);
	}

	/**
	 * Creates menu for the settings page
	 *
	 * @since 1.2
	 */
	public function settings_menu() {

		$this->hook = add_submenu_page(
			'edit.php?post_type=dflip', __DFLIP( 'dFlip Settings' ), __DFLIP( 'Settings' ), 'manage_options',
			$this->base->plugin_slug . '-settings',
			array( $this, 'settings_page' ) );

		if ( $this->hook ) {
			add_action( 'load-' . $this->hook, array( $this, 'update_settings' ) );
			// Load metabox assets.
			add_action(
				'load-' . $this->hook, array( $this, 'hook_page_assets' )
			);
		}
	}

	/**
	 * Callback to create the settings page
	 *
	 * @since 1.2
	 */
	public function settings_page() {

		$tabs = array(
//			'general'   => __DFLIP( 'General' ),
			'translate' => __DFLIP( 'Translate' )
		);

		//create tabs and content
		?>

		<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
		<form id="dflip-settings" method="post" class="dflip-settings">

			<?php
			wp_nonce_field( 'dflip_settings_nonce', 'dflip_settings_nonce' );
			submit_button( __DFLIP( 'Update Settings' ), 'primary', 'dflip_settings_submit', false );
			?>

		<div id="dflip-tabs">
			<h2 id="dflip-tabs-list" class="nav-tab-wrapper">
				<?php
				//create tabs
				$active_set = false;
				foreach ( (array) $tabs as $id => $title ) {
					?>
					<a class="nav-tab <?php echo( $active_set == false ? 'dflip-active' : '' ) ?>"
					   href="#dflip-tab-<?php echo $id ?>"><?php echo $title ?></a>
					<?php $active_set = true;
				}
				?>
			</h2>
			<?php

			$active_set = false;
			foreach ( (array) $tabs as $id => $title ) {
				?>
				<div id="dflip-tab-<?php echo $id ?>"
				     class="dflip-tabs <?php echo( $active_set == false ? "dflip-active" : "" ) ?>">

					<?php
					$active_set = true;

					//create content for tab
					$function = $id . "_tab";
					call_user_func( array( $this, $function ) );

					?>
				</div>
			<?php } ?>
		</div>
		</form>
	<?php

	}

	public function hook_page_assets() {
		add_action( 'admin_enqueue_scripts', array( $this, 'meta_box_styles_scripts' ) );
	}

	/**
	 * Loads styles and scripts for our metaboxes.
	 *
	 * @since 1.0.0
	 *
	 * @return null Bail out if not on the proper screen.
	 */
	public function meta_box_styles_scripts() {


		// Load necessary metabox styles.
		wp_register_style(
			$this->base->plugin_slug . '-setting-metabox-style',
			plugins_url( 'assets/css/metaboxes.css', $this->base->file ),
			array(),
			$this->base->version
		);
		wp_enqueue_style( $this->base->plugin_slug . '-setting-metabox-style' );

		// Load necessary metabox scripts.
		wp_register_script(
			$this->base->plugin_slug . '-setting-metabox-script',
			plugins_url( 'assets/js/metaboxes.js', $this->base->file ),
			array( 'jquery', 'jquery-ui-tabs' ),
			$this->base->version
		);
		wp_enqueue_script( $this->base->plugin_slug . '-setting-metabox-script' );


	}

	/**
	 * Creates the UI for General tab
	 *
	 * @since 1.0.0
	 *
	 */
	public function general_tab() { }


	/**
	 * Creates the UI for Translate tab
	 *
	 * @since 1.0.0
	 *
	 */
	public function translate_tab() {
		?>

		<!--Turn on/off Sound-->
		<div id="dflip_text_toggle_sound_box" class="dflip-box">

			<label for="dflip_text_toggle_sound" class="dflip-label">
				<?php echo "Turn on/off Sound"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Turn on/off Sound</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_toggle_sound' ); ?>"
				       type="text" name="_dflip[text_toggle_sound]" id="dflip_text_toggle_sound" class=""/>
			</div>

		</div>

		<!--Toggle Thumbnails-->
		<div id="dflip_text_toggle_thumbnails_box" class="dflip-box">

			<label for="dflip_text_toggle_thumbnails" class="dflip-label">
				<?php echo "Toggle Thumbnails"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Toggle Thumbnails</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_toggle_thumbnails' ); ?>"
				       type="text" name="_dflip[text_toggle_thumbnails]" id="dflip_text_toggle_thumbnails" class=""/>
			</div>

		</div>

		<!--Toggle Outline/Bookmark-->
		<div id="dflip_text_toggle_outline_box" class="dflip-box">

			<label for="dflip_text_toggle_outline" class="dflip-label">
				<?php echo "Toggle Outline/Bookmark"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Toggle Outline/Bookmark</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_toggle_outline' ); ?>"
				       type="text" name="_dflip[text_toggle_outline]" id="dflip_text_toggle_outline" class=""/>
			</div>

		</div>

		<!--Previous Page-->
		<div id="dflip_text_previous_page_box" class="dflip-box">

			<label for="dflip_text_previous_page" class="dflip-label">
				<?php echo "Previous Page"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Previous Page</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_previous_page' ); ?>"
				       type="text" name="_dflip[text_previous_page]" id="dflip_text_previous_page" class=""/>
			</div>

		</div>

		<!--Next Page-->
		<div id="dflip_text_next_page_box" class="dflip-box">

			<label for="dflip_text_next_page" class="dflip-label">
				<?php echo "Next Page"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Next Page</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_next_page' ); ?>"
				       type="text" name="_dflip[text_next_page]" id="dflip_text_next_page" class=""/>
			</div>

		</div>

		<!--Toggle Fullscreen-->
		<div id="dflip_text_toggle_fullscreen_box" class="dflip-box">

			<label for="dflip_text_toggle_fullscreen" class="dflip-label">
				<?php echo "Toggle Fullscreen"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Toggle Fullscreen</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_toggle_fullscreen' ); ?>"
				       type="text" name="_dflip[text_toggle_fullscreen]" id="dflip_text_toggle_fullscreen" class=""/>
			</div>

		</div>

		<!--Zoom In-->
		<div id="dflip_text_zoom_in_box" class="dflip-box">

			<label for="dflip_text_zoom_in" class="dflip-label">
				<?php echo "Zoom In"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Zoom In</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_zoom_in' ); ?>"
				       type="text" name="_dflip[text_zoom_in]" id="dflip_text_zoom_in" class=""/>
			</div>

		</div>

		<!--Zoom Out-->
		<div id="dflip_text_zoom_out_box" class="dflip-box">

			<label for="dflip_text_zoom_out" class="dflip-label">
				<?php echo "Zoom Out"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Zoom Out</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_zoom_out' ); ?>"
				       type="text" name="_dflip[text_zoom_out]" id="dflip_text_zoom_out" class=""/>
			</div>

		</div>

		<!--Toggle Help-->
		<div id="dflip_text_toggle_help_box" class="dflip-box">

			<label for="dflip_text_toggle_help" class="dflip-label">
				<?php echo "Toggle Help"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Toggle Help</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_toggle_help' ); ?>"
				       type="text" name="_dflip[text_toggle_help]" id="dflip_text_toggle_help" class=""/>
			</div>

		</div>

		<!--Single Page Mode-->
		<div id="dflip_text_single_page_mode_box" class="dflip-box">

			<label for="dflip_text_single_page_mode" class="dflip-label">
				<?php echo "Single Page Mode"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Single Page Mode</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_single_page_mode' ); ?>"
				       type="text" name="_dflip[text_single_page_mode]" id="dflip_text_single_page_mode" class=""/>
			</div>

		</div>

		<!--Double Page Mode-->
		<div id="dflip_text_double_page_mode_box" class="dflip-box">

			<label for="dflip_text_double_page_mode" class="dflip-label">
				<?php echo "Double Page Mode"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Double Page Mode</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_double_page_mode' ); ?>"
				       type="text" name="_dflip[text_double_page_mode]" id="dflip_text_double_page_mode" class=""/>
			</div>

		</div>

		<!--Download PDF File-->
		<div id="dflip_text_download_PDF_file_box" class="dflip-box">

			<label for="dflip_text_download_PDF_file" class="dflip-label">
				<?php echo "Download PDF File"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Download PDF File</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_download_PDF_file' ); ?>"
				       type="text" name="_dflip[text_download_PDF_file]" id="dflip_text_download_PDF_file" class=""/>
			</div>

		</div>

		<!--Goto First Page-->
		<div id="dflip_text_goto_first_page_box" class="dflip-box">

			<label for="dflip_text_goto_first_page" class="dflip-label">
				<?php echo "Goto First Page"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Goto First Page</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_goto_first_page' ); ?>"
				       type="text" name="_dflip[text_goto_first_page]" id="dflip_text_goto_first_page" class=""/>
			</div>

		</div>

		<!--Goto Last Page-->
		<div id="dflip_text_goto_last_page_box" class="dflip-box">

			<label for="dflip_text_goto_last_page" class="dflip-label">
				<?php echo "Goto Last Page"; ?>
			</label>

			<div class="dflip-desc">
				<?php echo "Tanslate text for <code>Goto Last Page</code>" ?>
			</div>

			<div class="dflip-option">
				<input value="<?php echo $this->get_config( 'text_goto_last_page' ); ?>"
				       type="text" name="_dflip[text_goto_last_page]" id="dflip_text_goto_last_page" class=""/>
			</div>

		</div>

		<!--Clear-fix-->
		<div class="dflip-box"></div>
	<?php

	}

	/**
	 * Update settings
	 *
	 * @since 1.2.0.1
	 *
	 * @return null Invalid nonce / no need to save
	 */
	public function update_settings() {

		// Check form was submitted
		if ( ! isset( $_POST['dflip_settings_submit'] ) ) {
			return;
		}

		// Check nonce is valid
		if ( ! wp_verify_nonce( $_POST['dflip_settings_nonce'], 'dflip_settings_nonce' ) ) {
			return;
		}

		$data = $_POST['_dflip'];

		// Update options
		update_option( '_dflip_settings', $data);

		// Show confirmation
		add_action( 'admin_notices', array( $this, 'updated_settings' ) );

	}

	/**
	 * display a saved notice
	 *
	 * @since 1.2.0.1
	 */
	public function updated_settings() {
		?>
		<div class="updated">
			<p><?php _e( 'Settings updated. - w  p  l o c k  e r .c  o m ', 'DFLIP' ); ?></p>
		</div>
	<?php

	}
	/**
	 * Helper method for retrieving config values.
	 *
	 * @since 1.0.0
	 *
	 * @param string $key The config key to retrieve.
	 *
	 * @return string Key value on success, empty string on failure.
	 */
	public function get_config( $key ) {

		$values = get_option( '_dflip_settings', true );
		$value  = isset( $values[ $key ] ) ? $values[ $key ] : '';

		$default = isset( $this->fields[ $key ] )
			? is_array( $this->fields[ $key ] )
				? isset( $this->fields[ $key ]['std'] )
					? $this->fields[ $key ]['std']
					: ''
				: $this->fields[ $key ]
			: '';

		/* set standard value */
		if ( $default !== null ) {
			$value = $this->filter_std_value( $value, $default );
		}

		return $value;

	}

	/**
	 * Helper function to filter standard option values.
	 *
	 * @param     mixed $value Saved string or array value
	 * @param     mixed $std   Standard string or array value
	 *
	 * @return    mixed     String or array
	 *
	 * @access    public
	 * @since     1.0.0
	 */
	public function filter_std_value( $value = '', $std = '' ) {

		$std = maybe_unserialize( $std );

		if ( is_array( $value ) && is_array( $std ) ) {

			foreach ( $value as $k => $v ) {

				if ( '' == $value[ $k ] && isset( $std[ $k ] ) ) {

					$value[ $k ] = $std[ $k ];

				}

			}

		} else {
			if ( '' == $value && $std !== null ) {

				$value = $std;

			}
		}

		return $value;

	}


	/**
	 * Returns the singleton instance of the class.
	 *
	 * @since 1.2.0
	 *
	 * @return object DFlip_Settings object.
	 */
	public static function get_instance() {

		if ( ! isset( self::$instance )
		     && ! ( self::$instance instanceof DFlip_Settings )
		) {
			self::$instance = new DFlip_Settings();
		}

		return self::$instance;

	}
}

// Load the DFlip_Settings class.
$dflip_settings = DFlip_Settings::get_instance();
