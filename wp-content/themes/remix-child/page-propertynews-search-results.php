<?php get_header(); ?>
<?php 
    $request_uri = $_SERVER["REQUEST_URI"];
    $uriArray = explode("=", $request_uri);
    if (sizeof($uriArray) > 1) { 
        $keywords = urldecode($uriArray[1]);
    }
?>
<div class="container search-results">
    <div class="adzone full-width adzone-top">
        <?php echo do_shortcode("[pro_ad_display_adzone id=2168]"); ?>
    </div>
    <div class="search-results-label">
        <?php 
            if(!empty($keywords)){
                echo '<div>搜索结果：' . $keywords . '</div>';
            }
        ?>
    </div>
    <div class="content">
        <div class="search-page-main grid_9">
            <div class="search-template">
                <?php echo facetwp_display( 'template', 'property_news_search_template' ); ?>
            </div>
            <div class="pager">
                <?php echo facetwp_display( 'pager' ); ?>
            </div>
        </div>
        <div class="search-page-sidebar grid_3">
            <div class="container">
                <div class="label">广告</div>
                <div><?php echo do_shortcode("[pro_ad_display_adzone id=2192]"); ?></div>
            </div>
        </div>
    </div>
    <div class="adzone full-width adzone-bottom">
        <?php echo do_shortcode("[pro_ad_display_adzone id=2194]"); ?>
    </div>
</div>



<?php get_footer(); ?>