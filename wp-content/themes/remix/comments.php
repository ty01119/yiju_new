<?php 

/**
 * Comments
 * 
 * @package Remix
 * @author Codevz
 * @link http://Codevz.com
 */

if ( post_password_required() ) { return; } ?>

<h4 class="tt"><i class="fa fa-comments mi"></i><?php echo get_comments_number(); ?></h4><span class="liner"></span>

<?php if ( have_comments() ) : global $wp_query; ?>
	<div id="commentlist-container">
		<ol class="commentlist">
			<?php wp_list_comments( 'avatar_size=35&type=comment' ); ?>	
		</ol>
		<?php if ( get_comment_pages_count() > 1 && get_option('page_comments') ) : ?>
			<ul class="page-numbers">
				<li><?php previous_comments_link(); ?></li>
				<li><?php next_comments_link(); ?></li>
			</ul>
		<?php endif; ?>
	</div>
<?php endif; ?>
<?php if ( comments_open() ) { comment_form(); } ?>