<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.

//
// Start Theme Options
//
$options = array();
$options[]   = array(
  'name'     => 'migration_to_v3',
  'title'    => __( 'Migration v2.x to v3.x', 'remix' ),
  'fields'   => array(
    array(
      'type'    => 'notice',
      'class'   => 'info',
      'content' => '<div style="font-size: 13px;line-height: 24px;">If this is first time you moved to version 3 of remix theme, 
		So you need to follow this steps to migrate from older versions to version 3, 
		Please follow below steps to moving all of your previous options and meta settings into the new database rows.</div>'
    ),
    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => '<div style="font-size: 13px;line-height: 24px;">Please before importing demo, make sure your server PHP memory limit and max_execution_time is ready for this work.</div>'
    ),
    array(
      'type'    => 'heading',
      'content' => 'Step 1: Migrate metaboxes'
    ),
	array(
		'type'    => 'content',
		'content' => '
<div class="cd_migrations">
<div class="cd_migration_metaboxes">
<input 
	type="button" 
	name="codevz_migration_metaboxes" 
	data-action="codevz_migration_metaboxes" 
	data-nonce="'.wp_create_nonce('cd_migration_nonce').'" 
	class="button button-primary" 
	value="Start migrating metaboxes"
>
<img src="' . get_admin_url() . '/images/spinner.gif" style="display:none">
<b></b>
</div>
</div>
		',
	),
    array(
      'type'    => 'heading',
      'content' => 'Step 2: Migrate options'
    ),
	array(
		'type'    => 'content',
		'content' => '
<script type="text/javascript">
jQuery(document).ready(function ($) {
$(".cd_delete_old_rows input").bind("keyup change", function(e) {
	if ( $( this ).val() === "please_clean_database" ) {
		$(".cd_delete_old_rows .button").removeAttr("disabled");
	} else {
		$(".cd_delete_old_rows .button").attr("disabled", "disabled");
	}
});
$(".cd_migrations").on("click",".button", function(event) {
	event.preventDefault();
	var $this 	= $(this).parent(),
		img 	= $this.find("img"),
		msg 	= $this.find("b"),
		nonce 	= $(this).data("nonce"),
		key 	= $(".please_clean_database").val(),
		action 	= $(this).data("action");

	var r = confirm("Are you sure ?");
	if ( !r ) {
		return;
	}

	img.fadeIn();
	msg.html( "Please wait! it may take a minutes." );
	$(this).attr( "disabled", "disabled" );

	$.ajax({
		type: "POST",
		url: ajaxurl,
		data: "action=" + action +"&key=" + key +"&nonce=" + nonce,
		success: function( response ) {
			if ( response === "Done, Please wait ..." ) {
				setTimeout(function(){
					window.location.reload();
				}, 100);
			}
			if ( response === "" || response === "0" || response === "-1" ) {
				$this.find("input").removeAttr( "disabled" );
			}
			img.fadeOut();
			msg.html( response );
		}
	});
});
});
</script>

<div class="cd_migrations">
<div class="cd_migration_options">
<input 
	type="button" 
	name="codevz_migration_options" 
	data-action="codevz_migration_options" 
	data-nonce="'.wp_create_nonce('cd_migration_nonce').'" 
	class="button button-primary" 
	value="Start migrating options"
>
<img src="' . get_admin_url() . '/images/spinner.gif" style="display:none">
<b></b>
</div>
</div>
		',
	),
    array(
      'type'    => 'heading',
      'content' => 'Step 3: Delete old settings'
    ),
	array(
		'type'    => 'content',
		'content' => '
<div class="cd_migrations">
<div class="cd_delete_old_rows">Type this: please_clean_database <br /><br />
<input type="text" name="delete_old_rows_key" class="please_clean_database"> 
<input 
	type="button" 
	name="codevz_delete_old_rows" 
	data-action="codevz_delete_old_rows" 
	data-nonce="'.wp_create_nonce('cd_migration_nonce').'" 
	class="button button-primary" 
	value="Clean up database" disabled="disabled">
<img src="' . get_admin_url() . '/images/spinner.gif" style="display:none">
<b></b>
</div>
<br /><div style="font-size: 14px;line-height: 24px;color: red">Warning: Before doing this step please make sure all of your site pages (Back-end and Front-end) working fine with Remix v3</div>
<br /><p>If you have any problem, Please contact with codevz support.</p>
</div>
		',
	),
  )
);

$options[] = array(
	'name' 		=> 'header',
	'title' 	=> __( 'Header', 'remix' ),
	'sections' 	=> array(

		array(
			'name'   => 'logo_menu',
			'title'  => __( 'Logo + Menu', 'remix' ),
			'fields' => array(

				array(
					'id'   => 'logo',
					'type'    => 'upload',
					'title'		=> __('Logo', 'remix'),
				),
				codevz_style_kit_options(array(
					'id' 		=> '_css_logo',
					'title' 	=> __('Logo style', 'remix'),
					'selector' 	=> '.logo',
					'fields' 	=> array('float', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom')
				)),
				codevz_style_kit_options(array(
					'id' 		=> '_css_menu',
					'title' 	=> __('Menu container style', 'remix'),
					'selector' 	=> '.sf-menu',
					'fields' 	=> array(
						'float', 'font-family', 'font-size', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom'
					)
				)),
				codevz_style_kit_options(array(
					'id' 		=> '_css_menu_a',
					'title' 	=> __('First level links', 'remix'),
					'selector' 	=> '.sf-menu a',
					'fields' 	=> array(
						'color', 'background-color', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom'
					)
				)),
				codevz_style_kit_options(array(
					'id' 		=> '_css_menu_a_hover',
					'title' 	=> __('Hover + active links', 'remix'),
					'selector' 	=> '.sf-menu li:hover > a, .sf-menu li > a:hover, .sf-menu > .selectedLava > a, .page-template-page-onepage .sf-menu li.current a',
					'fields' 	=> array(
						'color', 'background-color', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom'
					)
				)),
				codevz_style_kit_options(array(
					'id' 		=> '_css_menu_a_hover_underline',
					'title' 	=> __('Hover menu underline', 'remix'),
					'selector' 	=> '.sf-menu .back .left',
					'fields' 	=> array(
						'background-color', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'border-style', 'border-width', 'border-color', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom'
					)
				)),
				codevz_style_kit_options(array(
					'id' 		=> '_css_menu_inner_ul',
					'title' 	=> __('Inner ul container', 'remix'),
					'selector' 	=> '.sf-menu ul, .sf-menu li:hover ul, .sf-menu li.sfHover ul',
					'fields' 	=> array(
						'background-color', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom'
					)
				)),
				codevz_style_kit_options(array(
					'id' 		=> '_css_menu_a_a',
					'title' 	=> __('Second level links', 'remix'),
					'selector' 	=> '.sf-menu li li a',
					'fields' 	=> array(
						'color', 'background-color', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom'
					)
				)),
				codevz_style_kit_options(array(
					'id' 		=> '_css_menu_a_a_hover',
					'title' 	=> __('Second level links hover', 'remix'),
					'selector' 	=> '.sf-menu li li:hover > a, .sf-menu li li > a:hover, .sf-menu .current-menu-parent .current_page_item > a, .sf-menu li .current_page_parent > a',
					'fields' 	=> array(
						'color', 'background-color', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom'
					)
				)),
				codevz_style_kit_options(array(
					'id' 		=> '_css_menu_a_a_hover_after',
					'title' 	=> __('Second level hover Background', 'remix'),
					'selector' 	=> '.sf-menu li li:hover > a:after, .sf-menu li li > a:hover:after, .sf-menu .current-menu-parent .current_page_item > a:after, .sf-menu li .current_page_parent > a:after, .sf-menu li li.child_menu_item > a:after',
					'fields' 	=> array( 'background-color', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom' )
				)),
				array(
					'id'    => 'fullscreen_menu',
					'type'  => 'switcher',
					'title' => __('Fullscreen menu', 'remix')
				),
				codevz_style_kit_options(array(
					'id' 		=> '_css_fullscreen_menu_icon',
					'title' 	=> __('Fullscreen menu icon style', 'remix'),
					'selector' 	=> 'header .full_menu',
					'fields' 	=> array(
						'color', 'background-color', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom'
					)
				)),

			),
		),

		array(
			'name'   => 'top_bar',
			'title'  => __( 'Top bar', 'remix' ),
			'fields' => array(

				codevz_style_kit_options(array(
					'id' 		=> '_css_topbar',
					'title' 	=> __('Top bar row style', 'remix'),
					'selector' 	=> '.little-head',
					'unset' 	=> array(
						'font-family', 'font-size', 'width', 'height', 'background'
					)
				)),
				array(
					'id'		=> 'search_placeholder',
					'type'		=> 'text',
					'title'		=> __('Search placeholder', 'remix'),
				),
				codevz_style_kit_options(array(
					'id' 		=> '_css_header_search_input',
					'title' 	=> __('Search input style', 'remix'),
					'selector' 	=> 'header .search input',
					'unset' 	=> array(
						'font-family', 'height', 'background'
					)
				)),
				array(
					'id'		=> 'header_custom_text',
					'type'		=> 'textarea',
					'title'		=> __('Custom text or code', 'remix'),
					'help'		=> __('Between social and search form', 'remix')
				),
				array(
					'id'		=> 'login',
					'type'		=> 'text',
					'title'		=> __('Login button title', 'remix'),
					'help'		=> __('For disabling login, leave this field empty', 'remix')
				),
				array(
					'id'		=> 'login_custom_link',
					'type'		=> 'text',
					'title'		=> __('Login custom link', 'remix')
				),
				array(
					'id'		=> 'register',
					'type'		=> 'text',
					'title'		=> __('Registeration title', 'remix'),
					'help'		=> __('For disabling this tab, leave this field empty', 'remix')
				),
				array(
					'id'		=> 'lost_p',
					'type'		=> 'text',
					'title'		=> __('Lost password title', 'remix'),
					'help'		=> __('For disabling this tab, leave this field empty', 'remix')
				),
				array(
					'id'		=> 'lost_p_btn',
					'type'		=> 'text',
					'title'		=> __('Lost password button', 'remix'),
					'attributes'=> array(
						'placeholder' => __('Get new password', 'remix')
					)
				),
				array(
					'id'		=> 'username',
					'type'		=> 'text',
					'title'		=> __('Username', 'remix'),
				),
				array(
					'id'		=> 'password',
					'type'		=> 'text',
					'title'		=> __('Password', 'remix'),
				),
				array(
					'id'		=> 'email',
					'type'		=> 'text',
					'title'		=> __('Email', 'remix'),
				),
				array(
					'id'		=> 'username_email',
					'type'		=> 'text',
					'title'		=> __('Username or email', 'remix'),
				),
				array(
					'id'		=> 'login_error',
					'type'		=> 'text',
					'title'		=> __('Login error', 'remix'),
					'attributes'=> array(
						'placeholder' => __('Username or password is wrong.', 'remix')
					)
				),
				array(
					'id'		=> 'register_success',
					'type'		=> 'text',
					'title'		=> __('Registration success', 'remix'),
					'attributes'=> array(
						'placeholder' => __('Registration was completed, Password was sent to your email.', 'remix')
					)
				),
				array(
					'id'		=> 'lost_p_error_not_fount',
					'type'		=> 'text',
					'title'		=> __('Lost password: user not found', 'remix'),
					'attributes'=> array(
						'placeholder' => __('Can\'t find user with this information', 'remix')
					)
				),
				array(
					'id'		=> 'lost_p_success',
					'type'		=> 'text',
					'title'		=> __('Lost password: success', 'remix'),
					'attributes'=> array(
						'placeholder' => __('Email sent, Please check your email.', 'remix')
					)
				),
				array(
					'id'		=> 'my_profile',
					'type'		=> 'text',
					'title'		=> __('My profile', 'remix')
				),
				array(
					'id'		=> 'edit_my_profile',
					'type'		=> 'text',
					'title'		=> __('Edit my profile', 'remix')
				),
				array(
					'id'		=> 'music_submission',
					'type'		=> 'text',
					'title'		=> __('Music submission', 'remix')
				),
				array(
					'id'		=> 'logout',
					'type'		=> 'text',
					'title'		=> __('Logout', 'remix')
				),

			),
		),

		array(
			'name'   => 'social',
			'title'  => __( 'Social icons', 'remix' ),
			'fields' => array(

		        array(
		          'id'              => 'social',
		          'type'            => 'group',
		          'title'           => __('Add social', 'remix'),
		          'button_title'    => 'Add social',
		          'fields'          => array(
		            array(
		              'id'          => 'title',
		              'type'        => 'text',
		              'title'       => __('Title', 'remix'),
					  'attributes'	=> array( 'id' => 'acc_title' )
		            ),
		            array(
		              'id'          => 'social-icon',
		              'type'        => 'icon',
		              'title'       => __('Icon', 'remix')
		            ),
		            array(
		              'id'          => 'social-link',
		              'type'        => 'text',
		              'title'       => __('Link', 'remix')
		            ),
		          )
		        ),
		        array(
		          'id'    => 'social_position',
		          'type'  => 'select',
		          'title' => __('Position', 'remix'),
		          'options' => array(
		            'on_head' 		=> __('Default', 'remix'),
		            'fixed_left' 	=> __('Left of screen', 'remix'),
		            'fixed_right' 	=> __('Right of screen', 'remix')
		          ),
		          'radio'   => true,
		          'default' => 'on_head',
		        ),
				array(
					'id'		=> 'social_colored',
					'type'		=> 'switcher',
					'title'		=> __('Colored', 'remix')
				),
				array(
					'id'		=> 'social_circular',
					'type'		=> 'switcher',
					'title'		=> __('Circular', 'remix')
				),

			),
		),

		array(
			'name'   => 'header_more',
			'title'  => __( 'More', 'remix' ),
			'fields' => array(

				codevz_style_kit_options(array(
					'id' 		=> '_css_header',
					'title' 	=> __('Header outer style', 'remix'),
					'selector' 	=> '#header',
					'unset' 	=> array('font-family', 'font-size', 'float', 'width', 'height')
				)),
				codevz_style_kit_options(array(
					'id' 		=> '_css_header_row',
					'title' 	=> __('Header inner row', 'remix'),
					'selector' 	=> '#header > .row',
					'unset' 	=> array('font-family', 'font-size', 'float', 'height')
				)),
				array(
					'id'		=> 'sticky',
					'type'		=> 'switcher',
					'title'		=> __('Sticky', 'remix')
				),
				array(
					'id'		=> 'sticky_smart',
					'type'		=> 'switcher',
					'title'		=> __('Smart sticky', 'remix'),
					'default'	=> true
				),
				codevz_style_kit_options(array(
					'id' 		=> '_css_sticky',
					'title' 	=> __('Sticky row style', 'remix'),
					'selector' 	=> '.ONsticky',
					'unset' 	=> array('font-family', 'font-size', 'float', 'width')
				)),
		        array(
		          'id'    => 'cover',
		          'type'  => 'select',
		          'title' => __('Page cover type', 'remix'),
		          'help'  => __('Default cover for all pages', 'remix'),
		          'options' => array(
		            '2' => __('None', 'remix'),
		            '3' => __('Image', 'remix'),
		            '4' => __('Revolution Slider', 'remix'),
		            '5' => __('Master Slider', 'remix'),
		          ),
		          'radio'   => true,
		          'default' => '2',
		        ),
		        array(
		          'id'    => 'img',
		          'type'  => 'upload',
		          'title' => __('Upload image', 'remix'),
		          'help'  => __('You can create your cover photo with one PSD that included in download package', 'remix'),
		          'dependency'  => array( 'cover', '==', '3' )
		        ),
		        array(
		          'id'    => 'rev',
		          'type'  => 'select',
		          'title' => __('Select RevSlider', 'remix'),
		          'options' => codevz_get_revSlider(),
		          'dependency'  => array( 'cover', '==', '4' )
		        ),
		        array(
		          'id'    => 'master',
		          'type'  => 'select',
		          'title' => __('Select MasterSlider', 'remix'),
		          'options' => codevz_get_masterSlider(),
		          'dependency'  => array( 'cover', '==', '5' )
		        ),
				array(
					'id'		=> 'cover_position',
					'type'		=> 'switcher',
					'title'		=> __('Set cover after header', 'remix'),
					'help'		=> __('If you\'ve set cover to image, then breadcrumbs will display below of image.', 'remix'),
					'default'	=> false,
					'dependency'=> array( 'cover', 'any', '4,5' )
				),

			)
		),

	),
);

$options[] = array(
	'name' 		=> 'public_styles',
	'title' 	=> __( 'Public styles', 'remix' ),
	'fields' => array(
		array(
			'id'        => 'size_width',
			'type'      => 'slider',
			'title'     => __( 'Site width', 'remix' ),
			'options'   => array(
			    'step'    => 1,
			    'min'     => 960,
			    'max'     => 1440,
			    'unit'    => 'px'
			)
		),
		array(
			'id'        => 'size_color',
			'type'      => 'color_picker',
			'title'     => __( 'Site primary color', 'remix' )
		),
		array(
			'id'        => 'light',
			'type'      => 'switcher',
			'title'     => __( 'Light version ?', 'remix' )
		),
		codevz_style_kit_options(array(
			'id' 		=> '_css_body',
			'title' 	=> __('Body', 'remix'),
			'selector' 	=> 'body',
			'unset' 	=> array('float', 'width', 'height')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_headlines',
			'title' 	=> __('H1 to h6 font', 'remix'),
			'selector' 	=> 'h1, h2, h3, h4, h5, h6',
			'fields' 	=> array('color', 'font-family', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_layout',
			'title' 	=> __('Layout in boxed version', 'remix'),
			'selector' 	=> '#layout',
			'unset' 	=> array('float', 'width', 'height')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_buttons',
			'title' 	=> __('Buttons', 'remix'),
			'selector' 	=> '.button, .page-numbers a, .page-numbers span, .pagination a, .pagination > b, .widget_product_search #searchsubmit, .post-password-form input[type=submit], .wpcf7-submit, .submit_user, #commentform #submit',
			'unset' 	=> array('font-family', 'background', 'float', 'width', 'height')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_widgets_boxes',
			'title' 	=> __('Widgets, Boxes, Alphabet, Breadcrumbs', 'remix'),
			'selector' 	=> '.Alphabet li a, .breadcrumbIn, article, .def-block, .wpb_flickr_widget, .vc_carousel, .wpb_video_widget',
			'unset' 	=> array('font-family', 'font-size', 'float', 'width', 'height')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_widgets_boxes_title',
			'title' 	=> __('Boxes title', 'remix'),
			'selector' 	=> 'h4.tt, h3.tt, .widget_gardengeneralposts h4, .list-custom-taxonomy-widget h4',
			'unset' 	=> array('float', 'width', 'height')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_widgets_boxes_title_first',
			'title' 	=> __('Boxes title first letter', 'remix'),
			'selector' 	=> 'h4.tt::first-letter, h3.tt::first-letter, .widget_gardengeneralposts h4::first-letter, .list-custom-taxonomy-widget h4::first-letter',
			'fields' 	=> array( 'color', 'custom' )
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_cdeffect_bg_hover',
			'title' 	=> __('Images overlay background color', 'remix'),
			'selector' 	=> '.cdEffect:hover:before',
			'fields' 	=> array( 'background-color', 'custom' )
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_cdeffect_h3',
			'title' 	=> __('Images overlay title', 'remix'),
			'selector' 	=> '.cdEffect h3',
			'unset' 	=> array('float', 'width', 'height', 'background-color', 'background', 'border-width', 'border-style', 'border-color', 'border-radius', 'font-family')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_tooltip',
			'title' 	=> __('Tooltip', 'remix'),
			'selector' 	=> '.tooltip',
			'unset' 	=> array('float', 'width', 'height', 'background', 'font-family')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_tooltip',
			'title' 	=> __('Tooltip', 'remix'),
			'selector' 	=> '.tooltip',
			'unset' 	=> array('float', 'width', 'height', 'background', 'font-family')
		)),

	)
);

$options[] = array(
	'name' 		=> 'general',
	'title' 	=> __( 'General', 'remix' ),
	'sections' 	=> array(

		array(
			'name'   => 'layout',
			'title'  => __( 'Layout', 'remix' ),
			'fields' => array(

				array(
					'id'		=> 'responsive',
					'type'		=> 'switcher',
					'title'		=> __('Responsive', 'remix'),
					'default' 	=> true
				),
				array(
					'id'		=> 'breadcrumbs',
					'type'		=> 'switcher',
					'title'		=> __('Breadcrumbs', 'remix'),
					'help'		=> __('Default pages', 'remix'),
					'default' 	=> true
				),
				array(
					'id'		=> 'full_boxed',
					'type'		=> 'image_select',
					'title'		=> __('Full or boxed', 'remix'),
					'options'	=> array(
						'full'			=> CD_URI .'/img/admin/full.png',
						'boxed'			=> CD_URI .'/img/admin/boxed.png',
						'boxed-margin'	=> CD_URI .'/img/admin/boxed-margin.png',
					),
					'default'	=> 'full',
					'radio'		=> true
				),
				array(
					'id'		=> 'layout',
					'type'		=> 'image_select',
					'title'		=> __('Layout', 'remix'),
					'help'		=> __('Default layout', 'remix'),
					'options'	=> array(
						'without-sidebar' => CD_URI .'/img/admin/full.png',
						'sidebar-right'	=> CD_URI .'/img/admin/right.png',
						'sidebar-left'	=> CD_URI .'/img/admin/left.png',
						'both-sidebar'	=> CD_URI .'/img/admin/both-sides.png',
						'both-sidebar-right'=> CD_URI .'/img/admin/both-right.png',
						'both-sidebar-left'	=> CD_URI .'/img/admin/both-left.png',
					),
					'default'	=> 'sidebar-right',
					'radio'		=> true,
		            'attributes' => array(
		              'data-depend-id' => 'layout'
		            )
				),
		        array(
		          'id'    	=> 'primary',
		          'type'  	=> 'select',
		          'title' 	=> __('Primary Sidebar', 'remix'),
		          'options' => codevz_get_sidebars(),
		          'default' => 'primary',
		          'dependency' => array( 'layout', 'any', 'sidebar-right,sidebar-left,both-sidebar,both-sidebar-left,both-sidebar-right' )
		        ),
		        array(
		          'id'    	=> 'secondary',
		          'type' 	=> 'select',
		          'title' 	=> __('Secondary Sidebar', 'remix'),
		          'options' => codevz_get_sidebars(),
		          'default' => 'secondary',
		          'dependency' => array( 'layout', 'any', 'both-sidebar,both-sidebar-left,both-sidebar-right' )
		        ),
				array(
				  'id'              => 'sidebars',
				  'type'            => 'group',
				  'title'    		=> __('Add sidebar', 'remix'),
				  'button_title'    => __('Add sidebar', 'remix'),
				  'fields'          => array(
				    array(
				      'id'          => 'id',
				      'type'        => 'text',
				      'title'       => __('Unique ID', 'remix')
				    ),
				  )
				),
				array(
					'id'		=> 'scrollbar',
					'type'		=> 'select',
					'title'		=> __('Scrollbar', 'remix'),
					'options'	=> array(
						'1'	=> __('Default', 'remix'),
						'2'	=> __('NiceScroll', 'remix'),
					),
					'default'	=> '1'
				),

			),
		),

		array(
			'name'   => 'seo',
			'title'  => __( 'Seo', 'remix' ),
			'fields' => array(

				array(
					'id'		=> 'description',
					'type'		=> 'text',
					'title'		=> __('Site description', 'remix'),
					'help'		=> __('If you fill this field, also og meta tags will add to your site automatically', 'remix'),
					'attributes' => array( 'placeholder' => 'Remix - music wordpress theme' )
				),
				array(
					'id'		=> 'keywords',
					'type'		=> 'text',
					'title'		=> __('Site keywords', 'remix'),
					'attributes' => array( 'placeholder' => 'music, rap, pop, dubstep' )
				),

			),
		),

		array(
			'name'   => 'maintenance',
			'title'  => __( 'Maintenance', 'remix' ),
			'fields' => array(

				array(
				  'id'      => 'maintenance',
				  'type'    => 'text',
				  'title'   => __( 'Page ID', 'remix' ),
				  'help'    => __( 'Edit your page in address bar you can find page ID, then copy it here', 'remix' ),
				),

			),
		),

		array(
			'name'   => '404',
			'title'  => __( '404', 'remix' ),
			'fields' => array(

				array(
				  'id'      => '404',
				  'type'    => 'textarea',
				  'title'   => __( '404 error message', 'remix' )
				),

			),
		),

		array(
			'name'   => 'preloader',
			'title'  => __( 'Preloader', 'remix' ),
			'fields' => array(

				array(
					'id'		=> 'pageloader',
					'type'		=> 'switcher',
					'title'		=> __('Preloader', 'remix'),
					'default' 	=> false,
				),
				array(
					'id'		=> 'pageloader_bg',
					'type'		=> 'background',
					'title'		=> __('Background', 'remix'),
					'dependency'  => array( 'pageloader', '==', true )
				),
				array(
					'id'		=> 'pageloader_img',
					'type'		=> 'upload',
					'title'		=> __('Image', 'remix'),
					'dependency'  => array( 'pageloader', '==', true )
				),
				array(
					'id'		=> 'pageloader_time',
					'type'		=> 'text',
					'after'		=> 'ms',
					'title'		=> __('Custom time', 'remix'),
					'default'	=> '5000',
					'dependency'  => array( 'pageloader', '==', true )
				),

			),
		),

		array(
			'name'   => 'wp_login',
			'title'  => __( 'WP login', 'remix' ),
			'fields' => array(

				array(
					'id'		=> 'wp_login_logo',
					'type'		=> 'upload',
					'title'		=> __('Logo', 'remix')
				),
				array(
					'id'		=> 'wp_login_bg',
					'type'		=> 'background',
					'title'		=> __('Background', 'remix')
				),
				array(
					'id'		=> 'wp_login_form',
					'type'		=> 'color_picker',
					'title'		=> __('Form background', 'remix')
				),

			),
		),

		array(
			'name'   => 'custom_codes',
			'title'  => __( 'Custom codes', 'remix' ),
			'fields' => array(

				array(
					'id'		=> 'head_codes',
					'type'		=> 'textarea',
					'title'		=> __('Head', 'remix')
				),
				array(
					'id'		=> 'foot_codes',
					'type'		=> 'textarea',
					'title'		=> __('Body', 'remix')
				),

			),
		),

		array(
			'name'   => 'hooks',
			'title'  => __( 'Hooks', 'remix' ),
			'fields' => array(

				array(
					'id'		=> 'before_header',
					'type'		=> 'textarea',
					'title'		=> __('Before header', 'remix')
				),
				array(
					'id'		=> 'after_header',
					'type'		=> 'textarea',
					'title'		=> __('After header', 'remix')
				),
				array(
					'id'		=> 'before_content',
					'type'		=> 'textarea',
					'title'		=> __('Before content', 'remix')
				),
				array(
					'id'		=> 'after_content',
					'type'		=> 'textarea',
					'title'		=> __('After content', 'remix')
				),
				array(
					'id'		=> 'before_sidebar_p',
					'type'		=> 'textarea',
					'title'		=> __('Before primary sidebar', 'remix')
				),
				array(
					'id'		=> 'after_sidebar_p',
					'type'		=> 'textarea',
					'title'		=> __('After primary sidebar', 'remix')
				),
				array(
					'id'		=> 'before_sidebar_s',
					'type'		=> 'textarea',
					'title'		=> __('Before secondary sidebar', 'remix')
				),
				array(
					'id'		=> 'after_sidebar_s',
					'type'		=> 'textarea',
					'title'		=> __('After secondary sidebar', 'remix')
				),
				array(
					'id'		=> 'before_footer',
					'type'		=> 'textarea',
					'title'		=> __('Before footer', 'remix')
				),
				array(
					'id'		=> 'between_footer',
					'type'		=> 'textarea',
					'title'		=> __('Between footer-subfooter', 'remix')
				),
				array(
					'id'		=> 'after_footer',
					'type'		=> 'textarea',
					'title'		=> __('After subfooter', 'remix')
				),

			),
		),

		array(
			'name'   => 'more_general',
			'title'  => __( 'More', 'remix' ),
			'fields' => array(
				array(
				  'id'              => 'add_image_size',
				  'type'            => 'group',
				  'title'    		=> __('Override images size', 'remix'),
				  'help'    		=> __('After adding new size you need to install Regen. Thumbnails plugin and once run it from Appearance > Tools', 'remix'),
				  'button_title'    => __('Add new', 'remix'),
				  'fields'          => array(
	                array(
	                  'id'      => 'id',
	                  'type'    => 'select',
	                  'title'   => __('ID', 'remix'),
	                  'options' => array(
	                    'tumb' 			=> '85, 85, true',
	                    'medium_post' 	=> '280, 255, true',
	                    'custom_carousel' => '1000, 1000, true',
	                    'cover' 		=> '500, 500, true',
	                    'player_cover' 	=> '180, 180, true',
	                    'masonry' 		=> '540, 9999',
	                    'free_width' 	=> '9999, 500 - also used for big posts',
	                  ),
	                  'radio'   => true,
	                  'default' => 'tumb'
	                ),
				    array(
				      'id'          => 'width',
				      'type'        => 'number',
				      'title'       => __('Width', 'remix')
				    ),
				    array(
				      'id'          => 'height',
				      'type'        => 'number',
				      'title'       => __('height', 'remix')
				    ),
				    array(
				      'id'          => 'crop',
				      'type'        => 'switcher',
				      'title'       => __('Crop', 'remix')
				    ),
				  )
				),
			),
		),

	),
);

$options[] = array(
	'name' 		=> 'ajax',
	'title' 	=> __( 'Ajax', 'remix' ),
	'fields' => array(

		array(
			'id'		=> 'ajax',
			'type'		=> 'switcher',
			'title'		=> __('Ajax', 'remix'),
			'help'		=> __('Continuesly music player', 'remix'),
			'default' 	=> false,
		),
		array(
		  'id'    => 'ajax_cover',
		  'type'  => 'switcher',
		  'title' => __('Cover', 'remix'),
		  'dependency'  => array( 'ajax', '==', true )
		),
		array(
		  'id'    => 'ajax_autoplay',
		  'type'  => 'switcher',
		  'title' => __('Auto play', 'remix'),
		  'dependency'  => array( 'ajax', '==', true ),
			'default' 	=> false,
		),
		array(
		  'id'    => 'ajax_repeat',
		  'type'  => 'switcher',
		  'title' => __('Repeat', 'remix'),
		  'dependency'  => array( 'ajax', '==', true )
		),
		array(
		  'id'              => 'ajax_tracks',
		  'type'            => 'group',
		  'title'           => __('Track(s)', 'remix'),
		  'button_title'    => __('Add track', 'remix'),
		  'accordion_title' => 'title',
		  'fields'          => array(
		    array(
		      'id'      => 'track_type',
		      'type'    => 'select',
		      'title'   => __('Type', 'remix'),
		      'options' => array(
		        'old' => __( 'Archive', 'remix' ),
		        'new' => __( 'New', 'remix' )
		      ),
		      'radio'   => true,
		      'default' => 'old'
		    ),
		    array(
		      'id'      => 'track',
		      'type'    => 'text',
		      'title'   => __( 'Track ID', 'remix' ),
		      'help'    => __( 'Edit your song page then in address bar you can find song ID', 'remix' ),
		      'dependency'  => array( 'track_type', '==', 'old' ),
		    ),
		    array(
		      'id'          => 'title',
		      'type'        => 'text',
		      'title'       => __('Title', 'remix'),
		      'dependency'  => array( 'track_type', '==', 'new' )
		    ),
		    array(
		      'id'      => 'artist',
		      'type'    => 'text',
		      'class'   => 'multiple',
		      'title'   => __( 'Artist(s) ID', 'remix' ),
		      'help'    => __( 'Edit your artist page then in address bar you can find artist ID, In this field you can add multiple IDs eg. 22, 34, 64', 'remix' ),
		      'query_args'  => array(
		        'post_type' => 'artists',
		        'orderby'   => 'title',
		        'order'     => 'ASC',
		        'posts_per_page' => 20,
		      ),
		      'dependency'  => array( 'track_type', '==', 'new' ),
		    ),
		    array(
		      'id'          => 'artists',
		      'type'        => 'text',
		      'title'       => __('Custom artist', 'remix'),
		      'dependency'  => array( 'track_type', '==', 'new' ),
		    ),
		    array(
		      'id'      => 'type',
		      'type'    => 'select',
		      'title'   => __('Type', 'remix'),
		      'options' => array(
		        'mp3'       => __('MP3', 'remix'),
		        'shoutcast' => __('Shoutcast', 'remix'),
		        //'radionomy' => __('Radionomy', 'remix'),
		        'icecast'   => __('Icecast (Beta)', 'remix'),
		      ),
		      'radio'   => true,
		      'default' => 'mp3',
		      'dependency'  => array( 'track_type', '==', 'new' ),
		    ),
		    array(
		      'id'          => 'mp3',
		      'type'        => 'upload',
		      'title'       => __('Audio', 'remix'),
		      'attributes'    => array(
		        'placeholder' => __('MP3 or Radio stream URL', 'remix')
		      ),
		      'settings'   => array(
		        'upload_type'  => 'audio/mpeg',
		        'frame_title'  => 'Upload / Select',
		        'insert_title' => 'Insert',
		      ),
		      'dependency'  => array( 'track_type', '==', 'new' ),
		    ),
		    array(
		      'id'          => 'history',
		      'type'        => 'text',
		      'title'       => __('Radio history URL', 'remix'),
		      'dependency'  => array( 'type', '!=', 'mp3' )
		    ),
		    array(
		      'id'          => 'poster',
		      'type'        => 'image',
		      'title'       => __('Cover', 'remix'),
		      'dependency'  => array( 'track_type', '==', 'new' ),
		    ),
		    array(
		      'id'          => 'buy_icon_a',
		      'type'        => 'icon',
		      'title'       => __('Icon', 'remix') . ' 1',
		      'dependency'  => array( 'track_type', '==', 'new' ),
		    ),
		    array(
		      'id'          => 'buy_title_a',
		      'type'        => 'text',
		      'title'       => __('Title', 'remix'),
		      'dependency'  => array( 'buy_icon_a|track_type', '!=|==', '|new' ),
		    ),
		    array(
		      'id'          => 'buy_link_a',
		      'type'        => 'text',
		      'title'       => __('Link', 'remix'),
		      'dependency'  => array( 'buy_icon_a|track_type', '!=|==', '|new' ),
		      'attributes'    => array(
		        'placeholder' => 'http://'
		      ),
		    ),
		    array(
		      'id'          => 'buy_icon_b',
		      'type'        => 'icon',
		      'title'        => __('Icon', 'remix') . ' 2',
		      'dependency'  => array( 'track_type', '==', 'new' ),
		    ),
		    array(
		      'id'          => 'buy_title_b',
		      'type'        => 'text',
		      'title'       => __('Title', 'remix'),
		      'dependency'  => array( 'buy_icon_b|track_type', '!=|==', '|new' ),
		    ),
		    array(
		      'id'          => 'buy_link_b',
		      'type'        => 'text',
		      'title'       => __('Link', 'remix'),
		      'dependency'  => array( 'buy_icon_b|track_type', '!=|==', '|new' ),
		      'attributes'    => array(
		        'placeholder' => 'http://'
		      ),
		    ),
		    array(
		      'id'          => 'buy_icon_c',
		      'type'        => 'icon',
		      'title'       => __('Icon', 'remix') . ' 3',
		      'dependency'  => array( 'track_type', '==', 'new' ),
		    ),
		    array(
		      'id'          => 'buy_title_c',
		      'type'        => 'text',
		      'title'       => __('Title', 'remix'),
		      'dependency'  => array( 'buy_icon_c|track_type', '!=|==', '|new' ),
		    ),
		    array(
		      'id'          => 'buy_link_c',
		      'type'        => 'text',
		      'title'       => __('Link', 'remix'),
		      'dependency'  => array( 'buy_icon_c|track_type', '!=|==', '|new' ),
		      'attributes'    => array(
		        'placeholder' => 'http://'
		      ),
		    ),
		    array(
		      'id'          => 'buy_icon_d',
		      'type'        => 'icon',
		      'title'       => __('Icon', 'remix') . ' 4',
		      'dependency'  => array( 'track_type', '==', 'new' ),
		    ),
		    array(
		      'id'          => 'buy_title_d',
		      'type'        => 'text',
		      'title'       => __('Title', 'remix'),
		      'dependency'  => array( 'buy_icon_d|track_type', '!=|==', '|new' ),
		    ),
		    array(
		      'id'          => 'buy_link_d',
		      'type'        => 'text',
		      'title'       => __('Link', 'remix'),
		      'dependency'  => array( 'buy_icon_d|track_type', '!=|==', '|new' ),
		      'attributes'    => array(
		        'placeholder' => 'http://'
		      ),
		    ),
		    array(
		      'id'      => 'lyric',
		      'type'    => 'select',
		      'title'   => __('Lyric', 'remix'),
		      'options' => 'posts',
		      'class'   => 'chosen',
		      'query_args'  => array(
		        'post_type' => 'lyrics',
		        'orderby'   => 'title',
		        'order'     => 'ASC',
		        'posts_per_page' => -1,
		      ),
		      'dependency'  => array( 'track_type', '==', 'new' ),
		      'attributes' => array(
		        'data-placeholder' => __('Lyric', 'remix'),
		        'style' => 'width: 320px'
		      ),
		      'default_option' => __( 'Select', 'remix'),
		    ),
		    array(
		      'id'          => 'buy_custom',
		      'type'        => 'textarea',
		      'dependency'  => array( 'track_type', '==', 'new' ),
		      'title'       => __('Custom HTML', 'remix')
		    ),
		  ),
		  'dependency'  => array( 'ajax', '==', true )
		),
		array(
		  'id'    => 'ajax_shuffle',
		  'type'  => 'switcher',
		  'title' => __('Shuffle on first load', 'remix'),
		  'dependency'  => array( 'ajax', '==', true ),
			'default' 	=> false,
		),

	),
);

$options[] = array(
	'name' 		=> 'footer',
	'title' 	=> __( 'Footer', 'remix' ),
	'fields' => array(

		array(
			'id'		=> 'footer',
			'type'		=> 'image_select',
			'title'		=> __('Footer', 'remix'),
			'options'	=> array(
				'1' => CD_URI .'/img/admin/footer-1.png',
				'2' => CD_URI .'/img/admin/footer-2.png',
				'3' => CD_URI .'/img/admin/footer-3.png',
				'4' => CD_URI .'/img/admin/footer-4.png',
			),
			'default'	=> '3',
			'radio'		=> true
		),
		array(
			'id'		=> 'subfooter',
			'type'		=> 'switcher',
			'title'		=> __('Sub footer', 'remix'),
			'default' 	=> true
		),
		array(
			'id'		=> 'totop',
			'type'		=> 'switcher',
			'title'		=> __('Back to top', 'remix'),
			'default' 	=> true
		),
		array(
			'id'		=> 'copyright',
			'type'		=> 'textarea',
			'title'		=> __( 'Copyright', 'remix' )
		),

		codevz_style_kit_options(array(
			'id' 		=> '_css_footer',
			'title' 	=> __('Footer row style', 'remix'),
			'selector' 	=> '#footer',
			'unset' 	=> array('float', 'width', 'height')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_subfooter',
			'title' 	=> __('Subfooter row style', 'remix'),
			'selector' 	=> '.footer-last',
			'unset' 	=> array('float')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_backtotop',
			'title' 	=> __('Back to top button style', 'remix'),
			'selector' 	=> '#toTop',
			'unset' 	=> array('font-family', 'width', 'height')
		)),

	),
);

/* Posts */
$options[]   = array(
	'name'     => 'blog',
	'title'    => __('Blog', 'remix'),
	'icon'     => 'fa fa-file-text-o',
	'fields'   => array(

		array(
			'id'		=> 'excerpt',
			'type'		=> 'number',
			'title'		=> __('Excerpt length', 'remix'),
		    'attributes' => array(
		      'step' => '1',
		      'min' => '-1',
		      'max' => '100'
		    )
		),
		array(
			'id'		=> 'read_more',
			'type'		=> 'text',
			'title'		=> __('Read more', 'remix')
		),
		array(
			'id'		=> 'pagination',
			'type'		=> 'switcher',
			'title'		=> __('Numeric pagination', 'remix'),
			'default' 	=> true
		),
		array(
			'id'		=> 'next_page',
			'type'		=> 'text',
			'title'		=> __( 'Next page', 'remix' )
		),
		array(
			'id'		=> 'prev_page',
			'type'		=> 'text',
			'title'		=> __( 'Previous page', 'remix' )
		),
		array(
			'id'		=> 'not_found',
			'type'		=> 'text',
			'title'		=> __( 'Not found', 'remix' )
		),
		array(
			'id'		=> 'related_title',
			'type'		=> 'text',
			'title'		=> __( 'Related items title', 'remix' ),
			'help'		=> __( 'This option is public', 'remix' )
		),
		array(
			'id'		=> 'm_post_thumb',
			'type'		=> 'switcher',
			'title'		=> __('Medium thumb', 'remix'),
			'default' 	=> false,
		),
		array(
			'id'		=> 'post_related',
			'type'		=> 'number',
			'title'		=> __('Related', 'remix'),
		    'attributes' => array(
		      'step' => '1',
		      'min' => '-1',
		      'max' => '200'
		    )
		),
		array(
		  'id'    => 'post_related_by',
		  'type'  => 'select',
		  'title' => __('Related by', 'remix'),
		  'options' => array(
			'cats' 		=> __('Categories', 'remix'),
			'tags' 		=> __('Tags', 'remix'),
			'artists' 	=> __('Artists', 'remix'),
			'rand' 		=> __('Random', 'remix'),
		  ),
		  'radio'   => true,
		  'default' => 'tags',
		),
		array(
			'id'		=> 'post_related_grid',
			'type'		=> 'switcher',
			'title'		=> __('Related grid', 'remix'),
			'default' 	=> false,
		),
		array(
		  'id'      => 'post_meta',
		  'type'    => 'checkbox',
		  'class' 	=> 'horizontal',
		  'title'   => __( 'Single page elements', 'remix' ),
		  'options' => array(
			'image'		=> __('Featured image', 'remix'),
			'author'	=> __('Author', 'remix'),
			'date'		=> __('Date', 'remix'),
			'cats'		=> __('Categories', 'remix'),
			'tags'		=> __('Tags', 'remix'),
			'author_box'=> __('Author Box', 'remix'),
			'next_prev' => __('Next-Prev items', 'remix'),
		  ),
		),
		array(
		  'id'          => 'post_like',
		  'type'        => 'icon',
		  'title'       => __('Like', 'remix')
		),
		array(
		  'id'          => 'post_dislike',
		  'type'        => 'icon',
		  'title'       => __('Dislike', 'remix')
		),

	)
);

/* Artists */
$options[]   = array(
  'name'     => 'artists',
  'title'    => __('Artists', 'remix'),
  'icon'     => 'fa fa-user',
  'fields'   => array(
    array(
      'id'        => 'artists_settings',
      'type'      => 'fieldset',
		'setting_args' => array(
			'transport'	=> 'postMessage'
		),
      'fields'    => array(
		array(
		  'type'    => 'notice',
		  'class'   => 'info',
		  'content' => __('After doing changes to slugs, go to Settings > Permalinks and once save it!', 'remix')
		),
		array(
			'id'		=> 'slug',
			'type'		=> 'text',
			'title'		=> __('Slug', 'remix'),
			'attributes'=> array( 'placeholder' => 'artists' )
		),
		array(
			'id'		=> 'title',
			'type'		=> 'text',
			'title'		=> __('Title', 'remix'),
			'attributes'=> array( 'placeholder' => ucfirst( 'artists' ) )
		),
		array(
			'id'		=> 'cat_slug',
			'type'		=> 'text',
			'title'		=> __('Category slug', 'remix'),
			'attributes'=> array( 'placeholder' => 'artists/cat' )
		),
		array(
			'id'		=> 'cat_title',
			'type'		=> 'text',
			'title'		=> __('Category title', 'remix'),
			'attributes'=> array( 'placeholder' => 'Categories' )
		),
		array(
			'id'		=> 'alphabet_slug',
			'type'		=> 'text',
			'title'		=> __('Alphabet slug', 'remix'),
			'attributes'=> array( 'placeholder' => 'artists/browse' )
		),
		array(
			'id'		=> 'alphabet_title',
			'type'		=> 'text',
			'title'		=> __('Alphabet title', 'remix'),
			'attributes'=> array( 'placeholder' => 'Alphabet' )
		),
	  ),
	),
    array(
      'id'        => 'artists_query',
      'type'      => 'fieldset',
      'fields'    => array(
		array(
			'id'		=> 'posts_per_page',
			'type'		=> 'number',
			'title'		=> __('Posts per page', 'remix'),
            'attributes' => array(
              'step' => '1',
              'min' => '-1',
              'max' => '100'
            )
		),
		array(
		  'id'    => 'order',
		  'type'  => 'select',
		  'title' => __('Order', 'remix'),
		  'options' => array(
			'DESC'		=> 'DESC',
			'ASC'		=> 'ASC'
		  ),
		  'radio'   => true,
		  'default' => 'DESC',
		),
		array(
		  'id'    => 'orderby',
		  'type'  => 'select',
		  'title' => __('Order by', 'remix'),
		  'options' => array(
			'date'		=> 'date',
			'ID'		=> 'ID',
			'rand' 		=> 'rand',
			'author' 	=> 'author',
			'title' 	=> 'title',
			'name' 		=> 'name',
			'type' 		=> 'type',
			'modified' 	=> 'modified',
			'parent' 	=> 'parent',
			'comment_count' => 'comment_count',
		  ),
		  'radio'   => true,
		  'default' => 'date',
		),
        array(
          'id'              => 'extra',
          'type'            => 'group',
          'title'           => __('Query', 'remix'),
          'desc'            => __('More about: ', 'remix').'<a href="//codex.wordpress.org/Class_Reference/WP_Query" target="_blank">WP_Query</a><br /><br />',
          'button_title'    => 'Add New',
          'fields'          => array(
            array(
              'id'          => 'key',
              'type'        => 'text',
              'title'       => __('key', 'remix'),
            ),
            array(
              'id'          => 'value',
              'type'        => 'text',
              'title'       => __('Value', 'remix')
            ),
          )
        ),
      ),
    ),
	array(
		'id'		=> 'artists_col',
		'type'		=> 'number',
		'title'		=> __('Artists columns', 'remix'),
		'default' => '4',
		'attributes' => array(
			'step' => '1',
			'min' => '1',
			'max' => '4'
		)
	),
	array(
		'id'		=> 'artists_layout',
		'type'		=> 'image_select',
		'title'		=> __('Layout', 'remix'),
		'help'		=> __('Default layout', 'remix'),
		'options'	=> array(
			'inherit'         	=> CD_URI .'/img/admin/default.png',
			'without-sidebar' 	=> CD_URI .'/img/admin/full.png',
			'sidebar-right'		=> CD_URI .'/img/admin/right.png',
			'sidebar-left'		=> CD_URI .'/img/admin/left.png',
			'both-sidebar'		=> CD_URI .'/img/admin/both-sides.png',
			'both-sidebar-right'=> CD_URI .'/img/admin/both-right.png',
			'both-sidebar-left'	=> CD_URI .'/img/admin/both-left.png',
		),
		'default'	=> 'sidebar-right',
		'radio'		=> true,
        'attributes' => array(
          'data-depend-id' => 'artists_layout'
        )
	),
    array(
      'id'    	=> 'artists_primary',
      'type'  	=> 'select',
      'title' 	=> __('Primary Sidebar', 'remix'),
      'options' => codevz_get_sidebars(),
      'default' => 'primary',
      'dependency' => array( 'artists_layout', 'any', 'sidebar-right,sidebar-left,both-sidebar,both-sidebar-left,both-sidebar-right' )
    ),
    array(
      'id'    	=> 'artists_secondary',
      'type' 	=> 'select',
      'title' 	=> __('Secondary Sidebar', 'remix'),
      'options' => codevz_get_sidebars(),
      'default' => 'secondary',
      'dependency' => array( 'artists_layout', 'any', 'both-sidebar,both-sidebar-left,both-sidebar-right' )
    ),
    array(
      'id'    => 'artists_cover',
      'type'  => 'select',
      'title' => __('Cover type', 'remix'),
      'options' => array(
        '1' => __('Default', 'remix'),
        '2' => __('None', 'remix'),
        '3' => __('Image', 'remix'),
        '4' => __('Revolution Slider', 'remix'),
        '5' => __('Master Slider', 'remix'),
      ),
      'radio'   => true,
      'default' => '1',
    ),
    array(
      'id'    => 'artists_img',
      'type'  => 'upload',
      'title' => __('Upload image', 'remix'),
      'help'  => __('You can create your cover photo with one PSD that included in download package', 'remix'),
      'dependency'  => array( 'artists_cover', '==', '3' )
    ),
    array(
      'id'    => 'artists_rev',
      'type'  => 'select',
      'title' => __('Select RevSlider', 'remix'),
      'options' => codevz_get_revSlider(),
      'dependency'  => array( 'artists_cover', '==', '4' )
    ),
    array(
      'id'    => 'artists_master',
      'type'  => 'select',
      'title' => __('Select MasterSlider', 'remix'),
      'options' => codevz_get_masterSlider(),
      'dependency'  => array( 'artists_cover', '==', '5' )
    ),
	array(
		'id'		=> 'artists_slider_position',
		'type'		=> 'switcher',
		'title'		=> __('Display slider after header', 'remix'),
		'default'	=> false,
		'dependency'=> array( 'artists_cover', 'any', '4,5' )
	),
    array(
      'id'      => 'artists_related_cpt',
      'type'    => 'checkbox',
	  'class' 	=> 'horizontal',
      'title'   => __( 'Artists related items ?', 'remix' ),
	  'options' => array(
		'post'		=> __('Posts', 'remix'),
		'artists'	=> __('Artists', 'remix'),
		'songs'		=> __('Songs', 'remix'),
		'videos'	=> __('Videos', 'remix'),
		'playlists'	=> __('Playlists', 'remix'),
		'podcasts'	=> __('Podcasts', 'remix'),
		'events'	=> __('Events', 'remix'),
		'gallery'	=> __('Galleries', 'remix'),
		'lyrics'	=> __('Lyrics', 'remix'),
		'product'	=> __('Products', 'remix'),
	  ),
    ),
	array(
		'id'		=> 'artists_related',
		'type'		=> 'number',
		'title'		=> __('Related limit', 'remix'),
		'help'		=> __('0 = off', 'remix'),
        'attributes' => array(
          'step' => '1',
          'min' => '-1',
          'max' => '200'
        )
	),
	array(
	  'id'    => 'artists_related_by',
	  'type'  => 'select',
	  'title' => __('Related artists by', 'remix'),
	  'options' => array(
	    'cats' 		=> __('Categories', 'remix'),
	    'tags' 		=> __('Tags', 'remix'),
	    'alphabet' 	=> __('Alphabet', 'remix'),
	    'rand' 		=> __('Random', 'remix'),
	  ),
	  'radio'   => true,
	  'default' => 'alphabet',
	),
	array(
		'id'		=> 'artists_related_grid',
		'type'		=> 'switcher',
		'title'		=> __('Related grid', 'remix'),
		'default' 	=> false,
	),
    array(
      'id'      => 'artists_breadcrumbs',
      'type'    => 'switcher',
      'title'   => __('Breadcrumbs', 'remix'),
		'default' 	=> false,
    ),
    array(
      'id'      => 'artists_browse_all',
      'type'    => 'text',
      'title'   => __('Browse all', 'remix')
    ),
    array(
      'id'      => 'artists_alphabet',
      'type'    => 'checkbox',
	  'class' 	=> 'horizontal',
      'title'   => __( 'Alphabet list', 'remix' ),
      'help'    => __( 'Which artists pages display alphabet', 'remix' ),
	  'options' => array(
		'archive'	=> __('Archives', 'remix'),
		'tax'		=> __('Taxonomies', 'remix'),
		'single'	=> __('Singular', 'remix'),
	  ),
    ),
	array(
	  'id'      => 'artists_meta',
	  'type'    => 'checkbox',
	  'class' 	=> 'horizontal',
	  'title'   => __( 'Artist page elements', 'remix' ),
	  'options' => array(
		'image'		=> __('Featured image', 'remix'),
		'author'	=> __('Author', 'remix'),
		'date'		=> __('Date', 'remix'),
		'cats'		=> __('Categories', 'remix'),
		'tags'		=> __('Tags', 'remix'),
		'author_box'=> __('Author Box', 'remix'),
		'next_prev' => __('Next-Prev items', 'remix'),
	  ),
	),
	array(
	  'id'          => 'artists_like',
	  'type'        => 'icon',
	  'title'       => __('Like', 'remix')
	),
	array(
	  'id'          => 'artists_dislike',
	  'type'        => 'icon',
	  'title'       => __('Dislike', 'remix')
	),
  )
);


/* MusicPlayer */
$options[]   = array(
	'name'     => 'player',
	'title'    => __('Music Player', 'remix'),
	'fields'   => array(

		array(
			'id'      => 'logged_in_can_dl',
			'type'    => 'text',
			'help'    => 'If you fill out this form, then just logged in users can download music by clicking on download icon',
			'title'   => __('Logged-in users can download music', 'remix')
		),
		array(
		  'id'      => 'player_cover',
		  'type'    => 'select',
		  'title'   => __( 'Cover', 'remix' ),
		  'options' => array(
			'player_cover'	=> __( 'Square', 'remix' ),
			'masonry'		=> __( 'Free height', 'remix' ),
		  )
		),
		array(
			'id'      => 'player_popup',
			'type'    => 'text',
			'title' => __('Popup', 'remix')
		),
		array(
			'id'      => 'player_org_page',
			'type'    => 'text',
			'title' => __('Back to music page', 'remix')
		),
		array(
			'id'      => 'player_share',
			'type'    => 'text',
			'title' => __('Share', 'remix')
		),
		array(
			'id'      => 'player_repeat',
			'type'    => 'text',
			'title' => __('Repeat', 'remix')
		),
		array(
			'id'      => 'play_all_tracks',
			'type'    => 'text',
			'title' => __('Play all tracks', 'remix')
		),
		array(
			'type'     	 => 'heading',
			'content'    => __('Player styles', 'remix')
		),

		codevz_style_kit_options(array(
			'id' 		=> '_css_jp_controls',
			'title' 	=> __('Controls holder', 'remix'),
			'selector' 	=> '.jp-interface',
			'fields' 	=> array('background-color', 'margin-bottom', 'border-radius', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_jp_playlist',
			'title' 	=> __('Playlist', 'remix'),
			'selector' 	=> '.jp-playlist',
			'fields' 	=> array('background-color', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left', 'margin-top', 'margin-bottom', 'height', 'border-style', 'border-width', 'border-color', 'border-radius', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_jp_playlist_a',
			'title' 	=> __('Tracks color', 'remix'),
			'selector' 	=> '.jp-playlist .jp-playlist-item',
			'fields' 	=> array('color', 'font-size', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_js_playlist_a_hover',
			'title' 	=> __('Tracks hover & played', 'remix'),
			'selector' 	=> '.jp-playlist .jp-playlist-item:hover, .jp-playlist li.played a, li.played, .jp-playlist li:hover, .jp-playlist li:hover a',
			'fields' 	=> array('color', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_jp_playlist_buy',
			'title' 	=> __('Tracks buy icons', 'remix'),
			'selector' 	=> '.jp-playlist .buytrack a',
			'fields' 	=> array('color', 'font-size', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_jp_play_pause',
			'title' 	=> __('Play, pause icons color', 'remix'),
			'selector' 	=> '.jp-play i, .jp-pause i',
			'fields' 	=> array('color', 'background-color', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_jp_rotating',
			'title' 	=> __('Pause circle', 'remix'),
			'selector' 	=> '.rotating',
			'fields' 	=> array('border-top-color', 'border-bottom-color', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_jp_other_icons',
			'title' 	=> __('Other icons color', 'remix'),
			'selector' 	=> '.jp-previous, .jp-next, .jp-mute, .jp-unmute, .jp-repeat, .jp-repeat-off, .popup_share .fa, .player .fa-bars, .toggle_player',
			'fields' 	=> array('color', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_jp_play_bar',
			'title' 	=> __('Play bar color', 'remix'),
			'selector' 	=> '.jp-play-bar',
			'fields' 	=> array('background-color', 'custom')
		)),
		codevz_style_kit_options(array(
			'id' 		=> '_css_ajax_player',
			'title' 	=> __('Ajax player background', 'remix'),
			'selector' 	=> '.ajax_player, .toggle_player',
			'fields' 	=> array('background-color', 'custom')
		)),

	)
);

/* Post types */
$options[] = cpt_options( array( 'songs', __( 'Songs', 'remix' ), 'music' ) );
$options[] = cpt_options( array( 'playlists', __( 'Playlists', 'remix' ), 'list-alt' ) );
$options[] = cpt_options( array( 'podcasts', __( 'Podcasts', 'remix' ), 'microphone' ) );
$options[] = cpt_options( array( 'videos', __( 'Videos', 'remix' ), 'play' ) );
$options[] = cpt_options( array( 'events', __( 'Events', 'remix' ), 'ticket' ) );

/* Countdown */
$options[]   = array(
  'name'     => 'countodown',
  'title'    => __('Countdown', 'remix'),
  'icon'     => 'fa fa-clock-o',
  'fields'   => array(
	array(
		'id'      => 'countdown_day',
		'type'    => 'text',
		'title' => __('Days', 'remix')
	),
	array(
		'id'      => 'countdown_hour',
		'type'    => 'text',
		'title' => __('Hours', 'remix')
	),
	array(
		'id'      => 'countdown_minute',
		'type'    => 'text',
		'title' => __('Minutes', 'remix')
	),
	array(
		'id'      => 'countdown_seconds',
		'type'    => 'text',
		'title' => __('Seconds', 'remix')
	),
	array(
		'id'      => 'countdown_expired',
		'type'    => 'text',
		'title' => __('Expired', 'remix')
	),
  )
);

$options[] = cpt_options( array( 'gallery', __( 'Gallery', 'remix' ), 'photo' ) );
$options[] = cpt_options( array( 'lyrics', __( 'Lyrics', 'remix' ), 'file-o' ) );

/* bbpress */
$options[]   = array(
  'name'     => 'bbpress',
  'title'    => __('bbPress', 'remix'),
  'fields'   => array(
	array(
		'id'		=> 'bbpress_layout',
		'type'		=> 'image_select',
		'title'		=> __('Layout', 'remix'),
		'help'		=> __('Default layout', 'remix'),
		'options'	=> array(
			'inherit'         	=> CD_URI .'/img/admin/default.png',
			'without-sidebar' 	=> CD_URI .'/img/admin/full.png',
			'sidebar-right'		=> CD_URI .'/img/admin/right.png',
			'sidebar-left'		=> CD_URI .'/img/admin/left.png',
			'both-sidebar'		=> CD_URI .'/img/admin/both-sides.png',
			'both-sidebar-right'=> CD_URI .'/img/admin/both-right.png',
			'both-sidebar-left'	=> CD_URI .'/img/admin/both-left.png',
		),
		'default'	=> 'sidebar-right',
		'radio'		=> true,
        'attributes' => array(
          'data-depend-id' => 'bbpress_layout'
        )
	),
    array(
      'id'    	=> 'bbpress_primary',
      'type'  	=> 'select',
      'title' 	=> __('Primary Sidebar', 'remix'),
      'options' => codevz_get_sidebars(),
      'default' => 'primary',
      'dependency' => array( 'bbpress_layout', 'any', 'sidebar-right,sidebar-left,both-sidebar,both-sidebar-left,both-sidebar-right' )
    ),
    array(
      'id'    	=> 'bbpress_secondary',
      'type' 	=> 'select',
      'title' 	=> __('Secondary Sidebar', 'remix'),
      'options' => codevz_get_sidebars(),
      'default' => 'secondary',
      'dependency' => array( 'bbpress_layout', 'any', 'both-sidebar,both-sidebar-left,both-sidebar-right' )
    ),
    array(
      'id'    => 'bbpress_cover',
      'type'  => 'select',
      'title' => __('Cover type', 'remix'),
      'options' => array(
        '1' => __('Default', 'remix'),
        '2' => __('None', 'remix'),
        '3' => __('Image', 'remix'),
        '4' => __('Revolution Slider', 'remix'),
        '5' => __('Master Slider', 'remix'),
      ),
      'radio'   => true,
      'default' => '1',
    ),
    array(
      'id'    => 'bbpress_img',
      'type'  => 'upload',
      'title' => __('Upload image', 'remix'),
      'help'  => __('You can create your cover photo with one PSD that included in download package', 'remix'),
      'dependency'  => array( 'bbpress_cover', '==', '3' )
    ),
    array(
      'id'    => 'bbpress_rev',
      'type'  => 'select',
      'title' => __('Select RevSlider', 'remix'),
      'options' => codevz_get_revSlider(),
      'dependency'  => array( 'bbpress_cover', '==', '4' )
    ),
    array(
      'id'    => 'bbpress_master',
      'type'  => 'select',
      'title' => __('Select MasterSlider', 'remix'),
      'options' => codevz_get_masterSlider(),
      'dependency'  => array( 'bbpress_cover', '==', '5' )
    ),
	array(
		'id'		=> 'bbpress_slider_position',
		'type'		=> 'switcher',
		'title'		=> __('Display slider after header', 'remix'),
		'default'	=> false,
		'dependency'=> array( 'bbpress_cover', 'any', '4,5' )
	),
  )
);

/* budypress */
/* woocommerce */
$options[]   = array(
  'name'     => 'woocommerce',
  'title'    => __('woocommerce', 'remix'),
  'fields'   => array(
	array(
		'id'      => 'woocommerce_breadcrumbs',
		'type'    => 'switcher',
		'title'   => __('Breadcrumbs', 'remix')
	),
	array(
		'id'      => 'woocommerce_cart',
		'type'    => 'switcher',
		'title'   => __('Cart on header', 'remix'),
		'default' 	=> false,
	),
	array(
		'id'		=> 'woocommerce_layout',
		'type'		=> 'image_select',
		'title'		=> __('Layout', 'remix'),
		'help'		=> __('Default layout', 'remix'),
		'options'	=> array(
			'inherit'         	=> CD_URI .'/img/admin/default.png',
			'without-sidebar' 	=> CD_URI .'/img/admin/full.png',
			'sidebar-right'		=> CD_URI .'/img/admin/right.png',
			'sidebar-left'		=> CD_URI .'/img/admin/left.png',
			'both-sidebar'		=> CD_URI .'/img/admin/both-sides.png',
			'both-sidebar-right'=> CD_URI .'/img/admin/both-right.png',
			'both-sidebar-left'	=> CD_URI .'/img/admin/both-left.png',
		),
		'default'	=> 'sidebar-right',
		'radio'		=> true,
        'attributes' => array(
          'data-depend-id' => 'woocommerce_layout'
        )
	),
    array(
      'id'    	=> 'woocommerce_primary',
      'type'  	=> 'select',
      'title' 	=> __('Primary Sidebar', 'remix'),
      'options' => codevz_get_sidebars(),
      'default' => 'primary',
      'dependency' => array( 'woocommerce_layout', 'any', 'sidebar-right,sidebar-left,both-sidebar,both-sidebar-left,both-sidebar-right' )
    ),
    array(
      'id'    	=> 'woocommerce_secondary',
      'type' 	=> 'select',
      'title' 	=> __('Secondary Sidebar', 'remix'),
      'options' => codevz_get_sidebars(),
      'default' => 'secondary',
      'dependency' => array( 'woocommerce_layout', 'any', 'both-sidebar,both-sidebar-left,both-sidebar-right' )
    ),
    array(
      'id'    => 'woocommerce_cover',
      'type'  => 'select',
      'title' => __('Cover type', 'remix'),
      'options' => array(
        '1' => __('Default', 'remix'),
        '2' => __('None', 'remix'),
        '3' => __('Image', 'remix'),
        '4' => __('Revolution Slider', 'remix'),
        '5' => __('Master Slider', 'remix'),
      ),
      'radio'   => true,
      'default' => '1',
    ),
    array(
      'id'    => 'woocommerce_img',
      'type'  => 'upload',
      'title' => __('Upload image', 'remix'),
      'help'  => __('You can create your cover photo with one PSD that included in download package', 'remix'),
      'dependency'  => array( 'woocommerce_cover', '==', '3' )
    ),
    array(
      'id'    => 'woocommerce_rev',
      'type'  => 'select',
      'title' => __('Select RevSlider', 'remix'),
      'options' => codevz_get_revSlider(),
      'dependency'  => array( 'woocommerce_cover', '==', '4' )
    ),
    array(
      'id'    => 'woocommerce_master',
      'type'  => 'select',
      'title' => __('Select MasterSlider', 'remix'),
      'options' => codevz_get_masterSlider(),
      'dependency'  => array( 'woocommerce_cover', '==', '5' )
    ),
	array(
		'id'		=> 'woocommerce_slider_position',
		'type'		=> 'switcher',
		'title'		=> __('Display slider after header', 'remix'),
		'default'	=> false,
		'dependency'=> array( 'woocommerce_cover', 'any', '4,5' )
	),
  )
);

/* Backup */
$options[]   = array(
  'name'     => 'backup',
  'title'    => __('Backup / Restore', 'remix'),
  'fields'   => array(
    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => __('You can save your current options. Download a Backup and Import.', 'remix'),
    ),
    array(
      'type'    => 'backup',
    )
  )
);

function cpt_options( $i ){
	
	$cols = array(
		'id'		=> $i[0].'_col',
		'type'		=> 'number',
		'title'		=> __('Columns', 'remix'),
		'default' => '2',
		'attributes' => array(
			'step' => '1',
			'min' => '1',
			'max' => '4'
		)
	);
	$cols = ( $i[0] !== 'events' ) ? $cols : array('type'=>'subheading','content'=>'');
	
	$gallery = array(
		'id'		=> 'gallery_modern',
		'type'		=> 'switcher',
		'title'		=> __('Modern gallery', 'remix'),
		'default' 	=> false,
	);
	$gallery = ( $i[0] === 'gallery' ) ? $gallery : array('type'=>'subheading','content'=>'');
	
	return array(
	  'name'     => $i[0],
	  'title'    => $i[1],
	  'icon'     => 'fa fa-'.$i[2],
	  'fields'   => array(
        array(
          'id'        => $i[0].'_settings',
          'type'      => 'fieldset',
			'setting_args' => array(
				'transport'	=> 'postMessage'
			),
          'fields'    => array(
			array(
			  'type'    => 'notice',
			  'class'   => 'info',
			  'content' => __('After doing changes to slugs, go to Settings > Permalinks and once save it!', 'remix')
			),
			array(
				'id'		=> 'slug',
				'type'		=> 'text',
				'title'		=> __('Slug', 'remix'),
				'attributes'=> array( 'placeholder' => $i[0] )
			),
			array(
				'id'		=> 'title',
				'type'		=> 'text',
				'title'		=> __('Title', 'remix'),
				'attributes'=> array( 'placeholder' => $i[1] )
			),
			array(
				'id'		=> 'cat_slug',
				'type'		=> 'text',
				'title'		=> __('Category slug', 'remix'),
				'attributes'=> array( 'placeholder' => $i[0].'/cat' )
			),
			array(
				'id'		=> 'cat_title',
				'type'		=> 'text',
				'title'		=> __('Category title', 'remix'),
				'attributes'=> array( 'placeholder' => 'Categories' )
			),
		  ),
		),
        array(
          'id'        => $i[0].'_query',
          'type'      => 'fieldset',
          'fields'    => array(
			array(
				'id'		=> 'posts_per_page',
				'type'		=> 'number',
				'title'		=> __('Posts per page', 'remix'),
	            'attributes' => array(
	              'step' => '1',
	              'min' => '-1',
	              'max' => '100'
	            )
			),
			array(
			  'id'    => 'order',
			  'type'  => 'select',
			  'title' => __('Order', 'remix'),
			  'options' => array(
				'DESC'		=> 'DESC',
				'ASC'		=> 'ASC'
			  ),
			  'radio'   => true,
			  'default' => 'DESC',
			),
			array(
			  'id'    => 'orderby',
			  'type'  => 'select',
			  'title' => __('Order by', 'remix'),
			  'options' => array(
				'date'		=> 'date',
				'ID'		=> 'ID',
				'rand' 		=> 'rand',
				'author' 	=> 'author',
				'title' 	=> 'title',
				'name' 		=> 'name',
				'type' 		=> 'type',
				'modified' 	=> 'modified',
				'parent' 	=> 'parent',
				'comment_count' => 'comment_count',
			  ),
			  'radio'   => true,
			  'default' => 'date',
			),
	        array(
	          'id'              => 'extra',
	          'type'            => 'group',
	          'title'           => __('Query', 'remix'),
	          'desc'            => __('More about: ', 'remix').'<a href="//codex.wordpress.org/Class_Reference/WP_Query" target="_blank">WP_Query</a><br /><br />',
	          'button_title'    => 'Add New',
	          'fields'          => array(
	            array(
	              'id'          => 'key',
	              'type'        => 'text',
	              'title'       => __('key', 'remix'),
	            ),
	            array(
	              'id'          => 'value',
	              'type'        => 'text',
	              'title'       => __('Value', 'remix')
	            ),
	          )
	        ),
	      ),
	    ),
		$cols,
		$gallery,
		array(
			'id'		=> $i[0].'_layout',
			'type'		=> 'image_select',
			'title'		=> __('Layout', 'remix'),
			'help'		=> __('Default layout', 'remix'),
			'options'	=> array(
				'inherit'         	=> CD_URI .'/img/admin/default.png',
				'without-sidebar' 	=> CD_URI .'/img/admin/full.png',
				'sidebar-right'		=> CD_URI .'/img/admin/right.png',
				'sidebar-left'		=> CD_URI .'/img/admin/left.png',
				'both-sidebar'		=> CD_URI .'/img/admin/both-sides.png',
				'both-sidebar-right'=> CD_URI .'/img/admin/both-right.png',
				'both-sidebar-left'	=> CD_URI .'/img/admin/both-left.png',
			),
			'default'	=> 'sidebar-right',
			'radio'		=> true,
            'attributes' => array(
              'data-depend-id' => $i[0].'_layout'
            )
		),
        array(
          'id'    	=> $i[0].'_primary',
          'type'  	=> 'select',
          'title' 	=> __('Primary Sidebar', 'remix'),
          'options' => codevz_get_sidebars(),
          'default' => 'primary',
          'dependency' => array( $i[0].'_layout', 'any', 'sidebar-right,sidebar-left,both-sidebar,both-sidebar-left,both-sidebar-right' )
        ),
        array(
          'id'    	=> $i[0].'_secondary',
          'type' 	=> 'select',
          'title' 	=> __('Secondary Sidebar', 'remix'),
          'options' => codevz_get_sidebars(),
          'default' => 'secondary',
          'dependency' => array( $i[0].'_layout', 'any', 'both-sidebar,both-sidebar-left,both-sidebar-right' )
        ),
        array(
          'id'    => $i[0].'_cover',
          'type'  => 'select',
          'title' => __('Cover type', 'remix'),
          'options' => array(
            '1' => __('Default', 'remix'),
            '2' => __('None', 'remix'),
            '3' => __('Image', 'remix'),
            '4' => __('Revolution Slider', 'remix'),
            '5' => __('Master Slider', 'remix'),
          ),
          'radio'   => true,
          'default' => '1',
        ),
        array(
          'id'    => $i[0].'_img',
          'type'  => 'upload',
          'title' => __('Upload image', 'remix'),
          'help'  => __('You can create your cover photo with one PSD that included in download package', 'remix'),
          'dependency'  => array( $i[0].'_cover', '==', '3' )
        ),
        array(
          'id'    => $i[0].'_rev',
          'type'  => 'select',
          'title' => __('Select RevSlider', 'remix'),
          'options' => codevz_get_revSlider(),
          'dependency'  => array( $i[0].'_cover', '==', '4' )
        ),
        array(
          'id'    => $i[0].'_master',
          'type'  => 'select',
          'title' => __('Select MasterSlider', 'remix'),
          'options' => codevz_get_masterSlider(),
          'dependency'  => array( $i[0].'_cover', '==', '5' )
        ),
		array(
			'id'		=> $i[0].'_slider_position',
			'type'		=> 'switcher',
			'title'		=> __('Display slider after header', 'remix'),
			'default'	=> false,
			'dependency'=> array( $i[0].'_cover', 'any', '4,5' )
		),
		array(
			'id'		=> $i[0].'_related',
			'type'		=> 'number',
			'title'		=> __('Related', 'remix'),
			'help'		=> __('0 = off', 'remix'),
            'attributes' => array(
              'step' => '1',
              'min' => '-1',
              'max' => '200'
            )
		),
		array(
		  'id'    => $i[0].'_related_by',
		  'type'  => 'select',
		  'title' => __( 'Related by', 'remix' ),
		  'options' => array(
		    'artists' 	=> __( 'Artists', 'remix' ),
		    'cats' 		=> __( 'Categories', 'remix' ),
		    'tags' 		=> __( 'Tags', 'remix' ),
		    'rand' 		=> __( 'Random', 'remix' ),
		  ),
		  'radio'   => true,
		  'default' => 'artists',
		),
		array(
			'id'		=> $i[0].'_related_grid',
			'type'		=> 'switcher',
			'title'		=> __('Related grid', 'remix'),
			'default' 	=> false,
		),
        array(
          'id'      => $i[0].'_breadcrumbs',
          'type'    => 'switcher',
          'title'   => __('Breadcrumbs', 'remix'),
		  'default' 	=> true
        ),
        array(
          'id'      => $i[0].'_alphabet',
          'type'    => 'checkbox',
		  'class' 	=> 'horizontal',
          'title'   => __( 'Alphabet list', 'remix' ),
          'help'    => __( 'Which pages display alphabet', 'remix' ),
		  'options' => array(
			'archive'	=> __('Archives', 'remix'),
			'tax'		=> __('Taxonomies', 'remix'),
			'single'	=> __('Singular', 'remix'),
		  ),
        ),
		array(
		  'id'      => $i[0].'_meta',
		  'type'    => 'checkbox',
		  'class' 	=> 'horizontal',
		  'title'   => __( 'Single page elements', 'remix' ),
		  'options' => array(
			'image'		=> __('Featured image', 'remix'),
			'author'	=> __('Author', 'remix'),
			'date'		=> __('Date', 'remix'),
			'cats'		=> __('Categories', 'remix'),
			'tags'		=> __('Tags', 'remix'),
			'author_box'=> __('Author Box', 'remix'),
			'next_prev' => __('Next-Prev items', 'remix'),
		  ),
		),
		array(
		  'id'          => $i[0].'_like',
		  'type'        => 'icon',
		  'title'       => __('Like', 'remix')
		),
		array(
		  'id'          => $i[0].'_dislike',
		  'type'        => 'icon',
		  'title'       => __('Dislike', 'remix')
		),

	  )
	);
}

// Customizer
if ( class_exists('CSF_Customize') ) {
	CSF_Customize::instance( $options, '_cs_options' );
}

// Theme Options
if ( class_exists('CSF_Options') ) {
	$csf_settings = array(
		'option_name'      => '_cs_options',
		'menu_title'       => 'Theme Options',
		'menu_type'        => 'theme', // menu, submenu, options, theme, etc.
		'menu_slug'        => 'themes.php',
		'ajax_save'        => false,
		'show_search'      => true,
		'show_reset'       => true,
		'show_footer'      => false,
		'show_all_options' => true,
		'sticky_header'    => true,
		'save_defaults'    => true,
		'framework_title'  => 'Theme Options <small>by Codevz</small>',
	);
	//CSF_Options::instance( $csf_settings, $options );
}

//
//
// Helper function
//
//
function codevz_get_sidebars( $options = array() ) {
	$sidebars = (array) get_option( 'sidebars_widgets' );
	foreach ( $sidebars as $i => $w ) {
		if ( isset( $i ) && ( $i !== 'array_version' && $i !== 'jr-insta-shortcodes' && $i !== 'wp_inactive_widgets' ) ) {
			$options[ $i ] = ucwords( $i );
		}
	}

	return $options;
}

function codevz_get_masterSlider( $options = array() ) {
	if ( class_exists( 'Master_Slider' ) ) {
		global $mspdb;
		if ( $ms = $mspdb->get_sliders_list( $limit = 0, $offset  = 0, $orderby = 'ID', $sort = 'ASC' ) ) {
			foreach ( $ms as $slider ) {
				$options[ $slider['ID'] ] = esc_attr( $slider['title'] );
			}
		} else {
			$options = array( __('No sliders found, Please create new from MasterSlider menu', 'remix') );
		}
	} else {
		$options = array( __('Sorry! MasterSlider is not installed or activated', 'remix') );
	}

	return $options;
}

function codevz_get_revSlider( $options = array() ) {
	if ( class_exists( 'RevSliderAdmin' ) ) {
		$slider  = new RevSlider();
		$options = $slider->getArrSlidersShort();
		if ( empty( $options ) ) {
			$options = array( __('No sliders found, Please create new from Revolution Slider menu', 'remix') );
		}
	} else {
		$options = array( __('Sorry! Revolution Slider is not installed or activated', 'remix') );
	}

	return $options;
}

function codevz_style_kit_options( $args ) {

	$fields = $final = array();
	$fields['color'] = array(
		'id'    => 'color',
		'type'  => 'color_picker',
		'title' => __( 'Text color', 'remix' )
	);
	$fields['background-color'] = array(
		'id'    => 'background-color',
		'type'  => 'color_picker',
		'title' => __( 'Background color', 'remix' )
	);
	$fields['font-family'] = array(
		'id'    => 'font-family',
		'type'  => 'text',
		'title' => __( 'Google font name', 'remix' ),
		'help'  => 'eg. Raleway',
		'desc'  => '<a href="//fonts.google.com/" target="_blank">' . __( 'List of fonts names', 'remix' ) . '</a>'
	);
	$fields['font-size'] = array(
		'id'        => 'font-size',
		'type'      => 'slider',
		'title'     => __( 'Font size', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => 0,
		    'max'     => 120,
		    'unit'    => 'px'
		)
	);
	$fields['background'] = array(
		'id'    => 'background',
		'type'  => 'background',
		'title' => __( 'Background', 'remix' )
	);
	$fields['width'] = array(
		'id'        => 'width',
		'type'      => 'slider',
		'title'     => __( 'Width', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => 0,
		    'max'     => 1000,
		    'unit'    => 'px'
		)
	);
	$fields['height'] = array(
		'id'        => 'height',
		'type'      => 'slider',
		'title'     => __( 'Height', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => 0,
		    'max'     => 400,
		    'unit'    => 'px'
		)
	);
	$fields['float'] = array(
		'id'    => 'float',
		'type'  => 'select',
		'title' => __( 'Position', 'remix' ),
		'options' => array(
			'left' 	=> __( 'Left', 'remix' ),
			'right' => __( 'Right', 'remix' ),
			'none;display:table;margin-left: auto !important;margin-right: auto !important' 	=> __( 'Center', 'remix' ),
			'none' 	=> __( 'None', 'remix' ),
		)
	);

	$fields['padding-top'] = array(
		'id'        => 'padding-top',
		'type'      => 'slider',
		'title'     => __( 'Padding top', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => 0,
		    'max'     => 200,
		    'unit'    => 'px'
		)
	);
	$fields['padding-right'] = array(
		'id'        => 'padding-right',
		'type'      => 'slider',
		'title'     => __( 'Padding right', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => 0,
		    'max'     => 200,
		    'unit'    => 'px'
		)
	);
	$fields['padding-bottom'] = array(
		'id'        => 'padding-bottom',
		'type'      => 'slider',
		'title'     => __( 'Padding bottom', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => 0,
		    'max'     => 200,
		    'unit'    => 'px'
		)
	);
	$fields['padding-left'] = array(
		'id'        => 'padding-left',
		'type'      => 'slider',
		'title'     => __( 'Padding left', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => 0,
		    'max'     => 200,
		    'unit'    => 'px'
		)
	);

	$fields['margin-top'] = array(
		'id'        => 'margin-top',
		'type'      => 'slider',
		'title'     => __( 'Margin top', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => -200,
		    'max'     => 200,
		    'unit'    => 'px'
		)
	);
	$fields['margin-right'] = array(
		'id'        => 'margin-right',
		'type'      => 'slider',
		'title'     => __( 'Margin right', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => -200,
		    'max'     => 200,
		    'unit'    => 'px'
		)
	);
	$fields['margin-bottom'] = array(
		'id'        => 'margin-bottom',
		'type'      => 'slider',
		'title'     => __( 'Margin bottom', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => -200,
		    'max'     => 200,
		    'unit'    => 'px'
		)
	);
	$fields['margin-left'] = array(
		'id'        => 'margin-left',
		'type'      => 'slider',
		'title'     => __( 'Margin left', 'remix' ),
		'options'   => array(
		    'step'    => 1,
		    'min'     => -200,
		    'max'     => 200,
		    'unit'    => 'px'
		)
	);

	$fields['border-style'] = array(
		'id'    => 'border-style',
		'type'  => 'select',
		'title' => __( 'Border', 'remix' ),
		'options' => array(
			'solid'		=> 'solid',
			'dotted'	=> 'dotted',
			'dashed'	=> 'dashed',
		)
	);
	$fields['border-width'] = array(
		'id'    => 'border-width',
		'type'  => 'text',
		'title' => __( 'Border width', 'remix' ),
		'help'  => 'The border width property is used to generate lines around element. You can set this four positions respectively: <br />Top Right Bottom Left <br/><br/>eg. 2px 2px 2px 2px'
	);
	$fields['border-color'] = array(
		'id'    => 'border-color',
		'type'  => 'color_picker',
		'title' => __( 'Border color', 'remix' )
	);
	$fields['border-radius'] = array(
		'id'    => 'border-radius',
		'type'  => 'text',
		'title' => __( 'Border radius', 'remix' ),
		'help'  => 'eg. 100% or 6px or 0 6px 6px 6px',
	);
	$fields['custom'] = array(
		'id'    => 'custom',
		'type'  => 'textarea',
		'title' => __( 'Custom style', 'remix' ),
		'help'  => 'You can add custom css styling for this element. <br /><br />eg. text-align: center;'
	);

	if ( isset( $args['fields'] ) ) {
		foreach ( $args['fields'] as $field ) {
			if ( $field === 'border-top-color' ) {
				$fields['border-top-color'] = array(
					'id'    => 'border-top-color',
					'type'  => 'color_picker',
					'title' => __( 'Border color', 'remix' )
				);
				$fields['border-bottom-color'] = array(
					'id'    => 'border-bottom-color',
					'type'  => 'color_picker',
					'title' => __( 'Border 2 color', 'remix' )
				);
			}
			$final[] = $fields[ $field ];
		}
	} else if ( isset( $args['unset'] ) ) {
		foreach ( $args['unset'] as $field ) {
			unset( $fields[ $field ] );
		}
		$final = $fields;
	} else {
		$final = $fields;
	}

	$final = array_merge( array(
		array(
			'id'   		 => 'title',
			'title'   	 => ' ',
			'type'       => 'text',
			'default'    => $args['title'],
			'dependency' => array( 'xxx', '==', 'xxx' ),
		),
		array(
			'id'   		 => 'selector',
			'title'   	 => ' ',
			'type'       => 'text',
			'default'    => $args['selector'],
			'dependency' => array( 'xxx', '==', 'xxx' ),
		)
	), $final );

	return array(
		'id'              => $args['id'],
		'type'            => 'group',
		'limit' 		  => 1,
		'title'           => $args['title'],
		'button_title'    => __( 'Customize', 'remix' ),
		'accordion_title' => __( 'Customize', 'remix' ),
		'fields'          => $final
	);
}

//
// Start migration functions to v3.x
//
add_action( 'wp_ajax_codevz_migration_options', 'codevz_migration_options' );
add_action( 'wp_ajax_codevz_migration_metaboxes', 'codevz_migration_metaboxes' );
add_action( 'wp_ajax_codevz_delete_old_rows', 'codevz_delete_old_rows' );
add_action( 'wp_ajax_codevz_migration_done', 'codevz_migration_done' );
function codevz_migration_options() {
	check_ajax_referer( 'cd_migration_nonce', 'nonce' );
	
	$old = get_option( 'option_tree' );
	$new = array();

	$new['activation'] = '';
	$new['maintenance'] = '';
	$new['pageloader_bg'] = '';
	$new['pageloader_img'] = '';
	$new['ajax_tracks'] = '';
	$new['before_header'] = '';
	$new['after_header'] = '';
	$new['before_content'] = '';
	$new['after_content'] = '';
	$new['before_sidebar_p'] = '';
	$new['before_sidebar_s'] = '';
	$new['after_sidebar_p'] = '';
	$new['after_sidebar_s'] = '';
	$new['before_footer'] = '';
	$new['between_footer'] = '';
	$new['after_footer'] = '';

	if ( $old['meta_desc'] || $old['meta_key'] ) {
		$new['seo'] = 1;
	} else {
		$new['seo'] = 0;
	}
	$new['description'] = $old['meta_desc'];
	$new['keywords'] = $old['meta_key'];
	$new['rss'] = $old['rss-feed'];

	$new['pageloader'] = 0;
	$new['pageloader_time'] = $old['remix_loading_time'];

	$new['wp_login_logo'] = $old['login_logo'];

	$new['head_codes'] = $old['custom-codes-head'];
	$new['foot_codes'] = $old['custom-codes-footer'];
	if ( !empty( $old['custom-css-head'] ) ) {
		$new['head_codes'] .= '<style>' . $old['custom-css-head'] . '</style>';
	}
	if ( !empty( $old['custom-js-head'] ) ) {
		$new['head_codes'] .= '<script type="text/javascript">' . $old['custom-js-head'] . '</script>';
	}

	$new['ajax'] = 0;
	$new['ajax_autoplay'] = 0;
	$new['cover_position'] = 0;
	$new['ajax_cover'] = 1;
	$new['ajax_repeat'] = 1;

	$new['responsive'] = $old['responsive'];
	$new['breadcrumbs'] = $old['T20_breadcrumbs'];
	$new['layout'] = $old['layout-global'];
	$new['primary'] = 'primary';
	$new['secondary'] = 'secondary';

	foreach ( (array) $old['sidebar-areas'] as $olll ) {
		unset( $old['sidebar-areas'][$olll]['title'] );
	}
	$new['sidebars'] = $old['sidebar-areas'];

	$new['scrollbar'] = '1';
	$new['footer'] = $old['footer-widgets'];
	$new['totop'] = $new['subfooter'] = 1;
	$new['add_image_size'] = null;

	$new['copyright'] = $old['copyright'];
	$new['sticky'] = $old['sticky'];
	$new['cover'] = $old['page_cover_type'];
	$new['img'] = $old['page_cover_image'];
	$new['rev'] = $old['page_cover_slider'];
	$new['master'] = $old['page_cover_master'];
	$new['search'] = ( $old['header-search'] === 'on' ) ? 1 : 0;
	$new['search_placeholder'] = $old['eader-search-placeholder'];
	$new['header_custom_text'] = $old['header-custom-text'];
	$new['login'] = $old['login_signin'];
	$new['register'] = $old['login_reg'];
	$new['lost_p'] = $old['login_lost'];
	$new['lost_p_btn'] = 'Get new password';
	$new['username'] = $old['login_user'];
	$new['password'] = $old['login_pass'];
	$new['email'] = 'Email';
	$new['username_email'] = $old['lost_pass_placeholder'];
	$new['login_error'] = $old['lost_pass_error_d'];
	$new['register_success'] = $old['signup_successful'];
	$new['lost_p_error_not_fount'] = $old['lost_pass_error_c'];
	$new['lost_p_success'] = $old['lost_pass_error_g'];
	$new['my_profile'] = $old['your_profile'];
	$new['edit_my_profile'] = $old['edit_your_profile'];
	$new['logout'] = $old['logout'];
	$new['music_submission'] = 'Submit music';
	$new['login_custom_link'] = $old['register_btn_link'];

	$new['social'] = $old['social-links'];
	$new['social_position'] = $old['social_position'];
	$new['social_colored'] = ( $old['social_color'] === 'on' ) ? 1 : 0;
	$new['social_circular'] = ( $old['social_circle'] === 'on' ) ? 1 : 0;

	$new['excerpt'] = $old['excerpt-length'];
	$new['read_more'] = $old['read_more_text'];
	$new['pagination'] = ( $old['wp_pagenavi'] === 'on' ) ? 1 : 0;
	$new['next_page'] = $old['nav_newer_tr'];
	$new['prev_page'] = $old['nav_older_tr'];
	$new['not_found'] = $old['archive_tr'];
	$new['related_title'] = $old['related_post_tr'];
	$new['m_post_thumb'] = 0;
	$new['post_related'] = $old['related_posts_num'];
	$new['post_related_by'] = $old['related_posts'];
	$new['post_meta'] = array( 'image', 'author', 'date', 'cats', 'tags', 'next_prev', 'author_box' );
	$new['post_like'] = 'fa fa-heart';
	$new['post_dislike'] = 'fa fa-heart-o';

	$pts = array( 'artists', 'songs', 'videos', 'podcasts', 'playlists', 'events', 'gallery', 'lyrics' );

	foreach ( $pts as $pt ) {
		$new[$pt . '_settings'] = array();
		$new[$pt . '_settings']['slug'] = $old[$pt . '_slug'];
		$new[$pt . '_settings']['title'] = $old[$pt . '_title'];
		$new[$pt . '_settings']['cat_slug'] = $pt . '/cat';
		$new[$pt . '_settings']['cat_title'] = 'Categories';
		$new[$pt . '_settings']['alphabet_slug'] = $old[$pt . '_tax_slug'];
		$new[$pt . '_settings']['alphabet_title'] = $old[$pt . '_tax_title'];

		$new[$pt . '_query'] = array();
		$new[$pt . '_query']['posts_per_page'] = $old['ppp_artists'];
		$new[$pt . '_query']['order'] = $old[$pt . '_order'];
		$new[$pt . '_query']['orderby'] = $old[$pt . '_orderby'];
		$new[$pt . '_query']['extra'] = null;

		$new[$pt . '_col'] = '4';
		$new[$pt . '_layout'] = $old[$pt . '_layout'];
		$new[$pt . '_primary'] = $old[$pt . '_p'];
		$new[$pt . '_secondary'] = $old[$pt . '_s'];
		$new[$pt . '_cover'] = $old[$pt . '_cover_type'];
		$new[$pt . '_img'] = $old[$pt . '_cover_image'];
		$new[$pt . '_rev'] = $old[$pt . '_cover_slider'];
		$new[$pt . '_master'] = $old[$pt . '_cover_master'];
		$new[$pt . '_slider_position'] = 0;
		$new[$pt . '_related_cpt'] = array( 'songs', 'videos', 'events' );
		$new[$pt . '_related'] = '6';
		$new[$pt . '_related_by'] = 'cat';

		$new[$pt . '_breadcrumbs'] = ( $old[$pt . '_breadcrumb'] === 'on' ) ? 1 : 0;
		$new[$pt . '_browse_all'] = $old['alphabet_browseall'];
		$new[$pt . '_alphabet'] = array('archive', 'tax');
		$new[$pt . '_meta'] = array( 'cats', 'tags', 'next_prev' );
		$new[$pt . '_like'] = 'fa fa-heart';
		$new[$pt . '_dislike'] = 'fa fa-heart-o';
		$new[$pt . '_related_grid'] = 0;
	}
	
	$new['gallery_modern'] = 0;
	$new['player_cover'] = 'player_cover';
	$new['player_popup'] = 'Popup';
	$new['player_org_page'] = 'Back to music page';
	$new['player_share'] = 'Share';
	$new['player_repeat'] = 'Repeat';

	$new['bbpress_layout'] = $old['layout-bbp'];
	$new['bbpress_primary'] = $old['bbp_primary'];
	$new['bbpress_secondary'] = $old['bbp_secondary'];
	$new['bbpress_cover'] = 1;
	$new['bbpress_img'] = $new['bbpress_rev'] = $new['bbpress_master'] = '';
	$new['bbpress_slider_position'] = 0;
	$new['woocommerce_breadcrumbs'] = 1;
	$new['woocommerce_cart'] = ( $old['shop_head'] === 'on' ) ? 1 : 0;
	$new['woocommerce_layout'] = $old['layout-woo'];
	$new['woocommerce_primary'] = $old['_woo_primary'];
	$new['woocommerce_secondary'] = $old['_woo_secondary'];
	$new['woocommerce_cover'] = $old['woo_cover_type'];
	$new['woocommerce_img'] = $old['woo_cover_image'];
	$new['woocommerce_rev'] = $old['woo_cover_slider'];
	$new['woocommerce_master'] = $old['woo_cover_master'];
	$new['woocommerce_slider_position'] = 1;

	$new['countdown_day'] = 'Days';
	$new['countdown_hour'] = 'Hours';
	$new['countdown_minute'] = 'Minutes';
	$new['countdown_seconds'] = 'Seconds';
	$new['countdown_plus'] = '';
	$new['countdown_expired'] = 'Expired';
	$new['add_customizer_setting'] = null;
	$new['add_customizer_typo'] = null;

	$new['404'] = '';
	$new['import'] = '';
	$new['play_all_tracks'] = 'Play all tracks';
	$new['post_related_grid'] = 0;

	$new['logo'] = $old['custom-logo'];
	$new['full_boxed'] = $old['full-boxed'];

	/* Import new options */
	update_option( '_cs_options', $new );

	if ( ! $old || $old === false ) {
		echo '<p style="color: red">Couldn\'t copy theme option settings, Please try again, if again you see this error, So you need to re-setting up your theme options manually.</p>';
	} else {
		echo '<p><img src="'. get_admin_url() . '/images/yes.png"> Completed successfully </p>';
	}

	die();
}

/* Migrate metaboxes */
function codevz_migration_metaboxes() {
	check_ajax_referer( 'cd_migration_nonce', 'nonce' );

	$posts = get_posts( array(
		'post_type' => array( 'post', 'page', 'artists', 'songs', 'videos', 'gallery', 'events' ),
		'numberposts' => -1,
	));

	$ids = wp_list_pluck( $posts, 'ID' );

	foreach ( $ids as $id ) {

		$public = $songs = $videos = $events = $gallery = $artists = array();
		$artist_nameaa = (array) get_post_meta( $id, 'artist_nameaa', 1 );

		$public = array(
			'hiding_elms'	=> array(),
			'show_alphabet'	=> 0,
			'cover'			=> get_post_meta( $id, 'page_cover_type', 1 ),
			'img'			=> get_post_meta( $id, 'page_cover_image', 1 ),
			'rev'			=> get_post_meta( $id, 'page_cover_slider', 1 ),
			'master'		=> get_post_meta( $id, 'page_cover_master', 1 ),
			'cover_position'=> 0,
			'layout'		=> get_post_meta( $id, '_layout', 1 ),
			'primary'		=> get_post_meta( $id, '_sidebar_primary', 1 ),
			'secondary'		=> get_post_meta( $id, '_sidebar_secondary', 1 ),
			'artist'		=> $artist_nameaa,
		);

		if ( 'songs' === get_post_type( $id ) ) {
			$tracks = (array) get_post_meta( $id, 'playlist', 1 );

			foreach ( $tracks as $tr => $rt ) {
				if ( ! empty( $rt['buy_icon_a'] ) ) {
					$tracks[ $tr ]['buy_icon_a'] = 'fa fa-' . $rt['buy_icon_a'];
				}
				if ( ! empty( $rt['buy_icon_b'] ) ) {
					$tracks[ $tr ]['buy_icon_b'] = 'fa fa-' . $rt['buy_icon_b'];
				}
				if ( ! empty( $rt['buy_icon_c'] ) ) {
					$tracks[ $tr ]['buy_icon_c'] = 'fa fa-' . $rt['buy_icon_c'];
				}
				if ( ! empty( $rt['buy_icon_d'] ) ) {
					$tracks[ $tr ]['buy_icon_d'] = 'fa fa-' . $rt['buy_icon_d'];
				}
			}

			$songs = array(
				'player'				=> get_post_meta( $id, 'rx_player', 1 ),
				'tracks'				=> $tracks,
				'embed'					=> get_post_meta( $id, 'custom_player', 1 ),
				'player_cover'			=> get_post_meta( $id, 'showcover', 1 ),
				'autoplay'				=> get_post_meta( $id, 'auto_play', 1 ),
				'loadRingColor'			=> get_post_meta( $id, 'loadRingColor', 1 ),
				'playRingColor'			=> get_post_meta( $id, 'playRingColor', 1 ),
				'waveformDataColor'		=> get_post_meta( $id, 'waveformDataColor', 1 ),
				'waveformDataLineRatio'	=> get_post_meta( $id, 'waveformDataLineRatio', 1 ),
				'eqDataColor'			=> get_post_meta( $id, 'eqDataColor', 1 ),
				'eqDataLineRatio'		=> get_post_meta( $id, 'eqDataLineRatio', 1 ),
				'peakDataColor'			=> get_post_meta( $id, 'peakDataColor', 1 ),
				'useAmplifier'			=> get_post_meta( $id, 'useAmplifier', 1 ),
				'scaleArcWidth'			=> get_post_meta( $id, 'scaleArcWidth', 1 )
			);

			$downloads = get_post_meta( $id, 'download_count', 1 );
			$plays = get_post_meta( $id, 'ozy_post_plays_count', 1 );

			update_post_meta( $id, 'cd_plays', $plays );
			update_post_meta( $id, 'cd_downloads', $downloads );
		}

		if ( 'videos' === get_post_type( $id ) ) {
			$videos = array(
				'video_type'	=> get_post_meta( $id, 'video_type', 1 ),
				'vimeo'			=> get_post_meta( $id, 'vimeo_id', 1 ),
				'youtube'		=> get_post_meta( $id, 'youtube_id', 1 ),
				'embed'			=> get_post_meta( $id, 'video_code', 1 ),
				'lightbox'		=> get_post_meta( $id, 'video_lightbox', 1 ),
			);

			$views = get_post_meta( $id, 'ozy_post_plays_count', 1 );
			update_post_meta( $id, 'cd_views', $views );
		}

		if ( 'events' === get_post_type( $id ) ) {
			$old_date = get_post_meta( $id, 'date_event', 1 );
			$new_date = date( "Y/m/j H:i", strtotime( $old_date ) );
			$events = array(
				'event_date'	=> $new_date,
				'end_event_date'=> $new_date,
				'buy_title'		=> get_post_meta( $id, 'buy_event', 1 ),
				'buy_link'		=> get_post_meta( $id, 'buy_link_event', 1 ),
				'venue'			=> get_post_meta( $id, 'venue_event', 1 ),
				'class'			=> get_post_meta( $id, 'event_class', 1 )
			);
			update_post_meta( $id, 'cd_event_date', $new_date );
			update_post_meta( $id, 'cd_end_event_date', $new_date );
		}

		if ( 'gallery' === get_post_type( $id ) ) {
			$gallery = array(
				'gallery'		=> get_post_meta( $id, 'gallery_image', 1 )
			);
		}

		if ( 'artists' === get_post_type( $id ) ) {
			$artists = array(
				'fb'			=> get_post_meta( $id, 'artist_fb_page', 1 ),
				'social'		=> get_post_meta( $id, 'artist_social', 1 )
			);
		}

		$update_all = array_merge( $public, $songs, $videos, $events, $gallery, $artists );

		update_post_meta( $id, 'cd_meta', $update_all );

		$likes = get_post_meta( $id, 'like', 1 );
		$dislikes = get_post_meta( $id, 'dislike', 1 );

		update_post_meta( $id, 'cd_likes', $likes );
		update_post_meta( $id, 'cd_dislikes', $dislikes );

	}

	wp_reset_postdata();

	echo '<p><img src="'. get_admin_url() . '/images/yes.png"> Completed successfully </p>';
	die();
}

function codevz_delete_old_rows() {
	check_ajax_referer( 'cd_migration_nonce', 'nonce' );

	if ( isset( $_POST['key'] ) && $_POST['key'] !== 'please_clean_database') {
		echo 'Wrong key ... Please try again.';
		die();
	}

	$metabox = array( 
		'page_title', 'page_cover_type', 'page_cover_image', 'page_cover_slider', 'page_cover_master', 
		'_layout', '_sidebar_primary', '_sidebar_secondary', 'page_breadcrumb', 'artist_nameaa', 
		'gallery_image', 'rx_player', 'playlist', 'custom_player', 'showcover', 'video_lightbox',  
		'auto_play', 'video_type', 'youtube_id', 'vimeo_id', 'video_code', 'artist_fb_page', 
		'artist_social', 'date_event', 'date_event', 'buy_event', 'buy_link_event', 'venue_event', 
		'event_class', 'loadRingColor', 'playRingColor', 'waveformDataColor', 'waveformDataLineRatio',  
		'eqDataColor', 'eqDataLineRatio', 'peakDataColor', 'useAmplifier', 'scaleArcWidth' , 'content_position'  
	);
	
	foreach ( $metabox as $key ) {
		delete_post_meta_by_key( $key );
	}

	delete_option( 'option_tree' );
	delete_option( 'option_tree_copy' );
	delete_option( 'option_tree_settings' );

	echo '<p><img src="'. get_admin_url() . '/images/yes.png"> Deleted successfully </p>';
	die();
}

function codevz_migration_done() {
	check_ajax_referer( 'cd_migration_nonce', 'nonce' );
	update_option( 'cd_migration_done', true );

	echo 'Done, Please wait ...';
	die();
}

//
//
// Custom CSS output
//
//
class Codevz_Customizer_CSS_Output {
	
	public function __construct() {

		$this->upload = wp_upload_dir();
		$this->cache_dir = trailingslashit( $this->upload['basedir'] ) . 'cd-custom-css';
		$this->cache_url = trailingslashit( $this->upload['baseurl'] ) . 'cd-custom-css';

		add_action( 'customize_save_after', array( $this, 'save' ) );
		add_action( 'csf/save/validate/after', array( $this, 'csf_after_save' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_css' ), 100 );
	}

	public function enqueue_css() {

		if ( ! is_dir( $this->cache_dir ) ) {

			if( ! function_exists( 'WP_Filesystem' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}

			WP_Filesystem();
			global $wp_filesystem;
			$wp_filesystem->mkdir( $this->cache_dir );
		}

		if ( is_writable( $this->cache_dir ) && ! isset( $_POST['wp_customize'] ) ) {

			$has_cached = get_option( 'css_cached' );

			if ( ! $has_cached ) {
				$this->cache_css();
			}

			global $blog_id;
			$is_multisite = is_multisite() ? '-' . $blog_id : '';

			wp_enqueue_style( 'cd-custom', $this->cache_url .'/custom-style'. $is_multisite .'.css', array(), null );
		} else {
			add_action( 'wp_head', function() { echo '<style>'. $this->out() .'</style>'; }, 99 );
		}

	}

	public function cache_css() {

		if ( is_multisite() ) {
			global $blog_id;
			$css_file = $this->cache_dir . '/custom-style-'. $blog_id .'.css';
		} else {
			$css_file = $this->cache_dir . '/custom-style.css';
		}

		$css  = "/* Last modified: " . current_time( 'd m Y, h:s:i' ) . " */ ";
		$css .= $this->out();

		update_option( 'cd_custom_css', $this->out() );

		if ( ! function_exists( 'WP_Filesystem' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}

		WP_Filesystem();
		global $wp_filesystem;

		if ( ! $wp_filesystem->put_contents( $css_file, $css, FS_CHMOD_FILE ) ) {
			update_option( 'css_cached', false );
		} else {
			update_option( 'css_cached', true );
		}

	}

	public function save( $request ) {
		update_option( 'css_cached', false );
	}

	public function csf_after_save( $request ) {
		update_option( 'css_cached', false );
		return $request;
	}

	public function out() {
		$all = (array) get_option( '_cs_options' );
		$out = $font_family = '';

		// Size width
		if ( isset( $all['size_width'] ) && $all['size_width'] ) {
			$width = $all['size_width'];
			$unit = preg_replace( '/\d+/u', '', $width );
			if ( isset( $all['full_boxed'] ) && $all['full_boxed'] === 'full' ) {
				$out .= '.row{width: ' . $width . '}';
			} else {
				$row = $all['size_width'] - ( ( $unit === '%' ) ? 5 : 60 );
				$out .= '#layout{width: ' . $width . '}.row{width: ' . $row . $unit . '}';
			}
		}

		// Primary color
		if ( isset( $all['size_color'] ) && $all['size_color'] ) {
			$out .= '
	a:hover, .sf-menu li:hover > a, .sf-menu li > a:hover, .sf-menu > .selectedLava > a, 
	.list .current a, .event-date, .item_small span a:hover, .countdown li span, .page-template-page-onepage .sf-menu li.current a, 
	.likes_dislikes .liked, .likes_dislikes .disliked, header .full_menu {
		color: ' . $all['size_color'] . '
	}

	.button,.page-numbers a,.page-numbers span,.pagination a,.pagination > b,.widget_product_search #searchsubmit,
	.post-password-form input[type="submit"], .wpcf7-submit, .submit_user, #commentform #submit, .sf-menu li li a span, .sf-menu .back .left, .jp-play-bar,
	.jp-volume-bar-value, .commentlist li.bypostauthor > .comment-body:after,.commentlist li.comment-author-admin > .comment-body:after, .tagcloud a:hover, 
	.dropcap, #toTop:hover, .cdEffect:hover:before, .sf-menu li li > a:after {
		background-color: ' . $all['size_color'] . '
	}

	::selection, 
	::-moz-selection {
		background-color: ' . $all['size_color'] . '
	}

	textarea:focus, input:focus, h4.tt, h3.tt, .widget_gardengeneralposts h4, .list-custom-taxonomy-widget h4, .list-custom-taxonomy-widget li.current a, .widget_archive li.current a, 
	.widget_categories li.current a, .widget_nav_menu li.current a, .widget_meta li.current a, .widget_pages li.current a, .widget_archive li:hover a, .widget_pages li:hover a, 
	.widget_meta li:hover a, .widget_nav_menu li:hover a, .widget_categories li:hover a, .list-custom-taxonomy-widget a:hover, .widget_categories li li:hover a, .widget_nav_menu li li:hover a, 
	.widget_meta li li:hover a, .widget_pages li li:hover a, .widget_archive li li:hover a, .widget_pages li li:hover a, .widget_meta li li:hover a, .widget_nav_menu li li:hover a, 
	.widget_nav_menu li:hover > a, .widget_categories li li:hover a,.widget_recent_comments li:hover, .widget_rss li:hover, .wtext a.active, .wtext a:hover, .dropcap-border,
	#bbpress-forums #bbp-user-wrapper h2.entry-title {
		border-color: ' . $all['size_color'] . '
	}
	.cd_doing span, .rotating {
		border-top-color: ' . $all['size_color'] . '
	}
	';
		}

		foreach ( $all as $id => $val ) {
			if ( strpos( $id, '_css_' ) === false || empty( $val ) ) {
				continue;
			} else {
				$inner_out = '';
				foreach ( $val as $styles ) {
					if ( empty( $styles['selector'] ) ) {
						$styles['selector'] = '.NOT_FOUND_SELECTOR';
					}

					$selector = $styles['selector'];
					unset( $styles['selector'] );
					unset( $styles['title'] );

					foreach ( $styles as $property => $value ) {
						if ( is_array( $value ) ) {
							foreach ( $value as $pp => $vv ) {
								$vv = ( $pp === 'image' ) ? 'url(' . $vv . ')' : $vv;
								$inner_out .= empty( $vv ) ? '' : $property . '-' . $pp . ':' . $vv . ';';
							}
						} else if ( $property !== 'custom' ) {
							$inner_out .= empty( $value ) ? '' : $property . ':' . $value . ';';
							$font_family .= ( $property === 'font-family' && $value ) ? "@import '//fonts.googleapis.com/css?family=" . str_replace( ' ', '+', $value ) . "';" : '';
						} else {
							$inner_out .= $value;
						}
					}
				}

				$out .= $selector . '{' . $inner_out . '}';
			}
		}

		return $font_family . $out;
	}

}
new Codevz_Customizer_CSS_Output();
