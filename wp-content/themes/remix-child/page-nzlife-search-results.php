<?php get_header(); ?>
<?php 
    $request_uri = $_SERVER["REQUEST_URI"];
    $uriArray = explode("=", $request_uri);
    if (sizeof($uriArray) > 1) { 
        $keywords = urldecode($uriArray[1]);
    }
?>
<div class="container search-results">
    <div class="adzone full-width adzone-top">
        <?php echo do_shortcode("[pro_ad_display_adzone id=2168]"); ?>
    </div>
    <div class="search-results-label">
        <?php 
            if(!empty($keywords)){
                echo '<div>搜索结果：' . $keywords . '</div>';
            }
        ?>
    </div>
    <div class="content">
        <div class="search-page-main grid_9">
            <div class="search-filters">
                <div class="filter-row">
                    <div class="filter-label">类型</div>
                    <div class="filters"><?php echo facetwp_display( 'facet', 'service_type' ); ?></div>
                </div>
                <div class="filter-row">
                    <div class="filter-label">位置</div>
                    <div class="filters"><?php echo facetwp_display( 'facet', 'service_location' ); ?></div>
                </div>
                <div class="filter-row">
                    <div class="filter-label">语言</div>
                    <div class="filters"><?php echo facetwp_display( 'facet', 'service_language' ); ?></div>
                </div>
            </div>
            <div class="search-sortings">
                <a href="#">推荐</a> <a href="#">相关度</a> <a href="#">时间</a>
            </div>
            <div class="search-template">
                <?php echo facetwp_display( 'template', 'company_search_template' ); ?>
            </div>
            <div class="pager">
                <?php echo facetwp_display( 'pager' ); ?>
            </div>
        </div>
        <div class="search-page-sidebar grid_3">
            <div class="container">
                <div class="label">广告</div>
                <div><?php echo do_shortcode("[pro_ad_display_adzone id=2192]"); ?></div>
            </div>
        </div>
    </div>
    <div class="adzone full-width adzone-bottom">
        <?php echo do_shortcode("[pro_ad_display_adzone id=2194]"); ?>
    </div>
</div>



<?php get_footer(); ?>