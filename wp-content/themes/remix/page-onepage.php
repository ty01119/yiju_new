<?php

/**
 * Template Name: One-page
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

	get_header();
	global $codevz;
	$codevz->content_before();

	if ( have_posts() ) { 
		the_post();
		the_content();
	}

	$codevz->content_after();
	get_sidebar();
	get_footer();
