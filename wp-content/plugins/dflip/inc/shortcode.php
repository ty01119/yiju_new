<?php

/**
 * Created by PhpStorm.
 * User: Deepak
 * Date: 5/3/2016
 * Time: 2:27 PM
 */
class DFlip_ShortCode
{

	/**
	 * Holds the singleton class object.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	public static $instance;

	/**
	 * Holds the base DFlip class object.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	public $base;

	/**
	 * Primary class constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// Load the base class object.
		$this->base = DFlip::get_instance();

		// Load shortcode hooks and filters.
		add_shortcode('dflip', array($this, 'shortcode'));
	}

	/**
	 * Builds the dFlip Shortcode for the plugin
	 *
	 * This implements the functionality of the Image-Text Shortcode for
	 * displaying Image and text blocks in a post.
	 *
	 * @since 1.0.0
	 *
	 * @param array   $attr Attributes of the shortcode.
	 * @param string   $content Content of the button or thumb
	 *
	 * @return string HTML content to display image-text.
	 */
	public function shortcode($attr, $content ='') {

		$atts_default = array(
			'class' => '',
			'id'    => '',
		    'type' => 'book',
		    'tags' =>''
		);

		$post_defaults = array(
			'webgl'        => 'true',
			'class'        => '',
			'id'           => '',
			'source_type'  => 'pdf',
			'pdf_source'   => '',
			'pdf_thumb'   => '',
			'pages'        => array(),
			'outline'      => '',
			'bg_color'     => '#fff',
			'height'       => 500,
			'duration'     => 800,
			'hard'         => 'cover',
			'auto_outline' => 'false',
			'overwrite_outline' => 'false',
			'enable_download' => 'true',
			'direction' => 1,
			'page_mode' => 0,
			'auto_sound'   => 'true',
			'texture_size' => 1600
		);

		$settings_defaults = array(

			'text_toggle_sound'     => "Turn on/off Sound",
			'text_toggle_thumbnails' => "Toggle Thumbnails",
			'text_toggle_outline'    => "Toggle Outline/Bookmark",
			'text_previous_page'     => "Previous Page",
			'text_next_page'         => "Next Page",
			'text_toggle_fullscreen' => "Toggle Fullscreen",
			'text_zoom_in'           => "Zoom In",
			'text_zoom_out'          => "Zoom Out",
			'text_toggle_help'       => "Toggle Help",
			'text_single_page_mode'   => "Single Page Mode",
			'text_double_page_mode'   => "Double Page Mode",
			'text_download_PDF_file'  => "Download PDF File",
			'text_goto_first_page'    => "Goto First Page",
			'text_goto_last_page'     => "Goto Last Page"

		);

		$atts = shortcode_atts($atts_default, $attr, 'dflip');

		$type = $atts['type'];

		//get Id
		$post_id = $atts['id'];
		$id = 'df_' . $post_id;

		$post_meta = get_post_meta($post_id, '_dflip_data');
		$dflip_settings = get_option( '_dflip_settings', true );

		$title = $content;
		if($title == '')
			$title = get_the_title($post_id);
		if($title ==''){
			$title = "Open Book";
		}
		$post_data = array();

		if (is_array($post_meta) && count($post_meta) > 0) {
			$post_data = $post_meta[0];
		}

		$settings_data = array();

		if (is_array($dflip_settings)) {
			$settings_data = $dflip_settings;
		}

		$data = shortcode_atts($post_defaults, $post_data, 'dflip');
		$settings = shortcode_atts($settings_defaults, $settings_data, 'dflip');

		$source_type = $data['source_type'];
		$pdf_source = $data['pdf_source'];
		$data['source'] = '';

		$thumb_url ='';
		$tags = $atts['tags'];

		if ($source_type == 'pdf') {
			$data['source'] = $pdf_source;
			$thumb_url = $data['pdf_thumb'];
		}

		if ($source_type == 'image') {
			$pages = array_map('maybe_unserialize', $data['pages']);
			$source_list = array();
			$links = array();
			$index = 0;
			foreach ($pages as $image) {
				if($thumb_url==''){
					$thumb_url = $image['url'];
				}
				if ($image['url'] !== '') {
					array_push($source_list, $image['url']);
				}
				if (isset($image['hotspots']) && $image['hotspots'] !== '') {
					$links[ $index ] = $image['hotspots'];
				}
				$index++;
			}
			$data['links'] = $links;
			$data['source'] = $source_list;
		}

		unset($data['pages']);
		unset($data['pdf_source']);

		$data['text'] = array(
			'toggleSound'=> $settings['text_toggle_sound'],
			'toggleThumbnails'=> $settings['text_toggle_thumbnails'],
			'toggleOutline'=> $settings['text_toggle_outline'],
			'previousPage'=> $settings['text_previous_page'],
			'nextPage'=> $settings['text_next_page'],
			'toggleFullscreen'=> $settings['text_toggle_fullscreen'],
			'zoomIn'=> $settings['text_zoom_in'],
			'zoomOut'=> $settings['text_zoom_out'],
			'toggleHelp'=> $settings['text_toggle_help'],
			'singlePageMode'=> $settings['text_single_page_mode'],
			'doublePageMode'=> $settings['text_double_page_mode'],
			'downloadPDFFile'=> $settings['text_download_PDF_file'],
			'gotoFirstPage'=> $settings['text_goto_first_page'],
			'gotoLastPage'=> $settings['text_goto_last_page'],
		);

		$html ="";
		if($type=='button') {
			$html = '<div class="_df_button" id="' . $id . '">'. $title .'</div>';
		}
		else if($type=='thumb'){
			$html = '<div class="_df_thumb" id="' . $id . '" df-thumb="'. $thumb_url .'" df-tags="'. $tags .'">'. $title .'</div>';
		}
		else {
			$html = '<div class="_df_book df-container df-loading" id="' . $id . '"></div>';
		}

		$code = 'var option_' . $id . ' = ' . json_encode($data) . ';';

		$html .= '<script>'. $code . '</script>';


		return $html;

	}


	/**
	 * Returns the singleton instance of the class.
	 *
	 * @since 1.0.0
	 *
	 * @return object dFlip_PostType object.
	 */
	public static function get_instance() {

		if (!isset(self::$instance)
			&& !(self::$instance instanceof DFlip_ShortCode)
		) {
			self::$instance = new DFlip_ShortCode();
		}

		return self::$instance;

	}

}

//Load the dFlip Plugin Class
$dflip_shortcode = DFlip_ShortCode::get_instance();


