<?php 

/**
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 *
 * Remix is a very powerful theme and virtually anything can be customized.
 * If you need any help altering a function, just let us know!
 * Customizations aren't included for free but if it's a simple task I'll be sure to help :)
 *
 * Text Domain: remix
 */

/* Constants */
define( 'CD_THEME', 'Remix' );
define( 'CD_AUTHOR', 'Codevz' );
define( 'CD_DIR', get_template_directory() );
define( 'CD_URI', get_template_directory_uri() );

/* Required files */
load_template( CD_DIR . '/inc/TGM.class.php' );
require_once CD_DIR . '/csf/codestar-framework.php';
load_template( CD_DIR . '/inc/theme_options.php' );

/* Remix core */
class CodevzRemix {

	private static $instance = NULL;

	public function __construct() {

		global $post, $paged, $wp_query;
		$this->post = &$post;
		$this->paged = &$paged;
		$this->wp_query = &$wp_query;
		$this->unique = 0;

		/* Actions */
		$this->action( 'after_setup_theme', 'setup' );
		$this->action( 'personal_options_update', 'save_author_icons' );
		$this->action( 'edit_user_profile_update', 'save_author_icons' );
		$this->action( 'widgets_init', 'register_sidebars' );
		$this->action( 'tgmpa_register', 'plugins' );
		$this->action( 'wp_enqueue_scripts', 'enqueue' );
		$this->action( 'save_post', 'save_post', 11, 3 );
		$this->action( 'nav_menu_css_class', 'nav_class', 10, 2 );
		$this->action( 'pre_get_posts', 'pre_get_posts_action', 11 );
		$this->action( 'login_enqueue_scripts', 'wp_login' );
		$this->action( 'daily_plays', 'daily_plays' );
		$this->action( 'switch_theme', 'deactive_daily_plays' );
		$this->action( 'wp', 'wp_extra' );
		$this->action( 'wp_ajax_radio_history', 'radio_history' );
		$this->action( 'wp_ajax_nopriv_radio_history', 'radio_history' );
		$this->action( 'wp_ajax_new_nonce', 'new_nonce' );
		$this->action( 'wp_ajax_nopriv_new_nonce', 'new_nonce' );
		$this->action( 'wp_ajax_codevz_counter', 'codevz_counter' );
		$this->action( 'wp_ajax_nopriv_codevz_counter', 'codevz_counter' );
		$this->action( 'wp_ajax_nopriv_login', 'login' );
		$this->action( 'wp_ajax_nopriv_lost_p', 'lost_p' );
		$this->action( 'wp_ajax_nopriv_register', 'register' );
		$this->action( 'wp_ajax_ajax_query', 'query' );
		$this->action( 'wp_ajax_nopriv_ajax_query', 'query' );
		$this->action( 'wp_ajax_likes_dislikes', 'likes_dislikes' );
		$this->action( 'wp_ajax_nopriv_likes_dislikes', 'likes_dislikes' );
		$this->action( 'wp_ajax_ajax_search', 'ajax_search' );
		$this->action( 'wp_ajax_nopriv_ajax_search', 'ajax_search' );

		/* Filters */
		$this->filter( 'wp_list_categories', 'cats_html' );
		$this->filter( 'get_archives_link', 'archive_html' );
		$this->filter( 'user_contactmethods', 'author_icons', 10, 1 );
		$this->filter( 'site_transient_update_plugins', 'plugin_updates' );
		$this->filter( 'wp_head', 'head' );
		$this->filter( 'wp_footer', 'footer', 100 );
		$this->filter( 'comment_text', 'likes_dislikes_c', 1000 );
		$this->filter( 'image_resize_dimensions', 'upscale', 10, 6 );
		$this->filter( 'cron_schedules', 'additional_event' );
		$this->filter( 'login_redirect', 'login_redirect', 10, 3 );
		$this->filter( 'template_include', 'template_include' );
		$this->filter( 'rss2_ns', 'itunes_namespace' );
		$this->filter( 'the_content_feed', 'itunes_rss' );
		$this->filter( 'pre_get_posts', 'pre_get_posts_filter' );

		/* Woo */
		if ( function_exists( 'is_woocommerce' ) ) {
			$this->filter( 'add_to_cart_fragments', 'woo_cart' );
			add_filter( 'loop_shop_columns', function() {
				return 3;
			});
		}

		/* Remove emoji = better performance */
		if ( $this->option( 'remove_emoji', 1 ) ) {
			remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
			remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
			remove_action( 'wp_print_styles', 'print_emoji_styles' );
			remove_action( 'admin_print_styles', 'print_emoji_styles' );
		}

	}

	public static function get_instance() {
		if ( NULL === self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/* Action */
	public function action( $h, $f, $p = 10, $a = 1 ) {
		add_action( $h, array( &$this, $f), $p, $a );
	}

	/* Filter */
	public function filter( $t, $f, $p = 10, $a = 1 ) {
		add_action( $t, array( &$this, $f), $p, $a );
	}

	/* Get meta */
	public function meta( $id = null, $key = 'cd_meta' ) {
		$id = $id ? $id : ( isset( $this->post->ID ) ? $this->post->ID : '');
		return $id ? get_post_meta( $id, $key, true ) : '';
	}

	/* Get option */
	public function option( $o, $d = null ) {
		$all = get_option( '_cs_options' );

		/* For demo purpose */
		if ( isset( $_GET['ajax'] ) ) {
			$all['ajax'] = 1;
		}

		return empty( $all[ $o ] ) ? $d : $all[ $o ];
	}

	/* Get hooks */
	public function hook( $i = '', $r = 1 ) {
		$r = ( $r && $this->option( $i ) );
		echo $r ? '<div class="row clr">' : '';
		echo do_shortcode( $this->option( $i ) );
		echo $r ? '</div>' : '';
	}

	/* Setup theme */
	public function setup() {
		/* Language */
		load_theme_textdomain( 'remix', CD_DIR . '/lang' );

		/* Widgets */
		foreach ( glob( CD_DIR .  '/inc/widgets/*.php' ) as $i ) {
			load_template ( $i );
		}

		/* Menus */
		register_nav_menus(array(
			'primary' => esc_html__( 'Primary', 'remix' ), 
			'onepage' => esc_html__( 'One-page', 'remix' ), 
			'footer'  => esc_html__( 'Footer', 'remix' )
		));

		/* Images size */
		add_image_size( 'tumb', 85, 85, true );				// Thumbs
		add_image_size( 'medium_post', 280, 255, true );	// Post M
		add_image_size( 'cover', 500, 500, true );			// Cover
		add_image_size( 'player_cover', 180, 180, true );	// Player
		add_image_size( 'masonry', 540, 9999 );				// Masonry
		add_image_size( 'free_width', 9999, 500 );			// Free width
		add_image_size( 'custom_carousel', 1000, 1000, true ); // Custom

		/* Override images size */
		$ois = $this->option( 'add_image_size', array() );
		foreach ( $ois as $i ) {
			add_image_size( $i['id'], $i['width'], $i['height'], $i['crop'] );
		}

		/* Supports */
		add_theme_support( 'title-tag' );
		add_theme_support( 'html5' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'woocommerce' );
		add_theme_support( 'bbpress' );
		add_filter( 'widget_text', 'do_shortcode' );
		$content_width = isset( $content_width ) ? $content_width : 1060;
	}

	/* Sidebars */
	public function register_sidebars() {
		$sidebars = array( 'primary', 'secondary', 'footer-1', 'footer-2', 'footer-3', 'footer-4' );
		$new = $this->option( 'sidebars', array() );
		foreach ( $new as $i ) {
			$sidebars[] = str_replace( ' ', '_', $i['id'] );
		}

		foreach ( $sidebars as $id ) {
			register_sidebar( array( 
				'name'			=> ucwords( $id ),
				'id'			=> $id,
				'before_widget'	=> '<section id="%1$s" class="def-block widget %2$s">',
				'after_widget'	=> '</section>',
				'before_title'	=> '<h4 class="tt">',
				'after_title'	=> '</h4><span class="liner"></span>'
			) );
		}
	}

	/* Get post type */
	public function get_post_type( $id = '' ) {
		$cpt = '';
		if ( is_search() || is_tag() ) {
			$cpt = '';
		} else if ( get_post_type( $id ) || is_single() ) {
			$cpt = ( get_post_type( $id ) === 'post' ) ? '' : get_post_type( $id ) . '_';
			$cpt = ( function_exists( 'is_bbpress' ) && is_bbpress() ) ? 'bbpress_' : $cpt;
			$cpt = function_exists( 'is_woocommerce' ) && ( is_shop() || is_woocommerce() ) ? 'woocommerce_' : $cpt;
		} else if ( is_tax() ) {
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			if ( get_taxonomy( $term->taxonomy ) ) {
				$cpt = get_taxonomy( $term->taxonomy )->object_type[0] . '_';
			}
		} else if ( is_post_type_archive() ) {
			$cpt = get_post_type_object( get_query_var( 'post_type' ) )->name . '_';
		} else if ( function_exists( 'is_bbpress' ) && is_bbpress() ) {
			$cpt = 'bbpress_';
		} else {
			$cpt = 'post_';
		}

		return $cpt;
	}

	public function get_page_type( $o = '' ) {
		if ( is_page() ) {
			$o = is_front_page() ? 'front' : 'page';
		} else if ( is_single() ) {
			$o = 'single';
		} else if ( is_archive() ) {
			$o = 'archive';
		} else if ( is_category() ) {
			$o = 'category';
		} else if ( is_tax() ) {
			$o = 'tax';
		}

		return $o;
	}

	/* Redirects */
	public function template_include( $template ) {

		$mt = $this->option( 'maintenance' );
		if ( $mt && ! is_user_logged_in() && ! is_page( $mt ) ) {
			wp_redirect( get_the_permalink( $mt ) );
			exit;
		}

		if ( ! isset( $_GET ) ) {
			return $template;
		}

		/* Popup player */
		$template = isset( $_GET['player'] ) ? $this->popup_player() : $template;

		/* User submission */
		$template = isset( $_GET['submission'] ) ? $this->submission() : $template;

		/* User info */
		$template = isset( $_GET['edit_info'] ) ? $this->edit_info() : $template;

		/* For codevz support */
		if ( isset( $_GET['details'] ) && $_GET['details'] === 'xxx' ) {
			$theme = wp_get_theme();
			echo '<ul style="margin: 30px 10px;line-height: 24px">';
			echo '<li>Theme = ' . $theme->get( 'Name' ) . ' ' . $theme->get( 'Version' ) . '</li>';
			echo '<li>phpversion = ' . phpversion() . '</li>';
			echo '<li>memory_limit = ' . ini_get( 'memory_limit' ) . '</li>';
			echo '<li>post_max_size = ' . ini_get( 'post_max_size' ) . '</li>';
			echo '<li>upload_max_filesize = ' . ini_get( 'upload_max_filesize' ) . '</li>';
			echo '<li>max_execution_time = ' . ini_get( 'max_execution_time' ) . '</li>';
			$mgu = memory_get_usage( 0 );
			$mgu1 = memory_get_usage( 1 );
			echo '<li>memory_get_usage( 0 ) = ' . @round( $mgu / 1048576, 2 ) . ' M</li>';
			echo '<li>memory_get_usage( 1 ) = ' . @round( $mgu1 / 1048576, 2 ) . ' M</li>';
			$mgpu = memory_get_peak_usage( 0 );
			$mgpu1 = memory_get_peak_usage( 1 );
			echo '<li>memory_get_peak_usage( 0 ) = ' . @round( $mgpu / 1048576, 2 ) . ' M</li>';
			echo '<li>memory_get_peak_usage( 1 ) = ' . @round( $mgpu1 / 1048576, 2 ) . ' M</li>';
			echo '</ul>';
			error_reporting( -1 );
			exit;
		}

		return $template;
	}

	/* Edit user page */
	public function edit_info() {

		if ( ! is_user_logged_in() ) { return; }

		get_header();
		global $current_user;
		wp_get_current_user();
		$id = $current_user->ID;
		$meta = get_user_meta( $id );
		wp_enqueue_style( 'csf', CD_URI . '/csf/assets/css/csf.css' );
		echo '<article>';
		echo '<h4 class="tt">' . $this->option( 'edit_my_profile' ) . '</h4><span class="liner"></span>';

		if ( ! empty( $_POST ) ) {

			if ( ! empty( $_POST['password'] ) && ! empty( $_POST['re_password'] ) && $_POST['password'] === $_POST['re_password'] ) {
				wp_set_password( wp_filter_nohtml_kses( $_POST['password'] ), $id );
				echo '<pre>Settings updated successfuly.</pre>';
			} else if ( ! empty( $_POST['password'] ) && ! empty( $_POST['re_password'] ) && $_POST['password'] !== $_POST['re_password'] ) {
				echo '<pre>Passwords not match, Please try again ...</pre>';
			} else {	
				echo '<pre>Settings updated successfuly.</pre>';
			}

			if ( ! empty( $_POST['nickname'] ) ) {
				update_user_meta( $id, 'nickname', wp_filter_nohtml_kses( $_POST['nickname'] ) );
			}
			if ( ! empty( $_POST['email'] ) ) {
				wp_update_user( array( 'ID' => $id, 'user_email' => wp_filter_nohtml_kses( $_POST['email'] ) ) );
			}
			if ( ! empty( $_POST['description'] ) ) {
				update_user_meta( $id, 'description', wp_filter_nohtml_kses( $_POST['description'] ) );
			}
			if ( ! empty( $_POST['website'] ) ) {
				wp_update_user( array( 'ID' => $id, 'user_url' => wp_filter_nohtml_kses( $_POST['website'] ) ) );
			}
			foreach ( $this->icons() as $i ) {
				if ( ! empty( $_POST[ $i ] ) ) {
					update_user_meta( $id, $i, wp_filter_nohtml_kses( $_POST[ $i ] ) );
				}
			}

		}

	?>
			<form method="post">
				<div class="csf-onload">
				<?php 
					echo csf_add_field( array(
						'id'    => 'nickname',
						'name'  => 'nickname',
						'type'  => 'text',
						'title' => esc_html__('Nick name', 'remix')
					), isset( $_POST['nickname'] ) ? wp_filter_nohtml_kses( $_POST['nickname'] ) : wp_filter_nohtml_kses( $meta['nickname'][0] ) );

					echo csf_add_field( array(
						'id'    => 'email',
						'name'  => 'email',
						'type'  => 'text',
						'title' => esc_html__('Email', 'remix')
					), isset( $_POST['email'] ) ? wp_filter_nohtml_kses( $_POST['email'] ) : wp_filter_nohtml_kses( $current_user->user_email ) );

					echo csf_add_field( array(
						'id'    => 'description',
						'name'  => 'description',
						'type'  => 'textarea',
						'settings' => array(
							'textarea_rows' => 7,
							'media_buttons' => false,
						),
						'title' => esc_html__('Description', 'remix')
					), isset( $_POST['description'] ) ? wp_filter_nohtml_kses( $_POST['description'] ) : wp_filter_nohtml_kses( $meta['description'][0] ) );

					echo csf_add_field( array(
						'id'    => 'password',
						'name'  => 'password',
						'type'  => 'text',
						'title' => esc_html__('New password', 'remix')
					), '' );

					echo csf_add_field( array(
						'id'    => 're_password',
						'name'  => 're_password',
						'type'  => 'text',
						'title' => esc_html__('re-type password', 'remix')
					), '' );

					echo csf_add_field( array(
						'id'    => 'website',
						'name'  => 'website',
						'type'  => 'text',
						'title' => esc_html__('Website', 'remix')
					), isset( $_POST['website'] ) ? wp_filter_nohtml_kses( $_POST['website'] ) : wp_filter_nohtml_kses( $current_user->user_url ) );

					foreach ( $this->icons() as $i ) {
						echo csf_add_field( array(
							'id'    => $i,
							'name'  => $i,
							'type'  => 'text',
							'title' => ucwords( $i )
						), isset( $_POST[ $i ] ) ? wp_filter_nohtml_kses( $_POST[ $i ] ) : wp_filter_nohtml_kses( isset( $meta[ $i ][0] ) ? $meta[ $i ][0] : '' ) );
					}
				?>
				</div>
				<input type="submit" class="button medium mtt clr" value="Update" />
			</form>

		</article>
		<?php
		get_footer(); 

	}

	/* Popup player */
	public function popup_player() { ?>
		<!DOCTYPE html>
		<head xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
			<meta charset="<?php bloginfo( 'charset' );?>" />
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
			<?php 
				echo "<link rel='stylesheet' href='" . CD_URI . '/style.css' . "' type='text/css' media='all' />";
				echo $this->option( 'light' ) ? "<link rel='stylesheet' href='" . CD_URI . '/css/light.css' . "' type='text/css' media='all' />" : '';
				echo "<link rel='stylesheet' href='" . CD_URI . '/css/responsive.css' . "' type='text/css' media='all' />";
				echo "<link rel='stylesheet' href='" . CD_URI . '/csf/assets/css/font-awesome.min.css' . "' type='text/css' media='all' />";
				echo is_rtl() ? "<link rel='stylesheet' href='" . CD_URI . '/rtl.css' . "' type='text/css' media='all' />" : '';
				echo '<script type="text/javascript" src="' . includes_url( 'js/jquery/jquery.js' ) . '"></script>';
				echo '<script type="text/javascript" src="' . includes_url( 'js/jquery/jquery-migrate.min.js' ) . '"></script>';
				$ccss = get_option( 'cd_custom_css' );
				echo $ccss ? '<style>' . $ccss . '</style>': '';
			?>
		</head>

		<body class="player_popup scroll" data-ajax="<?php echo admin_url( 'admin-ajax.php' ); ?>" data-theme="<?php echo CD_URI; ?>">
			<div id="layout" class="<?php echo $this->option( 'full_boxed', 'full' ); ?>">
				<?php 
					$this->player( $_GET['player'], array( 'class' => 'popup' ) );

					echo '<script type="text/javascript" src="' . CD_URI . '/js/nicescroll.js' . '"></script>';
					echo '<script type="text/javascript" src="' . CD_URI . '/js/jplayer.js' . '"></script>';
					echo '<script type="text/javascript" src="' . CD_URI . '/js/custom.js' . '"></script>';
				?>
			</div>
		</body>
		</html>
	<?php 

	}

	/* Music submission page */
	public function submission() {

		if ( ! $this->option( 'music_submission' ) ) {
			return false;
		}

		get_header();
		$submited = $error = false;
	?>

		<article>
			<script src="//code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css" />

			<h4 class="tt"> <?php echo $this->option( 'music_submission' ); ?> </h4><span class="liner"></span>
		<?php 
		if ( ! is_user_logged_in() ) {
			echo '<pre>Please login for submiting new music.</pre>';
		} else {
			wp_enqueue_style('thickbox');
			wp_enqueue_script('thickbox');
			wp_enqueue_script( 'media-upload');
			wp_enqueue_media();
			wp_enqueue_style( 'csf', CD_URI . '/csf/assets/css/csf.css' );
			wp_enqueue_script( 'csf-plugins', CD_URI . '/csf/assets/js/csf-plugins.js', array( 'jquery' ), '', true );
			wp_enqueue_script( 'csf', CD_URI . '/csf/assets/js/csf.js', array( 'jquery' ), '', true );

			if ( ! empty( $_POST ) ) {
				$required = array( 'title', 'content', 'cover', 'mp3' );
				$error_o = '';
				foreach ( $required as $key ) {
					if ( empty( $_POST[ $key ] ) ) {
						$error_o .= "<li>Please set $key</li>";
						$error = true;
					}
				}
				echo empty( $error_o ) ? '' : '<ul class="error block">' . $error_o . '</ul>';

				if ( ! $error ) {
					$submited = true;
					echo '<pre>Thanks for your submission, We will review your music soon.</pre>';

					$current_user = wp_get_current_user();
					$post = array(
						'post_type'		=> 'songs',
						'post_title'    => wp_filter_nohtml_kses( $_POST['title'] ),
						'post_content'  => $_POST['content'],
						'post_author'   => $current_user->ID,
						'tags_input' 	=> wp_filter_nohtml_kses( $_POST['tags'] )
					);

					$post_id = wp_insert_post( $post );

					$cats = isset( $_POST['cats'] ) ? $_POST['cats'] : array();
					wp_set_object_terms( $post_id, array_map( 'intval', $cats ), 'songs_cat' );

					set_post_thumbnail( $post_id, wp_filter_nohtml_kses( $_POST['cover'] ) );

					$post_meta = array(
						'cover'			=> '1',
						'player_cover'	=> true,
						'autoplay'		=> true,
						'repeat'		=> false,
						'player'		=> 'rx_player',
						'artist' 		=> isset( $_POST['artist'] ) ? $_POST['artist'] : '',
						'tracks' 		=> array(
							array(
								'type'			=> 'mp3',
								'title'			=> wp_filter_nohtml_kses( $_POST['title'] ),
								'mp3'			=> wp_filter_nohtml_kses( $_POST['mp3'] ),
								'poster'		=> wp_filter_nohtml_kses( $_POST['cover'] ),
								'artist'		=> isset( $_POST['artist'] ) ? $_POST['artist'] : '',
								'lyric'			=> wp_filter_nohtml_kses( $_POST['lyric'] ),
								'buy_icon_a'	=> wp_filter_nohtml_kses( $_POST['buy_icon_a'] ),
								'buy_title_a'	=> wp_filter_nohtml_kses( $_POST['buy_title_a'] ),
								'buy_link_a'	=> wp_filter_nohtml_kses( $_POST['buy_link_a'] ),
								'buy_icon_b'	=> wp_filter_nohtml_kses( $_POST['buy_icon_b'] ),
								'buy_title_b'	=> wp_filter_nohtml_kses( $_POST['buy_title_b'] ),
								'buy_link_b'	=> wp_filter_nohtml_kses( $_POST['buy_link_b'] ),
								'buy_icon_c'	=> wp_filter_nohtml_kses( $_POST['buy_icon_c'] ),
								'buy_title_c'	=> wp_filter_nohtml_kses( $_POST['buy_title_c'] ),
								'buy_link_c'	=> wp_filter_nohtml_kses( $_POST['buy_link_c'] ),
								'buy_icon_d'	=> wp_filter_nohtml_kses( $_POST['buy_icon_d'] ),
								'buy_title_d'	=> wp_filter_nohtml_kses( $_POST['buy_title_d'] ),
								'buy_link_d'	=> wp_filter_nohtml_kses( $_POST['buy_link_d'] ),
								'buy_custom'	=> '',
							)
						)
					);
					update_post_meta( $post_id, 'cd_meta', $post_meta );
				}
			} ?>
				<form method="post">
				<div class="csf-onload">
			<?php 
				if ( ! $submited ) {
					echo csf_add_field( array(
						'id'    => 'title',
						'name'  => 'title',
						'type'  => 'text',
						'title' => esc_html__('Title', 'remix')
					), isset( $_POST['title'] ) ? wp_filter_nohtml_kses( $_POST['title'] ) : '' );

					echo csf_add_field( array(
						'id'    => 'content',
						'name'  => 'content',
						'type'  => 'Wysiwyg',
						'settings' => array(
							'textarea_rows' => 5,
							'media_buttons' => false,
						),
						'title' => esc_html__('Description', 'remix')
					), isset( $_POST['content'] ) ? wp_filter_nohtml_kses( $_POST['content'] ) : '' );

					echo csf_add_field( array(
						'id'    => 'cover',
						'name'  => 'cover',
						'type'  => 'image',
						'title' => esc_html__('Cover', 'remix')
					), isset( $_POST['cover'] ) ? wp_filter_nohtml_kses( $_POST['cover'] ) : '' );

					echo csf_add_field( array(
						'id'    => 'mp3',
						'name'  => 'mp3',
						'type'  => 'upload',
						'title' => esc_html__('MP3', 'remix'),
						'settings'   => array(
							'upload_type'  => 'audio/mpeg',
							'frame_title'  => 'Upload / Select',
							'insert_title' => 'Insert',
						)
					), isset( $_POST['mp3'] ) ? wp_filter_nohtml_kses( $_POST['mp3'] ) : '' );

					echo csf_add_field( array(
						'id'      => 'artist',
						'name'    => 'artist',
						'type'    => 'checkbox',
						'title'   => esc_html__('Artist(s)', 'remix'),
						'options' => 'posts',
						'query_args'  => array(
							'post_type' => 'artists',
							'orderby'   => 'title',
							'order'     => 'ASC',
							'posts_per_page' => -1,
						)
					), isset( $_POST['artist'] ) ? $_POST['artist'] : '' );

					foreach ( array( 'a', 'b', 'c', 'd' ) as $i ) {
						echo csf_add_field( array(
							'id'    => 'buy_icon_' . $i,
							'name'  => 'buy_icon_' . $i,
							'type'  => 'icon',
							'title' => esc_html__('Icon', 'remix') . ' ' . $i
						), isset( $_POST['buy_icon_' . $i] ) ? wp_filter_nohtml_kses( $_POST['buy_icon_' . $i] ) : '' );
						echo csf_add_field( array(
							'id'    => 'buy_title_' . $i,
							'name'  => 'buy_title_' . $i,
							'type'  => 'text',
							'title' => esc_html__('Title', 'remix') . ' ' . $i
						), isset( $_POST['buy_title_' . $i] ) ? wp_filter_nohtml_kses( $_POST['buy_title_' . $i] ) : '' );
						echo csf_add_field( array(
							'id'    => 'buy_link_' . $i,
							'name'  => 'buy_link_' . $i,
							'type'  => 'text',
							'title' => esc_html__('Link', 'remix') . ' ' . $i
						), isset( $_POST['buy_link_' . $i] ) ? wp_filter_nohtml_kses( $_POST['buy_link_' . $i] ) : '' );
					}

					echo csf_add_field( array(
			          'id'      => 'lyric',
			          'name'    => 'lyric',
			          'type'    => 'select',
			          'title'   => esc_html__('Lyric(s)', 'remix'),
			          'options' => 'posts',
			          'query_args'  => array(
			            'post_type' => 'lyrics',
			            'orderby'   => 'title',
			            'order'     => 'ASC',
			            'posts_per_page' => -1,
			          ),
			          'default_option' => esc_html__( 'Select', 'remix')
					), isset( $_POST['lyric'] ) ? wp_filter_nohtml_kses( $_POST['lyric'] ) : '' );

					echo csf_add_field( array(
			          'id'      => 'cats',
			          'name'    => 'cats',
			          'type'    => 'checkbox',
			          'title'   => esc_html__('Categories', 'remix'),
			          'options' => 'categories',
			          'query_args'  => array(
						'type'         => 'songs',
						'taxonomy'     => 'songs_cat',
						'orderby'      => 'name',
						'order'        => 'ASC',
			          )
					), isset( $_POST['cats'] ) ? $_POST['cats'] : '' );

					echo csf_add_field( array(
			          'id'      => 'tags',
			          'name'    => 'tags',
			          'type'    => 'text',
			          'desc'    => esc_html__('Separate by commas', 'remix'),
			          'title'   => esc_html__('Tags', 'remix')
					), isset( $_POST['tags'] ) ? wp_filter_nohtml_kses( $_POST['tags'] ) : '' ); ?>
    <div id="csf-modal-icon" class="csf-modal csf-modal-icon">
      <div class="csf-modal-table">
        <div class="csf-modal-table-cell">
          <div class="csf-modal-overlay"></div>
          <div class="csf-modal-inner">
            <div class="csf-modal-title">
              <?php _e( 'Add Icon', 'csf' ); ?>
              <div class="csf-modal-close csf-icon-close"></div>
            </div>
            <div class="csf-modal-header csf-text-center">
              <input type="text" placeholder="<?php _e( 'Search a Icon...', 'csf' ); ?>" class="csf-icon-search" />
            </div>
            <div class="csf-modal-content"><div class="csf-icon-loading"><?php _e( 'Loading...', 'csf' ); ?></div></div>
          </div>
        </div>
      </div>
    </div>
					</div>
					<input type="submit" class="button medium mtt clr" value="Submit" />
					</form>
				<?php 
				}
			} ?>
		</article>
	<?php get_footer(); 
	}

	/* Get menu */
	public function menu( $l, $c = 'sf-menu' ) {
		$res = $this->option('responsive') ? ' data-responsive="1"' : '';
		$l = ( $l === 'primary' && is_page_template( 'page-onepage.php' ) ) ? 'onepage' : $l;
		wp_nav_menu(array(
			'theme_location' => $l,
			'container' => false,
			'fallback_cb' => '',
			'items_wrap' => '<ul class="' . $c . '"' . $res . '>%3$s</ul>',
			'walker' => new Codevz_Walker_nav()
		));
	}

	public function pagination( $query = 0 ) {

		$query = $query ? $query : $this->wp_query;
		$total = $query->max_num_pages;
		$prev = is_rtl() ? ' <i class="fa fa-caret-right"></i> ' : ' <i class="fa fa-caret-left"></i> ';
		$next = is_rtl() ? ' <i class="fa fa-caret-left"></i> ' : ' <i class="fa fa-caret-right"></i> ';

		$big = 999999999;
		if ( $total < 1 ) {
			return;
		}
		if( $this->option( 'pagination' ) )  {
			$format = get_option('permalink_structure') ? 'page/%#%/' : '&paged=%#%';
			echo paginate_links( array(
				'base'		=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'	=> $format,
				'current'	=> max( 1, $query->query_vars['paged'] ),
				'total'		=> $total,
				'mid_size'	=> 3,
				'type'		=> 'list',
				'prev_text'	=> $prev,
				'next_text'	=> $next,
			) );
		} else if ( get_next_posts_link() || get_previous_posts_link() ) {
			echo '
				<ul class="page-numbers clr">
					<li>'.get_next_posts_link( $this->option( 'next_page', ' Next ' ), $total ).'</li>
					<li>'.get_previous_posts_link( $this->option( 'prev_page', ' Previous ' ), $total ).'</li>
				</ul>
			';
		}
	}

	public function next_prev_item() {
		$cpt = $this->get_post_type();
		$tax = ( $cpt === 'post_' ) ? 'category' : $cpt . 'cat';		
		$prevPost = get_previous_post( true, '', $tax ) ? get_previous_post( true, '', $tax ) : get_previous_post();
		$nextPost = get_next_post( true, '', $tax ) ? get_next_post( true, '', $tax ) : get_next_post();

		if ( $prevPost || $nextPost ) { ?>
			<ul class="next_prev">
				<?php if( $prevPost ) { ?>
					<li class="previous">
						<?php $prevthumbnail = get_the_post_thumbnail( $prevPost->ID, 'tumb' ); ?>
						<?php previous_post_link( '%link', $prevthumbnail . ' <strong><i class="fa fa-chevron-left"></i></strong> <span>%title</span>' ); ?>
					</li>
				<?php } if( $nextPost ) { ?>
					<li class="next">
						<?php $nextthumbnail = get_the_post_thumbnail( $nextPost->ID, 'tumb' ); ?>
						<?php next_post_link( '%link', $nextthumbnail . ' <strong><i class="fa fa-chevron-right"></i></strong> <span>%title</span>' ); ?>
					</li>
				<?php } ?>
			</ul>
		<?php 
		}
	}

	public function sidebar_p() {
		$p = 'primary';
		$m = $this->meta();

		if ( ( is_page() || is_single() ) && ! empty( $m[ $p ] ) ) {
			$p = isset( $m[ $p ] ) ? $m[ $p ] : $p;
		} else {
			$p = $this->option( $this->get_post_type() . $p, $p );
		}

		return $p;
	}

	public function sidebar_s() {
		$s = 'secondary';
		$m = $this->meta();

		if ( ( is_page() || is_single() ) && ! empty( $m[ $s ] ) ) {
			$s = isset( $m[ $s ] ) ? $m[ $s ] : $s;
		} else {
			$s = $this->option( $this->get_post_type() . $s, $s );
		}

		return $s;
	}

	public function dynamic_sidebar( $n ) {
		$this->hook( 'before_sidebar_' . $n[0], 0 );
		dynamic_sidebar( $n );
		$this->hook( 'after_sidebar_' . $n[0], 0 );
	}
	
	public function page_title() {
		global $post;
		$m = $this->meta();
		$m = empty( $m['hiding_elms'] ) ? array() : $m['hiding_elms'];

		if ( is_single() ) {
			$i = single_post_title( '', false );
		} else if ( is_archive() ) {
			$i = get_the_archive_title();
			$i = substr( $i, strpos( $i, ':' ) + 1 );
		} else if ( is_search() ) {
			$i = get_search_query();
		} else if ( is_home() ) {
			$i = '';
		} else {
			$i = get_the_title();
		}

		$title = ( is_front_page() || empty( $i ) ) ? '' : '<h3 class="tt">'.$i.'</h3><span class="liner"></span>';
		
		if ( ( is_single() || is_page() ) && in_array( 'title', $m ) ) {
			$title = '';
		}

		echo $title;
		echo ( is_category() && category_description() ) ? category_description() : '';
		echo ( is_tag() && tag_description() ) ? tag_description() : '';
		echo ( is_tax() && term_description( get_query_var('term_id'), get_query_var( 'taxonomy' ) ) ) ? term_description( get_query_var('term_id'), get_query_var( 'taxonomy' ) ) : '';
	}

	public function content_before() {
		$cpt = $this->get_post_type();
		$l = $this->option( $cpt . 'layout', $this->option( 'layout', 'sidebar-right' ) );

		if ( is_page() || is_single() && $cpt !== 'bbpress_' ) {
			$m = $this->meta();
			if ( isset( $m['layout'] ) && $m['layout'] !== 'inherit' ) {
				$l = $m['layout'];
			}
		}

		if ( $l === 'both-sidebar' ) {
			echo '<aside class="grid_3 alpha">';
			$this->dynamic_sidebar( $this->sidebar_p() );
			echo '</aside><div class="grid_6">';
		} else if ( $l === 'both-sidebar-right' ) {
			echo '<div class="grid_6 alpha">';
		} else if ( $l === 'sidebar-right' ) {
			echo '<div class="grid_8 alpha">';
		} else if ( $l === 'both-sidebar-left' ) {
			echo '<div class="grid_6 righter omega">';
		} else if ( $l === 'sidebar-left' ) {
			echo '<div class="grid_8 righter omega">';
		} else {
			echo '<div class="grid_12 clr">';
		}

		$this->hook( 'before_content', 0 );
	}

	public function content_after() {
		$this->hook( 'after_content', 0 );
		echo '</div>';
	}

	public function comments() {
		if ( comments_open() ) {
			echo '<section id="comments" class=""><div class="def-block clr">';
			comments_template();
			echo '</div></section>';
		}
	}

	public function excerpt( $c, $l, $s = 0 ) {

		if ( ! $l ) {
			return;
		} else if ( $l === '-1' ) {
			echo $c;
			return;
		}

		$btn = $this->option( 'read_more' ) ? ' ... <a class="Rmore button small" href="'. get_permalink( $this->post->ID ) . '"><span>' . $this->option( 'read_more' ) . '</span></a>' : ' ...';

		$c = $s ? $c : preg_replace( '`\[[^\]]*\]`', '', $c );
		$c = esc_attr( strip_tags( $c ) );
		$e = explode( ' ', $c, $l );
		if ( count( $e ) >= $l ) {
			array_pop( $e );
			$e = implode( " ", $e ) . $btn;
		} else {
			$e = implode( " ", $e );
		}

		echo $e;
	}

	public function search_form() {
		?><div class="search">
			<form method="get" id="search" action="<?php echo home_url( '/' ); ?>" autocomplete="off">
				<input name="nonce" type="hidden" value="<?php echo wp_create_nonce('ajax_search_nonce'); ?>" />
				<input name="s" type="text" placeholder="<?php echo $this->option( 'search_placeholder' ); ?>">
				<i class="fa fa-cog search_opt"></i>
				<div class="post_types">
					<?php
						$cpts = get_post_types( array( 'public' => true ), 'names' );
						unset( $cpts['attachment'] );
						unset( $cpts['essential_grid'] );
						foreach ( $cpts as $cpt ) {
							echo '<div class="clr">';
							echo '<label>' . ucwords( $cpt ) . '</label>';
							echo '<input type="checkbox" name="post_type[]" value="' . $cpt . '" />';
							echo '<br /></div>';
						}
					?>
				</div>
				<button type="submit"><i class="fa fa-search"></i></button>
			</form>
			<div class="ajax_search_results"></div>
		</div><?php
	}

	/* Get social icons */
	public function social( $s = array(), $c = array() ) {
		if ( ! empty( $s ) ) {
			echo '<div class="social ' . implode( ' ', $c ) . '">';
			foreach ( $s as $i ) {
				echo '<a class="tip" href="'.$i['social-link'].'" title="'.$i['title'].'" target="_blank"><i class="fa fa-' . str_replace( 'fa fa-', '', $i['social-icon'] ) . '"></i></a>';
			}
			echo '</div>';
		}
	}

	public function author_box() {
		if ( get_the_author_meta( 'description' ) ) { ?>
			<div class="def-block mbf clr">
			<?php if ( is_single() ) { ?>
				<h4 class="tt"><?php the_author_meta('display_name'); ?></h4><span class="liner"></span>
			<?php } ?>
				<div class="author_box clr">
					<div class="author_avatar"><?php echo get_avatar(get_the_author_meta('user_email'),'90'); ?></div>
					<p class="author_desc"><?php the_author_meta('description'); ?></p>
					<div class="widget_social">
						<div class="social with_color">
						<?php 
							if ( get_the_author_meta( 'url' ) ) { ?>
								<a href="<?php echo get_the_author_meta( 'url' ); ?>" class="tip" title="<?php echo get_the_author_meta('display_name'); ?> <?php esc_html__('Website', 'remix'); ?>" target="_blank"><i class="fa fa-home"></i></a>
							<?php }
							foreach ( $this->icons() as $v ) {
								if ( get_the_author_meta( $v ) ) {
									echo '<a href="'.get_the_author_meta( $v ).'" class="tip" title="'.get_the_author_meta('display_name').' '. ucwords($v).'" target="_blank"><i class="fa fa-'.$v.'"></i></a>';
								}
							}
						?>
						</div>
					</div>
				</div>
			</div>
		<?php }
	}

	/* Get post type icon */
	public function get_icon( $i ) {
		$icons = array(
			'artists' 	=> 'user',
			'videos' 	=> 'play',
			'songs' 	=> 'music',
			'playlists' => 'music',
			'lyrics' 	=> 'file-o',
			'events' 	=> 'ticket',
			'gallery' 	=> 'photo',
			'podcasts' 	=> 'microphone',
			'product' 	=> 'shopping-cart',
			'download' 	=> 'shopping-cart',
			'post' 		=> 'link'
		);

		return isset( $icons[ $i ] ) ? $icons[ $i ] : 'link';
	}

	/* Get post type subtitle */
	public function get_subtitle( $cpt, $out = '' ) {
		$id = $this->post->ID;
		$meta = $this->meta( $id );

		if ( $cpt === 'artists' ) {
			$terms = get_the_terms( $id, $cpt . '_cat' );
			if ( $terms && ! is_wp_error( $terms ) ) {
				$arr = array();
				foreach ( $terms as $term ) {
				    $arr[] = $term->slug;
				}
				$out = implode( ', ', $arr );
			}
		} else if ( $cpt === 'product' ) {
			$product = get_product( $id );
			$price   = $product->get_price_html();
			if ( $price ) {
				$out = $price;
			}
		} else if ( $cpt === 'events' ) {
			if ( isset( $meta['event_date'] ) ) {
				$out = date_i18n( get_option('date_format') . ' ' . get_option('time_format') , strtotime( $meta['event_date'] ) );
				//$out .= isset( $meta['venue'] ) ? '<br />' . $meta['venue'] : '';
			}
		} else {
			if ( ! empty( $cpt ) && $cpt !== 'post' && ! empty( $meta['artist'] ) ) {
				$out = $this->get_artists( $meta['artist'], 0 );
			} else {
				$out = the_time( get_option( 'date_format' ) );
			}
		}

		return $out;
	}

	/* Ajax posts */
	public function posts( $atts, $content = '' ) {
		$default = array(
			'ajax_id' 		=> 2 + $this->unique++,
			'post_type' 	=> 'post',
			'posts_per_page'=> get_option( 'posts_per_page', 4 ),
			'multisite' 	=> is_multisite() ? get_current_blog_id() : null,
			'ajax_style' 	=> 'button', /* button-scroll */
			'layout' 		=> 'classic', /* classic-modern */
			'modern_col' 	=> 'four_col',
			'button_label' 	=> 'Load More',
			'no_more' 		=> 'Not found more posts'
		);
		$atts = wp_parse_args( array_filter( $atts ), $default );

		ob_start(); ?>
		<div id="listing_<?php echo $atts['ajax_id']; ?>" data-param='<?php echo json_encode( $atts, JSON_HEX_APOS ); ?>'>
			<div class="clr <?php echo $atts['layout']; ?> type_<?php echo $atts['ajax_style']; ?>">
				<div class="posts">
					<div class="ajax-size"></div>
					<?php echo $this->query( $atts ); ?>
				</div>
			</div>

			<?php if ( $atts['ajax_style'] !== 'none' ) { ?>
			<div class="load_more">
				<a href="#" class="button medium">
					<img src="<?php echo  CD_URI; ?>/img/loading.gif" width="50" height="50" alt="1" />
					<span><?php echo $atts['button_label']; ?></span>
				</a>
			</div>
			<?php } ?>
		</div>
		<?php

		return ob_get_clean();
	}

	/* Ajax query */
	public function query( $atts = '', $is_ajax = 0 ) {

		/* Check */
		if ( ! $atts ) {
			$is_ajax = 1;
			$atts = $_GET;
		}

		if ( ! empty( $atts['multisite'] ) ) {
			switch_to_blog( $atts['multisite'] );
		}

		/* CPTs */
		$atts['post_type'] = explode( ',', $atts['post_type'] );

		/* Tax query */
		$taxes = array( '_cats', '_tags', 'alphabet', 'artists_cat', 'songs_cat', 'playlists_cat', 'podcasts_cat', 'lyrics_cat', 'videos_cat', 'gallery_cat', 'events_cat' );
		$tax_query = array();
		foreach ( $taxes as $tax ) {
			if ( isset( $atts[ $tax ] ) ) {
				$tax_array = explode( ',', $atts[ $tax ] );
				if ( $tax === '_cats' ) {
					$tax = 'category';
				} else if ( $tax === '_tags' ) {
					$tax = 'post_tags';
				} else if ( $tax === 'alphabet' ) {
					$tax = 'artist';
				}
				foreach ( $tax_array as $cat ) {
					if ( ! empty( $cat ) ) {
						$tax_query[] = array( 'taxonomy' => $tax, 'field' => 'slug', 'terms' => $cat );
					}
				}
			}
		}
		$atts['tax_query'] = empty( $tax_query ) ? null : wp_parse_args( $tax_query, array( 'relation' => 'OR' ) );

		if ( $atts['post_type'] === 'events' ) {
			$atts['order'] = 'ASC';
			$atts['orderby'] = 'meta_value';
			$atts['meta_key'] = 'cd_end_event_date';
			$atts['meta_query'] = array(
				array(
					'key' 	=> 'cd_end_event_date', 
					'value' => current_time( 'Y/m/j H:i' ), 
					'type' 	=> 'DATETIME',
					'compare' => '>='
				)
			);
		}

		$posts = new WP_Query( $atts );

		if ( $posts->have_posts() ) {

			ob_start();
			$bigger = isset( $atts['modern_col_size'] ) ? ' data-bigger="'.$atts['modern_col_size'].'"' : '';

			while ( $posts->have_posts() ) :
				$posts->the_post();

				if ( $atts['layout'] === 'classic' ) {
					$this->loop( 'post', 'm_post_thumb' );
				} else {
					$cpt = str_replace( '_', '', $this->get_post_type() );
				?>
					<article<?php echo $bigger; ?> <?php post_class( 'ajax-item ' . $atts['modern_col'] ); ?>>
						<div>

						<?php if ( has_post_thumbnail() ) { ?>
							<a class="cdEffect" href="<?php the_permalink(); ?>">
								<?php echo the_post_thumbnail( 'masonry' ); ?>
								<div class="overlay"></div>
								<i class="fa fa-<?php echo $this->get_icon( $cpt ); ?>"></i>
								<h3><?php the_title(); ?><small><?php echo $this->get_subtitle( $cpt ); ?></small></h3>
							</a>
						<?php } else { ?>
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<?php } ?>

						</div>
					</article>
				<?php }

			endwhile;
			wp_reset_postdata();

			$html = ob_get_clean();
		} else {
			$html = ( $is_ajax === 1 ) ? '-11' : $atts['no_more'];
		}

		if ( ! empty( $atts['multisite'] ) ) {
			restore_current_blog();
		}

		if ( $is_ajax === 1 ) {
			echo $html;
		} else {
			return $html;
		}

		die();
	}

	/* Loop */
	public function loop( $cpt, $classes = '', $gallery = 0, $meta_key = 0 ) {
		$meta = $this->meta();

		if ( ! $meta_key && ( $cpt === 'artists' || $cpt === 'videos' || $cpt === 'lyrics' || $cpt === 'podcasts' ) ) { ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
			<?php if ( $cpt === 'videos' ) { 
				if ( isset( $meta['lightbox'] ) && $meta['lightbox'] && $meta['video_type'] !== 'custom' ) {
					$link = ( $meta['video_type'] === 'vimeo' ) ? '//vimeo.com/' . $meta['vimeo'] : '//youtube.com/watch?v=' . $meta['youtube'];
					$lightbox = 'no-ajax lightbox';
				} else {
					$link = get_the_permalink();
					$lightbox = '';
				}
			?>
				<a class="cdEffect mb <?php echo $lightbox; ?>" href="<?php echo $link; ?>">
			<?php } else { ?>
				<a class="cdEffect mb" href="<?php the_permalink(); ?>">
			<?php } ?>
					<?php the_post_thumbnail( 'cover' ); ?>
					<i class="fa fa-<?php echo $this->get_icon( $cpt ); ?>"></i>
					<h3><?php the_title(); ?><small><?php echo $this->get_subtitle( $cpt ); ?></small></h3>
				</a>
			</article>

		<?php } else if ( $meta_key || $cpt === 'songs' || $cpt === 'playlists' ) { ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
				<div class="item_small mb">
					<?php if ( has_post_thumbnail() ): ?>
						<a class="cdEffect noborder" href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail( ( $meta_key ? 'player_cover' : 'tumb' ) ); ?>
							<?php echo ( $meta_key && $cpt === 'videos' ) ? '<i class="fa fa-play"></i>' : '<i class="fa fa-music"></i>'; ?>
						</a>
					<?php endif; ?>
					<div class="item-details">
						<h3><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
						<span><?php echo $this->get_artists( isset($meta['artist']) ? $meta['artist'] : '' ); ?></span>
						<?php if ( $meta_key && $cpt === 'videos' ) { ?>
						<span><i class="fa fa-eye mi"></i><?php echo get_post_meta( $this->post->ID, $meta_key, true ); ?></span>
						<?php } else { ?>
						<span><i class="fa fa-play mi"></i><?php echo $this->get_plays( $this->post->ID, ( $meta_key ? $meta_key : 'cd_plays' ) ); ?></span>
						<?php } ?>
					</div>
				</div>
			</article>

		<?php } else if ( $cpt === 'events' ) {

			$classes .= ' w_hr '; 
			$start_date = isset( $meta['event_date'] ) ? strtotime( $meta['event_date'] ) : '';
			$date = isset( $meta['end_event_date'] ) ? strtotime( $meta['end_event_date'] ) : '';
			if ( $date ) {
		?>
			<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?> itemscope itemtype="http://data-vocabulary.org/Event">
				<?php if ( has_post_thumbnail() ) { ?>
					<div class="event_date">
						<a class="cdEffect" href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail( 'medium_post' ); ?>
							<i class="fa fa-ticket"></i>
						</a>
					</div>
				<?php } ?>

				<div class="event_content <?php if ( has_post_thumbnail() ) {} else { ?>full_event<?php } ?>">
					<div class="inner_ec">
						<?php $buy_link_event = $meta['buy_link']; if ( $meta['buy_title'] ) { ?>
							<a target="_blank" href="<?php echo $buy_link_event; ?>" class="button medium flr <?php echo $meta['class']; ?>"><?php if ( $meta['class'] === '1') { ?><i class="fa fa-ticket mi"></i><?php } ?><?php echo $meta['buy_title']; ?></a>
						<?php } ?>
						<h3><a href="<?php the_permalink(); ?>" itemprop="url"><b itemprop="summary"><?php the_title(); ?></b></a></h3>
						<span class="hide" itemprop="description"><?php the_excerpt(); ?></span>
						<?php if ( $start_date !== $date ) { ?> 
							<span class="block mid"><i class="fa fa-clock-o mi"></i> <time itemprop="startDate" datetime="<?php echo date_i18n( get_option('date_format') . ' ' . get_option('time_format'), $start_date ); ?>"><?php echo date_i18n( get_option('date_format') . ' ' . get_option('time_format'), $start_date ); ?></time></span>
						<?php } ?>
						<span class="block mid"><i class="fa fa-clock-o mi"></i> <time itemprop="startDate" datetime="<?php echo date_i18n( get_option('date_format') . ' ' . get_option('time_format'), $date ); ?>"><?php echo date_i18n( get_option('date_format') . ' ' . get_option('time_format'), $date ); ?></time></span>
						<?php if ( $meta['venue'] ) : ?><span class="block mid" itemprop="location" itemscope itemtype="http://data-vocabulary.org/Organization"><i class="fa fa-map-marker mi"></i> <b itemprop="name"><?php echo $meta['venue']; ?></b></span><?php endif; ?>
						<?php $artists = $this->get_artists( isset($meta['artist']) ? $meta['artist'] : '' );
							if ( $artists ) { ?>
								<span class="block mid"><i class="fa fa-user mi"></i> <?php echo $artists; ?></span>
						<?php } ?>
					</div>
				</div>
			</article>

		<?php } } else if ( $cpt === 'gallery' ) {

			if ( $gallery ) {
				$images = isset( $meta['gallery'] ) ? explode( ',', $meta['gallery'] ) : array();
				foreach( $images as $id ) {
					if ( $id ) { ?>
					<li data-pile="<?php echo get_the_title(); ?>" class="<?php echo $classes; ?> mb">
						<a class="no-ajax" href="#" data-href="<?php $fff = wp_get_attachment_image_src( $id, 'large' ); echo isset( $fff[0] ) ? $fff[0] : ''; ?>">
							<img src="<?php $sss = wp_get_attachment_image_src( $id, 'cover' ); echo isset( $sss[0] ) ? $sss[0] : ''; ?>" width="auto" height="auto">
						</a>
					</li>
				<?php }
				}
			} else { ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
					<a class="cdEffect mb" href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail( 'cover' ); ?>
						<i class="fa fa-<?php echo $this->get_icon( $cpt ); ?>"></i>
						<h3><?php the_title(); ?><small><?php echo $this->get_subtitle( $cpt ); ?></small></h3>
					</a>
				</article>
			<?php
			}
		} else {
			$size = ( $this->option( 'm_post_thumb' ) || $classes === 'm_post_thumb' ) ? 'medium_post' : 'free_width';
			$mb = ( $size === 'free_width' ) ? 'mb' : '';
			?>
				<article <?php post_class( 'ajax-item classic clr ' . $size ); ?>>
					<?php if ( has_post_thumbnail() ) { ?>
						<a class="cdEffect fll <?php echo $mb; ?>" href="<?php the_permalink(); ?>">
							<?php echo the_post_thumbnail( $size ); ?>
							<i class="fa fa-<?php echo $this->get_icon( $cpt ); ?>"></i>
							<h3><small><?php echo $this->get_subtitle( $cpt ); ?></small></h3>
						</a>
					<?php } ?>
					
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<p class="mt"><?php $this->excerpt( get_the_content(), $this->option( 'excerpt' ) ); ?><p>
					
					<span class="mid"><i class="fa fa-clock-o mi"></i><?php the_time('F d, Y'); ?></span>
					<?php if ( comments_open() ) { ?><span><i class="fa fa-comments-o mi"></i><a class="post-comments" href="<?php comments_link(); ?>"><?php comments_number( '0', '1', '%' ); ?></a></span><?php } ?>
				</article>
				<hr />
			<?php
		}
	}

	/* Ajax search */
	public function ajax_search() {
		if ( isset( $_GET['nonce'] ) && ! wp_verify_nonce( $_GET['nonce'], 'ajax_search_nonce' ) ) {
			die ( 'Wrong security code' );
		}

		$s = sanitize_text_field( $_GET['s'] );
		$c = isset( $_GET['post_type'] ) ? $_GET['post_type'] : array( 'any' );
		$q = new WP_Query( array(
			'post_type' 	 => $c,
			's'              => $s,
			'posts_per_page' => 7,
			'orderby'		 => 'type',
			'fields'         => 'ids'
		));

		ob_start();
		if ( $q->have_posts() ) {
			$i = $title = 1;
			while ( $q->have_posts() && $i <= 6 ) {
				$q->the_post();
				$cpt = str_replace( '_', '', $this->get_post_type() );
				if ( $cpt === 'page' && ! in_array( 'page', $c ) ) {
					continue;
				}
				echo ( $title !== $cpt ) ? '<h5>' . ucwords( $cpt ) . '</h5>' : '';
				$title = $cpt; ?>
					<div id="post-<?php the_ID(); ?>" class="item_small">
						<?php if ( has_post_thumbnail() ): ?>
							<a class="cdEffect noborder" href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail( 'tumb' ); ?>
								<i class="fa fa-<?php echo $this->get_icon( $cpt ); ?>"></i>
							</a>
						<?php endif; ?>
						<div class="item-details">
							<h3><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
							<span><?php echo get_the_date(); ?></span>
						</div>
					</div>
				<?php
			$i++;
			}
		} else {
			echo '<h5>' . $this->option( 'not_found' ) . '</h5>';
		}

		if ( $q->post_count > 6 ) {
			unset( $_GET['action'] );
			echo '<a class="va_results" href="' . get_home_url() . '/?' . http_build_query( $_GET ) . '">View all results</div>';
		}

		echo ob_get_clean();
		wp_reset_postdata();

		die();
	}

	/* Countdown timer */
	public function countdown( $d, $c = '' ) {

		$s = strtotime( current_time( 'Y/m/j H:i' ) );
		$d = strtotime( $d );
		$j = array(
			'date'	=> $d - $s,
			'd'		=> $this->option( 'countdown_day', 'Days' ),
			'h'		=> $this->option( 'countdown_hour', 'Hours' ),
			'm'		=> $this->option( 'countdown_minute', 'Minutes' ),
			's'		=> $this->option( 'countdown_seconds', 'Seconds' ),
			'p'		=> $this->option( 'countdown_plus', ' ' ),
			'ex'	=> $this->option( 'countdown_expired', 'Expired' ),
		);

		echo "<ul data-countdown='" . json_encode( $j, JSON_HEX_APOS ) . "' class='countdown " . $c . "'></ul>";
	}

	/* Post type queries */
	public function pre_get_posts_action( $q ) {
		$t = array( 'artists', 'songs', 'videos', 'playlists', 'gallery', 'events', 'lyrics', 'podcasts' );

		/* Custom query */
		foreach ( $t as $s ) {
			$settings = $this->option( $s . '_query', array() );
			if ( ! is_admin() && ( is_post_type_archive( $s ) && isset( $q->query[ 'post_type' ] ) && $q->query[ 'post_type' ] === $s ) || ( is_tax( $s ) && isset( $q->query[ $s . '_cat' ] ) ) ) {
				foreach ( $settings as $key => $val ) {
					if ( $val && ! is_array( $val ) ) {
						$q->set( $key, $val );
					} else if ( is_array( $val ) ) {
						foreach ( $val as $k => $v ) {
							if ( $v ) {
								$q->set( $k, $v );
							}
						}
					}
				}
			}
		}

		/* Events order */
		if ( ! is_admin() && ( is_post_type_archive( 'events' ) && isset( $q->query[ 'post_type' ] ) && $q->query[ 'post_type' ] === 'events' ) || ( is_tax( 'events_cat' ) && isset( $q->query[ 'events_cat' ] ) ) ) {
			$q->set( 'order', 'ASC' );
			$q->set( 'orderby', 'meta_value' );
			$q->set( 'meta_key', 'cd_end_event_date' );
			$q->set( 'meta_query', array(
				'key' 	=> 'cd_end_event_date', 
				'value' => current_time( 'Y/m/d H:i' ), 
				'type' 	=> 'DATETIME',
				'compare' => '>='
			));
		}

		if ( isset( $q->query[ 'post_type' ] ) && $q->query[ 'post_type' ] === 'product' ) { 
			remove_filter('comments_template', 'dsq_comments_template');
		}
	}

	/* Add post types to tags page */
	public function pre_get_posts_filter( $q ) {
		$t = array( 'artists', 'songs', 'videos', 'playlists', 'gallery', 'events', 'lyrics', 'podcasts' );
		if ( is_tag() && $q->is_main_query() ) {
			$q->set( 'post_type', wp_parse_args( $t, array( 'post' ) ) );
		}
	}

	public function get_artists( $i = '', $link = 1 ) {
		$out = array();
		if ( $i ) {
			foreach( $i as $v ) {
				if ( $v ) {
					$title = get_the_title( $v );
					$out[] = $link ? '<a href="' . get_the_permalink( $v ) . '">' . $title . '</a>' : $title;
				}
			}
		}
		return $out ? implode( ', ', $out ) : '';
	}

	/* Current nav class */
	public function nav_class( $classes, $item ) {
		if ( have_posts() ) { 
			$c = get_post_type_object( get_post_type( $this->post->ID ) );
			$cs = !empty( $c ) ? $c->rewrite['slug'] : false;
			$ms = strtolower( trim( $item->url ) );
			if ( strpos( $ms, $cs ) !== false ) {
				$classes[] = 'parent_menu_item';
			}
		}

		if ( in_array( 'current-menu-item', $classes ) ) {
			$classes[] = 'child_menu_item';
		}

		return $classes;
	}

	/* Category widget */
	public function cats_html( $i ) {
		$i = preg_replace( '/cat-item\scat-item-(.?[0-9])\s/', '', $i );
		$i = preg_replace( '/current-cat/', 'current', $i );
		$i = preg_replace( '/\sclass="cat-item\scat-item-(.?[0-9])"/', '', $i );
		$i = preg_replace( '/\stitle="(.*?)"/', '', $i );
		$i = preg_replace( '/\sclass=\'children\'/', '', $i );
		$i = str_replace( '</a> (', '</a><span>(', $i );
		return str_replace( ')', ')</span>', $i );
	}

	public function archive_html($i) {
		$i = str_replace( '</a>&nbsp;(', '</a><span>(', $i );
		return str_replace( ')', ')</span>', $i );
	}

	/* Fix end events date */
	public function save_post( $post_id, $post ) {
		if ( !empty( $post->post_status ) && 'auto-draft' === $post->post_status ) {
			return;
		}
		if ( $post->post_type === 'events' ) {
			/* Check */
			$meta = get_post_meta( $post_id, 'cd_meta', true );
			$start = !empty( $meta['event_date'] ) ? $meta['event_date'] : '';
			$end = !empty( $meta['end_event_date'] ) ? $meta['end_event_date'] : $start;

			/* Update separate metabox */
			update_post_meta( $post_id, 'cd_event_date', $start );
			update_post_meta( $post_id, 'cd_end_event_date', $end );

			/* Set end if is empty */
			$meta['end_event_date'] = $end;
			update_post_meta( $post_id, 'cd_meta', $meta );
		}
	}

	/* Author icons */
	public function icons() {
		return array( 'facebook', 'twitter', 'dribbble', 'github', 'instagram', 'linkedin', 'pinterest', 'google-plus', 'soundcloud', 'youtube', 'star', 'envelope-o' );
	}

	public function author_icons( $i ) {
		$i = array();
		foreach ( $this->icons() as $v ) {
			$i[ $v ] = ucwords( $v );
		}
		unset( $i[ 'aim' ] );
		unset( $i[ 'jabber' ] );
		unset( $i[ 'yim' ] );

		return $i;		
	}

	public function save_author_icons( $i ) {
		if ( ! current_user_can( 'edit_user', $i ) ) { 
			return false;
		}

		$i = array();
		foreach ( $this->icons() as $v ) {
			$i[ $v ] = ucwords( $v );
			update_user_meta( $i, $v, $_POST[ $v ] );
		}
		
		return $i;
	}

	/* Logo */
	public function logo() {
		if ( $this->option( 'logo' ) ) {
			echo '<div class="logo is_logo tip" title="'.get_bloginfo('description').'"><a href="' . get_home_url() . '" rel="home"><img src="' . $this->option( 'logo' ) . '" alt="' . get_bloginfo('name') . '" width="auto" height="auto"></a></div>';
		} else {
			echo '<div class="logo is_text tip" title="'.get_bloginfo('description').'"><h1><a class="text_logo" href="' . get_home_url() . '" rel="home">' . get_bloginfo('name') . '</a></h1></div>';
		}
	}

	/* Carousel */
	public function related( $args = array() ) {

		$id = $this->post->ID;
		$cpt = get_post_type( $id );
		$meta = $this->meta();
		$cpts = array( 'songs', 'videos', 'playlists', 'podcasts', 'lyrics', 'gallery', 'events' );

		$args = wp_parse_args( $args, array(
			'addClass'		=> '',
			'arrows'		=> 'top',
			'section_title'	=> '',
			'by'			=> 'cats',
			'post_type'		=> $cpt,
			'post__not_in'	=> array( $id ),
			'posts_per_page'=> get_option( 'posts_per_page', '6' )
		) );

		if ( $args['by'] === 'cats' || $args['by'] === 'alphabet' ) {
			if ( $cpt === 'post' ) {
				$args['category__in'] = wp_get_post_categories( $id, array( 'fields'=>'ids' ) );
			} else {
				$taxonomy = ( $args['by'] === 'alphabet' ) ? 'artist' : $cpt . '_cat';
				$get_cats = get_the_terms( $id, $taxonomy );
				$get_cats = $get_cats ? $get_cats : '';
				if ( $get_cats ) {
					$tax = array('relation' => 'OR');
					foreach ( $get_cats as $key ) {
						$tax[] = array(
							'taxonomy' => $taxonomy,
							'terms' => $key->term_id
						);
					}
					$args['tax_query'] = $tax;
				}
			}
		} else if ( $args['by'] === 'tags' ) {
			$args['tag__in'] = wp_get_post_tags( $id, array( 'fields'=>'ids' ) );
		} else if ( $args['by'] === 'artists' ) {
			if ( $cpt === 'artists' ) {
				$args['meta_query'] = array( 
					array( 'key' => 'cd_meta', 'value' => '"' . $id . '"', 'compare' => 'LIKE' )
				);
			} else if ( in_array( $cpt, $cpts ) ) {
				$artists = isset( $meta['artist'] ) ? $meta['artist'] : array( '007' );
				$meta_query = array( 'relation' => 'OR' );
				foreach ( $artists as $key ) {
					$meta_query[] = array( 'key' => 'cd_meta', 'value' => '"' . $key . '"', 'compare' => 'LIKE' );
				}
				$args['meta_query'] = $meta_query;
			}
		} else if ( $args['by'] === 'rand' ) {
			$args['orderby'] = 'rand';
		}

		/* Tax query */
		$taxes = array( '_cats', '_tags', 'alphabet', 'artists_cat', 'songs_cat', 'playlists_cat', 'podcasts_cat', 'lyrics_cat', 'videos_cat', 'gallery_cat', 'events_cat' );
		$tax_query = array();
		foreach ( $taxes as $tax ) {
			if ( isset( $args[ $tax ] ) ) {
				$tax_array = explode( ',', $args[ $tax ] );
				if ( $tax === '_cats' ) {
					$tax = 'category';
				} else if ( $tax === '_tags' ) {
					$tax = 'post_tags';
				} else if ( $tax === 'alphabet' ) {
					$tax = 'artist';
				}
				foreach ( $tax_array as $cat ) {
					if ( ! empty( $cat ) ) {
						$tax_query[] = array( 'taxonomy' => $tax, 'field' => 'slug', 'terms' => $cat );
					}
				}
			}
		}
		$args['tax_query'] = !empty( $tax_query ) ? wp_parse_args( $tax_query, array( 'relation' => 'OR' ) ) : null;

		$query = new WP_Query( $args );

		$html = '';
		ob_start();

		$classes = array(
			empty( $args['section_title'] ) ? 'without_title' : '',
			empty( $args['outside_block'] ) ? 'def-block' : '',
			empty( $args['centermode'] ) ? '' : 'is_center',
			'arrows_' . $args['arrows'],
			$args['addClass']
		);

		if ( empty( $args['variablewidth'] ) ) {
			$image_size = isset( $args['image_size'] ) ? $args['image_size'] : 'medium_post';
		} else if ( $args['variablewidth'] ) {
			$image_size = ( $args['image_size'] !== 'medium_post' ) ? $args['image_size'] : 'free_width';
		}

		if ( $query->have_posts() ): ?>
			<section class="<?php echo implode( ' ', $classes ); ?> mbf clr">
				<?php if ( $args['section_title'] ) { ?>
				<h4 class="tt"><?php echo $args['section_title']; ?></h4><span class="liner"></span>
				<?php } ?>
				<div class="carousel" data-carousel='<?php echo json_encode( array_filter( $args ), JSON_HEX_APOS ); ?>'>
					<?php 
						while ( $query->have_posts() ) : $query->the_post();
						if ( has_post_thumbnail() ) { 
					?>
						<div id="post-<?php the_ID(); ?>" class="item">
							<a class="cdEffect" href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail( $image_size ); ?>
								<i class="fa fa-<?php echo $this->get_icon( get_post_type() ); ?>"></i>
								<h3><?php the_title(); ?><small><?php echo $this->get_subtitle( get_post_type() ); ?></small></h3>
							</a>
						</div>
					<?php } endwhile; ?>
				</div>
			</section>
		<?php 

		endif;
		echo ob_get_clean();
		wp_reset_postdata();
	}

	public function alphabet_list() { 
	?>
		<div class="row clr mb">
			<ul class="Alphabet">
				<li><a href="<?php echo get_post_type_archive_link( 'artists' ); ?>"><?php echo $this->option( 'artists_browse_all', 'Browse All' ); ?></a></li>
				<?php 
					wp_list_categories(array(
						'orderby'		=> 'name',
						'hide_empty'	=> 0,
						'pad_counts'	=> 0,
						'depth'			=> 1,
						'hierarchical'	=> 1,
						'taxonomy'		=> 'artist',
						'title_li'		=> ''
					));
				?>
			<ul>
		</div>
	<?php 
	}

	public function check_array_key( $k, $a ) {
		foreach ( $a as $v ) {
			if ( isset( $v[ $k ]  ) ) {
				return 1;
			}
		}
		return isset( $a[ $k ]  );
	}

	public function player( $id, $custom = array( 'class' => null ) ) {

		if ( ! $id && $id !== 'ajax' ) {
			echo '<pre> Not found ...! </pre>';
			return;
		}

		/* Params */
		$params = wp_parse_args( $custom, $this->meta( $id ) );
		$params['player'] = isset( $params['player'] ) ? $params['player'] : '';
		$cpt = get_post_type( $id );

		/* Embed code */
		if ( $params['player'] === 'custom_player' ) {
			echo $params['embed'];
			return;
		}

		$tracks = isset( $params['tracks'] ) ? $params['tracks'] : array();
		$is_playlist = 0;
		$is_ajax = ( $this->option( 'ajax' ) && $params[ 'class' ] !== 'popup' );

		/* If not tracks */
		if ( empty( $tracks ) ) {
			return;
		}
		$tracks = (array) $tracks;

		/* If is playlists */
		if ( $this->check_array_key( 'track', $tracks ) ) {
			$playlist = array();
			foreach ( $tracks as $track => $x ) {
				if ( $x['track_type'] === 'old' ) {
					$existed = $this->meta( $x['track'] );
					foreach ( (array) $existed['tracks'] as $key ) {

						/* Check if playlist selected as a track in VC */
						if ( ! empty( $key['track'] ) && $key['track_type'] === 'old' ) {
							$in_existed = $this->meta( $key['track'] );
							$in_existed['tracks'][0]['artist'] = isset( $in_existed['artist'] ) ? $in_existed['artist'] : '';
							$in_existed['tracks'][0]['track_id'] = $key['track'];
							unset( $key['track'] );
							$playlist[] = array_shift( $in_existed['tracks'] );
						} else {
							$key['plays'] = $this->get_plays( $x['track'] );
							$key['track_id'] = $x['track'];

							$gettrack = $this->meta( $x['track'] );
							$key['artist'] = isset( $gettrack['artist'] ) ? $gettrack['artist'] : '';
							$key['poster_big'] = $this->get_attachment( '', $x['track'], 'full' );
							$key['poster'] = $this->get_attachment( '', $x['track'], $this->option( 'player_cover', 'player_cover' ) );

							unset( $key['track'] );
							$playlist[] = $key;
						}
						
					}
				}  else {
					unset( $x['track'] );
					$playlist[] = $x;
				}
			}
			$tracks = $playlist;
			$is_playlist = 1;
		}

		/* Prepare data */
		foreach ( $tracks as $k => $v ) {
			/* Check cover */
			if ( ! isset( $tracks[ $k ]['poster'] ) || ( isset( $tracks[ $k ]['poster'] ) && empty( $tracks[ $k ]['poster'] ) ) ) {
				$tracks[ $k ]['poster_big'] = $this->get_attachment( '', $id, 'full' );
				$tracks[ $k ]['poster'] = $this->get_attachment( '', $id, $this->option( 'player_cover', 'player_cover' ) );
			} else if ( isset( $tracks[ $k ]['poster'] ) && is_numeric( $tracks[ $k ]['poster'] ) ) {
				$tracks[ $k ]['poster_big'] = $this->get_attachment( '', $tracks[ $k ]['poster'], 'full' );
				$tracks[ $k ]['poster'] = $this->get_attachment( '', $tracks[ $k ]['poster'], $this->option( 'player_cover', 'player_cover' ) );
			}

			/* Type */
			$tracks[ $k ]['type'] = ( ! empty( $tracks[ $k ]['type'] ) ) ? $tracks[ $k ]['type'] : 'mp3';
			$tracks[ $k ]['free'] = false;

			/* Plays */
			$tracks[ $k ]['plays'] = isset( $tracks[ $k ]['plays'] ) ? $tracks[ $k ]['plays'] : $this->get_plays( $id );

			/* Artists */
			if ( ! empty( $tracks[ $k ]['artist'] ) ) {
				$tracks[ $k ]['artist'] = $this->get_artists( (array) $tracks[ $k ]['artist'] );
			} else if ( ! empty( $params['artist'] ) ) {
				$tracks[ $k ]['artist'] = $this->get_artists( (array) $params['artist'] );
			} else {
				$tracks[ $k ]['artist'] = '';
			}

			/* Custom artist */
			if ( ! empty( $tracks[ $k ]['custom_artist'] ) ) {
				$tracks[ $k ]['artist'] .= empty( $tracks[ $k ]['artist'] ) ? $tracks[ $k ]['custom_artist'] : ', ' . $tracks[ $k ]['custom_artist'];
			}

			/* Check */
			$tracks[ $k ]['track_id'] = empty( $tracks[ $k ]['track_id'] ) ? $id : $tracks[ $k ]['track_id'];
			$tracks[ $k ]['title'] = empty( $tracks[ $k ]['title'] ) ? '' : $tracks[ $k ]['title'];
			$tracks[ $k ]['buy_custom'] = empty( $tracks[ $k ]['buy_custom'] ) ? '' : $tracks[ $k ]['buy_custom'];

			/* Title */
			$tracks[ $k ]['title'] = '<i class="fa fa-play mi"></i>&nbsp;' . $tracks[ $k ]['title'];
			$tracks[ $k ]['title'] .= ( $params['class'] === 'vc' || $cpt === 'playlists' ) ? '<span> - ' . wp_strip_all_tags( $tracks[ $k ]['artist'] ) . '</span>' : '';
			$buy_details = '<div class="buytrack">';
			$icons = array( 'a', 'b', 'c', 'd', 'e' );
			foreach ( $icons as $icon ) {
				if ( ! empty( $tracks[ $k ]['buy_title_' . $icon] ) ) {
					$ext = pathinfo( $tracks[ $k ]['buy_link_' . $icon], PATHINFO_EXTENSION );
					$ext = ( $ext === 'mp3' || $ext === 'zip' || $ext === 'rar' || $ext === 'wav' || $ext === 'mp4' ) ? 'download' : '';

					if ( $ext === 'download' && $this->option( 'logged_in_can_dl' ) && ! is_user_logged_in() ) {
						$ext = '';
						$tracks[ $k ]['buy_link_' . $icon] = '#';
						$tracks[ $k ]['buy_title_' . $icon] = $this->option( 'logged_in_can_dl' );
					}

					$track_id = empty( $tracks[ $k ]['track_id'] ) ? $id : $tracks[ $k ]['track_id'];
					$fix_icon = str_replace( 'icon-', '', $tracks[ $k ]['buy_icon_' . $icon] );
					$fix_icon = 'fa fa-' . str_replace( 'fa fa-', '', $tracks[ $k ]['buy_icon_' . $icon] );
					$buy_details .= '<a href="' . $tracks[ $k ]['buy_link_' . $icon] . '" target="_blank" data-id="' . $track_id . '" class="tip '.$ext.'_track" '.$ext.' title="'.$tracks[ $k ]['buy_title_' . $icon].'"><i class="' . $fix_icon . '"></i></a>';
				}
			}

			$buy_details .= !empty( $tracks[ $k ]['lyric'] ) ? '
				<a href="#lyric_' . $this->unique . '" class="popup_link no-ajax"><i class="tip fa fa-file-o" title="'.get_the_title( $tracks[ $k ]['lyric'] ).'"></i></a>
				<div id="lyric_' . $this->unique++ . '" class="popup big_popup">
					<div class="def-block widget scroll">
						'. apply_filters('the_content', get_post( $tracks[ $k ]['lyric'] )->post_content ) .'
					</div>
				</div>
			' : '';

			$buy_details .= !empty( $tracks[ $k ]['info_title'] ) ? '
				<a href="#info_' . $this->unique . '" class="popup_link no-ajax"><i class="tip fa fa-info" title="'.$tracks[ $k ]['info_title'].'"></i></a>
				<div id="info_' . $this->unique++ . '" class="popup big_popup">
					<div class="def-block widget">'.$tracks[ $k ]['info_content'].'</div>
				</div>
			' : '';

			$buy_details .= $tracks[ $k ]['buy_custom'];
			$buy_details .= '</div>';
			$tracks[ $k ]['title'] .= $buy_details;

			/* Fix artists for jPlayer */
			$tracks[ $k ]['artists'] = $tracks[ $k ]['artist'];
			unset( $tracks[ $k ]['artist'] );
		}
		$params['tracks'] = $tracks;

		/* unset these */
		$unset = array( 'cover', 'cover_position', 'img', 'hiding_elms', 'rev', 'master', 'outer_bg', 'inner_bg', 'primary', 'secondary', 'layout', 'breadcrumbs', 'footer', 'header', 'title' );
		foreach ( $unset as $key ) {
			unset( $params[ $key ] );
		}

		/* Get link structure for popup player */
		$format = get_option('permalink_structure') ? '?' : '&';

		/* Details bar */
		ob_start();
		if ( ! $params['class'] || $params['class'] === 'popup' ) { ?>
			<div class="share_dialog">
				<div class="jp-share">
					<h3 class="mb"> Share </h3>
					<div class="share-input mbt clr">
						<label>Embed : </label>
						<input onclick="this.focus();this.select()" type="text" value='<<?php echo 'iframe'; ?> width="100%" height="300" frameborder="no" src="<?php echo get_the_permalink( $id ) . $format; ?>player=<?php echo $id; ?>"></<?php echo 'iframe'; ?>>'>
					</div>
					<div class="jp-socials share-input clr">
						<label>Social : </label>
						<div class="social colored">
							<a rel="nofollow" class="tip no-ajax" href="//facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink( $id ); ?>" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a>
							<a rel="nofollow" class="tip no-ajax" href="//twitter.com/intent/tweet?original_referer=&url=<?php echo get_the_permalink( $id ); ?>&text=<?php echo get_the_title( $id ); ?>&hashtags=<?php echo get_bloginfo( 'name' ); ?>" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a>
							<a rel="nofollow" class="tip no-ajax" href="//plus.google.com/share?url=<?php echo get_the_permalink( $id ); ?>" target="_blank" title="Google+"><i class="fa fa-google-plus"></i></a>
						</div>
					</div>
					<div class="close-share tip" title="close"><i class="fa fa-close"></i></div>
				</div>
			</div>

			<div class="track_info mb clr">
				<div class="track_details">
				<?php 
					echo empty( $params['artist'] ) ? '' : '<span class="artist mid"><i class="fa fa-user mi"></i><b>' . $this->get_artists( (array) $params['artist'] ) . '</b></span>';
					echo '<span class="plays mid"><i class="fa fa-play mi"></i><b>' . $this->get_plays( $id ) . '</b></span>';
					$dls = get_post_meta( $id, 'cd_downloads', true );
					echo empty( $dls ) ? '' : '<span class="downloads mid"><i class="fa fa-download mi"></i><b>' . number_format_i18n( $dls ) . '</b></span>';
				?>
					<div class="current_title"></div>
				</div>

				<div class="popup_share">
					<a class="original_player no-ajax tip" href="<?php echo get_the_permalink( $id ) . $format . 'ref=' . $id; ?>" title="<?php echo $this->option( 'player_org_page' ); ?>"><i class="fa fa-external-link"></i></a>
					<a class="popup_player no-ajax tip miid" href="<?php echo get_the_permalink( $id ) . $format . 'player=' . $id; ?>" title="<?php echo $this->option( 'player_popup' ); ?>"><i class="fa fa-clone"></i></a>
					<a class="share_player no-ajax tip miid" href="#" title="<?php echo $this->option( 'player_share' ); ?>"><i class="fa fa-share-alt"></i></a>
				</div>
			</div><?php 
		}
		$details_bar = ob_get_clean();

		$buy_album = '';
		if ( isset( $params[ 'album_title' ] ) ) {
			$buy_album = '<a href="#buy_album" class="popup_link button medium buy_album">' . $params[ 'album_title' ] . '</a>';
			$buy_album .= '<div id="buy_album" class="popup"><div class="def-block widget">';
			$buy_album .= '<h4 class="tt">'. $params[ 'album_title' ] .'</h4><span class="liner"></span>';
			if ( is_array( $params['album_buy'] ) ) {
				foreach ( $params[ 'album_buy' ] as $key ) {
					$buy_album .= '<a target="_blank" class="button medium mb mid" href="'.$key[ 'link' ].'"><i class="' . $key[ 'icon' ] . ' mi"></i> ' . $key[ 'title' ] . '</a>';
				}
			}
			$buy_album .= '</div></div>';
		}

		/* if is AJAX */
		if ( $is_ajax && $params[ 'class' ] !== 'ajax' ) { ?>
			<div class="inline_tracks clr mb">

				<?php 
					if ( ! empty( $params['player_cover'] ) && $params['player_cover'] !== 'off' ) {
						echo '<div class="has_cover">';
						foreach ( $params['tracks'] as $key ) {
							echo '<a class="cdEffect fll" href="'.$key['poster_big'].'"><img src="'.$key['poster'].'" width="auto" height="auto"><i class="fa fa-expand"></i></a>'; 
							break;
						}
						echo $buy_album;
						echo '</div><div class="has_cover_content">';
					} else {
						echo '<div class="full_player">';
					}
				
					echo $details_bar;
				?>
					<div class="jp-audio">
						<div class="jp-type-playlist">
							<?php 
								$tracks_count = count( $params[ 'tracks' ] );
								$pat = $this->option( 'play_all_tracks' );
								echo ( $tracks_count > 1 && $pat ) ? '<a href="#" class="play_all_tracks button mb miidd">' . $this->option( 'play_all_tracks' ) . '</a>' : '';
							?>
							<div class="jp-playlist">
								<ul>
									<?php 
										foreach ( $params[ 'tracks' ] as $key ) {
											echo "<li><a class='playable' href='" . $key[ 'mp3' ] . "' data-track='" . json_encode( $key, JSON_HEX_APOS ) . "'>" . $key['title'] . "</a></li>";
										}
									?>
								</ul>
								<div class="clr mb"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php

			return;
		}

		if ( $params['player'] === 'rx_manager' && $params['class'] !== 'popup' ) {

			echo "<div class='sm_player mbf clr' data-id='" . $id . "' data-sm-player='" . json_encode( $params, JSON_HEX_APOS ) . "'>";
			echo '<div class="alpha grid_7">';
				if ( get_post_meta($id,'_thumbnail_id',false) ) {
					$thumb = get_post_meta($id,'_thumbnail_id',false); 
					$image = wp_get_attachment_image_src($thumb[0], 'cover', false);
					$imageIn = $image[0];
				} else {
					$thumb = $image = $imageIn = '';
				}
				echo '<a class="cdEffect" href="'.$imageIn.'"><img src="'.$imageIn.'" alt="';
				echo single_post_title();
				echo '" width="auto" height="auto"><i class="fa fa-expand"></i><h3><small>';
				echo single_post_title();
				echo '</small></h3></a>';
			echo '</div>';

			foreach ( $params['tracks'] as $v ) {
				echo '<div class="omega grid_5">';
					echo '<div class="ui360 ui360-vis"><a href="';
					echo $v['mp3'];
					echo '"></a></div>';
					echo '<div class="sm_details">';
						echo '<div class="sm_icons">';
						echo $v['artists'];
						$bet = $this->get_string_between( $v['title'], '<i class="fa fa-play mi"></i>', '<div class="buytrack"' );
						$bet = str_replace( $bet, ' ', $v['title'] );
						echo str_replace( '<i class="fa fa-play mi"></i>', ' ', $bet );
						echo '</div>';
						echo '<span><i class="fa fa-play mi"></i>  ' . $this->get_plays( $this->post->ID ) . '</span>';
					echo '</div>';
				echo '</div>';
				break;
			}
			echo '</div>';
		?>
			<link rel='stylesheet' id='360player-css' href='<?php echo CD_URI .'/js/sm/360player.css'; ?>' type='text/css' media='all' />
			<link rel='stylesheet' id='visualization-css' href='<?php echo CD_URI .'/js/sm/360player-visualization.css'; ?>' type='text/css' media='all' />
			<script type="text/javascript" src="<?php echo CD_URI .'/js/sm/berniecode-animator.js'; ?>"></script>
			<script type="text/javascript" src="<?php echo CD_URI .'/js/sm/soundmanager2.js'; ?>"></script>
			<script type="text/javascript" src="<?php echo CD_URI .'/js/sm/360player.js'; ?>"></script>
		<?php 

			return;
		}

		ob_start();
		echo $is_ajax ? '<i class="fa fa-music toggle_player"></i>' : '';
		?>	
			<div id="<?php echo $params['class']; ?>_codevz_<?php echo $this->unique++; ?>" class="player clr mb" data-id='<?php echo $id; ?>' data-player='<?php echo json_encode( $params, JSON_HEX_APOS ); ?>'>
				
				<div class="preloader">
					<span class="is_loading"><img src="<?php echo CD_URI; ?>/img/loading.gif" width="auto" height="auto" alt="1" /></span>
				</div>

				<?php 
					if ( ! empty( $params['player_cover'] ) && $params['player_cover'] !== 'off' ) {
						echo '<div class="has_cover"><span></span>'. $buy_album .'</div>';
						echo '<div class="has_cover_content">';
					} else {
						echo '<div class="full_player">';
					}
				
					echo $details_bar;
				?>
					<div class="jp-jplayer"></div>
					<div class="jp-audio" role="application" aria-label="media player">
						<div class="jp-type-playlist">
							<div class="jp-gui jp-interface">
								<div class="jp-controls-holder">
									<div class="jp-controls">
										<div class="jp-previous"><i class="fa fa-step-backward"></i></div>
										<div class="jp-play"><i class="fa fa-play"></i></div>
										<div class="jp-pause"><i class="fa fa-pause"></i><div class="rotating"></div></div>
										<div class="jp-next"><i class="fa fa-step-forward"></i></div>
									</div>
									<div class="jp-progress">
										<div id="jp-seek-bar" class="jp-seek-bar">
											<div id="jp-play-bar" class="jp-play-bar"></div>
											<div class="waveform"></div>
										</div>
									</div>
									<div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
									<div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
									<div class="jp-toggles">
										<div class="jp-repeat tip" title="<?php echo $this->option( 'player_repeat' ); ?>"><i class="fa fa-refresh"></i></div>
										<div class="jp-repeat-off tip" title="<?php echo $this->option( 'player_repeat' ); ?>"><i class="fa fa-refresh"></i></div>
										<div class="jp-shuffle tip" title="<?php echo $this->option( 'player_shuffle' ); ?>"><i class="fa fa-random"></i></div>
									</div>
									<div class="jp-volume-controls">
										<div class="volume">
											<span class="jp-mute"><i class="fa fa-volume-up"></i></span>
											<span class="jp-unmute"><i class="fa fa-volume-up"></i></span>
											<div class="vol">
												<div class="arrow"></div>
												<span class="jp-volume-bar">
													<span class="jp-volume-bar-value"></span>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="jp-playlist <?php echo $is_ajax ? 'scroll' : ''; ?>">
								<ul><li class="clr">&nbsp;</li></ul>
								<div class="clr mb"></div>
								<div id="history"></div>
							</div>
							<?php echo $is_ajax ? '<i class="fa fa-bars toggle_playlist"></i>' : ''; ?>
							<div class="jp-no-solution"></div>
						</div>
					</div>
				</div>
			</div><?php 

		echo ob_get_clean();
	}

	public function get_plays( $id, $i = 'cd_plays' ) {
		$m = $this->meta( $id, $i );
		return number_format_i18n( (int) $m );
	}

	public function codevz_counter() {

		$id = empty( $_GET['id'] ) ? '' : $_GET['id'];
		$key = empty( $_GET['key'] ) ? '' : 'cd_' . $_GET['key'];
		$cpt = get_post_type( $id );

		if ( (int) $id <= 0 || ! $key || $cpt === 'page' ) {
			die();
		}

		update_post_meta( $id, $key, $this->meta( $id, $key ) + 1 );
		update_post_meta( $id, $key . '_nd', $this->meta( $id, $key . '_nd' ) + 1 );
		update_post_meta( $id, $key . '_nw', $this->meta( $id, $key . '_nw' ) + 1 );
		update_post_meta( $id, $key . '_nm', $this->meta( $id, $key . '_nm' ) + 1 );

		die();
	}

	public function get_string_between( $string, $start, $end ) {
		$string = " " . $string;
		$ini = strpos( $string, $start );
		if ( $ini === 0 ) {
			return '';
		}
		$ini += strlen( $start );
		$len = strpos( $string, $end, $ini ) - $ini;
		return substr( $string, $ini, $len );
	}

	public function radio_history() {
		check_ajax_referer( 'radio_history', 'nonce' );

		$type = $_GET['type'];
		$history = $_GET['history'];
		$request = wp_remote_get( $history, array( 'user-agent' => "Mozilla\r\n\r\n" ) );
		$out = ' ';
		
		if ( is_wp_error( $request ) ) {
			$out = '<pre class="player_error">Error: ' . $request->get_error_message() . '</pre>';
		} else if ( $type === 'shoutcast' ) {
			$out = $request['body'];
			if ( strpos( $out, 'v2' ) ) {
				$before_t = '<table border="0" cellpadding="2" cellspacing="2"><tr><td><b>';
				$out = $before_t . $this->get_string_between( $out, $before_t, "</body></html>" );
			} else {
				$out = $this->get_string_between( $out, "Admin Login</a></font></td></tr></table></td></tr></table><br>", "<br><br><table");
			}
			$out = str_replace(
				array('<b>', '</b>', 'Current Song', '@'), 
				array('<b class=\'current\'>', '</b>', 'Current', ''), 
				$out
			);
		} else if ( $type === 'radionomy' ) {
			$out = $request['body'];
			$out = $this->get_string_between( $out, 'id="radioProfileTracklist">', '</div><script' );
		} else if ( $type === 'icecast' ) {
			$out = $request['body'];
			$out = $this->get_string_between( $out, '<td>Current Song:</td>', '</table>' );
			$out = '<table><tr><td>Current:</td>'.$out.'</tr></table>';
		}

		echo $out;
	    die();
	}

	public function new_nonce() {
		echo wp_create_nonce('radio_history');
		die();
	}

	/* Podcasts feed */
	public function itunes_namespace() {
		echo 'xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd"';
	}

	public function itunes_rss( $c ) {

		$id = $this->post->ID;
		$meta = $this->meta( $id );
		$track = !empty( $meta['tracks'] ) ? array_shift( $meta['tracks'] ) : array(array());
		$cpts = array( 'songs', 'podcasts', 'playlists' );
		
		$tags = get_the_tags( $id ) ? get_the_tags( $id ) : '';
		$keys = '';
		if ( $tags ) {
			foreach( $tags as $tag ) {
				$sep = $keys ? ', ' : '';
				$keys .= $sep . $tag->name;
			}
		}
		$keys = $keys ? $keys : $this->option( 'keywords', get_bloginfo( 'name' ) );
		
		if ( in_array( $post->post_type, $cpts ) ) {
		?>
			<itunes:author><?php echo get_the_author(); ?></itunes:author>
			<itunes:subtitle><?php echo get_the_title(); ?></itunes:subtitle>
			<itunes:summary><?php echo $this->excerpt( get_the_content(), 10 ); ?></itunes:summary>
			<itunes:image href="<?php echo $this->get_attachment( 'url' ); ?>" />
			<enclosure url="<?php echo $track['mp3']; ?>" length="1" type="audio/mpeg" />
			<guid><?php echo get_the_permalink(); ?></guid>
			<itunes:duration>0:00</itunes:duration>
			<itunes:keywords><?php echo $keys; ?></itunes:keywords>
		<?php
		}

	    return $c;
	}

	public function get_attachment( $key = '', $id = '', $size = '' ) {

		$id = $id ? $id : $this->post->ID;
		$id = get_post_thumbnail_id( $id ) ? get_post_thumbnail_id( $id ) : $id;
		$i = array();
		if ( $key && $id ) {
			$i = wp_prepare_attachment_for_js( $id );
		} else if ( $size ) { 
			$cover = wp_get_attachment_image_src( $id, $size, false );
			if ( $cover ) {
				return $cover[0];
			}
		}
		if ( ! empty( $i[ $key ] ) ) {
			return $i[ $key ];
		}
	}

	/* Woocommerce */
	function woo_cart( $fragments ) {
		global $woocommerce;
		
		ob_start(); ?>
			<div class="cart_head woocommerce">
				<a class="shop_icon" href="<?php echo $woocommerce->cart->get_cart_url(); ?>">
					<i class="fa fa-shopping-cart"></i>
					<div class="cart-contents"><?php echo sprintf(_n('%d item', '%d', $woocommerce->cart->cart_contents_count, 'remix'), $woocommerce->cart->cart_contents_count);?> - <?php echo $woocommerce->cart->get_cart_total(); ?></div>
				</a>
	        </div>
		<?php $fragments['.cart_head'] = ob_get_clean();
		
		return $fragments;
	}

	/* TGM */
	public function plugins() {
		$plugins = array(
			array(
				'name'     			=> 'RemixPlus',
				'slug'     			=> 'remixplus',
				'source'   			=> 'http://codevz.com/plugins/remixplus.zip', 
				'required' 			=> true
			),
			array(
				'name'     			=> 'Visual Composer',
				'slug'     			=> 'js_composer',
				'source'   			=> 'http://codevz.com/plugins/js_composer.zip', 
				'required' 			=> true
			),
			array(
				'name'     			=> 'RevSlider',
				'slug'     			=> 'revslider',
				'source'   			=> 'http://codevz.com/plugins/revslider.zip', 
			),
			array(
				'name'     			=> 'MasterSlider',
				'slug'     			=> 'masterslider',
				'source'   			=> 'http://codevz.com/plugins/masterslider.zip', 
			),
			array(
				'name'     			=> 'Essential Grid',
				'slug'     			=> 'essential-grid',
				'source'   			=> 'http://codevz.com/plugins/essential-grid.zip', 
			),
			array(
				'name' 				=> 'Contact Form 7',
				'slug' 				=> 'contact-form-7',
			),
			array(
				'name'     			=> 'Instagram Widget',
				'slug' 				=> 'instagram-slider-widget',
			),
			array(
				'name' 				=> 'WooCommerce',
				'slug' 				=> 'woocommerce',
			)
		);

		tgmpa( $plugins );
	}

	/* CSS-JS */
	public function enqueue() {
		wp_enqueue_script( 'cd-masonry', CD_URI . '/js/plugins.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'cd-jplayer', CD_URI . '/js/jplayer.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'cd-nicescroll', CD_URI . '/js/nicescroll.js', array( 'jquery' ), '', true );
		if ( $this->option( 'ajax' ) ) {
			wp_enqueue_script( 'cd-history', CD_URI . '/js/history.js', array( 'jquery' ), '', true );
			wp_enqueue_script( 'cd-ajax', CD_URI . '/js/ajax.js', array( 'jquery' ), '', true );
		}
		wp_enqueue_script( 'cd-custom', CD_URI . '/js/custom.js', array( 'jquery' ), '', true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		/* unWanted */
		wp_dequeue_style( 'prettyphoto' );
		wp_dequeue_style( 'prettyPhoto' );
		wp_dequeue_script( 'prettyphoto' );
		wp_dequeue_script( 'prettyphoto-init' );
		wp_dequeue_script( 'prettyPhoto' );
		wp_dequeue_script( 'prettyPhoto-init' );
					
		/* CSS */
		wp_enqueue_style( 'cd-remix', get_stylesheet_uri() );
		wp_enqueue_style( 'cd-font-awesome', CD_URI . '/csf/assets/css/font-awesome.min.css' );
		if ( function_exists( 'is_woocommerce' ) ) {
			wp_enqueue_style( 'cd-woo', CD_URI . '/css/woo.css' );
		}
		if ( is_rtl() ) {
			wp_enqueue_style( 'cd-rtl', CD_URI . '/rtl.css' );
		}
		if ( $this->option( 'light' ) ) {
			wp_enqueue_style( 'cd-light', CD_URI . '/css/light.css' );
		}
		if ( $this->option( 'responsive' ) ) {
			wp_enqueue_style( 'cd-responsive', CD_URI . '/css/responsive.css' );
		}
		wp_enqueue_style( 'cd-lightgallery', CD_URI . '/css/lightgallery/css/lightgallery.min.css' );
	}

	/* Stop update notification */
	public function plugin_updates( $v ) {
		$plugins = array( 'js_composer', 'revslider', 'masterslider', 'essential-grid' );

		foreach ( $plugins as $i ) {
			if( ! empty( $v->response[ $i . '/' . $i . '.php'] ) ) {
				unset( $v->response[ $i . '/' . $i . '.php'] );
			}
		}

		return $v;
	}

	/* wp_head */
	public function head() {

		if ( $this->option( 'seo' ) && ! is_404() ) {

			echo "\n".'<!-- Codevz Seo -->'."\n";

			/* Content description */
			ob_start();
			$this->excerpt( get_post_field( 'post_content', $this->post->ID ), 12 );
			$the_content = esc_attr( strip_tags( ob_get_clean() ) );

			$is_single = is_single();
			$title 	= $is_single ? get_the_title() : get_bloginfo('name');
			$desc 	= $is_single ? $the_content : $this->option( 'description', get_bloginfo( 'description' ) );
			$url 	= $is_single ? get_the_permalink() : get_home_url();
			$image 	= $is_single ? $this->get_attachment('url') : '';

			/* Single keywords */
			$tags = $is_single ? get_the_tags( $this->post->ID ) : '';
			$keys = '';
			if( $tags ) {
				foreach( $tags as $tag ) {
					$sep = $keys ? ', ' : '';
					$keys .= $sep . $tag->name;
				}
			}
			$keys = $keys ? $keys : $this->option( 'keywords', get_bloginfo( 'name' ) );

			echo '<meta name="title" content="' . wp_strip_all_tags( $title ) . '" />'."\n";
			echo wp_strip_all_tags( $image ) ? '<link rel="image_src" href="' . wp_strip_all_tags( $image ) . '">' : '' ."\n";
 			echo '<meta name="description" content="' . wp_strip_all_tags( $desc ) . '" />'."\n";
			echo '<meta name="keywords" content="' . wp_strip_all_tags( $keys ) . '" />'."\n";
			echo '<meta property="og:title" content="' . wp_strip_all_tags( $title ) . '" />'."\n";
 			echo '<meta property="og:description" content="' . wp_strip_all_tags( $desc ) . '"/>'."\n";
			echo '<meta property="og:type" content="website" />'."\n";
			echo '<meta property="og:url" content="' . wp_strip_all_tags( $url ) . '" />'."\n";
			echo '<meta property="og:site_name" content="' . get_bloginfo('name') . '" />'."\n";
			echo wp_strip_all_tags( $image ) ? '<meta property="og:image" content="' . wp_strip_all_tags( $image ) . '" />' : '' ."\n";

			echo "\n";
		}

		echo $this->option( 'head_codes' ); ?>

		<!--[if IE]>
			<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=EmulateIE8; IE=EDGE" />
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script type="text/javascript" src="<?php echo CD_URI .'/js/sm/excanvas.js'; ?>"></script>
		<![endif]--><?php 
	}

	/* wp_footer */
	function footer() {

		/* Ajax player */
		if ( $this->option( 'ajax' ) ) {
			echo '<div class="ajax_player"><div class="row clr">';
			$tracks = $this->option( 'ajax_tracks', array() );
			if ( $this->option( 'ajax_shuffle' ) ) {
				shuffle( $tracks );
			}
			$this->player( 
				'ajax', 
				array(
					'class' 		=> 'ajax',
					'tracks' 		=> $tracks,
					'player_cover' 	=> $this->option( 'ajax_cover' ),
					'autoplay' 		=> $this->option( 'ajax_autoplay' ),
					'repeat' 		=> $this->option( 'ajax_repeat' )
				)
			);
			echo '</div></div>';
		}

		/* Footer codes */
		echo $this->option( 'foot_codes' );

	}

	/* wp-login.php */
	function wp_login() { 
		$bg = $this->option( 'wp_login_bg' );
		$logo = $this->option( 'wp_login_logo' );
		$form = $this->option( 'wp_login_form' );
		echo '<style type="text/css">';
		if ( $bg ) { ?>
			body {
				background-image: url(<?php echo isset( $bg['image'] ) ? $bg['image'] : ''; ?>) !important;
				background-color: <?php echo isset( $bg['color'] ) ? $bg['color'] : 'inherit'; ?> !important;
				background-repeat: <?php echo isset( $bg['repeat'] ) ? $bg['repeat'] : 'repeat'; ?> !important;
				background-position: <?php echo isset( $bg['position'] ) ? $bg['position'] : 'left top'; ?> !important;
				background-attachment: <?php echo isset( $bg['attachment'] ) ? $bg['attachment'] : 'scroll'; ?> !important;
			}
		<?php } if ( $logo ) { ?>
			.login #login h1 a {
				background-image: url(<?php echo $logo; ?>) !important;
				padding-bottom: 70px !important;
				width: inherit !important;
				height: inherit !important;
				background-size: inherit !important
			}
		<?php } if ( $form ) { ?>
			.login form {
				background: <?php echo $form; ?> !important
			}
		<?php } 
		echo '</style>';
	}

	function upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){
		if ( ! $crop ) {
			return null;
		} 

		$aspect_ratio = $orig_w / $orig_h;
		$size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

		$crop_w = round($new_w / $size_ratio);
		$crop_h = round($new_h / $size_ratio);

		$s_x = floor( ($orig_w - $crop_w) / 2 );
		$s_y = floor( ($orig_h - $crop_h) / 2 );

		return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
	}

	/* Schedule */
	function additional_event( $schedules ) {
		$schedules['cddaily'] = array('interval' => 86400, 'display' => 'Once Daily');
		return $schedules;
	}

	function deactive_daily_plays () {
		wp_clear_scheduled_hook('daily_plays');
	}

	/* Action wp */
	function wp_extra() {
		if ( ! wp_next_scheduled( 'daily_plays' ) ) {
			$time = (int) current_time( 'U' );
			wp_schedule_event( $time, 'cddaily', 'daily_plays');
		}
	}

	/* Daily counter */
	function daily_plays() {

		$post_types = array( 'songs', 'podcasts', 'playlists', 'videos' );
		$keys = array( 'plays', 'downloads', 'views' );
		$pd = $pw = $pm = $uy = array();

		foreach ( $post_types as $cpt ) {
			foreach ( $keys as $key ) {
				if ( ( $cpt === 'videos' && $key !== 'views' ) || ( $cpt !== 'videos' && $key === 'views' ) ) {
					continue;
				} else {
					$posts = get_posts( array(
						'post_type' => $cpt,
						'numberposts' => -1
					));
					$ids = wp_list_pluck( $posts, 'ID' );

					foreach ( $ids as $id ) {
						$pd[ $id ] = get_post_meta( $id, 'cd_' . $key . '_nd', true );
						$uy[ $id ] = get_post_meta( $id, 'cd_' . $key, true );
						update_post_meta( $id, 'cd_' . $key . '_nd', 0 );

						if ( current_time('N') === get_option('start_of_week') ) {
							$pw[ $id ] = get_post_meta( $id, 'cd_' . $key . '_nw', true );
							update_post_meta( $id, 'cd_' . $key . '_nw', 0 );
						}

						if ( current_time('j') === 1 ) {
							$pm[ $id ] = get_post_meta( $id, 'cd_' . $key . '_nm', true );
							update_post_meta( $id, 'cd_' . $key . '_nm', 0 );
						}
					}
					// Update stored ids
					update_option( 'cd_' . $key . '_pd_' . $cpt, $pd );
					update_option( 'cd_' . $key . '_pw_' . $cpt, $pw );
					update_option( 'cd_' . $key . '_pm_' . $cpt, $pm );
					update_option( 'cd_' . $key . '_until_yesterday_' . $cpt, $uy );
					// Reset for next cpt and key
					$pd = $pw = $pm = $uy = array();
					wp_reset_postdata();
				}
			}
		}
	}

	function login() {
		if ( ! wp_verify_nonce( $_POST['nonce'], 'login_nonce' ) ) {
			die ( 'Wrong security code' );
		}

		$user = wp_signon( array(
			'user_login' 	=> $_POST['username'],
			'user_password'	=> $_POST['password'],
			'remember'		=> true
		), false );

		echo is_wp_error( $user ) ? $this->option( 'login_error', 'Username or password is wrong.' ) : '';
		die();
	}

	function login_redirect( $rt, $r, $u ) {
		if ( isset( $GLOBALS['pagenow'] ) && $GLOBALS['pagenow'] === 'wp-login.php' ) {
			return $rt;
		} else {
			return $r;
		}
	}

	function login_html() {

		$login 		= $this->option( 'login' );
		$register 	= $this->option( 'register' );
		$lost_p 	= $this->option( 'lost_p' );

		if ( $login ) {
			global $current_user;
			wp_get_current_user();

			if ( is_user_logged_in() ) {
				echo '<a href="#login" class="popup_link logged_in_user">' . get_avatar( $current_user->user_email, 26 ) . '</a>';
			?> 
				<section id="login" class="popup">
					<div class="def-block widget">
						<h4 class="tt"> <?php echo $current_user->display_name; ?> </h4><span class="liner"></span>
						<div><a href="<?php echo get_author_posts_url( $current_user->ID ); ?>"><i class="fa fa-user mi"></i> <?php echo $this->option( 'my_profile' ); ?></a></div>
						<div><a href="<?php echo site_url('/author/' . $current_user->display_name . '?edit_info'); ?>"><i class="fa fa-edit mi"></i> <?php echo $this->option( 'edit_my_profile' ); ?></a></div>
						<?php if ( $this->option( 'music_submission' ) ) { ?>
						<div><a href="<?php echo site_url('/author/' . $current_user->display_name . '?submission'); ?>"><i class="fa fa-pencil mi"></i> <?php echo $this->option( 'music_submission' ); ?></a></div>
						<?php } ?>
						<div><a href="<?php echo wp_logout_url( home_url() ); ?>"><i class="fa fa-sign-out mi"></i> <?php echo $this->option( 'logout' ); ?></a></div>
					</div>
				</section>
			<?php 
			} else {
				echo $login ? '<a href="#login" class="popup_link button small"><span>' . $login . '</span></a>' : '';
			?> 
			<section id="login" class="popup">
				<div class="widget def-block tabs">
					<div class="tabs-nav wtext clr">
						<?php echo $login ? '<a href="#tab-login">' . $login . '</a>' : ''; ?>
						<?php echo $register ? '<a href="#tab-register">' . $register . '</a>' : ''; ?>
						<?php echo $lost_p ? '<a href="#tab-lost">' . $lost_p . '</a>' : ''; ?>
					</div>

					<div class="tabs-content clr">

						<?php if ( $login ) { ?>
						<div id="tab-login" class="tab">
							<form>
								<span class="error mb"></span>
								<input name="action" type="hidden" value="login" />
								<input name="nonce" type="hidden" value="<?php echo wp_create_nonce('login_nonce'); ?>" />
								<input name="username" type="text" placeholder="<?php echo $this->option( 'username' ); ?>" />
								<input name="password" type="password" placeholder="<?php echo $this->option( 'password' ); ?>" />
								<input type="submit" class="hidden" value="Submit">
								<a href="#" class="submit button small"><span><?php echo $login; ?></span></a>
							</form>
						</div>
						<?php } ?>

						<?php if ( $register ) { ?>
						<div id="tab-register" class="tab">
							<form>
								<span class="error mb"></span>
								<input name="action" type="hidden" value="register" />
								<input name="nonce" type="hidden" value="<?php echo wp_create_nonce('register_nonce'); ?>" />
								<input name="username" type="text" placeholder="<?php echo $this->option( 'username' ); ?>" />
								<input name="email" type="email" placeholder="<?php echo $this->option( 'email' ); ?>" />
								<input name="password" type="password" placeholder="<?php echo $this->option( 'password' ); ?>" />
								<input name="password_" type="password" placeholder="<?php echo $this->option( 'password' ); ?>" />
								<input type="submit" class="hidden" value="Submit">
								<a href="#" class="submit button small"><span><?php echo $this->option( 'register', 'Register' ); ?></span></a>
							</form>
						</div>
						<?php } ?>

						<?php if ( $lost_p ) { ?>
						<div id="tab-lost" class="tab">
							<form>
								<span class="error mb"></span>
								<input name="action" type="hidden" value="lost_p" />
								<input name="nonce" type="hidden" value="<?php echo wp_create_nonce('lost_p_nonce'); ?>" />
								<input name="username" type="text" placeholder="<?php echo $this->option( 'username_email' ); ?>" />
								<input type="submit" class="hidden" value="Submit">
								<a href="#" class="submit button small"><span><?php echo $this->option( 'lost_p_btn' ); ?></span></a>
							</form>
						</div>
						<?php } ?>
					</div>

					<?php do_action( 'wordpress_social_login' ); ?> 
				</div>
			</section>
		<?php }
		}
	}

	function register() {
		if ( ! wp_verify_nonce( $_POST['nonce'], 'register_nonce' ) ) {
			die ( 'Wrong security code' );
		} else if ( $_POST['username'] === $_POST['password'] ) {
			die( 'Username and password can not be same' );
		} else if ( $_POST['password'] !== $_POST['password_'] ) {
			die( 'Paswords not match' );
		}

		/* Prepare */
		$pass = $_POST['password'];
		$info = array();
		$info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = $info['user_login'] = sanitize_user( $_POST['username'] );
		$info['user_pass'] = $pass;
		$info['user_email'] = sanitize_email( $_POST['email'] );

		/* Check email */
		if ( ! is_email( $info['user_email'] ) ) {
			die( 'Wrong email, Please try again !' );
		}
		
		/* Register */
	    $user = wp_insert_user( $info );

		/* Check and Send email */
	 	if ( is_wp_error( $user ) ){	
			$error = $user->get_error_codes();

			if ( in_array( 'empty_user_login', $error ) ) {
				die( $user->get_error_message( 'empty_user_login' ) );
			} else if ( in_array( 'existing_user_login', $error ) ) {
				die( $user->get_error_message( 'existing_user_login' ) );
			} else if ( in_array( 'existing_user_email', $error ) ) {
				die( $user->get_error_message( 'existing_user_email' ) );
			}
	    } else {
			$from = 'do-not-reply@'.preg_replace( '/^www\./', '', $_SERVER['SERVER_NAME'] ); 
			$subject = get_bloginfo('name'). ' - Registration successful';
			$sender = 'From: '.get_bloginfo('name').' <'.$from.'>' . "\r\n";
			
			$message = 'Thank you for resigtration.<br /><ul>
				<li>Username: ' . $info['user_nicename'] . '</li>
				<li>Password: ' . $pass . '</li>
				<li><a href="' . get_home_url() . '/?login"> Click here to login </a></li>
			</ul>';

			$headers[] = 'MIME-Version: 1.0' . "\r\n";
			$headers[] = 'Content-type: text/html; charset=UTF-8' . "\r\n";
			$headers[] = "X-Mailer: PHP \r\n";
			$headers[] = $sender;
				
			$mail = wp_mail( $info['user_email'], $subject, $message, $headers );

			die( $this->option( 'register_success', 'Registration was completed, Password was sent to your email.' ) ); 
	    }
	}

	function lost_p() {
		if ( ! wp_verify_nonce( $_POST['nonce'], 'lost_p_nonce' ) ) {
			die ( 'Wrong security code' );
		}

		$account = $_POST['username'];

		/* Check */
		if ( is_email( $account ) && email_exists( $account ) ) {
			$get_by = 'email';
		} else if ( validate_username( $account ) && username_exists( $account ) ) {
			$get_by = 'login';
		} else {
			die( $this->option( 'lost_p_error_not_fount', 'Can\'t find user with this information' ) );
		}

		/* New pass */
		$pass = wp_generate_password();

		/* Get user data */
		$user = get_user_by( $get_by, $account );
		/* Update user */
		$update_user = wp_update_user( array ( 'ID' => $user->ID, 'user_pass' => $pass ) );
			
		/* if update user return true, so send email containing the new password */
		if( $update_user ) {
			$from = 'do-not-reply@' . preg_replace( '/^www\./', '', $_SERVER['SERVER_NAME'] ); 
			$to = $user->user_email;
			$subject = 'Your new password';
			$sender = 'From: '.get_bloginfo('name').' <'.$from.'>' . "\r\n";
			
			$message = 'Your new password is : ' . $pass . '<br /><br /><a href="' . get_home_url() . '/?login' . '">' . get_home_url() . '/?login' . '</a>';
				
			$headers[] = 'MIME-Version: 1.0' . "\r\n";
			$headers[] = 'Content-type: text/html; charset=UTF-8' . "\r\n";
			$headers[] = "X-Mailer: PHP \r\n";
			$headers[] = $sender;
				
			$mail = wp_mail( $to, $subject, $message, $headers );
			if( $mail ) {
				echo $this->option( 'lost_p_success', 'Email sent, Please check your email.' );
			} else {
				echo 'Server unable to send email.';
			}
		} else {
			echo 'Please try again !';
		}

		die();
	}

	function get_likes_dislikes( $icons, $id = '', $cm = false ) {

		$id 		= $id ? $id : $this->post->ID;
		$user_ip 	= $_SERVER['REMOTE_ADDR'];
		$user_id 	= is_user_logged_in() ? get_current_user_id() : 0;
		if ( $cm ) {
			$likes 		= (int) get_comment_meta( $id, 'cd_likes', true );
			$dislikes 	= (int) get_comment_meta( $id, 'cd_dislikes', true );
			$all_users 	= (array) get_comment_meta( $id, 'cd_liked_disliked_users', true );
		} else {
			$likes 		= (int) $this->meta( $id, 'cd_likes' );
			$dislikes 	= (int) $this->meta( $id, 'cd_dislikes' );
			$all_users 	= (array) $this->meta( $id, 'cd_liked_disliked_users' );
		}
		$check_user = $this->AlreadyLiked( $user_id, $user_ip, $id, $all_users );
		$like_title = ( $check_user === 'like' ) ? 'You have already Liked this post' : 'Like';
		$dislike_title = ( $check_user === 'dislike' ) ? 'You have already Disliked this post' : 'Dislike';

		$liked 		= ( $check_user === 'like' ) ? 'liked' : '';
		$disliked 	= ( $check_user === 'dislike' ) ? 'disliked' : '';

		echo '<div class="likes_dislikes" 
			data-id="'.$id.'" 
			data-cm="'.$cm.'" 
			data-nonce="'.wp_create_nonce( 'likes_dislikes_nonce' ).'"
		>';

		echo ( ! empty( $icons[0] ) ) ? '
			<a href="#" class="tip like '.$liked.'" data-type="like" title="'.$like_title.'">
				<i class="'.$icons[0].'"></i> 
				<b>'.$likes.'</b>
			</a>' : '';
		echo ( ! empty( $icons[1] ) ) ? '
			<a href="#" class="tip dislike '.$disliked.'" data-type="dislike" title="'.$dislike_title.'">
				<i class="'.$icons[1].'"></i> 
				<b>'.$dislikes.'</b>
			</a>' : '';

		echo '</div>';
	}

	function likes_dislikes() {
		
		if ( ! wp_verify_nonce( $_POST['nonce'], 'likes_dislikes_nonce' ) ) {
			die ( 'Wrong nonce!' );
		}

		$out 		= array();
		$user_ip 	= $_SERVER['REMOTE_ADDR'];
		$user_id 	= is_user_logged_in() ? get_current_user_id() : 0;
		$id 		= $_POST['id'];
		$cm 		= $_POST['cm'];
		$type 		= $_POST['type'];
		if ( $cm ) {
			$likes 		= (int) get_comment_meta( $id, 'cd_likes', true );
			$dislikes 	= (int) get_comment_meta( $id, 'cd_dislikes', true );
			$all_users 	= (array) get_comment_meta( $id, 'cd_liked_disliked_users', true );
		} else {
			$likes 		= (int) $this->meta( $id, 'cd_likes' );
			$dislikes 	= (int) $this->meta( $id, 'cd_dislikes' );
			$all_users 	= (array) $this->meta( $id, 'cd_liked_disliked_users' );
		}
		$check_user = $this->AlreadyLiked( $user_id, $user_ip, $id, $all_users );

		if ( $type === 'like' ) {
			$all_users[ $user_ip ] = $all_users[ $user_id ] = 'like';
			$out['like'] = 'You have already Liked this';
			$out['dislike'] = 'Dislike';
			$out['like_class'] = 'liked';
			$out['dislike_class'] = '';
			if ( $check_user === 'like' ) {
				$likes = --$likes;
				unset($all_users[ $user_ip ]);
				unset($all_users[ $user_id ]);
				$out['like'] = 'Like';
				$out['like_class'] = '';
			} else if ( $check_user === 'dislike' ) {
				$likes = ++$likes;
				$dislikes = --$dislikes;
			} else {
				$likes = ++$likes;
			}
			if ( $cm ) {
				update_comment_meta( $id, 'cd_likes', $likes );
				update_comment_meta( $id, 'cd_dislikes', $dislikes );
			} else {
				update_post_meta( $id, 'cd_likes', $likes );
				update_post_meta( $id, 'cd_dislikes', $dislikes );
			}
		} else {
			$all_users[ $user_ip ] = $all_users[ $user_id ] = 'dislike';
			$out['dislike'] = 'You have already Disliked this';
			$out['like'] = 'Like';
			$out['like_class'] = '';
			$out['dislike_class'] = 'disliked';

			if ( $check_user === 'dislike' ) {
				$dislikes = --$dislikes;
				unset($all_users[ $user_ip ]);
				unset($all_users[ $user_id ]);
				$out['like'] = 'Like';
				$out['dislike'] = 'Dislike';
				$out['like_class'] = $out['dislike_class'] = '';
			} else if ( $check_user === 'like' ) {
				$likes = --$likes;
				$dislikes = ++$dislikes;
			} else {
				$dislikes = ++$dislikes;
			}
			if ( $cm ) {
				update_comment_meta( $id, 'cd_likes', $likes );
				update_comment_meta( $id, 'cd_dislikes', $dislikes );
			} else {
				update_post_meta( $id, 'cd_likes', $likes );
				update_post_meta( $id, 'cd_dislikes', $dislikes );
			}
		}

		unset( $all_users[0] );

		if ( $cm ) {
			update_comment_meta( $id, 'cd_liked_disliked_users', $all_users );
			$out['likes'] 		= (int) get_comment_meta( $id, 'cd_likes', true );
			$out['dislikes'] 	= (int) get_comment_meta( $id, 'cd_dislikes', true );
		} else {
			update_post_meta( $id, 'cd_liked_disliked_users', $all_users );
			$out['likes'] 		= (int) $this->meta( $id, 'cd_likes' );
			$out['dislikes'] 	= (int) $this->meta( $id, 'cd_dislikes' );
		}

		$out['message'] = empty( $out['like'] ) ? 'Ooops! Please try again.' : 'Rated';
		echo json_encode( $out );
		die();
	}

	function likes_dislikes_c( $i ) {
		if ( is_admin() ) {
			return $i;
		}
		ob_start();
		$this->get_likes_dislikes( 
			array( 'fa fa-thumbs-up', 'fa fa-thumbs-down' ), 
			get_comment_ID(), 
			true
		);
		$i .= ob_get_clean();

		return $i;
	}

	function AlreadyLiked( $user_id, $user_ip, $id, $all_users ) {
		$user = $user_id ? $user_id : $user_ip;
		if ( isset( $all_users[ $user ] ) ) {
			return $all_users[ $user ];
		} else {
			return 'New User';
		}
	}

	function breadcrumbs() {
		$bc = $this->get_bread_crumb_array();
		$out = array();
		$i = 1;
		foreach ( $bc as $ancestor ) {
			if ( $i === count( $bc ) ) {
				$out[] = '<b itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="inactive_l"><a href="' . $_SERVER['REQUEST_URI'] . '" onclick="return false;" itemprop="url"><span itemprop="title">' . $ancestor['title'] . '</span></a></b>';
			} else {
				$out[] = '<b itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="' . $ancestor['link'] . '" itemprop="url"><span itemprop="title">' . $ancestor['title'] . '</span></a></b>';
			}
			$i++;
		}
		echo implode( ' <i class="fa fa-angle-right"></i> ', $out );
	}

	function get_bread_crumb_array() {
		global $post;
	 
		$bc = array();
		$bc[] = array( 'title' => '<i class="hide">'.get_bloginfo('name').'</i><i class="fa fa-home tip" title="Home"></i>', 'link' => esc_url( get_home_url() ) . '/' );
		$bc = $this->add_posts_page_array( $bc );
		if ( is_404() ) {
			$bc[] = array( 'title' => '%s', 'link' => false );
		} else if ( is_search() ) {
			$bc[] = array( 'title' => get_search_query(), 'link' => false );
		} else if ( is_tax() ) {
			$taxonomy = get_query_var( 'taxonomy' );
			$term = get_term_by( 'slug', get_query_var( 'term' ), $taxonomy );
			if ( get_taxonomy( $term->taxonomy ) ) {
				$ptn = get_taxonomy( $term->taxonomy )->object_type[0];
				$bc[] = array( 'title' => ucwords($ptn), 'link' => get_post_type_archive_link( $ptn ) );
			}
			$bc[] = array( 'title' => sprintf( '%s', $term->name ), 'link' => get_term_link( $term->term_id, $term->slug ) );
		} else if ( is_attachment() ) {
			if ( $post->post_parent ) {
				if ( $parent_post = get_post( $post->post_parent ) ) {
					$singular_bread_crumb_arr = $this->get_singular_bread_crumb_array( $parent_post );
					$bc = array_merge( $bc, $singular_bread_crumb_arr );
				}
			}
			$bc[] = array( 'title' => $parent_post->post_title, 'link' => get_permalink( $parent_post->ID ) );
			$bc[] = array( 'title' => sprintf( '%s', $post->post_title ), 'link' => get_permalink( $post->ID ) );
		} else if ( is_singular() && ! is_front_page() ) {
			$singular_bread_crumb_arr = $this->get_singular_bread_crumb_array( $post );
			$bc = array_merge( $bc, $singular_bread_crumb_arr );
			$bc[] = array( 'title' => $post->post_title, 'link' => get_permalink( $post->ID ) );
		} else if ( is_category() ) {
			global $cat;

			$category = get_category( $cat );
			if ( $category->parent != 0 ) {
				$ancestors = array_reverse( get_ancestors( $category->term_id, 'category' ) );
				foreach ( $ancestors as $ancestor_id ) {
					$ancestor = get_category( $ancestor_id );
					$bc[] = array( 'title' => $ancestor->name, 'link' => get_category_link( $ancestor->term_id ) );
				}
			}
			$bc[] = array( 'title' => sprintf( '%s', $category->name ), 'link' => get_category_link( $cat ) );
		} else if ( is_tag() ) {
			global $tag_id;
			$tag = get_tag( $tag_id );
			$bc[] = array( 'title' => sprintf( '%s', $tag->name ), 'link' => get_tag_link( $tag_id ) );
		} else if ( is_author() ) {
			$author = get_query_var( 'author' );
			$bc[] = array( 'title' => sprintf( '%s', get_the_author_meta( 'display_name', get_query_var( 'author' ) ) ), 'link' => get_author_posts_url( $author ) );
		} else if ( is_day() ) {
			if ( $m = get_query_var( 'm' ) ) {
				$year = substr( $m, 0, 4 );
				$month = substr( $m, 4, 2 );
				$day = substr( $m, 6, 2 );
			} else {
				$year = get_query_var( 'year' );
				$month = get_query_var( 'monthnum' );
				$day = get_query_var( 'day' );
			}
			$month_title = $this->get_month_title( $month );
			$bc[] = array( 'title' => sprintf( '%s', $year ), 'link' => get_year_link( $year ) );
			$bc[] = array( 'title' => sprintf( '%s', $month_title ), 'link' => get_month_link( $year, $month ) );
			$bc[] = array( 'title' => sprintf( '%s', $day ), 'link' => get_day_link( $year, $month, $day ) );
		} else if ( is_month() ) {
			if ( $m = get_query_var( 'm' ) ) {
				$year = substr( $m, 0, 4 );
				$month = substr( $m, 4, 2 );
			} else {
				$year = get_query_var( 'year' );
				$month = get_query_var( 'monthnum' );
			}
			$month_title = $this->get_month_title( $month );
			$bc[] = array( 'title' => sprintf( '%s', $year ), 'link' => get_year_link( $year ) );
			$bc[] = array( 'title' => sprintf( '%s', $month_title ), 'link' => get_month_link( $year, $month ) );
		} else if ( is_year() ) {
			if ( $m = get_query_var( 'm' ) ) {
				$year = substr( $m, 0, 4 );
			} else {
				$year = get_query_var( 'year' );
			}
			$bc[] = array( 'title' => sprintf( '%s', $year ), 'link' => get_year_link( $year ) );
		} else if ( is_post_type_archive() ) {
			$post_type = get_post_type_object( get_query_var( 'post_type' ) );
			$bc[] = array( 'title' => sprintf( '%s', $post_type->label ), 'link' => get_post_type_archive_link( $post_type->name ) );
		}
		return $bc;
	}

	function get_singular_bread_crumb_array( $post ) {
		$bc = array();
		$post_type = get_post_type_object( $post->post_type );

		if ( $post_type && $post_type->has_archive ) {
			$bc[] = array( 'title' => sprintf( '%s', $post_type->label ), 'link' => get_post_type_archive_link( $post_type->name ) );
		}

		if ( is_post_type_hierarchical( $post_type->name ) ) {
			$ancestors = array_reverse( get_post_ancestors( $post ) );
			if ( count( $ancestors ) ) {
				$ancestor_posts = get_posts( 'post_type=' . $post_type->name . '&include=' . implode( ',', $ancestors ) );
				foreach( (array) $ancestors as $ancestor ) {
					foreach ( (array) $ancestor_posts as $ancestor_post ) {
						if ( $ancestor === $ancestor_post->ID ) {
							$bc[] = array( 'title' => apply_filters( 'the_title', $ancestor_post->post_title ), 'link' => get_permalink( $ancestor_post->ID ) );
						}
					}
				}
			}
		} else {
			$post_type_taxonomies = get_object_taxonomies( $post_type->name, false );
			if ( is_array( $post_type_taxonomies ) && count( $post_type_taxonomies ) ) {
				foreach( $post_type_taxonomies as $tax_slug => $taxonomy ) {
					if ( $taxonomy->hierarchical && $tax_slug !== 'post_tag' && $tax_slug !== 'artists_cat' ) {
						$terms = get_the_terms( $this->post->ID, $tax_slug );
						if ( $terms ) {
							$term = array_shift( $terms );
							if ( $term->parent != 0  ) {
								$ancestors = array_reverse( get_ancestors( $term->term_id, $tax_slug ) );
								foreach ( $ancestors as $ancestor_id ) {
									$ancestor = get_term( $ancestor_id, $tax_slug );
									$bc[] = array( 'title' => $ancestor->name, 'link' => get_term_link( $ancestor, $tax_slug ) );
								}
							}
							$bc[] = array( 'title' => $term->name, 'link' => get_term_link( $term, $tax_slug ) );
							break;
						}
					}
				}
			}
		}

		$artists = $this->meta( $post->ID );
		$artists = isset( $artists['artist'] ) ? $artists['artist'] : '';
		if ( $artists ) {
			foreach( $artists as $artist ) {
				if ( $artist ) {
					$multiple_artist = get_post( $artist );
					$bc[] = array( 'title' => $multiple_artist->post_title, 'link' => get_post_permalink($multiple_artist) );
				}
			}
		}

		return $bc;
	}

	function add_posts_page_array( $bc ) {
		if ( is_page() || is_front_page() || is_author() || is_date() ) {
			return $bc;
		} else if ( is_category() ) {
			$tax = get_taxonomy( 'category' );
			if ( count( $tax->object_type ) != 1 || $tax->object_type[0] != 'post' ) {
				return $bc;
			}
		} else if ( is_tag() ) {
			$tax = get_taxonomy( 'post_tag' );
			if ( count( $tax->object_type ) != 1 || $tax->object_type[0] != 'post' ) {
				if ( isset( $_GET['post_type'] ) ) {
					$bc[] = array( 'title' => get_post_type_object( $_GET['post_type'] )->labels->name, 'link' => get_post_type_archive_link( $_GET['post_type'] ) );
				}
				return $bc;
			}
		} else if ( is_tax() ) {
			$tax = get_taxonomy( get_query_var( 'taxonomy' ) );
			if ( count( $tax->object_type ) != 1 || $tax->object_type[0] != 'post' ) {
				return $bc;
			}
		} else if ( is_home() && ! get_query_var( 'pagename' ) ) {
			return $bc;
		} else {
			$post_type = get_query_var( 'post_type' ) ? get_query_var( 'post_type' ) : 'post';
			if ( $post_type != 'post' ) {
				return $bc;
			}
		}
		if ( get_option( 'show_on_front' ) === 'page' && get_option( 'page_for_posts' ) && ! is_404() ) {
			$posts_page = get_post( get_option( 'page_for_posts' ) );
			$bc[] = array( 'title' => $posts_page->post_title, 'link' => get_permalink( $posts_page->ID ) );
		}

		return $bc;
	}

	function get_month_title( $monthnum = 0 ) {
		global $wp_locale;
		$monthnum = (int)$monthnum;
		$date_format = get_option( 'date_format' );
		if ( in_array( $date_format, array( 'DATE_COOKIE', 'DATE_RFC822', 'DATE_RFC850', 'DATE_RFC1036', 'DATE_RFC1123', 'DATE_RFC2822', 'DATE_RSS' ) ) ) {
			$month_format = 'M';
		} else if ( in_array( $date_format, array( 'DATE_ATOM', 'DATE_ISO8601', 'DATE_RFC3339', 'DATE_W3C' ) ) ) {
			$month_format = 'm';
		} else {
			preg_match( '/(^|[^\\\\]+)(F|m|M|n)/', str_replace( '\\\\', '', get_option( 'date_format' ) ), $m );
			if ( !empty( $m[2] ) ) {
				$month_format = $m[2];
			} else {
				$month_format = 'F';
			}
		}

		switch ( $month_format ) {
			case 'F' :
				$month = $wp_locale->get_month( $monthnum );
				break;
			case 'M' :
				$month = $wp_locale->get_month_abbrev( $wp_locale->get_month( $monthnum ) );
				break;
			default :
				$month = $monthnum;
		}
		return $month;
	}

}

$codevz = new CodevzRemix();

/*  Walker Nav  */
class Codevz_Walker_nav extends Walker_Nav_Menu {
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
        $class_names = $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="'. esc_attr( $class_names ) . '"';
 
        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $description  = ! empty( $item->description ) ? '<span class="sub">'.esc_attr( $item->description ).'</span>' : '';
        /* if($depth != 0) { $description = $append = $prepend = ""; } */

        $item_output = $args->before;
        $item_output .= '<a'. $attributes .'>';
        $item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
        $item_output .= $description.$args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;
 
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id );
    }
}
