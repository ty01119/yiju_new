/**
 * Custom JS
 * 
 * @package Remix
 * @author Codevz
 * @link http://codevz.com
 */

var $ = jQuery, AjaxURL = $('body').data('ajax'), Theme = $('body').data('theme');


$(document).ready(function() {
    'use strict';

    /* Menu */
    var nav = $( '.sf-menu' );
    if ( nav.length ) {
        nav.superfish({
            delay: 100,
            onInit: function() {
                var currents = $( '> .current-menu-item, > .parent_menu_item, > .current-menu-parent', nav );
                if ( currents.length ) {
                    currents.addClass('selectedLava');
                } else if ( $( '.current_page_parent', nav ).length ) {
                    $( '.current_page_parent', nav ).closest('.menu-item-has-children').addClass('selectedLava');
                } else {
                    $( '> li:first-child', nav ).addClass('selectedLava');
                }
                /*nav.lavaLamp({
                    target:'> li:not(.child_menu_item)', 
                    setOnClick: false,
                    click: function() {
                        return true;
                    }
                });*/
                $( '.sf-with-ul' ).each(function(){
                    if ( ! $( 'i', this ).length ) {
                        if ( $( '.sub', this ).length ) {
                            $( '.sub', this ).before( '<i class="fa fa-angle-down"></i>' )
                        } else {
                            $( this ).append( '<i class="fa fa-angle-down"></i>' );
                        }
                    }
                });
                $( 'ul ul .fa-angle-down' ).addClass('fa-angle-right').removeClass('fa-angle-down');
                //setTimeout( function() {
                     $( nav ).addClass( 'cd_no_sub' );
                    if ( $('.sf-menu > li a span').length ) {
                        var h = $('.sf-menu li a span').parent().height();
                        if ( h ) {
                            $( nav ).removeClass( 'cd_no_sub' );
                            $( 'li a', nav ).css( 'height', h );
                            $( 'li li a', nav ).css( 'height', 'inherit' );
                        }
                    } else {
                        $( 'li a', nav ).css( 'padding', '0 40px 20px 0' );
                        $( 'li li a', nav ).css( 'padding', '8px 15px' );
                        $( 'li ul', nav ).css( 'top', '11px' );
                    }
                //}, 1000 );
                //setTimeout( function() {
                    if ( $( '> li.selectedLava', nav ).length ) {
                        var l = Math.round( $( '> li.selectedLava', nav ).offset().left - nav.offset().left );
                        $( '.left', nav ).css( 'width', $( '> li.selectedLava', nav ).width() );
                        $( '.back', nav ).css( 'width', $( '> li.selectedLava', nav ).width() );
                        $( '.back', nav ).css( 'left', l );
                    }
                //}, 1000 );
            }
        });
    }

    /* MobileMenu */
    if ( ! $( "header .mobile" ).length ) {
        $( 'header' ).prepend( '<div class="row"><a class="mobile" href="#"><i class="fa fa-remove fa-bars"></i></a><div class="mobile_nav"></div></div>' );
        $( '.mobile_nav' ).html( $( '#mobile' ).clone().html() );
        $( '.mobile_nav li, .mobile_nav ul, .mobile_nav a' ).removeClass();
        $( '.mobile_nav li' ).removeAttr( 'id' );
        $( '.mobile_nav .fa-angle-right' ).addClass( 'fa-angle-down' ).removeClass( 'fa-angle-right' );
        $( '.mobile_nav .fa-angle-down' ).wrap(function() {
            return '<a href="#" class="openUl" />';
        });
        $( '.mobile_nav .openUl' ).parent().after( $( '.openUl' ) );
        $( '.mobile_nav .openUl' ).next('.openUl').remove();

        $( '.mobile' ).on('click', function() {
            $( '.mobile_nav' ).slideToggle().toggleClass( 'isOpen' );
            $( 'i', this ).toggleClass( 'fa-bars' );
            return false;
        });
        $( '.mobile_nav .openUl' ).on('click', function() {
            $( this ).next().slideToggle();
            return false;
        });

        $( '.mobile_nav' ).on( 'click', function(e) {
            e.stopPropagation();
        });
        
        $( 'body, .mobile_nav a' ).on( 'click', function(e) {
            if ( e.target.className == 'fa fa-angle-down' ) {
                return;
            }
            $( '.mobile_nav' ).slideUp();
            $( '.mobile i' ).addClass( 'fa-bars' );
        });
    }

    /* Fullscreen menu */
    var menu_fullscreen = $( '.fullscreen_menu' );
    var head_full = menu_fullscreen.closest('#header');
    if ( head_full.length ) {
        $( '.full_menu', head_full ).detach();
        $( '.headdown .row', head_full ).append( '<i class="fa fa-remove fa-bars full_menu"></i><div class="fullscreen_menu_on"><div class="fullmenu_inner clr"></div></div>' );

        if ( $( '.social', head_full ).length ) {
            var social4 = $( '.social', head_full )[0].outerHTML,
                sf4 = $( '#mobile', head_full )[0].outerHTML;

            $( '.fullmenu_inner' ).html( sf4 + social4 );
            $( '.fullmenu_inner .sf-menu > li' ).css({opacity: 0,marginLeft: -40});
            $( '.fullmenu_inner .sf-menu li li, .fullmenu_inner .sf-with-ul > i' ).remove();
            $( '.fullmenu_inner .sf-with-ul' ).removeClass();
        }

        $( '.full_menu, .fullscreen_menu_on', head_full ).on('click', function(e) {
            $( '.fullmenu_inner .sf-menu > li' ).css({opacity: 0,marginLeft: -40});
            $( '.full_menu', head_full ).toggleClass( 'fa-bars' );
            $( '.fullscreen_menu_on' ).fadeToggle();

            $( '.fullmenu_inner .sf-menu > li' ).each(function(i) {
                $( this ).delay( 200 * i ).animate({opacity: 1,marginLeft: 30}).animate({marginLeft: 0});
            });
        });

        $( '.fullmenu_inner', head_full ).on('click', function(e) {
            e.stopPropagation();
        });

        $( '.fullscreen_menu_on .sub-menu' ).prev('a').on('click', function(e) {
            if ( e.target === this ) {
                $( this ).next( '.sub-menu' ).slideToggle();
            }
            e.preventDefault();
        });

        // Ajax re-call
        if ( $.fn.ajaxify ) {
            var History = window.History;
            if ( !History.enabled ) {
                return false;
            } else {
                $( '.fullscreen_menu_on li:not(.menu-item-has-children) a' ).on('click', function( e ) {
                    var $t = $(this),
                        url = $t.attr('href'),
                        title = $t.attr('title') || null;

                    /* Continue as normal for cmd clicks etc */
                    if ( e.which == 2 || e.metaKey ) { return true; }

                    History.pushState( null, title, url );
                    e.preventDefault();

                    /* Fix Nicescroll */
                    $( '.nicescroll-rails' ).detach();

                    $( '.fullscreen_menu_on' ).fadeOut();
                    $( '.full_menu', head_full ).toggleClass( 'fa-bars' );
                    return false;
                });
            }
            $( '.fullscreen_menu_on .fa-angle-down' ).detach();
        }
    }


    /* iframes size */
    var $iframes = $( '.cd_iframe, object, embed, .textwidget iframe' ).not('.wp-embedded-content');
    $iframes.each(function() {
        $( this ).attr( 'data-aspectRatio', this.height / this.width ).removeAttr( 'height width' );
    });
    $(window).resize(function() {
        $iframes.each(function() {
            var newWidth = $( this ).parent().width();
            var $el = $( this );
            $el.width( newWidth ).height( newWidth * $el.attr( 'data-aspectRatio' ) );
        });
    }).resize();

});
