<?php

/**
 * @author Codevz
 * @link http://Codevz.com
 *
 * Countdown add-one for visual composer
 * Shortcode name: codevz_player_sm
 */

if ( ! defined( 'ABSPATH' ) ) { die( '-1' ); }

class CodevzCountdown extends CodevzVc {

	function __construct() {
		add_action( 'init', array( $this, 'in' ) );
		add_shortcode( 'codevz_countdown', array( $this, 'out' ) );
	}

	function in() {
		vc_map( array(
			'category'		=> $this->cat(),
			'base'			=> 'codevz_countdown',
			'name'			=> __('Countdown', 'cd'), 
			'icon'			=> 'dashicons-before dashicons-clock cd_ico',
			'params'		=> array(
				array(
					"type"        	=> "textfield",
					"heading"     	=> __("Date", 'cd'),
					"description" 	=> __("2016/12/18 20:00", 'cd'),
					'edit_field_class'=> $this->col('6'),
					"param_name"  	=> "date",
					"admin_label" => true
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Size', 'cd'),
					'param_name' 	=> 'size',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'Default', 'cd' ) => '',
						'M' => 'vc_timer_m',
						'L' => 'vc_timer_l',
						'XL' => 'vc_timer_xl',
						'XXL' => 'vc_timer_xxl',
					)
				),
				array(
					'type' 			=> 'dropdown',
					'heading' 		=> __('Position', 'cd'),
					'param_name' 	=> 'position',
					'edit_field_class'=> $this->col('6'),
					'value'		=> array(
						__( 'Center', 'cd' ) => 'tac',
						__( 'Left', 'cd' ) => 'tal',
						__( 'Right', 'cd' ) => 'tar'
					)
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> __('Class', 'cd'),
					'param_name'	=> 'extra_class',
					'edit_field_class'=> $this->col('6'),
				),
			)

		));
	}

	function out( $atts, $content = '' ) {
		$atts = shortcode_atts( array(
			'date'			=> '',
			'position' 		=> 'tac',
			'size' 			=> '',
			'extra_class' 	=> '',
		), $atts );

		global $codevz;

		if ( $codevz ) {
			ob_start();

			if ( isset( $_GET['vc_editable'] ) ) { ?>
				<script type='text/javascript'>
					/* <![CDATA[ */
						codevzCountdown();
					/* ]]> */
				</script>
			<?php }

			echo "<div class='clr " . $atts['extra_class'] . "'>";
			echo "<div class='clr " . $atts['size'] . " " . $atts['position'] . "'>";
			$codevz->countdown( $atts['date'] );
			echo "</div>";
			echo "</div>";

			return ob_get_clean();
		} else {
			return 'This theme do not support this shortcode.';
		}
	}

}

new CodevzCountdown();
